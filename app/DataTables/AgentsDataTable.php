<?php

namespace App\DataTables;

use App\Models\Agent;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Yajra\DataTables\DataTableAbstract;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class AgentsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('status', function ($agent) {
                return view('auth.agent.datatable.agent_status', ['status' => $agent->status]);
            })
            ->editColumn('agreement_date', function ($agent) {
                return $agent->agreement_date ? with(new Carbon($agent->agreement_date))->format('d-m-Y') : '';;
            })
            ->filterColumn('agreement_date', function ($query, $keyword) {
                $query->whereRaw("DATE_FORMAT(agreement_date,'%d-%m-%Y') like ?", ["%$keyword%"]);
            })
            ->addColumn('profiles', function ($agent) {
                if ($agent->profiles->count() == 0)
                    return "Not Found";
                foreach ($agent->profiles as $profile) {
                    if ($profile->data_name == 'shop_name') {
                        return $profile->data_value;
                        break;
                    }
                }
            })
            ->addColumn('profile_picture', function ($agent) {
                if ($agent->getFirstMedia(config('const.media.profile_picture')))
                    return view("auth.agent.datatable.agent_image", ['image' => $agent->getFirstMediaUrl(config('const.media.profile_picture'))]);
                return "Not Found";
            })
            ->addColumn('action', function ($agent) {
                return view("auth.agent.datatable.action", ['agent' => $agent]);
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param Agent $model
     * @return Builder
     */
    public function query(Agent $model)
    {
        return $model->newQuery()->with('profiles');
    }

    public function dbTableDom()
    {
        return '<"row"<"col-md-12"<"row"<"col-md-6"B><"col-md-6"f> > ><"col-md-12"rt> <"col-md-12"<"row"<"col-md-5"i><"col-md-7"p>>> >';
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('agents-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->processing(false)
            ->languagePaginatePrevious('<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>')
            ->languagePaginateNext('<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>')
            ->languageInfo("Showing page _PAGE_ of _PAGES_")
            ->languageSearch('<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>')
            ->languageSearchPlaceholder("Search...")
            ->languageLengthMenu("Results :  _MENU_")
            ->lengthMenu([7, 10, 20, 50])
            ->pageLength(50)
            ->orderBy(0);;
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('name'),
            Column::make('phone'),
            Column::make('profile_picture')->orderable(false)->searchable(false)->title("Profile Picture"),
            Column::make('agreement_date'),
            Column::make('profiles', 'profiles.data_value')->title('Shop Name')->orderable(false),
            Column::make('balance'),
            Column::make('status'),
            Column::make('action'),
        ];
    }


    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Agents_' . date('YmdHis');
    }
}
