<?php

namespace App\DataTables;

use App\Models\Agent;
use App\Models\AgentOffer;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Yajra\DataTables\DataTableAbstract;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class AgentsStockDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return DataTableAbstract
     */
    public function dataTable($query)
    {

        return datatables()->eloquent($query)->editColumn('agent.profiles', function ($offer) {
            return $offer->agent->shopName->data_value;
        })->addColumn('action', function ($stock) {
            return view('auth.reports.datatables.actions', ['stock' => $stock]);
        });
    }

    /**
     * Get query source of dataTable.
     *
     * @param AgentOffer $model
     * @return Builder
     */
    public function query(AgentOffer $model)
    {
        return $model->newQuery()->with(['agent', 'variation'])->whereHas('agent')->whereHas('variation')->where(['offer_status' => config('const.agent_status.active')]);
    }


    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('offer-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->processing(false)
            ->languagePaginatePrevious('<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>')
            ->languagePaginateNext('<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>')
            ->languageInfo("Showing page _PAGE_ of _PAGES_")
            ->languageSearch('<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>')
            ->languageSearchPlaceholder("Search...")
            ->languageLengthMenu("Results :  _MENU_")
            ->lengthMenu([7, 10, 20, 50])
            ->pageLength(50)
            ->orderBy(0);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('id'),
            Column::make('variation.variant_name')->title('Variation Name'),
            Column::make('variation.cost')->title("Current Cost"),
            Column::make('variation.price')->title("Price"),
            Column::make('agent.name')->title("Agent Name"),
            Column::make('offer_stock')->title('stock'),
            Column::make('action')->orderable(false)->searchable(false),
        ];
    }


    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Offers' . date('YmdHis');
    }
}
