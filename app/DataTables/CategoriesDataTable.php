<?php

namespace App\DataTables;

use App\Models\Category;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTableAbstract;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class CategoriesDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->query($query)
            ->addColumn('image', function ($category) {
                $image = get_media_file(Category::where(['id' => $category->id])->first(), config('const.media.category_image'));
                return view("image", ['image' => $image]);
            })
            ->addColumn('action', function ($query) {
                $category = Category::where(['id' => $query->id])->first();
                return view("auth.category.datatable.actions", ['category' => $category]);
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param Category $model
     * @return \Illuminate\Database\Query\Builder
     */
    public function query(Category $model)
    {
        return DB::table('categories')
            ->leftJoin('categories as cates', 'categories.category_id', 'cates.id')
            ->select(["categories.id", "categories.name", "cates.name as parent"]);
    }

    public function dbTableDom()
    {
        return '<"row"<"col-md-12"<"row"<"col-md-6"B><"col-md-6"f> > ><"col-md-12"rt> <"col-md-12"<"row"<"col-md-5"i><"col-md-7"p>>> >';
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('categories-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->processing(false)
            ->dom($this->dbTableDom())
            ->languagePaginatePrevious('<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>')
            ->languagePaginateNext('<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>')
            ->languageInfo("Showing page _PAGE_ of _PAGES_")
            ->languageSearch('<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>')
            ->languageSearchPlaceholder("Search...")
            ->languageLengthMenu("Results :  _MENU_")
            ->buttons(
                Button::make('copy')->className('btn'),
                Button::make('excel')->className('btn'),
                Button::make('csv')->className('btn'),
                Button::make('print')->className('btn')

            )
            ->lengthMenu([7, 10, 20, 50])
            ->pageLength(50)
            ->orderBy(1, 'asc');
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('image')->searchable(false)->orderable(false),
            Column::make('name'),
            Column::make('parent', 'cates.name')->title('category'),
            Column::make('action')->orderable(false)->searchable(false)
        ];
    }


    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Categories_' . date('YmdHis');
    }
}
