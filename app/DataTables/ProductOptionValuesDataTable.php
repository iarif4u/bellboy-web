<?php

namespace App\DataTables;

use App\Models\ProductOptionValue;
use Illuminate\Database\Eloquent\Builder;
use Yajra\DataTables\DataTableAbstract;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class ProductOptionValuesDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', function ($option_value) {
                return view("auth.product.datatables.product_option_values_actions", ['option_value' => $option_value]);
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param ProductOptionValue $model
     * @return Builder
     */
    public function query(ProductOptionValue $model)
    {
        return $model->newQuery()->with('option');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {

        return $this->builder()
            ->setTableId('product-option-values-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->processing(false)
            ->languagePaginatePrevious('<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>')
            ->languagePaginateNext('<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>')
            ->languageInfo("Showing page _PAGE_ of _PAGES_")
            ->languageSearch('<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>')
            ->languageSearchPlaceholder("Search...")
            ->languageLengthMenu("Results :  _MENU_")
            ->buttons(
                Button::make('copy')->className('btn'),
                Button::make('excel')->className('btn'),
                Button::make('csv')->className('btn'),
                Button::make('print')->className('btn')

            )
            ->lengthMenu([7, 10, 20, 50])
            ->pageLength(50)
            ->orderBy(0,'asc');
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('value')->title("Option Value"),
            Column::make('option.name')->title("Product Option"),
            Column::make('action')->orderable(false)->searchable(false),
        ];
    }


    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'ProductOptions_value_' . date('YmdHis');
    }
}
