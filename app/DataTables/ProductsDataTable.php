<?php

namespace App\DataTables;

use App\Models\Product;
use Illuminate\Database\Eloquent\Builder;
use Yajra\DataTables\DataTableAbstract;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class ProductsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()->eloquent($query)
            ->editColumn('brand.name', function ($product) {
                return ($product->brand) ? $product->brand->name : "";
            })
            ->editColumn('discount.discount_name',function ($product){
                return ($product->discount) ? $product->discount->discount_name : "";
            })
            ->editColumn('status',function ($product){
                return view('auth.product.datatables.product_status',['product'=>$product]);
            })
            ->editColumn('categories', function (Product $product) {
                return $product->categories->map(function ($category) {
                    return $category->name;
                })->implode('<br>');
            })->orderColumn('categories', 'id $1')->filterColumn('categories', function ($query, $keyword) {
                $query->whereHas('categories', function ($q) use ($keyword) {
                    return $q->whereRaw("name like ?", ["%$keyword%"]);
                });
            })->addColumn('action',function ($product){
                return view('auth.product.datatables.product_actions',['product'=>$product]);
            })
            ->rawColumns(['categories']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param Product $model
     * @return Builder
     */
    public function query(Product $model)
    {
        if ($this->request->has('category')){
            $category = $this->request->input('category');
            return $model->newQuery()->with(['brand', 'discount', 'categories'])
                ->whereHas('categories', function ($query) use ($category) {
                    $query->where('categories.id', '=',$category);
                });
        }
        return $model->newQuery()->with(['brand', 'discount', 'categories']);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('products-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->processing(false)
            ->languagePaginatePrevious('<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>')
            ->languagePaginateNext('<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>')
            ->languageInfo("Showing page _PAGE_ of _PAGES_")
            ->languageSearch('<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>')
            ->languageSearchPlaceholder("Search...")
            ->languageLengthMenu("Results :  _MENU_")
            ->lengthMenu([7, 10, 20, 50])
            ->pageLength(50)
            ->orderBy(0);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('id'),
            Column::make('product_name'),
            Column::make('product_code'),
            Column::make('brand.name')->title("brand name"),
            Column::make('categories'),
            Column::make('discount.discount_name')->title("discount"),
            Column::make('status'),
            Column::make('action')->orderable(false),
        ];
    }


    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Products_' . date('YmdHis');
    }
}
