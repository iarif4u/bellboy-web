<?php

namespace App\DataTables;

use App\Models\AccountExpense;
use App\Models\AccountIncome;
use App\Models\AccountStatement;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Yajra\DataTables\DataTableAbstract;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class StatementsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()->eloquent($query)
            ->addColumn('credit', function ($statement) {
                return ($statement->type == AccountIncome::class) ? $statement->amount : '';
            })->addColumn('debit', function ($statement) {
                return ($statement->type == AccountExpense::class) ? $statement->amount : '';
            })->addColumn('date', function ($statement) {
                if ($statement->type == AccountExpense::class) {
                    return AccountExpense::where(['id' => $statement->ref_id])->first()->date;
                } else {
                    return AccountIncome::where(['id' => $statement->ref_id])->first()->date;
                }
            })->addColumn('reference',function ($statement){
                if ($statement->type == AccountExpense::class) {
                    return AccountExpense::where(['id' => $statement->ref_id])->first()->reference;
                } else {
                    return AccountIncome::where(['id' => $statement->ref_id])->first()->reference;
                }
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param AccountStatement $model
     * @return Builder
     */
    public function query(AccountStatement $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('statements-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->processing(false)
            ->languagePaginatePrevious('<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>')
            ->languagePaginateNext('<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>')
            ->languageInfo("Showing page _PAGE_ of _PAGES_")
            ->languageSearch('<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>')
            ->languageSearchPlaceholder("Search...")
            ->languageLengthMenu("Results :  _MENU_")
            ->lengthMenu([7, 10, 20, 50])
            ->rowCallback('function(nRow, aData, iDisplayIndex){
                    console.log(iDisplayIndex);
                $("td:first", nRow).html(iDisplayIndex +1);
               return nRow;
            }')
            ->orderBy(0)
            ->pageLength(50);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {//["type", "amount", "calculate_amount", "ref_id"]
        return [
            Column::make('id')->title('SL'),
            Column::make('date')->name('created_at'),
            Column::make('credit')->name('amount'),
            Column::make('debit')->name('amount'),
            Column::make('calculate_amount')->title('balance'),
            Column::make('reference'),
        ];
    }


    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Statements_' . date('YmdHis');
    }
}
