<?php

namespace App\Exports;

use App\Models\ProductsVariant;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ProductsVariantExport implements FromView, ShouldAutoSize
{
    public function view(): View
    {
        $product_variants = ProductsVariant::with(['product', 'unit', 'agentActiveOffers'])->get();
        return view('exports.product_variants', ['product_variants' => $product_variants]);
    }
}

