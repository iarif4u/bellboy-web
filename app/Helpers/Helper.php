<?php

use App\Models\AgentOffer;
use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use \Illuminate\Support\Str;

if (!function_exists('activeMenu')) {

    /**
     * description
     *
     * @param $route
     * @param string $output
     * @param mixed $not_match
     * @return string
     */
    function activeMenu($route, $output = "active", $not_match = null)
    {
        if (Route::currentRouteName() == $route) return $output;
        if ($not_match) return $not_match;
    }
}

if (!function_exists('areActiveRoutes')) {

    /**
     * description
     *
     * @param array $routes
     * @param string $output
     * @param null $not_match
     * @return string
     */
    function areActiveRoutes(array $routes, $output = "active", $not_match = null)
    {
        if (in_array(Route::currentRouteName(), $routes)) return $output;
        if ($not_match) return $not_match;
    }
}


if (!function_exists('str_slug')) {

    /**
     * Make Slug from string
     *
     * @param String $slug
     * @return string
     */
    function str_slug(string $slug)
    {
        return Str::slug($slug);
    }
}


if (!function_exists('file_name')) {

    /**
     * Make Appropriate file name from string
     *
     * @param String $filename
     * @return string
     */
    function file_name(string $filename)
    {
        $filename = normal_chars($filename);
        // reduce consecutive characters
        $filename = preg_replace(array('/ +/', '/_+/', '/-+/'), '-', $filename);
        $filename = preg_replace(array('/-*\.-*/', '/\.{2,}/'), '.', $filename);
        // lowercase for windows/unix interoperability http://support.microsoft.com/kb/100625
        $filename = mb_strtolower($filename, mb_detect_encoding($filename));
        // ".file-name.-" becomes "file-name"
        $filename = trim($filename, '.-');
        $filename = str_replace(['#', '/', '\\', ' '], '-', $filename);
        return (strlen($filename) > 0) ? $filename : uniqid("file-");
    }
}

if (!function_exists('normal_chars')) {

    /**
     * Make String in normal form
     *
     * @param $string
     * @return string
     */
    function normal_chars($string)
    {
        $string = htmlentities($string, ENT_QUOTES, 'UTF-8');
        $string = preg_replace('~&([a-z]{1,2})(acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', $string);
        $string = html_entity_decode($string, ENT_QUOTES, 'UTF-8');
        $string = preg_replace(array('~[^0-9a-z]~i', '~[ -]+~'), ' ', $string);
        return trim(basename(stripslashes(trim($string, ' -'))), ".\x00..\x20");
    }
}
if (!function_exists('add_media_file')) {

    /**
     * Add media file to modal by media library
     *
     * @param Model $model
     * @param $file
     * @param String $collection
     * @return string
     */
    function add_media_file(Model $model, $file, string $collection = "default")
    {
        $extension = $file->getClientOriginalExtension();
        $model->addMedia($file)->toMediaCollection($collection);
    }
}

if (!function_exists('add_url_media_file')) {

    /**
     * Add media file to modal by media library
     *
     * @param Model $model
     * @param $file
     * @param String $collection
     * @return string
     */
    function add_url_media_file(Model $model, $url, string $collection = "default")
    {
        $model->addMediaFromUrl($url)->toMediaCollection($collection);
    }
}

if (!function_exists('sync_media_file')) {

    /**
     * Sync media file to modal by media library
     *
     * @param Model $model
     * @param $file
     * @param String $collection
     * @return string
     */
    function sync_media_file(Model $model, $file, string $collection = "default")
    {

        $model->clearMediaCollection($collection);
        add_media_file($model, $file, $collection);
    }
}

if (!function_exists('profile_picture')) {

    /**
     * Get profile picture by media library
     *
     * @param Model $model
     * @return string
     */
    function profile_picture(Model $model)
    {

        return get_media_file($model,config('const.media.profile_picture'));
    }
}

if (!function_exists('nid_card')) {

    /**
     * Get profile picture by media library
     *
     * @param Model $model
     * @return string
     */
    function nid_card(Model $model)
    {
        return get_media_file($model,config('const.media.nid_card'));
    }
}

if (!function_exists('get_media_file')) {

    /**
     * Get profile picture by media library
     *
     * @param Model $model
     * @param String $collection
     * @return string
     */
    function get_media_file(Model $model, string $collection)
    {
        if ($model->getFirstMedia($collection)) {
            return asset($model->getFirstMediaUrl($collection));
        }
        return false;
    }
}
if (!function_exists('setEnv')) {

    /**
     * Update env file by name and value
     *
     * @param $name
     * @param $value
     * @return string
     */
    function setEnv($name, $value)
    {
        $path = base_path('.env');
        if (file_exists($path)) {
            file_put_contents($path, str_replace(
                $name . '=' . env($name), $name . '=' . $value, file_get_contents($path)
            ));
        }
    }
}

if (!function_exists('timezone_list')) {

    /**
     * Update env file by name and value
     *
     * @return array
     */
    function timezone_list()
    {
        $zones_array = array();
        $timestamp = time();
        foreach (timezone_identifiers_list() as $key => $zone) {
            date_default_timezone_set($zone);
            $zones_array[$key]['zone'] = $zone;
            $zones_array[$key]['diff_from_GMT'] = '(UTC/GMT ' . date('P', $timestamp) . ") " . $zone;
        }

        return $zones_array;
    }
}

if (!function_exists('settings_file_upload')) {
    /**
     * Upload settings files
     *
     * @param $upload_file
     * @param $dir
     * @return string
     */
    function settings_file_upload($upload_file, $dir)
    {
        $originName = rand(000, 111) . "_" . file_name($upload_file->getClientOriginalName());
        $extension = $upload_file->getClientOriginalExtension();
        $fileName = $originName . '.' . $extension;
        $upload_file->move($dir, $fileName);
        return $fileName;

    }
}
if (!function_exists('is_valid_domain_name')) {
    /**
     * Check valid domain or not
     *
     * @param $domain_name
     * @return string
     */
    function is_valid_domain_name($domain_name)
    {
        return (preg_match("/^([a-z\d](-*[a-z\d])*)(\.([a-z\d](-*[a-z\d])*))*$/i", $domain_name) //valid chars check
            && preg_match("/^.{1,253}$/", $domain_name) //overall length check
            && preg_match("/^[^\.]{1,63}(\.[^\.]{1,63})*$/", $domain_name)); //length of each label
    }
}

if (!function_exists('get_sms_config')) {
    /**
     * Get the sms configuration array
     *
     * @return object
     * @throws FileNotFoundException
     */
    function get_sms_config()
    {
        return json_decode(Storage::disk('local')->get('sms_config.json'));
    }
}
if (!function_exists('set_sms_config')) {
    /**
     * Get the sms configuration array
     *
     * @param array $config
     * @return bool
     */
    function set_sms_config(array $config)
    {
        return Storage::disk('local')->put('sms_config.json', json_encode($config));
    }
}

if (!function_exists('get_sms_config_header')) {
    /**
     * Get the sms configuration array
     *
     * @return array
     * @throws FileNotFoundException
     */
    function get_sms_config_header()
    {
        $headers = json_decode(Storage::disk('local')->get('sms_config.json'), true)['headers'];
        $options = [];
        foreach ($headers as $key => $value) {
            $options = array_merge($options, $value);
        }
        return $options;
    }
}
if (!function_exists('get_sms_config_option')) {
    /**
     * Get the sms configuration array
     *
     * @return array
     * @throws FileNotFoundException
     */
    function get_sms_config_option()
    {
        $others = json_decode(Storage::disk('local')->get('sms_config.json'), true)['params']['others'];
        $options = [];
        foreach ($others as $key => $value) {
            $options = array_merge($options, $value);
        }
        return $options;
    }
}

if (!function_exists('get_sms_config_all_options')) {
    /**
     * Get the sms configuration array
     *
     * @return array
     * @throws FileNotFoundException
     */
    function get_sms_config_all_options()
    {
        return array_merge(get_sms_config_header(), get_sms_config_option());
    }
}
if (!function_exists('en2bnNumber')) {
    /**
     * Get bangle number from english
     *
     * @param $number
     * @return array
     */
    function en2bnNumber($number)
    {
        $replace_array = array("১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯", "০", 'মধ্যাহ্ন', 'অপরাহ্ন');
        $search_array = array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0", 'AM', 'PM');
        return str_replace($search_array, $replace_array, $number);
    }
}
if (!function_exists('bn2enNumber')) {
    /**
     * Get bangle number from english
     *
     * @param $number
     * @return string
     */
    function bn2enNumber($number)
    {
        $replace_array = array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0", 'AM', 'PM');
        $search_array = array("১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯", "০", 'মধ্যাহ্ন', 'অপরাহ্ন');
        return str_replace($search_array, $replace_array, $number);
    }
}
if (!function_exists('isValidJson')) {
    /**
     * Check the string has valid json or not
     *
     * @param $string
     * @return bool
     */
    function isValidJson($string)
    {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }

}
if (!function_exists('humanDateTime')) {
    /**
     * Check the string has valid json or not
     *
     * @param $string
     * @return string
     */
    function humanDateTime($string)
    {
        return Carbon::parse($string)->diffForHumans();
    }
}
if (!function_exists('humanDateTime')) {
    /**
     * Check the string has valid json or not
     *
     * @param $string
     * @return string
     */
    function humanDateTime($string)
    {
        return Carbon::parse($string)->diffForHumans();
    }
}
if (!function_exists('unicode_no_validation')) {
    /**
     * Check the string has valid json or not
     *
     * @param $number
     * @return string
     */
    function unicode_no_validation($number)
    {
        return (bn2enNumber($number) > 0);
    }
}

if (!function_exists('coupon_code_calculate')) {
    function coupon_code_calculate($total, $code)
    {
        if ($code->type === config('const.coupon_codes.fixed')) {
            return $code->amount;
        } else {
            $percent = $total * ($code->amount / 100);
            if ($percent < $code->max_amount) {
                return round($percent);
            } else {
                return $code->max_amount;
            }
        }
    }
}

if (!function_exists('write_custom_log')) {
    function write_custom_log($log)
    {
        Log::channel('custom')->info($log);
    }
}

if (!function_exists('send_custom_sms')) {
    function send_custom_sms($number, $message)
    {
        try {
            smsapi(intval($number), $message, get_sms_config_all_options(), get_sms_config_header());
            write_custom_log("SMS send to " . $number);
        } catch (FileNotFoundException $e) {
            write_custom_log($e->getMessage());
        }
    }
}


if (!function_exists('get_item_status')) {
    function get_item_status($status)
    {
      $status_list =  [
            'pending' => 'Pending',
            'delivery_boy_collect' => 'Collected',
            'agent_collect' => 'received',
            'packed' => 'packed',
            'collected' => 'collected',
            'delivered' => 'delivered',
            'deny' => 'deny'
        ];
      return $status_list[$status];
    }
}

if (!function_exists('get_profile_pic')) {
    function get_profile_pic($customer)
    {
        $profile_pic = get_media_file($customer, config('const.media.profile_picture'));
        if (!$profile_pic) {
            $profile_pic = setting(config('const.settings.site_logo'), asset('assets/img/logo.png'));
        }
        return $profile_pic;
    }
}
if (!function_exists('order_total')) {
    function order_total($order)
    {
        if($order->coupon_discount>0){
            return $order->grand_total+$order->delivery_charge-$order->coupon_discount;
        }
        return $order->grand_total+$order->delivery_charge;
    }
}
if (!function_exists('pending_orders')) {
    function pending_orders()
    {
        return Order::where(['order_status' => config('const.order_status.pending')])->count();
    }
}
if (!function_exists('agent_offers')) {
    function agent_offers()
    {
        return AgentOffer::where(['offer_status' => config('const.agent_status.pending')])->count();
    }
}
