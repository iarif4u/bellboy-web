<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\ExpenseCategoriesDataTable;
use App\DataTables\ExpensesDataTable;
use App\DataTables\IncomesDataTable;
use App\DataTables\StatementsDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ExpenseCategoryDeleteRequest;
use App\Http\Requests\Admin\ExpenseCategoryStoreRequest;
use App\Http\Requests\Admin\ExpenseCategoryUpdateRequest;
use App\Http\Requests\Admin\ExpenseInfoRequest;
use App\Http\Requests\Admin\ExpenseStoreRequest;
use App\Http\Requests\Admin\ExpenseUpdateRequest;
use App\Http\Requests\Admin\IncomeInfoRequest;
use App\Http\Requests\Admin\IncomeStoreRequest;
use App\Http\Requests\Admin\IncomeUpdateRequest;
use App\Models\AccountExpense;
use App\Models\AccountIncome;
use App\Models\AccountStatement;
use App\Models\ExpenseCategory;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AccountStatementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param IncomesDataTable $dataTable
     * @return View
     */
    public function index(IncomesDataTable $dataTable)
    {
        return $dataTable->render('auth.account.income');
    }

    public function getIncomeInfo(IncomeInfoRequest $request)
    {
        return AccountIncome::with('note')->where(['id' => $request->input('income_id')])->firstOrFail();
    }

    /**
     * Make new income
     *
     * @param IncomeStoreRequest $request
     * @return RedirectResponse
     */
    public function makeNewIncome(IncomeStoreRequest $request)
    {
        DB::beginTransaction();
        try {
            $amount = $request->input('amount');
            $income = AccountIncome::create([
                "income_name" => $request->input('income_name'),
                "user_id" => auth()->id(),
                "income_amount" => $amount,
                "reference" => 'IN-' . date('YmdHis') . random_int(10, 99),
                "date" => $request->input('date')
            ]);
            if ($request->input('note')) {
                $income->note()->create(['note' => $request->input('note')]);
            }
            $account_statement = AccountStatement::latest()->first();
            AccountStatement::create([
                "type" => AccountIncome::class,
                "amount" => $amount,
                "calculate_amount" => ($account_statement) ? $account_statement->calculate_amount + $amount : $amount,
                'ref_id' => $income->id,
            ]);
            DB::commit();
            return redirect()->back()->with('success', "Income add successfully done");
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->withErrors(["Internal Server Error"])->with('exception', $exception->getMessage())->withInput();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param IncomeUpdateRequest $request
     * @return RedirectResponse
     */
    public function updateIncomeInfo(IncomeUpdateRequest $request)
    {
        DB::beginTransaction();
        try {
            $amount = $request->input('amount');
            $income = AccountIncome::with('note')->where(['id' => $request->input('income_id')])->firstOrFail();
            $difference = $amount - $income->income_amount;
            $income->update([
                "income_name" => $request->input('income_name'),
                "user_id" => auth()->id(),
                "income_amount" => $amount,
                "date" => $request->input('date')
            ]);
            if ($request->input('note')) {
                $income->note()->updateOrCreate(['note' => $request->input('note')]);
            } else {
                $income->note()->delete();
            }
            if ($difference != 0) {
                $statement = AccountStatement::where(['ref_id' => $income->id, "type" => AccountIncome::class])->first();
                $statement->amount = $amount;
                AccountStatement::where('id', '>=', $statement->id)->increment('calculate_amount', $difference);
                $statement->save();
            }
            DB::commit();
            return redirect()->back()->with('success', "Income update successfully done");
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->withErrors(["Internal Server Error"])->with('exception', $exception->getMessage())->withInput();
        }
    }

    /**
     * Add new expense
     *
     * @param ExpenseStoreRequest $request
     * @return RedirectResponse
     */
    public function addNewExpense(ExpenseStoreRequest $request)
    {
        DB::beginTransaction();
        try {
            $amount = $request->input('expense_amount');
            $expense = AccountExpense::create([
                "expense_name" => $request->input('expense_name'),
                "user_id" => auth()->id(),
                "category_id" => $request->input('expense_category'),
                "expense_amount" => $amount,
                "reference" => 'EXP-' . date('YmdHis') . random_int(10, 99),
                "date" => $request->input('date')
            ]);
            if ($request->input('expense_note')) {
                $expense->note()->create(['note' => $request->input('expense_note')]);
            }
            $account_statement = AccountStatement::latest()->first();
            AccountStatement::create([
                "type" => AccountExpense::class,
                "amount" => $amount,
                "calculate_amount" => ($account_statement) ? $account_statement->calculate_amount - $amount : $amount,
                'ref_id' => $expense->id,
            ]);
            DB::commit();
            return redirect()->back()->with('success', "Expense add successfully done");
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->withErrors(["Internal Server Error"])->with('exception', $exception->getMessage())->withInput();
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param ExpensesDataTable $dataTable
     * @return View
     */
    public function getExpenseListView(ExpensesDataTable $dataTable)
    {
        $categories = ExpenseCategory::all();
        return $dataTable->render('auth.account.expense', ['categories' => $categories]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param ExpenseCategoriesDataTable $dataTable
     * @return View
     */
    public function getExpenseCategoriesView(ExpenseCategoriesDataTable $dataTable)
    {
        return $dataTable->render('auth.account.expense_category');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ExpenseCategoryStoreRequest $request
     * @return RedirectResponse
     */
    public function addExpenseCategory(ExpenseCategoryStoreRequest $request)
    {
        try {
            ExpenseCategory::create(["category" => $request->input('category'), "user_id" => auth()->id()]);
            return redirect()->back()->with('success', "Expense category add successfully done");
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->withErrors(["Internal Server Error"])->with('exception', $exception->getMessage())->withInput();
        }
    }

    /**
     * Update expense category.
     *
     * @param ExpenseCategoryUpdateRequest $request
     * @return RedirectResponse
     */
    public function updateExpenseCategory(ExpenseCategoryUpdateRequest $request)
    {
        try {
            ExpenseCategory::where(['id' => $request->input('category_id')])->update(["category" => $request->input('category'), "user_id" => auth()->id()]);
            return redirect()->back()->with('success', "Expense category update successfully done");
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->withErrors(["Internal Server Error"])->with('exception', $exception->getMessage())->withInput();
        }
    }

    /**
     * Update expense category.
     *
     * @param ExpenseCategoryDeleteRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteExpenseCategory(ExpenseCategoryDeleteRequest $request)
    {
        try {
            ExpenseCategory::where(['id' => $request->input('category_id')])->first()->delete();
            return response()->json(['message' => "Expense category delete success"]);
        } catch (\Exception $exception) {
            return response()->json(['message' => "Internal Server Error", 'exception' => $exception->getMessage()], 500);
        }
    }

    /**
     * get expense info.
     *
     * @param ExpenseInfoRequest $request
     * @return Builder|Model|object
     */
    public function getExpenseInfo(ExpenseInfoRequest $request)
    {
        try {
            return AccountExpense::with(['note'])->where(['id' => $request->input('expense_id')])->first();
        } catch (\Exception $exception) {
            return response()->json(['message' => "Internal Server Error", 'exception' => $exception->getMessage()], 500);
        }
    }

    /**
     * Update expense info.
     *
     * @param ExpenseUpdateRequest $request
     * @return RedirectResponse
     */
    public function updateExpenseInfo(ExpenseUpdateRequest $request)
    {
        DB::beginTransaction();
        try {
            $amount = $request->input('expense_amount');
            $expense = AccountExpense::with('note')->where(['id' => $request->input('expense_id')])->firstOrFail();
            $difference = $amount - $expense->expense_amount;
            $expense->update([
                "expense_name" => $request->input('expense_name'),
                "user_id" => auth()->id(),
                "category_id" => $request->input('expense_category'),
                "expense_amount" => $amount,
                "date" => $request->input('date')
            ]);
            if ($request->input('expense_note')) {
                $expense->note()->updateOrCreate(['note' => $request->input('expense_note')]);
            } else {
                $expense->note()->delete();
            }
            if ($difference != 0) {
                $statement = AccountStatement::where(['ref_id' => $expense->id, "type" => AccountExpense::class])->first();
                $statement->amount = $amount;
                AccountStatement::where('id', '>=', $statement->id)->decrement('calculate_amount', $difference);
                $statement->save();
            }
            DB::commit();
            return redirect()->back()->with('success', "Expense update successfully done");
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->withErrors(["Internal Server Error"])->with('exception', $exception->getMessage())->withInput();
        }
    }

    /**
     * Delete expense
     *
     * @param ExpenseInfoRequest $request
     * @return Builder|Model|object
     */
    public function deleteExpenseInfo(ExpenseInfoRequest $request)
    {
        DB::beginTransaction();
        try {
            $expense = AccountExpense::with('note')->where(['id' => $request->input('expense_id')])->firstOrFail();
            $difference = $expense->expense_amount;
            $expense->note()->delete();
            $statement = AccountStatement::where(['ref_id' => $expense->id, "type" => AccountExpense::class])->first();
            AccountStatement::where('id', '>=', $statement->id)->increment('calculate_amount', $difference);
            $statement->delete();
            $expense->delete();
            DB::commit();
            return response()->json(['message' => "Expense delete success"]);
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json(['message' => "Internal Server Error", 'exception' => $exception->getMessage()], 500);
        }
    }

    public function deleteIncomeInfo(IncomeInfoRequest $request){
        DB::beginTransaction();
        try {
            $income = AccountIncome::with('note')->where(['id' => $request->input('income_id')])->firstOrFail();
            $income->note()->delete();
            $statement = AccountStatement::where(['ref_id' => $income->id, "type" => AccountIncome::class])->first();
            AccountStatement::where('id', '>=', $statement->id)->decrement('calculate_amount', $income->income_amount);
            $statement->delete();
            $income->delete();
            DB::commit();
            return response()->json(['message' => "Income delete success"]);
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json(['message' => "Internal Server Error", 'exception' => $exception->getMessage()], 500);
        }
    }
    public function statement(StatementsDataTable $dataTable){

        return $dataTable->render('auth.account.statement');
    }
}
