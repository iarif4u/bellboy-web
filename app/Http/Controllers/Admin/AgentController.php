<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\AgentsDataTable;
use App\DataTables\AgentsPriceApproveDataTable;
use App\DataTables\AgentsPriceOfferDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AgentBalanceWithdrawRequest;
use App\Http\Requests\Admin\AgentDeleteRequest;
use App\Http\Requests\Admin\AgentStatusRequest;
use App\Http\Requests\Admin\AgentStoreRequest;
use App\Http\Requests\Admin\AgentUpdateRequest;
use App\Http\Requests\Admin\AgentViewRequest;
use App\Models\Agent;
use App\Models\AgentOffer;
use App\Models\BalanceWithdraw;
use App\Models\Conversation;
use App\Models\Participant;
use App\Notifications\Agent\BalanceWithdrawNotification;
use App\Notifications\Agent\PriceApproveNotification;
use App\Repositories\Agent\AgentRepository;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;
use phpDocumentor\Reflection\Types\Object_;

class AgentController extends Controller
{
    // model property on class instances
    protected $agent;

    // Constructor to bind model to repo
    public function __construct(AgentRepository $agent)
    {
        $this->agent = $agent;
    }

    //form view for new agent make
    public function showNewAgentForm()
    {
        return view('auth.agent.new_agent');
    }

    //make new agent by post request
    public function makeNewAgent(AgentStoreRequest $request)
    {
        $request->validated();
        DB::beginTransaction();
        try {
            $this->agent->create($request);
            $agent = $this->agent->save_profile_data($request->only('shop_address', 'shop_name'));
            add_media_file($agent, $request->file('profile_picture'), config('const.media.profile_picture'));
            add_media_file($agent, $request->file('nid_card_front'), config('const.media.nid_card_front'));
            add_media_file($agent, $request->file('nid_card_back'), config('const.media.nid_card_back'));
            add_media_file($agent, $request->file('trade_license'), config('const.media.trade_license'));
            add_media_file($agent, $request->file('trade_license_second'), config('const.media.trade_license_second'));
            $conversation = Conversation::create([
                'title' => "Chat with " . $agent->name,
                'creator_id' => $agent->id,
                'creator_type' => Agent::class,
                'channel_id' => uniqid('chat_')
            ]);
            $admin = User::where(['chat' => true])->first();
            if (!$admin) {
                $admin = auth()->user();
            }
            $participants = [
                ['user_type' => Agent::class, 'user_id' => $agent->id],
                ['user_type' => User::class, 'user_id' => $admin->id]
            ];
            $conversation->participants()->createMany($participants);
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            return ($request->ajax()) ? response()->json(['message' => "Internal Server Error"], 500) : redirect()->back()->withErrors('Internal Server Error')->withInput();
        }
        return redirect()->back()->with('success', "New agent create successfully done");
    }

    //show agent list in datatable
    public function showAgentList(AgentsDataTable $dataTable)
    {
        return $dataTable->render('auth.agent.agent_list');
    }

    //update agent status
    public function updateAgentStatus(AgentStatusRequest $request)
    {
        try {
            $this->agent->update(['status' => $request->input('agent_status')], $request->input('agent_id'));
            return response()->json(['message' => ucfirst($request->input('agent_status') . " The Agent")]);
        } catch (\Exception $exception) {
            return response()->json(['message' => "Internal Server Error"], 500);
        }

    }

    //delete agent by delete request
    public function deleteAgentData(AgentDeleteRequest $request)
    {
        try {
            $this->agent->delete($request->input('agent_id'));
            return response()->json(['message' => "Agent Delete Successfully Done"]);
        } catch (\Exception $exception) {
            return response()->json(['message' => "Internal Server Error"], 500);
        }
    }

    public function showAgentEditView(int $agent_id)
    {
        $agent = $this->agent->show($agent_id);
        return view('auth.agent.edit_agent', ['agent' => $agent]);
    }

    public function getAgentDetails(AgentViewRequest $request)
    {
        return $this->agent->get_agent_details($request->input('agent_id'));
    }

    //update agent info
    public function updateAgentInfo(AgentUpdateRequest $request)
    {
        DB::beginTransaction();
        try {
            $agent_id = $request->input('agent_id');
            $update_array = [
                'name' => $request->input('agent_name'),
                'email' => $request->input('email'),
                'phone' => $request->input('phone'),
                'agreement_date' => $request->input('agreement_date'),
            ];
            $this->agent->update($update_array, $agent_id);
            if ($request->input('password', null) != null) {
                $this->agent->update(['password' => bcrypt($request->input('password'))], $agent_id);
            }
            $this->agent->show($agent_id);
            $agent = $this->agent->update_profile_data($request->only('shop_address', 'shop_name'), $agent_id);
            if ($request->hasFile('profile_picture')) {
                $agent->clearMediaCollection(config('const.media.profile_picture'));
                add_media_file($agent, $request->file('profile_picture'), config('const.media.profile_picture'));
            }
            if ($request->hasFile('nid_card_front')) {
                $agent->clearMediaCollection(config('const.media.nid_card_front'));
                add_media_file($agent, $request->file('nid_card_front'), config('const.media.nid_card_front'));
            }
            if ($request->hasFile('nid_card_back')) {
                $agent->clearMediaCollection(config('const.media.nid_card_back'));
                add_media_file($agent, $request->file('nid_card_back'), config('const.media.nid_card_back'));
            }
            if ($request->hasFile('trade_license')) {
                $agent->clearMediaCollection(config('const.media.trade_license'));
                add_media_file($agent, $request->file('trade_license'), config('const.media.trade_license'));
            }
            if ($request->hasFile('trade_license_second')) {
                $agent->clearMediaCollection(config('const.media.trade_license_second'));
                add_media_file($agent, $request->file('trade_license_second'), config('const.media.trade_license_second'));
            }
            DB::commit();
            return redirect()->back()->with("success", "Agent update successfully done");
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->withErrors([$exception->getMessage()])->withInput();
        }
    }

    /**
     * Display the specified resource by search.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function search(Request $request)
    {
        try {
            $limit = 10;
            $string = $request->input('search', '');
            $response = Agent::where('name', 'like', "%$string%")->limit($limit)->get();
            return response()->json(['total' => $response->count(), 'data' => $response]);
        } catch (\Exception $exception) {
            return response()->json(['message' => $exception->getMessage()], 500);
        }
    }

    /**
     * Get Agent offers
     *
     * @param AgentsPriceOfferDataTable $dataTable
     * @return void
     */
    public function getAgentOffers(AgentsPriceOfferDataTable $dataTable)
    {
        return $dataTable->render('auth.agent.agent_offers');
    }

    public function getAgentBalanceWithdrawView(AgentsDataTable $dataTable)
    {
        $agents = Agent::where(['withdraw_req' => true])->get();
        return view('auth.agent.payment', ['agents' => $agents]);
    }

    public function paymentAgentBalance(AgentBalanceWithdrawRequest $request)
    {
        DB::beginTransaction();
        try {
            $agent = Agent::where(['id' => $request->input('agent_id')])->first();
            $paymentType = $request->input('paymentType');
            $ref = $request->input('accNo', null);
            $paymentAmount = bn2enNumber($request->input('paymentAmount'));
            if ($agent->balance >= $paymentAmount) {
                BalanceWithdraw::create([
                    "agent_id" => $agent->id,
                    "payment_by" => auth()->id(),
                    "amount" => $paymentAmount,
                    "payment_method" => $paymentType,
                    "bank" => $request->input('bankName',null),
                    "reference" => $ref,
                    "note" => $request->input('paymentNote', null)
                ]);
                $agent->decrement('balance', $paymentAmount);
                $agent->withdraw_req=false;
                $agent->save();
                DB::commit();
                Notification::send($agent, new BalanceWithdrawNotification($paymentAmount,$paymentType));
                return  redirect()->back()->with('success',"Agent balance withdraw success");
            }else{
                return redirect()->back()->withErrors(["Insufficient balance"])->withInput();
            }
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->withErrors([$exception->getMessage()])->withInput();
        }
    }

    /**
     * Get Agent offers
     *
     * @param AgentsPriceApproveDataTable $dataTable
     * @return void
     */
    public function getAgentApproveOffers(AgentsPriceApproveDataTable $dataTable)
    {
        return $dataTable->render('auth.agent.agent_offers');
    }
}
