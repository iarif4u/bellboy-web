<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\BrandsDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\BrandDeleteRequest;
use App\Http\Requests\Admin\BrandStoreRequest;
use App\Http\Requests\Admin\BrandUpdateRequest;
use App\Models\Brand;
use Illuminate\Http\JsonResponse;
use Illuminate\View\View;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param BrandsDataTable $dataTable
     * @return View
     */
    public function showBrandtList(BrandsDataTable $dataTable)
    {
        return $dataTable->render('auth.brand.brand_list');
    }

    public function updateBrand(BrandUpdateRequest $request)
    {
        Brand::where(['id' => $request->input('brand_id')])->update(['name' => $request->input('brand_name'), 'added_by' => auth()->id()]);
        $brand = Brand::where(['id' => $request->input('brand_name')])->first();
        if ($request->hasFile('brand_img')) {
            sync_media_file($brand, $request->file('brand_img'), config('const.media.brand_image'));
        }
        return redirect()->back()->with('success', "Brand update successfully done");
    }

    public function makeNewBrand(BrandStoreRequest $request)
    {
        $brand = Brand::create(['name' => $request->input('brand_name'), 'added_by' => auth()->id()]);
        add_media_file($brand, $request->file('brand_img'), config('const.media.brand_image'));
        return redirect()->back()->with('success', "New brand add successfully done");
    }

    public function deleteBrand(BrandDeleteRequest $request){
        try {
            Brand::where(['id' => $request->input('brand_id')])->first()->delete();
            return response()->json(['message' => "Brand Delete Successfully Done"]);
        } catch (\Exception $exception) {
            return response()->json(['message' => "Internal Server Error"], 500);
        }
    }

    /**
     * Display the specified resource by search.
     *
     * @return JsonResponse
     */
    public function search()
    {
        try {
            $limit = 10;
            $string = \request()->input('search', '');
            $response = Brand::where('name', 'like', "%$string%")->limit($limit)->get();
            return response()->json(['total' => $response->count(), 'data' => $response]);
        } catch (\Exception $exception) {
            return response()->json(['message' => $exception->getMessage()], 500);
        }
    }
}
