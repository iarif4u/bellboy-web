<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\CategoriesDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CategoryDeleteRequest;
use App\Http\Requests\Admin\CategoryStoreRequest;
use App\Http\Requests\Admin\CategoryUpdateRequest;
use App\Models\Category;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;


class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param CategoriesDataTable $dataTable
     * @return View
     */
    public function showCategorytList(CategoriesDataTable $dataTable)
    {
        return $dataTable->render('auth.category.category_list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CategoryStoreRequest $request
     * @return RedirectResponse
     */
    public function makeNewCategory(CategoryStoreRequest $request)
    {
        $request->validateResolved();
        $category = Category::create(['name' => $request->input('category_name'), 'category_id' => $request->input('parent_category', null)]);
        add_media_file($category, $request->file('category_image'), config('const.media.category_image'));
        return redirect()->back()->with('success', "New Category add successfully done");
    }

    /**
     * Display the specified resource.
     *
     * @param Category $category
     * @return JsonResponse
     */
    public function search()
    {
        try {
            $limit = 10;
            $string = \request()->input('search', '');
            $response = Category::where('name', 'like', "%$string%")->get();
            return response()->json(['total' => $response->count(), 'data' => $response]);
        } catch (\Exception $exception) {
            return response()->json(['message' => $exception->getMessage()], 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Category $category
     * @return Response
     */
    public function edit(Category $category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CategoryUpdateRequest $request
     * @return RedirectResponse
     */
    public function updateCategory(CategoryUpdateRequest $request)
    {
        DB::beginTransaction();
        try {
            $category = Category::where(['id' => $request->input('category_id')])->first();
            $category->name = $request->input('category_name');
            $category->category_id = $request->input('parent_category', null);
            $category->save();
            if ($request->hasFile('category_image')) {
                sync_media_file($category, $request->file('category_image'), config('const.media.category_image'));
            }
            DB::commit();
            return redirect()->back()->with('success', "Category update successfully done");
        }catch (\Exception $exception){
            DB::rollBack();
            return redirect()->back()->withErrors($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param CategoryDeleteRequest $request
     * @return JsonResponse
     */
    public function deleteCategory(CategoryDeleteRequest $request)
    {
        try {
            Category::where(['id'=>$request->input('category_id')])->first()->delete();
            return response()->json(['message' => "Category Delete Successfully Done"]);
        } catch (\Exception $exception) {
            return response()->json(['message' => "Internal Server Error",'exception'=>$exception->getMessage()], 500);
        }
    }
}
