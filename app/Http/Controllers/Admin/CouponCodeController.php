<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\CouponCodesDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CouponCodeDeleteRequest;
use App\Http\Requests\Admin\CouponCodeStoreRequest;
use App\Http\Requests\Admin\CouponCodeUpdateRequest;
use App\Models\CouponCode;
use Illuminate\Http\Request;

class CouponCodeController extends Controller
{
    public function couponCodes(CouponCodesDataTable $dataTable)
    {
        return $dataTable->render('auth.coupon.coupon_codes');
    }

    public function updateCouponCode(CouponCodeUpdateRequest $request)
    {
        CouponCode::where(['id' => $request->input('coupon_code_id')])->update([
            "coupon_code" => $request->input('coupon_code'),
            "type" => $request->input('type'),
            "expire_date" => $request->input('expire_date'),
            "amount" => $request->input('amount'),
            "max_amount" => $request->input('max_amount'),
            "qty" => $request->input('qty')
        ]);
        return redirect()->back()->with('success', "Coupon Code update successfully done");
    }

    public function addNewCouponCode(CouponCodeStoreRequest $request)
    {
        CouponCode::create([
            "coupon_code" => $request->input('coupon_code'),
            "type" => $request->input('type'),
            "expire_date" => $request->input('expire_date'),
            "amount" => $request->input('amount'),
            "max_amount" => $request->input('max_amount'),
            "qty" => $request->input('qty')
        ]);
        return redirect()->back()->with('success', "Coupon Code added successfully done");
    }

    public function getCouponDiscount(Request $request)
    {
        $coupon = CouponCode::where(['id' => $request->input('coupon_id')])->first();
        $total = $request->input('total');
        return coupon_code_calculate($total, $coupon);
    }

    public function deleteCouponCode(CouponCodeDeleteRequest $request)
    {
        try {
            CouponCode::where(['id' => $request->input('coupon_id')])->first()->delete();
            return response()->json(['message' => "Coupon Code Delete Successfully Done"]);
        } catch (\Exception $exception) {
            return response()->json(['message' => "Internal Server Error", 'exception' => $exception->getMessage()], 500);
        }
    }
}
