<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\CustomersDataTable;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    public function getCustomers(CustomersDataTable $dataTable){
        return $dataTable->render('auth.customer.customers');
    }
}
