<?php

namespace App\Http\Controllers\Admin\Deliveryboy;


use App\DataTables\AreasDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AreaDeleteRequest;
use App\Http\Requests\Admin\AreaStoreRequest;
use App\Http\Requests\Admin\AreaUpdateRequest;
use App\Models\Area;
use App\Repositories\Area\AreaRepository;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\View\View;

class AreaController extends Controller
{
    // model property on class instances
    protected $area;

    // Constructor to bind model to repo
    public function __construct(AreaRepository $area)
    {
        $this->area = $area;
    }

    /**
     * Display a listing of the resource.
     *
     * @param AreasDataTable $dataTable
     * @return Factory|View
     */
    public function getAreaList(AreasDataTable $dataTable)
    {
        return $dataTable->render('auth.deliveryboy.area_list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AreaStoreRequest $request
     * @return JsonResponse|RedirectResponse
     * @return
     */
    public function makeNewArea(AreaStoreRequest $request)
    {
        try {
            $this->area->create($request);
            if ($request->ajax()) {
                return response()->json(['message' => "Area create successfully done"]);
            }
            return redirect()->back()->with('success', "Area create successfully done");
        } catch (\Exception $exception) {
            if ($request->ajax()) {
                return response()->json(['message' => $exception->getMessage()], 500);
            }
            return redirect()->back()->withErrors([$exception->getMessage()])->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Area $area
     * @return Response
     */
    public function show(Area $area)
    {
        //
    }

    /**
     * Delete specific area resource
     *
     * @param AreaDeleteRequest $request
     * @return JsonResponse
     */
    public function deleteArea(AreaDeleteRequest $request)
    {
        try {
            Area::where(['id' => $request->input('area_id')])->first()->delete();
            return response()->json(['message' => "Area Delete Successfully Done"]);
        } catch (\Exception $exception) {
            return response()->json(['message' => "Internal Server Error"], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param AreaUpdateRequest $request
     * @return RedirectResponse|JsonResponse
     */
    public function updateArea(AreaUpdateRequest $request)
    {
        try {
            $this->area->update(['area_name' => $request->input('area_name')], $request->input('area_id'));
            return redirect()->back()->with('success', "Area name update successfully done");
        } catch (\Exception $exception) {
            if ($request->ajax()) {
                return response()->json(['message' => $exception->getMessage()], 500);
            }
            return redirect()->back()->withErrors([$exception->getMessage()])->withInput();
        }
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return JsonResponse
     */
    public function searchAreaList()
    {
        try {
            $response = $this->area->search(\request()->input('search', ''));
            return response()->json(['total' => $response->count(), 'data' => $response]);
        } catch (\Exception $exception) {
            return response()->json(['message' => $exception->getMessage()], 500);
        }
    }
}
