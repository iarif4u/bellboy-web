<?php


namespace App\Http\Controllers\Admin\Deliveryboy;


use App\DataTables\DeliveryBoysDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\DeliveryBoyStoreRequest;
use App\Http\Requests\Admin\DeliveryBoyUpdateRequest;
use App\Http\Requests\Admin\DeliveryBoyViewRequest;
use App\Models\Conversation;
use App\Models\Deliveryboy;
use App\Models\ProfileData;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DeliveryBoyController extends Controller
{
    public function showNewDeliveryBoyForm()
    {
        return view('auth.deliveryboy.new_delivery_boy');
    }

    public function makeNewDeliveryBoy(DeliveryBoyStoreRequest $request)
    {
        DB::beginTransaction();
        try {
            $delivery_boy = Deliveryboy::create(
                [
                    'employee_id' => $request->input('employeeID'),
                    'name' => $request->input('name'),
                    'email' => $request->input('email'),
                    'password' => bcrypt($request->input('password')),
                    'phone' => $request->input('phone'),
                    'area_id' => $request->input('area'),
                    'status' => config('const.status.active')
                ]
            );
            $conversation = Conversation::create([
                'title' => "Chat with " . $delivery_boy->name,
                'creator_id' => $delivery_boy->id,
                'creator_type' => Deliveryboy::class,
                'channel_id' => uniqid('chat_')
            ]);
            $admin = User::where(['chat' => true])->first();
            if (!$admin) {
                $admin = auth()->user();
            }
            $participants = [
                ['user_type' => Deliveryboy::class, 'user_id' => $delivery_boy->id],
                ['user_type' => User::class, 'user_id' => $admin->id]
            ];
            $conversation->participants()->createMany($participants);
            $delivery_boy->profiles()->save(new ProfileData(['data_name' => 'address', 'data_value' => $request->input('address')]));
            add_media_file($delivery_boy, $request->file('profile_picture'), config('const.media.profile_picture'));
            add_media_file($delivery_boy, $request->file('nid_card'), config('const.media.nid_card'));
            DB::commit();
            return redirect()->back()->with('success', "Delivery boy added successfully done");
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->withErrors([$exception->getMessage()])->withInput();
        }
    }

    public function getDeliveryBoyList(DeliveryBoysDataTable $dataTable)
    {
        return $dataTable->render('auth.deliveryboy.delivery_boy_list');
    }

    public function showEditDeliveryBoyForm($deliveryboy_id)
    {
        $deliveryboy = Deliveryboy::with(['address', 'area'])->where(['id' => $deliveryboy_id])->firstOrFail();
        profile_picture($deliveryboy);
        return view('auth.deliveryboy.edit_delivery_boy', ['deliveryboy' => $deliveryboy]);
    }

    public function updateDeliveryBoy($deliveryboy_id, DeliveryBoyUpdateRequest $request)
    {
        DB::beginTransaction();
        try {
            Deliveryboy::where(['id' => $request->input('deliveryboy_id')])->update([
                'employee_id' => $request->input('employeeID'),
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'phone' => $request->input('phone'),
                'area_id' => $request->input('area')
            ]);
            $delivery_boy = Deliveryboy::where(['id' => $request->input('deliveryboy_id')])->firstOrFail();
            if ($request->input('password', false)) {
                $delivery_boy->password = bcrypt($request->input('password'));
            }
            $delivery_boy->profiles()->delete();
            $delivery_boy->profiles()->save(new ProfileData(['data_name' => 'address', 'data_value' => $request->input('address')]));
            if ($request->hasFile('profile_picture')) {
                sync_media_file($delivery_boy, $request->file('profile_picture'), config('const.media.profile_picture'));
            }
            if ($request->hasFile('nid_card')) {
                sync_media_file($delivery_boy, $request->file('nid_card'), config('const.media.nid_card'));
            }
            DB::commit();
            return redirect()->back()->with('success', "Delivery boy update successfully done");
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->withErrors([$exception->getMessage()])->withInput();
        }
    }

    public function getDeliveryBoyInfo(DeliveryBoyViewRequest $request)
    {
        $deliveryBoy = Deliveryboy::with(['area', 'address'])->select(['id', 'employee_id', 'name', 'email', 'phone', 'area_id',])->where(['id' => $request->input('delivery_boy_id')])->firstOrFail();
        $deliveryBoy->profile = profile_picture($deliveryBoy);
        $deliveryBoy->nid_card = nid_card($deliveryBoy);
        return $deliveryBoy;
    }

    public function deleteDeliveryBoy(DeliveryBoyViewRequest $request)
    {
        try {
            Deliveryboy::with(['profiles'])->where(['id' => $request->input('delivery_boy_id')])->first()->delete();
            return response()->json(['message' => "Delivery boy Delete Successfully Done"]);
        } catch (\Exception $exception) {
            return response()->json(['message' => "Internal Server Error"], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return JsonResponse
     */
    public function search()
    {
        try {
            $string = request()->input('search', '');
            $response =Deliveryboy::where('name','like',"%$string%")
                ->orWhere("employee_id",'like',"%$string%")
                ->orWhere("email",'like',"%$string%")
                ->orWhere("phone",'like',"%$string%")
                ->limit(10)->get();
            return response()->json(['total' => $response->count(), 'data' => $response]);
        } catch (\Exception $exception) {
            return response()->json(['message' => $exception->getMessage()], 500);
        }
    }
}
