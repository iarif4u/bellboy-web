<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\DiscountsDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\DiscountDeleteRequest;
use App\Http\Requests\Admin\DiscountStoreRequest;
use App\Http\Requests\Admin\DiscountUpdateRequest;
use App\Models\Discount;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class DiscountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param DiscountsDataTable $dataTable
     * @return View
     */
    public function showDiscountsView(DiscountsDataTable $dataTable)
    {
        return $dataTable->render('auth.discount.discount_list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param DiscountStoreRequest $request
     * @return RedirectResponse
     */
    public function makeNewDiscount(DiscountStoreRequest $request)
    {
        try {
            Discount::create([
                "discount_name"=>$request->input('discount_name'),
                "discount_type"=>$request->input('discount_type'),
                "discount"=>$request->input('discount'),
                "valid_till"=>$request->input('valid_till'),
                "status"=>config('const.status.active'),
                "added_by"=>auth()->id()
            ]);
            return redirect()->back()->with('success', "New Discount create successfully done");
        } catch (\Exception $exception) {
            return redirect()->back()->withErrors([$exception->getMessage()])->with('exception', $exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Discount $discount
     * @return Response
     */
    public function show(Discount $discount)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Discount $discount
     * @return Response
     */
    public function edit(Discount $discount)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function updateDiscount(DiscountUpdateRequest $request)
    {
        try {
            Discount::where(['id'=>$request->input('discount_id')])->update([
                "discount_name"=>$request->input('discount_name'),
                "discount_type"=>$request->input('discount_type'),
                "discount"=>$request->input('discount'),
                "valid_till"=>$request->input('valid_till'),
                "added_by"=>auth()->id()
            ]);
            return redirect()->back()->with('success', "Discount update successfully done");
        } catch (\Exception $exception) {
            return redirect()->back()->withErrors([$exception->getMessage()])->with('exception', $exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DiscountDeleteRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteDiscount(DiscountDeleteRequest $request)
    {
        try {
            Discount::where(['id'=>$request->input('discount_id')])->first()->delete();
            return response()->json(['message' => "Discount Delete Successfully Done"]);
        } catch (\Exception $exception) {
            return response()->json(['message' => "Internal Server Error",'exception'=>$exception->getMessage()], 500);
        }
    }
    /**
     * Display the specified resource.
     *
     * @return JsonResponse
     */
    public function search()
    {
        try {
            $limit = 10;
            $string = request()->input('search', '');
            $response = Discount::where('discount_name', 'like', "%$string%")
                ->where(['status'=>config('const.status.active')])
                ->where('valid_till', '>', DB::raw('NOW()'))->limit($limit)->get();
            $discounts = [];
            foreach ($response as $discount){
                $discounts[] = [
                    'id'=>$discount->id,
                    'discount_name'=>$discount->discount_name." (".ucfirst($discount->discount_type).")",
                ];
            }
            return response()->json(['total' => $response->count(), 'data' => $discounts]);
        } catch (\Exception $exception) {
            return response()->json(['message' => $exception->getMessage()], 500);
        }
    }
}
