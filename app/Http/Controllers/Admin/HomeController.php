<?php

namespace App\Http\Controllers\Admin;

use App\Events\MessageEvent;
use App\Events\OfferEvent;
use App\Events\OrderEvent;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\UserDeleteRequest;
use App\Http\Requests\Admin\UserStoreRequest;
use App\Http\Requests\Admin\UserUpdateRequest;
use App\Models\AccountStatement;
use App\Models\Agent;
use App\Models\Callcenter;
use App\Models\Manager;
use App\Models\Order;
use App\Models\ProductsVariant;
use App\Notifications\AgentPriceNotification;
use App\Repositories\OrderRepository;
use App\Repositories\User\UserRepository;
use App\User;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\View\View;
use RealRashid\SweetAlert\Facades\Alert;

class HomeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('auth:web');
    }

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index()
    {
        $orders_counter = OrderRepository::order_counter();
        $orders = Order::with(['items','customer'])->orderByDesc('id')->limit(10)->get();
        $popular_variations = ProductsVariant::withCount('order_items')->orderByDesc('order_items_count')->limit(15)->get();
        $statements = AccountStatement::with(['expense'])->limit(5)->orderByDesc('id')->get();
        return view('auth.dashboard',['statements'=>$statements,'orders_counter'=>$orders_counter,'orders'=>$orders,'popular_variations'=>$popular_variations]);
    }

    /**
     * Show register user list from user repository
     *
     * @return Factory|View
     */
    public function getUserList()
    {
        $users = [
            config('const.users.admin') => User::whereNotIn('id', [auth()->id()])->get(),
            config('const.users.manager') => Manager::all(),
            config('const.users.callcenter') => Callcenter::all(),
        ];
        return view('auth.user.user_list', ['users' => $users]);
    }

    public function getAddUserView()
    {
        return view('auth.user.add_user');
    }

    //inset user firebase token
    public function setUpFirebaseToken(Request $request)
    {
        $token = $request->input('firebase_token', false);
        if ($token) {
            auth()->user()->firebase_tokens()->updateOrCreate(['token' => $token]);
            return response()->json(['message' => "Firebase token add success"]);
        } else {
            return response()->json(['message' => "Firebase token add fail"], 400);
        }
    }

    public function addNewUser(UserStoreRequest $request)
    {
        $user = [
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
            'phone' => $request->input('phone'),
            'added_by' => auth()->id()
        ];
        DB::beginTransaction();
        try {
            $model = UserRepository::create($user, $request->input('user_type'));
            if ($model) {
                add_media_file($model, $request->file('profile_picture'), config('const.media.profile_picture'));
            } else {
                return redirect()->back()->withErrors(["User type is unknown"]);
            }
            DB::commit();
            return redirect()->back()->with('success', ucfirst($request->input('user_type')) . " add successfully done");
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->withErrors(["Something went wrong"])->with('exception', $exception->getMessage())->withInput();
        }
    }

    public function getUserEditView($user_id, $user_type)
    {
        $user = UserRepository::show($user_id, $user_type);
        return view('auth.user.user_edit', ['user' => $user, 'user_type' => $user_type]);
    }

    public function updateUserInfo($user_id, $user_type, UserUpdateRequest $request)
    {
        $user = [
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'phone' => $request->input('phone'),
            'added_by' => auth()->id()
        ];
        DB::beginTransaction();
        try {
            $user_model = UserRepository::show($user_id, $user_type);
            if ($request->input('password')) {
                $user['password'] = bcrypt($request->input('password'));
            }
            if ($request->input('user_type') == $user_type) {
                $user_model->update($user);
                if ($request->hasFile('profile_picture')) {
                    sync_media_file($user_model, $request->file('profile_picture'), config('const.media.profile_picture'));
                }
                $new_user = $user_model->id;
                $new_type = $user_type;
            } else {
                if (!array_key_exists('password', $user)) {
                    $user['password'] = $user_model->password;
                }
                $profile_picture = get_media_file($user_model, config('const.media.profile_picture'));
                $model = UserRepository::create($user, $request->input('user_type'));

                if ($request->hasFile('profile_picture')) {
                    add_media_file($model, $request->file('profile_picture'), config('const.media.profile_picture'));
                } else {
                    add_url_media_file($model, $profile_picture, config('const.media.profile_picture'));
                }
                $user_model->delete();
                $new_user = $model->id;
                $new_type = $request->input('user_type');
            }
            DB::commit();
            return redirect()->route('users.edit', ['user_id' => $new_user, 'user_type' => $new_type])->with('success', "User update successfully done");
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->withErrors(['Something went wrong'])->with('exception', $exception->getMessage());
        }

    }

    public function deleteUser(UserDeleteRequest $request)
    {
        $user_id = $request->input('user_id');
        $user_type = $request->input('user_type');
        try {
            $user_model = UserRepository::show($user_id, $user_type);
            $user_model->delete();
            return response()->json(['message' => "User Delete Successfully Done"]);
        } catch (\Exception $exception) {
            return response()->json(['message' => "Internal Server Error", 'exception' => $exception->getMessage()], 500);
        }
    }
}
