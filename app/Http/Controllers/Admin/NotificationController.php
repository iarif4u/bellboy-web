<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\NotificationRequest;
use App\Models\Agent;
use App\Models\Customer;
use App\Models\Deliveryboy;
use App\Models\NotificationToken;
use App\Models\UserNotification;
use App\Notifications\UserFCMNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;
class NotificationController extends Controller
{
    public function getNotificationView()
    {
        return view('auth.notification.notification');
    }

    public function sendNotification(NotificationRequest $request)
    {
        $notification_to = $request->input('notification_to');
        $title = $request->input('title');
        $body = $request->input('body');
        DB::beginTransaction();
        try {
            if ($notification_to == 'all_customer') {
                $user_type = NotificationToken::class;
                $to = NotificationToken::where(['type' => Customer::class])->get();
            } elseif ($notification_to == 'registered_customer') {
                $to = Customer::all();
                $user_type = Customer::class;
            } elseif ($notification_to == 'agent') {
                $to = Agent::all();
                $user_type = Agent::class;
            } elseif ($notification_to == 'delivery_boy') {
                $to = Deliveryboy::all();
                $user_type = Deliveryboy::class;
            } else {
                return redirect()->back()->withErrors(["Invalid notification request"]);
            }
            Notification::send($to, new UserFCMNotification($title, $body));
            UserNotification::create([
                "target"=>$user_type,
                "title"=>$title,
                "body"=>$body,
            ]);
            DB::commit();
            return redirect()->back()->with('success', "Notification send successfully done");
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->withErrors([$exception->getMessage()])->withInput();
        }
    }
}
