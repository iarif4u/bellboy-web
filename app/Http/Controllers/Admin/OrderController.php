<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\Manager\ApprovedOrdersDataTable;
use App\DataTables\Manager\CanceledOrdersDataTable;
use App\DataTables\Manager\DeliveredOrdersDataTable;
use App\DataTables\Manager\PendingOrdersDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\CancelOrderRequest;
use App\Http\Requests\Manager\OrderApproveRequest;
use App\Http\Requests\Manager\OrderInfoRequest;
use App\Http\Requests\Manager\OrderItemAgentAssignRequest;
use App\Http\Requests\UpdateOrderRequest;
use App\Models\Agent;
use App\Models\AgentOffer;
use App\Models\Customer;
use App\Models\Deliveryboy;
use App\Models\Order;
use App\Models\OrderDeliveryBoy;
use App\Models\OrderItem;
use App\Models\ProductsVariant;
use App\Notifications\Agent\NewOrderNotification;
use App\Notifications\Agent\PriceApproveNotification;
use App\Notifications\Customer\OrderApproveNotification;
use App\Notifications\Customer\OrderCancelledNotification;
use App\Notifications\DeliveryBoy\DeliveryBoyOrderNotification;
use App\Repositories\OrderRepository;
use App\Repositories\ProductRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;

class OrderController extends Controller
{
    public function getPendingOrders(PendingOrdersDataTable $dataTable)
    {
        return $dataTable->render('auth.order.pending_orders');
    }

    public function getApprovedOrders(ApprovedOrdersDataTable $dataTable)
    {
        return $dataTable->render('auth.order.approve_orders');
    }

    public function getDeliveredOrders(DeliveredOrdersDataTable $dataTable)
    {
        return $dataTable->render('auth.order.delivered_orders');
    }

    public function getCanceledOrders(CanceledOrdersDataTable $dataTable)
    {

        return $dataTable->render('auth.order.delivered_orders');
    }

    public function deliveryboy_search()
    {
        try {
            $string = request()->input('search', '');
            $response = Deliveryboy::where('name', 'like', "%$string%")
                ->orWhere("employee_id", 'like', "%$string%")
                ->orWhere("email", 'like', "%$string%")
                ->orWhere("phone", 'like', "%$string%")
                ->limit(10)->get();
            return response()->json(['total' => $response->count(), 'data' => $response]);
        } catch (\Exception $exception) {
            return response()->json(['message' => $exception->getMessage()], 500);
        }
    }

    public function agent_search()
    {
        try {
            $string = request()->input('search', '');
            $response = Agent::where('name', 'like', "%$string%")
                ->orWhere("email", 'like', "%$string%")
                ->limit(10)->get();
            return response()->json(['total' => $response->count(), 'data' => $response]);
        } catch (\Exception $exception) {
            return response()->json(['message' => $exception->getMessage()], 500);
        }
    }

    public function getOrderInfo(OrderInfoRequest $request)
    {
        return Order::with(['items', 'deliveryBoy', 'customer'])->where(['id' => $request->input('order_id')])->first();
    }


    public function addAgentToOrderItem(OrderItemAgentAssignRequest $request)
    {
        $variation_id = $request->input('variation_id');
        $item_cost = bn2enNumber($request->input('item_cost'));
        $agent_id = $request->input('agent');
        $item_stock = bn2enNumber($request->input('item_stock'));
        DB::beginTransaction();
        try {
            $agent_offer = ProductRepository::create_agent_new_offer($variation_id, $item_cost, $agent_id, $item_stock);
            $agent = Agent::where(['id' => $agent_id])->first();
            $offer = $agent_offer['offer'];
            $product_img = $agent_offer['product_img'];
            OrderItem::where(['id' => $request->input('order_item')])->update(['agent_id' => $agent_id]);
            DB::commit();
            Notification::send($agent, new PriceApproveNotification($offer, $product_img));
            return redirect()->back()->with('success', "Agent assign successfully done");
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->withErrors("Internal Server Error")->with("exception", $exception->getMessage());
        }
    }

    public function cancelTheOrder(CancelOrderRequest $request)
    {
        $order_id = $request->input('order_id');
        $comment = $request->input('comment');
        try {
            $order = Order::with('customer')->where(['id' => $order_id])->first();
            $order->update([
                'comment' => $comment,
                'order_modified' => Carbon::now(),
                'order_status' => config('const.order_status.deny')
            ]);
            OrderItem::where(['order_id' => $order_id])->update(['item_status' => config('const.order_item_status.deny')]);
            Notification::send($order->customer, new OrderCancelledNotification($order));
            return response()->json(['message' => 'Order cancelled successfully done']);
        } catch (\Exception $exception) {
            return response()->json(['message' => $exception->getMessage()], 500);
        }
    }

    public function updateOrder(UpdateOrderRequest $request)
    {

        $item_quantity = $request->input('productQuantity');
        $new_item_id = $request->input('new_item_id');
        $item_new_quantity = $request->input('productNewQuantity');
        $order_items = $request->input('order_item_id');
        $customer_address = $request->input('customer_address');
        $customer_name = $request->input('customer_name');
        $order_id = $request->input('order_id');
        $order = Order::with(['items', 'couponCode', 'customer'])->withCount('items')->where(['id' => $order_id])->first();
        if ($order->items_count == count($order_items)) {
            DB::beginTransaction();
            try {
                $total_price = 0;
                $total_value = 0;
                $items = [];
                if(is_array($new_item_id)):
                foreach ($new_item_id as $index => $variation_id) {
                    $productQty = $item_new_quantity[$index];
                    $variation = ProductsVariant::where(['id' => $variation_id])->first();
                    DB::rollBack();
                    $agent_offer = AgentOffer::with('agent')
                        ->where(['variation_id' => $variation->id, 'offer_status' => config('const.agent_status.active')])
                        ->where('offer_stock', '>=', $productQty)
                        ->orderBy('negotiate_price')
                        ->first();
                    $agent_id = null;
                    if ($agent_offer) {
                        if ($agent_offer->agent) {
                            $agent_id = $agent_offer->agent->id;
                        }
                    }
                    $variation_agent_cost = ($agent_offer) ? $agent_offer->negotiate_price : 0;
                    $discount_price = ProductRepository::product_price_after_discount($variation);
                    $value = ($agent_offer) ? ($discount_price - $variation_agent_cost) * $productQty : null;
                    $sub_total = $discount_price * $productQty;
                    $total_price = $total_price + $sub_total;
                    $total_value = ($agent_offer) ? $total_value + $value : $total_value;
                    $items[] = [
                        "variation_id" => $variation->id,
                        "quantity" => $productQty,
                        "cost" => $variation_agent_cost,
                        "price" => $variation->price,
                        "agent_id" => $agent_id,
                        "delivered_quantity" => 0,
                        "discount_id" => ProductRepository::product_discount_info($variation),
                        "discount_amount" => $discount_price,
                        "sub_total" => $sub_total,
                        "item_status" => config('const.order_item_status.pending'),
                        "item_value" => $value
                    ];
                }
                endif;
                $order->items()->createMany($items);
                foreach ($order_items as $index => $item) {
                    $quantity = $item_quantity[$index];
                    $order_item = OrderItem::where(['id' => $item, 'order_id' => $order_id])->first();
                    if ($order_item) {
                        if ($quantity > 0) {
                            $cost = $order_item->cost;
                            $value = ($order_item->discount_amount - $cost) * $quantity;
                            $sub_total = $order_item->discount_amount * $quantity;
                            $total_price = $total_price + $sub_total;
                            $total_value = $total_value + $value;
                            $order_item->sub_total = $sub_total;
                            $order_item->quantity = $quantity;
                            $order_item->item_value = $value;
                            $order_item->save();
                        } else {
                            $order_item->delete();
                        }
                    } else {
                        DB::rollBack();
                        break;
                    }
                }
                Customer::where(['id' => $order->customer->id])->update([
                    'name' => $customer_name,
                    'address' => $customer_address,
                ]);
                $gTotal=$total_price+$order->delivery_charge;
                if ($order->coupon_discount != null) {
                    $coupon_discount = coupon_code_calculate($total_price, $order->couponCode);
                    $order->coupon_discount = $coupon_discount;
                    $order->total = $gTotal - $coupon_discount;
                    $order->order_value = $total_value - $coupon_discount;
                } else {
                    $order->total = $gTotal;
                    $order->order_value = $total_value;
                }
                $order->grand_total = $gTotal;
                $order->save();
                DB::commit();
                return redirect()->back()->with('success', "Order update successfully done");
            } catch (\Exception $exception) {
                dd($exception->getMessage());
                DB::rollBack();
                return redirect()->back()->withErrors(["Something went wrong"])->with('exception', $exception->getMessage());

            }
        } else {
            return redirect()->back()->withErrors(["Order item doesn't match"]);
        }
    }

    public function addDeliveryBoyToOrder(OrderApproveRequest $request)
    {
        $order_items = $request->input('order_item_id');

        $agents = $request->input('agent_id');
        $order_id = $request->input('order_id');
        $customer_name = $request->input('customer_name');
        $customer_address = $request->input('customer_address');
        $delivery_boy = $request->input('delivery_boy');
        $itemList = OrderItem::where(['order_id' => $order_id])->count();
        if (count($order_items) == $itemList) {
            DB::beginTransaction();
            try {
                $order = Order::with(['customer', 'deliveryBoy'])->where(['id' => $order_id])->first();
                OrderRepository::update_order_items($agents, $order_items, $order);
                $order->delivery_boy = $delivery_boy;
                $order->order_status = config('const.order_status.approve');
                $order->order_modified = Carbon::now();
                $order->delivery_address = $customer_address;
                $order->save();
                OrderDeliveryBoy::create([
                    "order_id" => $order_id,
                    "delivery_boy_id" => $delivery_boy,
                    "status" => config('const.delivery_boy_order_status.assign'),
                    "status_time" => Carbon::now(),
                    "assigner_id" => auth()->id()
                ]);
                $order_item_list = OrderItem::where(['order_id' => $order_id])->get();
                $agent_list = [];
                foreach ($order_item_list as $order_item) {
                    if (!in_array($order_item->agent->id, $agent_list)) {
                        $agent_list[] = $order_item->agent->id;
                        Notification::send($order_item->agent, new NewOrderNotification($order_item));
                    }
                }
                $assiged_boy = Deliveryboy::where(['id'=>$delivery_boy])->first();
                Notification::send($assiged_boy, new DeliveryBoyOrderNotification($order));
                Notification::send($order->customer, new OrderApproveNotification($order));
                DB::commit();
                return redirect()->back()->with('success', "Order assign to delivery boy success");
            } catch (\Exception $exception) {
                DB::rollBack();
                return redirect()->back()->withErrors("Internal Server Error")->with("exception", $exception->getMessage());
            }
        } else {
            return redirect()->back()->withErrors("Internal Server Error");
        }
    }
}
