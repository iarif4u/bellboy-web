<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\DeletedProductsDataTable;
use App\DataTables\VariationListDataTable;
use App\Exports\ProductsVariantExport;
use App\Http\Requests\Admin\ProductAgentAssignRequest;
use App\Models\Agent;
use App\Models\ProductUnit;
use App\Notifications\Agent\OfferDenyNotification;
use App\Notifications\Agent\PriceApproveNotification;
use App\Repositories\ProductRepository;
use Illuminate\Support\Facades\Notification;
use App\DataTables\AgentsOfferDataTable;
use App\DataTables\ProductsDataTable;
use App\DataTables\ProductVariationsDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ProductDeleteRequest;
use App\Http\Requests\Admin\ProductDiscountRequest;
use App\Http\Requests\Admin\ProductOfferApproveRequest;
use App\Http\Requests\Admin\ProductStatusRequest;
use App\Http\Requests\Admin\ProductStoreRequest;
use App\Http\Requests\Admin\ProductUpdateRequest;
use App\Http\Requests\Admin\ProductVariationOptionRequest;
use App\Http\Requests\Admin\ProductVariationStoreRequest;
use App\Http\Requests\Admin\ProductVariationUpdateRequest;
use App\Models\AgentOffer;
use App\Models\Product;
use App\Models\ProductDiscount;
use App\Models\ProductPropertiesValue;
use App\Models\ProductsVariant;
use App\Models\VariationPrice;
use App\Models\VariationCost;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Maatwebsite\Excel\Facades\Excel;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param ProductsDataTable $dataTable
     * @return View
     */
    public function index(ProductsDataTable $dataTable)
    {
        $approve_offers = AgentOffer::with(['agent', 'variation'])->where('offer_status', '=', config('const.agent_status.active'))
            ->whereHas('agent')->count();
        $deleted_products = Product::onlyTrashed()->count();
        return $dataTable->render('auth.product.product_list', ['deleted_products' => $deleted_products, 'approve_offers' => $approve_offers]);
    }

    /**
     * Display a listing of the resource which has been deleted.
     *
     * @param DeletedProductsDataTable $dataTable
     * @return View
     */
    public function deleted(DeletedProductsDataTable $dataTable)
    {
        return $dataTable->render('auth.product.deleted_product_list');
    }


    /**
     * Display a listing of the resource.
     *
     * @param VariationListDataTable $dataTable
     * @return View
     */
    public function getProductVariations(VariationListDataTable $dataTable)
    {
        return $dataTable->render('auth.product.variation_list');

    }

    public function downloadVariations()
    {
        return Excel::download(new ProductsVariantExport(), 'products_' . date('d-m-Y_h_i_s_A') . '.xlsx');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create()
    {
        return view('auth.product.new_product');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProductStoreRequest $request
     * @return RedirectResponse
     */
    public function store(ProductStoreRequest $request)
    {
        DB::beginTransaction();
        try {
            $categories = $request->input('categories');
            $product = Product::create([
                "product_name" => $request->input('product_name'),
                "product_code" => $request->input('product_code'),
                "product_brand" => $request->input('brand', null),
                "description" => $request->input('product_description'),
                "product_discount" => $request->input('discount', null),
                "laundry" => $request->input('laundry', false),
                "added_by" => auth()->id(),
                "in_stock" => true
            ]);
            if (is_array($categories)) {
                $product->categories()->sync($categories);
            }
            //discount
            if ($request->input('discount')) {
                ProductDiscount::create([
                    "product_id" => $product->id,
                    "discount_id" => $request->input('discount')
                ]);
            }
            $properties = $request->input('properties_name');
            $properties_value = $request->input('properties_value');
            if (is_array($properties)):
                for ($i = 0; $i < sizeof($properties); $i++):
                    if (!is_null($properties_value[$i]) && strlen($properties_value[$i]) > 0) {
                        ProductPropertiesValue::create(["property_id" => $properties[$i], "product_id" => $product->id, "value" => $properties_value[$i]]);
                    }
                endfor;
            endif;
            DB::commit();
            return redirect()->back()->with('success', "New product create successfully done");
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->withErrors(["Internal Server Error"])->with('exception', $exception->getMessage())->withInput();
        }
    }

    /**
     * Add product discount and update if already exists
     *
     * @param ProductDiscountRequest $request
     * @return RedirectResponse
     */
    public function updateProductDiscount(ProductDiscountRequest $request)
    {
        try {
            $discount = $request->input('discount', null);
            $product = Product::with(['discount'])->where(['id' => $request->input('product_id')])->firstOrFail();
            $product->product_discount = $discount;
            if ($product->isDirty()) {
                ProductDiscount::where(["product_id" => $product->id])->update(['end' => Carbon::now()->subSecond()]);
                if ($discount !== null) {
                    ProductDiscount::create(["product_id" => $product->id, "discount_id" => $discount]);
                }
                $product->save();
            }
            DB::commit();
            return redirect()->back()->with('success', "Product discount update successfully done");
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->withErrors(["Internal Server Error"])->with('exception', $exception->getMessage())->withInput();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $product_id
     * @return View
     */
    public function updateProductView(int $product_id)
    {
        $product = Product::with(['brand', 'discount', 'categories', 'properties'])->where(['id' => $product_id])->firstOrFail();
        return view('auth.product.update_product', ['product' => $product]);
    }

    /**
     * Update the product status.
     *
     * @param ProductStatusRequest $request
     * @return JsonResponse
     */
    public function updateProductStatus(ProductStatusRequest $request)
    {
        try {
            Product::where(['id' => $request->input('product_id')])->update(['status' => $request->input('product_status')]);
            return response()->json(['message' => ucfirst($request->input('product_status') . " The Product")]);
        } catch (\Exception $exception) {
            return response()->json(['message' => "Internal Server Error"], 500);
        }
    }

    /**
     * Update product
     *
     * @param ProductUpdateRequest $request
     * @return RedirectResponse
     */
    public function updateProduct(ProductUpdateRequest $request)
    {
        DB::beginTransaction();
        try {
            $categories = $request->input('categories');
            $product_id = $request->input('product_id');
            $discount = $request->input('discount', null);
            $properties = $request->input('properties_name');
            $properties_value = $request->input('properties_value');

            $product = Product::with(['brand', 'discount', 'categories', 'properties'])->where(['id' => $product_id])->firstOrFail();
            if ($product->discount !== $discount) {
                ProductDiscount::where(["product_id" => $product->id])->update(['end' => Carbon::now()->subSecond()]);
                if ($discount !== null) {
                    ProductDiscount::create([
                        "product_id" => $product->id,
                        "discount_id" => $discount
                    ]);
                }
            }
            Product::where(['id' => $product->id])->update([
                "product_name" => $request->input('product_name'),
                "product_code" => $request->input('product_code'),
                "product_brand" => $request->input('brand', null),
                "description" => $request->input('product_description'),
                "product_discount" => $request->input('discount', null),
                "laundry" => $request->input('laundry', false),
                "added_by" => auth()->id(),
                "in_stock" => true
            ]);
            if (is_array($categories)) {
                $product->categories()->sync($categories);
            }
            if (is_array($properties)):
                for ($i = 0; $i < sizeof($properties); $i++):
                    if (!is_null($properties_value[$i]) && strlen($properties_value[$i]) > 0) {
                        ProductPropertiesValue::updateOrCreate(["property_id" => $properties[$i], "product_id" => $product->id], ["value" => $properties_value[$i]]);
                    }
                endfor;
            endif;
            DB::commit();
            return redirect()->back()->with('success', "Product update successfully done");
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->withErrors(["Internal Server Error"])->with('exception', $exception->getMessage())->withInput();
        }
    }

    /**
     * Delete specific product
     *
     * @param ProductDeleteRequest $request
     * @return JsonResponse
     */
    public function deleteProduct(ProductDeleteRequest $request)
    {
        try {
            Product::where(['id' => $request->input('product_id')])->first()->delete();
            return response()->json(['message' => "Product Delete Successfully Done"]);
        } catch (\Exception $exception) {
            return response()->json(['message' => "Internal Server Error", 'exception' => $exception->getMessage()], 500);
        }
    }

    /**
     * Restore specific product
     *
     * @param ProductDeleteRequest $request
     * @return JsonResponse
     */
    public function restore(ProductDeleteRequest $request)
    {
        try {
            Product::withTrashed()->find($request->input('product_id'))->restore();
            return response()->json(['message' => "Product Restore Successfully Done"]);
        } catch (\Exception $exception) {
            return response()->json(['message' => "Internal Server Error", 'exception' => $exception->getMessage()], 500);
        }
    }

    /**
     * Show specific product variations
     *
     * @param $product_id
     * @param ProductVariationsDataTable $dataTable
     * @return View
     */
    public function showProductVariation($product_id, ProductVariationsDataTable $dataTable)
    {
        $product = Product::with(['brand', 'discount', 'categories', 'properties'])->where(['id' => $product_id])->firstOrFail();
        return $dataTable->render('auth.product.variation.variation_list', ['product' => $product]);
    }

    /**
     * Show product variation add form
     *
     * @param $product_id
     * @return View
     */
    public function showNewProductVariationView($product_id)
    {
        $product = Product::with(['brand', 'discount', 'categories', 'properties'])->where(['id' => $product_id])->firstOrFail();
        $product_units = ProductUnit::all();
        return view('auth.product.variation.new_variation', ['product' => $product, 'product_units' => $product_units]);
    }

    /**
     * Update product variation add form
     *
     * @param $variant_id
     * @return View
     */
    public function showEditProductVariationView($variant_id)
    {
        $product_variant = ProductsVariant::with(['options', 'unit'])->where(['id' => $variant_id])->firstOrFail();
        $product = Product::with(['brand', 'discount', 'categories', 'properties'])->where(['id' => $product_variant->product_id])->firstOrFail();
        $product_units = ProductUnit::all();

        return view('auth.product.variation.edit_variation', ['product_units' => $product_units, 'product' => $product, 'product_variant' => $product_variant]);
    }

    /**
     * update variation to specific product
     *
     * @param ProductVariationOptionRequest $request
     * @return JsonResponse
     */
    public function deleteProductVariation(ProductVariationOptionRequest $request)
    {
        try {
            ProductsVariant::where(['id' => $request->input('variation_id')])->first()->delete();
            return response()->json(['message' => "Product Variant Delete Successfully Done"]);
        } catch (\Exception $exception) {
            return response()->json(['message' => "Internal Server Error", 'exception' => $exception->getMessage()], 500);
        }
    }

    /**
     * update variation to specific product
     *
     * @param ProductVariationUpdateRequest $request
     * @return RedirectResponse
     */
    public function updateProductVariation(ProductVariationUpdateRequest $request)
    {
        DB::beginTransaction();
        try {
            $product_variant = ProductsVariant::where(['id' => $request->input('variant_id')])->first();
            if ($product_variant->cost != $request->input('product_cost')) {
                VariationCost::where(["variant_id" => $product_variant->id, 'status' => true])->update(['to' => Carbon::now()->subSecond(), 'status' => false]);
                if ($request->input('product_cost', 0) > 0)
                    VariationCost::create(["variant_id" => $product_variant->id, "cost" => $request->input('product_cost')]);
            }
            if ($product_variant->price != $request->input('product_price')) {
                VariationPrice::where(["variant_id" => $product_variant->id, 'status' => true])->update(['to' => Carbon::now()->subSecond(), 'status' => false]);
                if ($request->input('product_price', 0) > 0)
                    VariationPrice::create(["variant_id" => $product_variant->id, "price" => $request->input('product_price')]);
            }
            $product_variant->update([
                "variant_name" => $request->input('variation_name'),
                "sku" => $request->input('sku'),
                "cost" => $request->input('product_cost'),
                "price" => $request->input('product_price'),
                "unit_id" => $request->input('variation_unit'),
                "unit_value" => $request->input('variation_unit_value'),
            ]);

            if (is_array($request->input('option_value'))) {
                $product_variant->options()->sync($request->input('option_value'));
            }
            if ($request->hasFile('product_image')) {
                sync_media_file($product_variant, $request->file('product_image'), config('const.product.media.product_image'));
            }
            DB::commit();
            return redirect()->back()->with('success', "Product variation update successfully done");
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->withErrors(["Internal Server Error"])->with('exception', $exception->getMessage())->withInput();
        }
    }

    /**
     * Add variation to specific product
     *
     * @param ProductVariationStoreRequest $request
     * @return RedirectResponse
     */
    public function addProductVariation(ProductVariationStoreRequest $request)
    {
        if (unicode_no_validation($request->input('product_cost')) && unicode_no_validation($request->input('product_cost'))) {
            DB::beginTransaction();
            try {
                $product_variant = ProductsVariant::create([
                    "product_id" => $request->input('product_id'),
                    "variant_name" => $request->input('variation_name'),
                    "sku" => $request->input('sku'),
                    "cost" => bn2enNumber($request->input('product_cost')),
                    "price" => bn2enNumber($request->input('product_price')),
                    "status" => config('const.product.status.active'),
                    "unit_id" => $request->input('variation_unit'),
                    "unit_value" => $request->input('variation_unit_value'),
                ]);
                $product_variant->options()->sync($request->input('option_value'));
                VariationCost::create(["variant_id" => $product_variant->id, "cost" => $request->input('product_cost')]);
                VariationPrice::create(["variant_id" => $product_variant->id, "price" => $request->input('product_price')]);
                add_media_file($product_variant, $request->file('product_image'), config('const.product.media.product_image'));
                DB::commit();
                return redirect()->back()->with('success', "Product variation add successfully done");
            } catch (\Exception $exception) {
                DB::rollBack();
                return redirect()->back()->withErrors(["Internal Server Error"])->with('exception', $exception->getMessage())->withInput();
            }
        } else {
            return redirect()->back()->withErrors(["Something went wrong"])->withInput();
        }
    }

    public function getVariationValues(ProductVariationOptionRequest $request)
    {
        $product_variation = ProductsVariant::with('options')->where(['id' => $request->input('variation_id')])->firstOrFail();
        $options = [];
        $values = [];
        foreach ($product_variation->options as $variation_options) {
            $options[$variation_options->option->name][] = $variation_options->value;
        }
        foreach ($options as $option => $value) {
            $values[] = ['option' => $option, 'value' => $value];
        }
        return $values;
    }

    public function getVariationAgentOffer($variation_id, AgentsOfferDataTable $dataTable)
    {
        return $dataTable->render('auth.product.variation.variation_offers');
    }

    public function approve_agent_offer(ProductOfferApproveRequest $request)
    {
        DB::beginTransaction();
        try {

            $offer = AgentOffer::where(['id' => $request->input('offer_id')])->first();
            VariationCost::updateOrCreate(["variant_id" => $offer->variation_id, "order_ref_id" => $offer->id, "cost" => $request->input('cost')]);
            AgentOffer::where('id', '!=', $offer->id)->where(['variation_id' => $offer->variation_id, 'offer_status' => config('const.agent_status.active')])->update(['offer_status' => config('const.agent_status.inactive')]);
            $product_variant = ProductsVariant::where(['id' => $offer->variation_id])->first();
            $product_img = get_media_file($product_variant, config('const.product.media.product_image'));
            $product_variant->update(['price' => $request->input('price'), 'cost' => $request->input('cost'), 'on_hand' => $request->input('stock')]);
            $offer->negotiate_price = $request->input('cost');
            $offer->offer_status = config('const.agent_status.active');
            $offer->offer_stock = $request->input('stock');
            $offer->save();
            DB::commit();
            Notification::send($offer->agent, new PriceApproveNotification($offer, $product_img));
            return redirect()->back()->with('success', "Agent offer approve success");
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->withErrors(["Internal Server Error"])->with('exception', $exception->getMessage())->withInput();
        }
    }

    public function deny_agent_offer(Request $request)
    {
        DB::beginTransaction();
        try {
            $offer = AgentOffer::where(['id' => $request->input('offer_id')])->first();
            $offer->offer_status = config('const.agent_status.deny');
            $offer->save();
            DB::commit();
            Notification::send($offer->agent, new OfferDenyNotification($offer));
            return response()->json(['message' => "Agent offer deny success"]);
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json(['message' => "Internal Server Error"], 500);
        }
    }

    public function assignProductAgent(ProductAgentAssignRequest $request)
    {
        DB::beginTransaction();
        try {
            $variation_id = $request->input('variation_id');
            $agent_id = $request->input('assign_agent');
            $cost = bn2enNumber($request->input('cost'));
            $stock = bn2enNumber($request->input('stock'));
            $agent_offer = ProductRepository::create_agent_new_offer($variation_id, $cost, $agent_id, $stock);
            $offer = $agent_offer['offer'];
            $product_img = $agent_offer['product_img'];
            $agent = Agent::where(['id' => $agent_id])->first();
            DB::commit();
            Notification::send($agent, new PriceApproveNotification($offer, $product_img));
            return redirect()->back()->with('success', "Agent assign to variation success");
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->withErrors(["Internal Server Error"])->with('exception', $exception->getMessage())->withInput();
        }
    }

    public function search(Request $request)
    {

        try {
            $limit = 20;
            $searchString = $request->input('search', '');
            $variations = $request->input('variations');
            $productList = Product::with(['variations'])
                ->where('product_name', 'like', '%' . $searchString . '%')
                ->orWhere('description', 'like', '%' . $searchString . '%')
                ->orWhere('product_code', 'like', '%' . $searchString . '%')
                ->orWhereHas('variations', function ($query) use ($searchString) {
                    $query->where('variant_name', 'like', '%' . $searchString . '%');
                })->limit($limit)->get();
            $response = ProductRepository::products_notIn_response($productList,$variations);
            return response()->json(['total' => $productList->count(), 'data' => $response]);
        } catch (\Exception $exception) {
            return response()->json(['message' => $exception->getMessage()], 500);
        }
    }
    public function variation_info(Request $request)
    {
        try {
            $variation_id = $request->input('variation_id');
            $variation = ProductsVariant::where(['id'=>$variation_id])->first();
            return ProductRepository::get_product_response($variation);
        } catch (\Exception $exception) {
            return response()->json(['message' => $exception->getMessage()], 500);
        }
    }
}

