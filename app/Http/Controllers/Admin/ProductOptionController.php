<?php

namespace App\Http\Controllers\Admin;


use App\DataTables\ProductOptionValuesDataTable;
use App\DataTables\VariationListDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ProductOptionDeleteRequest;
use App\Http\Requests\Admin\ProductOptionStoreRequest;
use App\Http\Requests\Admin\ProductOptionUpdateRequest;
use App\Http\Requests\Admin\ProductOptionValueDeleteRequest;
use App\Http\Requests\Admin\ProductOptionValueStoreRequest;
use App\Http\Requests\Admin\ProductOptionValueUpdateRequest;
use App\Models\ProductOption;
use App\Models\ProductOptionValue;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Yajra\DataTables\Facades\DataTables;

class ProductOptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param ProductOptionValuesDataTable $dataTable
     * @return View
     */
    public function showProductOptionsView(ProductOptionValuesDataTable $dataTable)
    {
        return $dataTable->render('auth.product.product_options');

    }


    /**
     * Show the datatable of product options list
     *
     * @return RedirectResponse|Response
     * @throws \Exception
     */
    public function getProductOptionsDataTable()
    {
        if (\request()->ajax()) {
            $product_options = ProductOption::select(['id', 'name']);
            return Datatables::of($product_options)
                ->addColumn('action', function ($product_option) {
                    return view("auth.product.datatables.product_options_actions", ['product_option' => $product_option]);
                })->make(true);
        } else {
            return redirect()->route('product.option.all');
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProductOptionStoreRequest $request
     * @return RedirectResponse
     */
    public function makeNewProductOption(ProductOptionStoreRequest $request)
    {
        try {
            ProductOption::create(["name" => $request->input('option_name')]);
            return redirect()->back()->with('success', "New product option create successfully done");
        } catch (\Exception $exception) {
            return redirect()->back()->withErrors(["Internal Server Error"])->with('exception', $exception->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ProductOptionUpdateRequest $request
     * @return RedirectResponse
     */
    public function updateProductOption(ProductOptionUpdateRequest $request)
    {
        try {
            ProductOption::where(['id' => $request->input('option_id')])->update(["name" => $request->input('option_name')]);
            return redirect()->back()->with('success', "Product option update successfully done");
        } catch (\Exception $exception) {
            return redirect()->back()->withErrors(["Internal Server Error"])->with('exception', $exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ProductOptionDeleteRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteProductOption(ProductOptionDeleteRequest $request)
    {
        try {
            ProductOption::where(['id' => $request->input('option_id')])->first()->delete();
            return response()->json(['message' => "Product option delete successfully done"]);
        } catch (\Exception $exception) {
            return response()->json(['message' => "Internal Server Error"], 500);
        }
    }

    public function searchProductOption(Request $request)
    {
        try {
            $limit = 10;
            $string = $request->input('search', '');
            $response = ProductOption::where('name', 'like', "%$string%")->limit($limit)->get();
            return response()->json(['total' => $response->count(), 'data' => $response]);
        } catch (\Exception $exception) {
            return response()->json(['message' => $exception->getMessage()], 500);
        }
    }

    public function searchProductOptionValue(Request $request)
    {
        try {
            $limit = 10;
            $string = $request->input('search', '');
            $response = ProductOptionValue::where(["option_id" => $request->input('option_id')])->where('value', 'like', "%$string%")->limit($limit)->get();
            return response()->json(['total' => $response->count(), 'data' => $response]);
        } catch (\Exception $exception) {
            return response()->json(['message' => $exception->getMessage()], 500);
        }
    }

    public function makeNewProductOptionValue(ProductOptionValueStoreRequest $request)
    {
        try {
            ProductOptionValue::create([
                "option_id" => $request->input('product_option'),
                "value" => $request->input('option_value')
            ]);
            return redirect()->back()->with('success', "New product option value create successfully done");
        } catch (\Exception $exception) {
            return redirect()->back()->withErrors(["Internal Server Error"])->with('exception', $exception->getMessage());
        }

    }

    public function updateProductOptionValue(ProductOptionValueUpdateRequest $request)
    {
        try {
            ProductOptionValue::where(['id' => $request->input('option_value_id')])->update(["option_id" => $request->input('product_option'), "value" => $request->input('option_value')]);
            return redirect()->back()->with('success', "Product option value update successfully done");
        } catch (\Exception $exception) {
            return redirect()->back()->withErrors(["Internal Server Error"])->with('exception', $exception->getMessage());
        }
    }

    public function deleteProductOptionValue(ProductOptionValueDeleteRequest $request)
    {
        try {
            ProductOptionValue::where(['id' => $request->input('option_value_id')])->first()->delete();
            return response()->json(['message' => "Product option value delete successfully done"]);
        } catch (\Exception $exception) {
            return response()->json(['message' => "Internal Server Error"], 500);
        }
    }
}
