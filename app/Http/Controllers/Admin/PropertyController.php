<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\ProductUnitsDataTable;
use App\DataTables\PropertiesDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ProductUnitStoreRequest;
use App\Http\Requests\Admin\ProductUnitUpdateRequest;
use App\Http\Requests\Admin\PropertyDeleteRequest;
use App\Http\Requests\Admin\PropertyStoreRequest;
use App\Http\Requests\Admin\PropertyUpdateRequest;
use App\Models\ProductUnit;
use App\Models\Property;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;

class PropertyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param PropertiesDataTable $dataTable
     * @return View
     */
    public function showProductPropertiesView(PropertiesDataTable $dataTable)
    {
        return $dataTable->render('auth.product.property_list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param PropertyStoreRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function makeNewProperty(PropertyStoreRequest $request)
    {
        try {
            Property::create(["name" => $request->input('property_name')]);
            return redirect()->back()->with('success', "New property create successfully done");
        } catch (\Exception $exception) {
            return redirect()->back()->withErrors(["Internal Server Error"])->with('exception', $exception->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateProperty(PropertyUpdateRequest $request)
    {
        try {
            Property::where(['id' => $request->input('property_id')])->update(["name" => $request->input('property_name')]);
            return redirect()->back()->with('success', "Property update successfully done");
        } catch (\Exception $exception) {
            return redirect()->back()->withErrors(["Internal Server Error"])->with('exception', $exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param PropertyDeleteRequest $request
     * @return JsonResponse
     */
    public function deleteProperty(PropertyDeleteRequest $request)
    {
        try {
            Property::where(['id' => $request->input('property_id')])->first()->delete();
            return response()->json(['message' => "Property delete successfully done"]);
        } catch (\Exception $exception) {
            return response()->json(['message' => "Internal Server Error"], 500);
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Property $property
     * @return JsonResponse
     */
    public function search()
    {
        try {
            $limit = 10;
            $string = request()->input('search', '');
            $response = Property::where('name', 'like', "%$string%")->limit($limit)->get();
            return response()->json(['total' => $response->count(), 'data' => $response]);
        } catch (\Exception $exception) {
            return response()->json(['message' => $exception->getMessage()], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ProductUnitsDataTable $dataTable
     * @return Response
     */
    public function showProductUnitsView(ProductUnitsDataTable $dataTable)
    {
        return $dataTable->render('auth.product.unit_list');
    }

    public function addProductUnit(ProductUnitStoreRequest $request)
    {
        ProductUnit::create(["unit_name" => $request->input('unit_name'), "added_by" => auth()->id()]);
        return redirect()->back()->with('success', "Unit add successfully done");
    }

    public function updateProductUnit(ProductUnitUpdateRequest $request)
    {
        ProductUnit::where(['id' => $request->input('unit_id')])->update(["unit_name" => $request->input('unit_name'), "added_by" => auth()->id()]);
        return redirect()->back()->with('success', "Unit update successfully done");
    }

    public function deleteProductUnit(Request $request){
        $request->validate([
            'unit_id' => 'required|exists:product_units,id',
        ],[
            'unit_id.required' => 'Unit id is required',
            'unit_id.exists' => 'Unit id is invalid',
        ]);
        try {
            ProductUnit::where(['id' => $request->input('unit_id')])->first()->delete();
            return response()->json(['message' => "Product unit delete successfully done"]);
        } catch (\Exception $exception) {
            return response()->json(['message' => "Internal Server Error"], 500);
        }
    }

}
