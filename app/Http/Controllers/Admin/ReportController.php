<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\AgentsStockDataTable;
use App\DataTables\OrdersDataTable;
use App\DataTables\TopSellingProductsDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ProductOfferApproveRequest;
use App\Models\AccountExpense;
use App\Models\AgentOffer;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\ProductsVariant;
use App\Models\VariationCost;
use App\Notifications\Agent\PriceApproveNotification;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;

class ReportController extends Controller
{
    public function getDailyReport()
    {
        $orders = Order::with(['customer'])->whereDate('created_at', Carbon::today())->orderByDesc('id')->get();
        return view('auth.reports.daily_report', ['orders' => $orders]);
    }

    public function getMonthlyReport(OrdersDataTable $dataTable)
    {
        return $dataTable->render('auth.reports.monthly_report');
    }

    public function getTopSellingReport(TopSellingProductsDataTable $dataTable)
    {

        return $dataTable->render('auth.reports.top_selling_products');
    }

    public function getStockReport(AgentsStockDataTable $dataTable)
    {

        return $dataTable->render('auth.reports.stock_report');
    }

    public function getProfitLossReport()
    {
        return view('auth.reports.profit_loss');
    }

    public function calculateProfitLossReport(Request $request)
    {
        $dates = explode('to', $request->input('dates'));
        if (count($dates) == 2) {
            $start_date = Carbon::parse(trim($dates[0]));
            $end_date = Carbon::parse(trim($dates[1]));
            $orderItems = OrderItem::selectRaw('*, cost*quantity as sub_costs,price*quantity as sub_price')->whereBetween('created_at', array($start_date, $end_date))->get();
            $expense = AccountExpense::whereBetween('created_at', array($start_date, $end_date))->get();
            return view('auth.reports.profit_loss', ['orderItems' => $orderItems, 'expense' => $expense, 'dates' => $request->input('dates')]);
        }
        return redirect()->back()->withErrors(["Something went wrong"]);
    }

    public function adjustStock(ProductOfferApproveRequest $request)
    {
        DB::beginTransaction();
        try {
            $offer = AgentOffer::where(['id' => $request->input('offer_id')])->first();
            $stock = $offer->offer_stock;
            $new_stock = $request->input('stock');
            VariationCost::updateOrCreate(["variant_id" => $offer->variation_id, "order_ref_id" => $offer->id, "cost" => $request->input('cost')]);
            $product_variant = ProductsVariant::where(['id' => $offer->variation_id])->first();
            $product_variant->update(['price' => $request->input('price'), 'cost' => $request->input('cost')]);
            if ($new_stock > $stock) {
                $product_variant->increment('on_hand', round($new_stock - $stock));
            } else {
                $product_variant->decrement('on_hand', round($stock - $new_stock));
            }
            $offer->negotiate_price = $request->input('cost');
            $offer->offer_stock = $new_stock;
            $offer->save();

            DB::commit();
            return redirect()->back()->with('success', "Agent stock update success");
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->withErrors(["Internal Server Error"])->with('exception', $exception->getMessage())->withInput();
        }
    }

    public function addStock(ProductOfferApproveRequest $request)
    {
        DB::beginTransaction();
        try {
            $offer = AgentOffer::where(['id' => $request->input('offer_id')])->first();
            VariationCost::updateOrCreate(["variant_id" => $offer->variation_id, "order_ref_id" => $offer->id, "cost" => $request->input('cost')]);
            $product_variant = ProductsVariant::where(['id' => $offer->variation_id])->first();
            $product_variant->increment('on_hand', $request->input('stock'));
            $offer->increment('offer_stock', $request->input('stock'));
            $product_variant->update(['price' => $request->input('price'), 'cost' => $request->input('cost')]);
            $offer->negotiate_price = $request->input('cost');
            $offer->save();
            DB::commit();
            return redirect()->back()->with('success', "Agent stock update success");
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->withErrors(["Internal Server Error"])->with('exception', $exception->getMessage())->withInput();
        }
    }
}
