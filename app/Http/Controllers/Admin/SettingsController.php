<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\MailSettingsRequest;
use App\Http\Requests\Admin\SettingsUpdateRequest;
use App\Http\Requests\Admin\SMSSettingsRequest;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use function GuzzleHttp\Promise\all;

class SettingsController extends Controller
{
    public function getGeneralSettingsView()
    {
        return view('auth.settings.general');
    }

    public function updateGeneralSettingsView(SettingsUpdateRequest $request)
    {
        try {
            $settings = $request->only(config('const.settings'));
            $phone = phone($request->input('phone'), ['BD'], $format = null);
            if ($request->hasFile('site_logo')) {
                $settings[config('const.settings.site_logo')] = asset('settings' . DIRECTORY_SEPARATOR . settings_file_upload($request->file('site_logo'), public_path('settings')));
            }
            if ($request->hasFile('favicon')) {
                $settings[config('const.settings.favicon')] = asset('settings' . DIRECTORY_SEPARATOR . settings_file_upload($request->file('favicon'), public_path('settings')));
            }
            $settings['phone']=$phone;
            setting($settings);
            setEnv('TIME_ZONE', setting('timeZone', 'Asia/Dhaka'));
            return redirect()->back()->with('success', "Setting info updated");
        } catch (\Exception $exception) {
            return redirect()->back()->withErrors(["Internal Server Error"])->with('exception', $exception->getMessage());
        }
    }

    public function getMailSettingsView()
    {
        return view('auth.settings.mail');
    }

    public function updateMailSettings(MailSettingsRequest $request)
    {
        if (!is_valid_domain_name($request->input('mailHost'))) {
            return redirect()->back()->withErrors(["Mail host must be a valid host"]);
        }
        setEnv('MAIL_HOST', $request->input('mailHost'));
        setEnv('MAIL_PORT', $request->input('mailPort'));
        setEnv('MAIL_FROM_ADDRESS', $request->input('mailAddress'));
        setEnv('MAIL_PASSWORD', $request->input('mailPassword'));
        setEnv('MAIL_FROM_NAME', "'" . $request->input('fromName') . "'");
        setEnv('MAIL_ENCRYPTION', $request->input('encryption'));
        return redirect()->back()->with('success', "Mail settings update successfully done");
    }

    public function getSMSSettingsView()
    {
        try {
            $sms_config = get_sms_config();
        } catch (FileNotFoundException $e) {
            return redirect()->route('home')->withErrors(['SMS configuration file not found',"Please contact with developers"]);
        }
        return view('auth.settings.sms', ["sms_config" => $sms_config]);
    }

    public function updateSMSSettings(SMSSettingsRequest $request)
    {
        $header_values = $request->input('header_value');
        $header_names = $request->input('header_name');
        $option_name = $request->input('option_name');
        $option_value = $request->input('option_value');
        $headers = [];
        $others = [];
        if (is_array($header_names) && is_array($header_values)):
            if (sizeof($header_names) == sizeof($header_values)) {
                foreach ($header_names as $index => $header) {
                    $headers[] = [$header => $header_values[$index]];
                }
            }
        endif;
        if (is_array($option_name) && is_array($option_value)):
            if (sizeof($option_name) == sizeof($option_value)) {
                foreach ($option_name as $index => $option) {
                    $others[] = [$option => $option_value[$index]];
                }
            }
        endif;
        $sms_config = [
            'method' => $request->input('method'),
            'url' => $request->input('url'),
            'params' => [
                'send_to_param_name' => $request->input('send_to_param_name'),
                'msg_param_name' => $request->input('msg_param_name'),
                'others' => $others,
            ],
            'headers' => $headers,
            'json' => false,
            'add_code' => false,
        ];
        set_sms_config($sms_config);
        return redirect()->back()->with('success', "SMS configuration update successfully done");
    }
}
