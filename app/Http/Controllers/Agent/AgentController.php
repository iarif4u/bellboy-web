<?php

namespace App\Http\Controllers\Agent;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AgentController extends Controller
{
    /**
     * Create a new AuthController instance.
     * black join in boyd
     *
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:agent');
    }
    /**
     * Get the authenticated User.
     *
     * @return JsonResponse
     */
    public function getCategories()
    {
        return response()->json($this->guard()->user(), 200);
    }

    public function setUpFirebaseToken(Request $request){
        return auth()->user();
    }
}
