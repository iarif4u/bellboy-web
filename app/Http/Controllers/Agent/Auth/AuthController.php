<?php

namespace App\Http\Controllers\Agent\Auth;

use App\Events\MessageEvent;
use App\Models\Agent;
use App\Models\Conversation;
use App\Models\FirebaseToken;
use App\Models\Message;
use App\User;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Routing\Controller;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:agent', ['except' => ['login']]);
    }

    /**
     * Get a JWT token via given credentials.
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
        if ($token = $this->guard()->attempt($credentials)) {
            return $this->respondWithToken($token);
        }

        return response()->json(['error' => 'Invalid Login Details'], 401);
    }

    /**
     * Get the authenticated User.
     *
     * @return JsonResponse
     */
    public function me()
    {
        $me = Agent::with(['shopName', 'shopAddress'])->where(['id' => auth()->id()])->first();
        $me->profile_img = get_media_file($me, config('const.media.profile_picture'));
        $me->nid_img = get_media_file($me, config('const.media.nid_card'));
        $me->shop_img = get_media_file($me, config('const.media.shop_img'));
        $me->agent_shop = $me->shopName->data_value;
        $me->agent_shop_address = $me->shopAddress->data_value;
        unset($me->media);
        return response()->json($me, 200);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function logout(Request $request)
    {
        if ($request->has('fcmToken')){
            FirebaseToken::where(['token' => $request->input('fcmToken')])->first()->delete();
        }
        $this->guard()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken($this->guard()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param string $token
     *
     * @return JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => $this->guard()->factory()->getTTL() * 60,
        ]);
    }

    public function uploadShopImage(Request $request)
    {
        if ($request->hasFile('shop_image')) {
            $agent = Agent::where(['id' => auth()->id()])->first();
            sync_media_file($agent, $request->file('shop_image'), config('const.media.shop_img'));
            $shopImage = get_media_file($agent, config('const.media.shop_img'));
            return response()->json(['error' => false, "message" => "Shop image upload success", 'shop_image' => $shopImage]);
        } else {
            return response()->json(['error' => true, "message" => "Shop image upload fail"]);
        }
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return Guard
     */
    public function guard()
    {
        return Auth::guard('agent');
    }

    public function messages()
    {
        $conversion = Conversation::where(['creator_id' => auth()->id(), 'creator_type' => Agent::class])->first();
        $messages = Message::where(['conversation_id' => $conversion->id])
            ->orderByDesc('id')->get();

        $data = [];
        foreach ($messages as $message) {

            $profile_pic = get_media_file($message->sender, config('const.media.profile_picture'));
            $data[] = ['_id' => $message->id,
                'text' => $message->message,
                'createdAt' => $message->created_at,

                'user' => [
                    '_id' => $message->sender->phone,
                    'name' => $message->sender->name,
                    'avatar' => ($profile_pic) ? $profile_pic : setting(config('const.settings.site_logo'), asset('assets/img/logo.png')),
                ]
            ];
        }
        return [
            'current_page' => 1,
            'next_page_url' => 10,
            'data' => $data
        ];
    }

    public function sendAgentMessage(Request $request)
    {
        $conversion = Conversation::where(['creator_id' => auth()->id(), 'creator_type' => Agent::class])->first();
        $msg = $request->input('message');
        $message = $conversion->messages()->create([
            'guid' => uniqid(),
            'sender_id' => auth()->id(),
            'sender_type' => Agent::class,
            'message' => $msg
        ]);
        $profile_pic = get_media_file($message->sender, config('const.media.profile_picture'));
        event(new MessageEvent($msg,$conversion->channel_id));
        return response()->json(['_id' => $message->id,
            'text' => $message->message,
            'createdAt' => $message->created_at,
            'user' => [
                '_id' => $message->sender->id,
                'name' => $message->sender->name,
                'avatar' => ($profile_pic) ? $profile_pic : setting(config('const.settings.site_logo'), asset('assets/img/logo.png')),
            ]
        ]);
    }

    public function balanceWithdrawRequest(){
        try{
        Agent::where(['id'=>auth()->id()])->update(['withdraw_req'=>true]);
            return response()->json(['error'=>false,'message'=>'Balance Withdraw Request Submitted']);
        }catch (\Exception $exception){
            return response()->json(['error'=>true],500);
        }
    }
}

