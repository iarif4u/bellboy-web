<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Message;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    //inset user firebase token
    public function setUpFirebaseToken(Request $request)
    {
        $token = $request->input('firebase_token', false);
        if ($token) {
            auth()->guard('agent')->user()->firebase_tokens()->updateOrCreate(['token' => $token]);
            return response()->json(['message' => "Firebase token add success"]);
        } else {
            return response()->json(['message' => "Firebase token add fail"], 400);
        }
    }


}
