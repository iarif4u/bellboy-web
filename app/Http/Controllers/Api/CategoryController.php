<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductsVariant;
use App\Repositories\ProductRepository;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    public function getParentCategories()
    {
        $categories = Category::with(['childrenCategories', 'products'])->whereDoesntHave('parent_category')->get();
        return ProductRepository::get_categories_data($categories);
    }

    public function getCategory($category_id)
    {
        $categories = Category::with(['childrenCategories', 'products'])->where(['category_id' => $category_id])->get();
        return ProductRepository::get_product_response($categories);
    }

    public function getPopularProducts()
    {
        $variations = ProductsVariant::withCount('order_items')->orderByDesc('order_items_count')->limit(15)->get();
        $products = [];
        foreach ($variations as $variation) {
            $products[] = ProductRepository::get_product_response($variation);
        }
        return ['popular' => $products, 'discounts' => $this->getDiscountProducts()];
    }

    public function getDiscountProducts()
    {
        $productList = Product::with(['discount'])->whereHas('discount')->get();
        return ProductRepository::product_list_response($productList);
    }

    public function getSearchProducts(Request $request)
    {
        $searchString = $request->input('search');
        $productList = Product::with(['variations'])->where('product_name', 'like', '%' . $searchString . '%')
            ->orWhere('description', 'like', '%' . $searchString . '%')
            ->orWhere('product_code', 'like', '%' . $searchString . '%')
            ->orWhereHas('variations', function ($query) use ($searchString) {
                $query->where('variant_name', 'like', '%' . $searchString . '%');
            })->get();
        return ProductRepository::product_list_response($productList);
    }
}
