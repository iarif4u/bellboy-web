<?php

namespace App\Http\Controllers\Api;

use App\Events\MessageEvent;
use App\Http\Controllers\Controller;
use App\Http\Requests\Customer\CustomerOTPLoginRequest;
use App\Http\Requests\Customer\CustomerOTPRequest;
use App\Models\Conversation;
use App\Models\CouponCode;
use App\Models\Customer;
use App\Models\CustomerCoupon;
use App\Models\FirebaseToken;
use App\Models\Manager;
use App\Models\Message;
use App\Models\NotificationToken;
use App\Models\OTPCode;
use App\Notifications\CustomerSMSNotification;
use App\Repositories\ChatRepository;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;

class CustomerController extends Controller
{
    public function saveAppsToken(Request $request)
    {
        try {
            $token = $request->input('token');
            NotificationToken::updateOrCreate([
                'token' => $token,
                'type' => Customer::class
            ]);
            return response()->json(['Token added successfully done']);
        } catch (\Exception $exception) {
            return response()->json($exception->getMessage());
        }
    }

    public function requestForOtp(CustomerOTPRequest $request)
    {
        $phone = phone($request->input('phone'), ['BD'], $format = null);
        $customer = Customer::where(['phone' => $phone])->first();
        if (!$customer) {
            $customer = Customer::updateOrCreate(['phone' => $phone], ['password' => bcrypt($phone)]);
        }
        $fcmToken = $request->input('fcmToken', false);
        if ($fcmToken) {
            $fToken = FirebaseToken::where(['token' => $fcmToken])->get();
            if ($fToken->count() == 0) {
                $customer->firebase_tokens()->updateOrCreate(['token' => $fcmToken]);
            }
        }
        $last_otp = $customer->otpCodes->last();
        if ($last_otp) {
            if (Carbon::parse($last_otp->valid_till)->diffInMinutes(Carbon::now()) > 3) {
                $otp_code = mt_rand(1000, 9999);
                $customer->otpCodes()->create(["otp_code" => $otp_code, "valid_till" => Carbon::now()->addMinutes(3)]);
            } elseif (Carbon::parse($last_otp->created_at)->diffInSeconds(Carbon::now()) > 40) {
                $otp_code = $last_otp->otp_code;
            } else {
                $otp_code = false;
            }
        } else {
            $otp_code = mt_rand(1000, 9999);
            $customer->otpCodes()->create(["otp_code" => $otp_code, "valid_till" => Carbon::now()->addMinutes(3)]);
        }
        if ($otp_code) {
            Notification::send($customer, new CustomerSMSNotification($otp_code));
            return response()->json(["error" => false, "message" => "OTP SMS send to your mobile"]);
        }
        return response()->json(["error" => false, "message" => "OTP SMS already send to your mobile"]);
    }

    public function requestForOtpLogin(CustomerOTPLoginRequest $request)
    {
        $phone = phone($request->input('phone'), ['BD'], $format = null);
        $otp_code = $request->input('otp_code');
        $customer = Customer::with('otpCodes', 'conversation')->where(['phone' => $phone])->first();
        $otp = OTPCode::where(['otpable_id'=>$customer->id,'otp_code' => $otp_code])->where('valid_till', '>', DB::raw('NOW()'))->first();
        if ($otp) {
            if ($customer->conversation == null):
                $conversation = Conversation::create([
                    'title' => "Chat with " . $customer->phone,
                    'creator_id' => $customer->id,
                    'creator_type' => Customer::class,
                    'channel_id' => uniqid('chat_')
                ]);
                $manager = Manager::where("chat", "=", true)->first();
                if ($manager) {
                    $participants = [
                        ['user_type' => Customer::class, 'user_id' => $customer->id],
                        ['user_type' => Manager::class, 'user_id' => $manager->id]
                    ];
                }
                $conversation->participants()->createMany($participants);
            endif;
            if ($token = $this->guard()->attempt(['phone' => $phone, 'password' => $phone])) {
                return response()->json([
                    'error' => false,
                    'access_token' => $token,
                    'token_type' => 'bearer',
                    'name'=>$customer->name,
                    'address'=>$customer->address,
                    'phone'=>$customer->phone,
                    'expires_in' => $this->guard()->factory()->getTTL() * 60,
                ]);

            }

            return response()->json(['error' => 'Invalid Login Details'], 401);
        } else {
            return response()->json(["error" => true, "message" => "OTP code is invalid"], 401);
        }
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return Guard
     */
    public function guard()
    {
        return Auth::guard('customer');
    }

    /**
     * Get the token array structure.
     *
     * @param string $token
     *
     * @return JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'error' => false,
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => $this->guard()->factory()->getTTL() * 60,
        ]);
    }

    /**
     * Refresh a token.
     *
     * @return JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken($this->guard()->refresh());
    }

    public function sendCustomerMessage(Request $request)
    {
        $customersNoConversation = Customer::with('conversation')->whereDoesntHave('conversation')->get();
        ChatRepository::conversation_fixed($customersNoConversation);
        $conversion = Conversation::where(['creator_id' => auth()->guard('customer')->id(), 'creator_type' => Customer::class])->first();
        $msg = $request->input('message');
        $message = $conversion->messages()->create([
            'guid' => uniqid(),
            'sender_id' => auth()->guard('customer')->id(),
            'sender_type' => Customer::class,
            'message' => $msg
        ]);
        $profile_pic = get_media_file($message->sender, config('const.media.profile_picture'));
        event(new MessageEvent($msg, $conversion->channel_id));
        return response()->json(['_id' => $message->id,
            'text' => $message->message,
            'createdAt' => $message->created_at,
            'user' => [
                '_id' => $message->sender->id,
                'name' => $message->sender->name,
                'avatar' => ($profile_pic) ? $profile_pic : setting(config('const.settings.site_logo'), asset('assets/img/logo.png')),
            ]
        ]);
    }

    public function getCustomerMessages()
    {
        $conversion = Conversation::where(['creator_id' => auth()->guard('customer')->id(), 'creator_type' => Customer::class])->first();
        $messages = Message::where(['conversation_id' => $conversion->id])->orderByDesc('id')->get();

        $data = [];
        foreach ($messages as $message) {
            if ($message->sender) {
                $profile_pic = get_media_file($message->sender, config('const.media.profile_picture'));

                $data[] = ['_id' => $message->id,
                    'text' => $message->message,
                    'createdAt' => $message->created_at,
                    'user' => [
                        '_id' => $message->sender->phone,
                        'name' => $message->sender->name,
                        'avatar' => ($profile_pic) ? $profile_pic : setting(config('const.settings.site_logo'), asset('assets/img/logo.png')),
                    ]
                ];
            }
        }
        return [
            'current_page' => 1,
            'next_page_url' => 10,
            'data' => $data
        ];
    }

    public function promocodeResponse(Request $request)
    {
        $promocode = $request->input('promocode', false);
        $unique_id = $request->input('unique_id');
        $phone = $request->input('phone');
        $token = $request->input('token');
        $couponCode = CouponCode::select(["id as promo_id", "customer_times","coupon_code", "type", "amount", "max_amount"])->where(['coupon_code' => $promocode])->where('expire_date', '>', DB::raw('NOW()'))->first();
        if ($couponCode) {
            $coupon_uses = CustomerCoupon::where(["coupon_id"=>$couponCode->id])->count();
            if($coupon_uses < $couponCode->max_amount){
                $customer_uses = ($token)?CustomerCoupon::where(["coupon_id"=>$couponCode->id,"device_id"=>$unique_id])->orWhere(['phone'=>$phone])->count():CustomerCoupon::where(["coupon_id"=>$couponCode->id,"device_id"=>$unique_id])->count();
                if($customer_uses < $couponCode->customer_times){
                    return $couponCode;
                }else{
                    return response()->json("Coupon code is invalid", 401);
                }
            }else{
                return response()->json("Coupon code is invalid", 401);
            }
        } else {
            return response()->json("Coupon code is invalid", 401);
        }
    }

}
