<?php

namespace App\Http\Controllers\Api;

use App\Events\OrderEvent;
use App\Http\Controllers\Controller;
use App\Models\AgentOffer;
use App\Models\CouponCode;
use App\Models\Customer;
use App\Models\CustomerCoupon;
use App\Models\Order;
use App\Models\ProductsVariant;
use App\Repositories\ProductRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    public function makeCustomerOrder(Request $request)
    {
        // return $request->input('phone');
        $request->validate([
            'phone' => 'required|phone:BD',
            'promocode' => 'nullable|array',
            'promocode.promo_id' => 'nullable|exists:coupon_codes,id',
            'cart' => 'required|array',
            'cart.*.productId' => 'required|exists:products_variants,id',
            'cart.*.price' => 'required|exists:products_variants,price',
            'cart.*.productQty' => 'required|numeric',

        ]);
        $phone = $request->input('phone');
        $address = $request->input('address');
        $device_id = $request->input('unique_id');
        $name = $request->input('name');
        $coupon_details = $request->input('promocode', null);
        $cart_list = $request->input('cart');
        $laundry_items = [];
        $shop_items = [];
        foreach ($cart_list as $item) {
            if ($item['laundry']) {
                $laundry_items[] = $item;
            } else {
                $shop_items[] = $item;
            }
        }
        DB::beginTransaction();
        try {

            $customer = auth()->guard('customer')->user();
            $coupon = null;
            if ($coupon_details != null) {
                $coupon = CouponCode::select(["id", "coupon_code", "type", "amount", "max_amount"])->where(['id' => $coupon_details["promo_id"]])->where('expire_date', '>', DB::raw('NOW()'))->first();
                if ($coupon == null) {
                    return response()->json("Promo code is invalid or expire", 422);
                }else{
                    CustomerCoupon::create([
                        "coupon_id"=>$coupon->id,
                        "device_id"=>$device_id,
                        "phone"=>$phone
                    ]);
                }
            }
            $customer->name = $name;
            $customer->address = $address;
            $customer->save();
            $laundry_charge = setting(config('const.settings.laundry_charge'));
            $delivery_charge = setting(config('const.settings.delivery_charge'));
            if (count($laundry_items) > 0) {
                $order_id = "#" . date("ds") . rand(0, 9) . $customer->id;
                $order = ProductRepository::make_customer_order($laundry_items, $coupon, $customer, $laundry_charge,true,$order_id);
                event(new OrderEvent($order));
                if (setting('phone')) {
                    send_custom_sms(setting('phone'), "Bellboy has new order, Order id: " . $order->order_id);
                } else {
                    write_custom_log("Setting phone number not found");
                }
            }
            if (count($shop_items) > 0) {
                $order_id = "#" . date("ds") . $customer->id . rand(0, 9);
                $order = ProductRepository::make_customer_order($shop_items, $coupon, $customer, $delivery_charge,false,$order_id);
                event(new OrderEvent($order));
                if (setting('phone')) {
                    send_custom_sms(setting('phone'), "Bellboy has new order, Order id: " . $order->order_id);
                } else {
                    write_custom_log("Setting phone number not found");
                }
            }


            DB::commit();

            return response()->json(['error' => false, "message" => "Product Ordering Success"]);
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json(["error" => true, "message" => $exception->getMessage()], 500);
        }
    }

    public function getCustomerOrders()
    {
        $orders = Order::with(['couponCode', 'items'])->where(['customer_id' => auth()->guard('customer')->id()])->orderByDesc('id')->get();
        $customer_orders = array();
        foreach ($orders as $order) {
            $orderItems = [];
            foreach ($order->items as $item):
                $discount = ProductRepository::product_discount($item->variation->product);
                $orderItems[] = [
                    "bnPrice" => en2bnNumber($item->price),
                    'discount' => $discount,
                    "discountBnPrice" => en2bnNumber($item->discount_amount),
                    "discountPrice" => $item->discount_amount,
                    "price" => $item->price,
                    "productId" => $item->variation->product->id,
                    "productName" => $item->variation->variant_name,
                    "productQty" => $item->quantity,
                    'unit' => $item->variation->unit->unit_name,
                    'unit_value' => $item->variation->unit_value,
                    'item_status' => $item->item_status,
                    'product_image' => get_media_file($item->variation, config('const.product.media.product_image')),
                    "valid_till" => ($discount) ? $discount->valid_till : null
                ];
            endforeach;

            $customer_orders[] = [
                'order' => $order,
                'display_status' => config('const.display_status')[$order->order_status],
                'order_modified' => date('Y-m-d', strtotime($order->order_modified)),
                'order_created_at' => date('Y-m-d', strtotime($order->created_at)),
                'items' => $orderItems,
                'promodcode' => $order->couponCode,
            ];
        }
        return $customer_orders;
    }
}
//{"bnPrice": "১২৫", "discount": [Object], "discountBnPrice": "১০৪", "discountPrice": 104, "price": 125, "productId": 8, "productName": "New Variation", "productQty": 1, "product_image": "http://192.168.42.116/Market/media/741/product1.jpg", "valid_till": "2020-03-28"}
