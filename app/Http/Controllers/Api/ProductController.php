<?php

namespace App\Http\Controllers\Api;

use App\Events\OfferEvent;
use App\Events\OrderEvent;
use App\Http\Controllers\Controller;
use App\Models\AgentOffer;
use App\Models\Category;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\ProductsVariant;
use App\Notifications\AgentPriceNotification;
use App\Notifications\Customer\OrderApproveNotification;
use App\Notifications\Customer\OrderPackedNotification;
use App\Notifications\DeliveryBoy\ProductsPackedNotification;
use App\Repositories\ProductRepository;
use App\User;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;
use PhpOffice\PhpSpreadsheet\Reader\Ods;

class ProductController extends Controller
{
    public function getCategoryProductsDetails($category_id)
    {
        $category = Category::with('products')->where(['id' => $category_id])->firstOrFail();
        $products = [];
        foreach ($category->products as $product) {
            foreach ($product->variations as $variation):
                $products[] = [
                    'product_id' => $product->id,
                    'product_name' => $product->product_name,
                    'variant_id' => $variation->id,
                    'variant_name' => $variation->variant_name,
                    'unit' => $variation->unit->unit_name,
                    'unit_value' => $variation->unit_value,
                    'product_image' => get_media_file($variation, config('const.product.media.product_image')),
                    'price' => $variation->price,
                    'description' => $product->description,
                    'in_stock' => $product->in_stock,
                    'status' => $product->status,
                    'discount' => ProductRepository::product_discount($product),
                    'properties' => ProductRepository::product_properties($product),
                    'brand_name' => $product->brand->name,
                    'variation_options' => ProductRepository::variation_options($variation),
                    'brand_img' => get_media_file($product->brand, config('const.media.brand_image')),
                ];
            endforeach;
        }
        return $products;
    }

    public function getCategoryProducts($category_id)
    {
        $category = Category::with(['products'])->where(['id' => $category_id])->firstOrFail();
        $products = [];
        foreach ($category->products as $product) {
            foreach ($product->variations as $variation):
                $products[] = [
                    'product_id' => $product->id,
                    'product_name' => $product->product_name,
                    'variant_id' => $variation->id,
                    'name' => $variation->variant_name,
                    'img' => get_media_file($variation, config('const.product.media.product_image')),
                    'price' => $variation->price,
                    'unit' => $variation->unit->unit_name,
                    'unit_value' => $variation->unit_value,
                    'bn_price' => en2bnNumber($variation->price),
                    'discount' => [
                        'name' => ($product->valid_discount) ? $product->valid_discount->discount_name : null,
                        'type' => ($product->valid_discount) ? $product->valid_discount->discount_type : null,
                        'discount' => ($product->valid_discount) ? $product->valid_discount->discount : null,
                        'bn_discount' => ($product->valid_discount) ? en2bnNumber($product->valid_discount->discount) : null,
                    ],
                    'variation_options' => ProductRepository::variation_options($variation),
                    'brand_name' => ($product->brand) ? $product->brand->name : null,
                ];
            endforeach;
        }
        return $products;
    }

    public function getProductVariationDetails($variation_id)
    {
        $product_variation = ProductsVariant::with(['product', 'unit'])->where(['id' => $variation_id])->firstOrFail();
        return ProductRepository::get_product_response($product_variation);
    }

    public function addProductToCart()
    {
        Cart::add([
            ['id' => '293ad', 'name' => 'Product 1', 'qty' => 1, 'price' => 10.00, 'weight' => 550],
            ['id' => '4832k', 'name' => 'Product 2', 'qty' => 1, 'price' => 10.00, 'weight' => 550, 'options' => ['size' => 'large']]
        ]);
        dd(Cart::total(), Cart::priceTotal());
    }

    public function getProductOffer(Request $request)
    {
        $request->validate([
            'variation_id' => 'required|exists:products_variants,id',
            'price' => 'required|numeric',
            'stock' => 'required|numeric',
        ]);
        $variation_id = $request->input('variation_id');
        $price = $request->input('price');
        $stock = $request->input('stock');
        $agent_offer = AgentOffer::updateOrCreate([
            "variation_id" => $variation_id,
            "agent_id" => auth()->id(),
            "offer_price" => $price,
            "offer_stock" => $stock,
            "offer_status" => config('const.agent_status.pending')
        ]);
        Notification::send(User::all(), new AgentPriceNotification($agent_offer));
        $url = route('product.variation.offers', ['variation_id' => $agent_offer->variation_id, 'offer_id' => $agent_offer->id]);
        event(new OfferEvent($url));
        return response()->json(['message' => "Your offer create successfully done"]);
        /*$variation = $this->getProductVariationDetails($variation_id);
        $variation['offer_price'] = $price;
        $variation["agent_id"] = $variation_id;
        $variation["offer_status"] = config('const.agent_status.pending');*/
        // AgentOffer::with(['agent'])->where(["agent_id" => auth()->id(), "offer_status" => config('const.agent_status.pending')])->get();

    }

    public function getAgentRequestProducts()
    {
        $agent_offers = AgentOffer::with(['variation'])->whereHas('variation')->where(["agent_id" => auth()->id()])->latest()->get();
        $products = [];
        foreach ($agent_offers as $agent_offer) {
            if ($agent_offer->variation):
            $variation = $agent_offer->variation;
            $product = $agent_offer->variation->product;
            if ($agent_offer->offer_status==config('const.agent_status.deny')||$agent_offer->offer_status==config('const.agent_status.pending')){
                $cost = $agent_offer->offer_price;
            }else{
                $cost = ProductRepository::get_agent_variation_cost($variation->id,auth()->id());
            }
            $products[] = [
                'offer_id' => $agent_offer->id,
                'offer_price' => $agent_offer->offer_price,
                'offer_stock' => en2bnNumber($agent_offer->offer_stock),
                'offer_status' => ucfirst($agent_offer->offer_status),
                'bn_offer_price' => en2bnNumber($cost),
                'product_id' => $product->id,
                'product_name' => $product->product_name,
                'unit' => $variation->unit->unit_name,
                'unit_value' => $variation->unit_value,
                'variant_id' => $variation->id,
                'name' => $variation->variant_name,
                'img' => get_media_file($variation, config('const.product.media.product_image')),
                'price' => $variation->price,
                'bn_price' => en2bnNumber($variation->price),
                'discount' => [
                    'name' => ($product->valid_discount) ? $product->valid_discount->discount_name : null,
                    'type' => ($product->valid_discount) ? $product->valid_discount->discount_type : null,
                    'discount' => ($product->valid_discount) ? $product->valid_discount->discount : null,
                    'bn_discount' => ($product->valid_discount) ? en2bnNumber($product->valid_discount->discount) : null,
                ],
                'variation_options' => ProductRepository::variation_options($variation),
                'brand_name' => ($product->brand) ? $product->brand->name : null,
            ];
            endif;
        }
        return $products;
    }

    public function getAgentPendingProducts()
    {

        $orderItems = OrderItem::with(['variation', 'order'])->whereHas('order', function ($query) {
            return $query->where(['order_status' => config('const.order_status.approve')]);
        })->where(['agent_id' => auth()->id()])->get();
        $pendingItems = [];
        foreach ($orderItems as $item) {
            $pendingItems[] = [
                'order_item_id' => $item->id,
                'variation_id' => $item->variation_id,
                'cost' => $item->cost,
                'variation_name' => $item->variation->variant_name,
                'quantity' => $item->quantity,
                'unit' => $item->variation->unit->unit_name,
                'unit_value' => $item->variation->unit_value,
                'item_status' => $item->item_status,
                'product_image' => get_media_file($item->variation, config('const.product.media.product_image')),
            ];
        }
        return $pendingItems;
    }

    public function getAgentCompleteProducts()
    {
        $orderItems = OrderItem::with(['variation', 'order'])->whereHas('order', function ($query) {
            return $query->where(['order_status' => config('const.order_status.complete')]);
        })->where(['agent_id' => auth()->id()])->get();
        $completeItems = [];
        foreach ($orderItems as $item) {
            $completeItems[] = [
                'order_item_id' => $item->id,
                'variation_id' => $item->variation_id,
                'cost' => $item->cost,
                'variation_name' => $item->variation->variant_name,
                'delivered_quantity' => $item->delivered_quantity,
                'quantity' => $item->quantity,
                'unit' => $item->variation->unit->unit_name,
                'unit_value' => $item->variation->unit_value,
                'item_status' => $item->item_status,
                'product_image' => get_media_file($item->variation, config('const.product.media.product_image')),
            ];
        }
        return $completeItems;
    }

    public function getAgentPendingProductDetails($item_order_id)
    {
        $item = OrderItem::with(['variation', 'order'])->where(['id' => $item_order_id])->orderByDesc('id')->first();

        return [
            'order_item_id' => $item->id,
            'variation_id' => $item->variation_id,
            'cost' => $item->cost,
            'variation_name' => $item->variation->variant_name,
            'delivered_quantity' => $item->delivered_quantity,
            'quantity' => $item->quantity,
            'unit' => $item->variation->unit->unit_name,
            'unit_value' => $item->variation->unit_value,
            'item_status' => ucfirst($item->item_status),
            'product_image' => get_media_file($item->variation, config('const.product.media.product_image')),
            'delivery_boy_name' => $item->order->deliveryBoy->name,
            'delivery_boy_phone' => $item->order->deliveryBoy->phone,
            'delivery_boy_photo' => get_media_file($item->order->deliveryBoy, config('const.media.profile_picture')),

        ];
    }

    public function getAgentPendingOrders()
    {
        $agent_id = auth()->guard('agent')->id();
        $orders = Order::with(['items' => function ($query) use ($agent_id) {
            $query->where('agent_id', '=', $agent_id);
        }, 'deliveryBoy'])->whereHas('items', function ($query) use ($agent_id) {
            $query->where('agent_id', '=', $agent_id);
        })->whereIn('order_status', config('const.order_processed_status'))->get();
        $orderList = [];
        foreach ($orders as $order) {
            $orderItems = ProductRepository::order_items_details($order->items);
            $orderList[] = [
                'id' => $order->id,
                'order_id' => $order->order_id,
                "order_status" => $order->order_status,
                'display_status' => config('const.display_status')[$order->order_status],
                "order_modified" => $order->order_modified,
                "delivery_time" => $order->delivery_time,
                'order_items' => $orderItems['items'],
                'total_cost' => $orderItems['total_cost'],
                'delivery_boy_name' => $order->deliveryBoy->name,
                'delivery_boy_phone' => $order->deliveryBoy->phone,
                'delivery_boy_photo' => get_media_file($order->deliveryBoy, config('const.media.profile_picture')),
            ];
        }
        return $orderList;
    }

    public function packAgentProduct(Request $request)
    {
        $order_item_id = intval($request->input('item_id'));
        if (is_integer($order_item_id)) {
            $agent_id = auth()->guard('agent')->id();
            DB::beginTransaction();
            try {
                $order_item= OrderItem::with(['order'])->where(["id"=>$order_item_id,'agent_id'=>$agent_id])->first();
                $order_item->update(['item_status'=>config('const.order_item_status.packed')]);
                $items = $packedItems = OrderItem::where(["order_id"=>$order_item->order->id,'agent_id'=>$agent_id])->get();
                $orderItems = ProductRepository::order_items_details($items);
                if ($orderItems['is_packed']){
                    $order_item->order->order_status = config('const.order_status.packed');
                    $order_item->order->save();
                }
                $packedItems = OrderItem::where(["order_id"=>$order_item->order->id,'item_status'=>config('const.order_item_status.pending')])->count();
                if ($packedItems==0){
                    Notification::send($order_item->order->customer, new OrderPackedNotification($order_item->order));
                    Notification::send($order_item->order->deliveryBoy, new ProductsPackedNotification($order_item->order));
                }
                DB::commit();
                return [
                    'id' => $order_item->order->id,
                    'order_id' => $order_item->order->order_id,
                    "order_status" => $order_item->order->order_status,
                    'display_status' => config('const.display_status')[$order_item->order->order_status],
                    "order_modified" => $order_item->order->order_modified,
                    "delivery_time" => $order_item->order->delivery_time,
                    'order_items' => $orderItems['items'],
                    'total_cost' => $orderItems['total_cost'],
                    'delivery_boy_name' => $order_item->order->deliveryBoy->name,
                    'delivery_boy_phone' => $order_item->order->deliveryBoy->phone,
                    'delivery_boy_photo' => get_media_file($order_item->order->deliveryBoy, config('const.media.profile_picture')),
                ];
            }catch (\Exception $exception){
                DB::rollBack();
                return response()->json(["message"=>"Something went wrong"],$exception->getCode());
            }

        }else{
            return response()->json(["message"=>"Something went wrong"],401);
        }
    }
}
