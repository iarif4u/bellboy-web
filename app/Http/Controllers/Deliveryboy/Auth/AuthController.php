<?php

namespace App\Http\Controllers\Deliveryboy\Auth;

use App\Events\MessageEvent;
use App\Models\Conversation;
use App\Models\Deliveryboy;
use App\Models\FirebaseToken;
use App\Models\Message;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Routing\Controller;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:deliveryboy', ['except' => ['login']]);
    }

    /**
     * Get a JWT token via given credentials.
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if ($token = $this->guard()->attempt($credentials)) {
            return $this->respondWithToken($token);
        }

        return response()->json(['error' => 'Invalid Login Details'], 401);
    }

    /**
     * Get the authenticated User.
     *
     * @return JsonResponse
     */
    public function me()
    {
        $me = Deliveryboy::with(['area','address'])->where(['id' => auth()->id()])->first();
        $me->profile_img = get_media_file($me, config('const.media.profile_picture'));
        unset($me->media);
        return response()->json($me, 200);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function logout(Request $request)
    {
        if ($request->has('fcmToken')){
            FirebaseToken::where(['token' => $request->input('fcmToken')])->first()->delete();
        }
        $this->guard()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken($this->guard()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param string $token
     *
     * @return JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => $this->guard()->factory()->getTTL() * 60,
        ]);
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return Guard
     */
    public function guard()
    {
        return Auth::guard('deliveryboy');
    }

    //inset user firebase token
    public function setUpFirebaseToken(Request $request)
    {
        $token = $request->input('firebase_token', false);
        if ($token) {
            $delivery_boy_id= auth()->guard('deliveryboy')->id();
            FirebaseToken::updateOrCreate(['token' => $token],["tokenable_id"=>$delivery_boy_id, "tokenable_type"=>Deliveryboy::class]);
            return response()->json(['message' => "Firebase token add success"]);
        } else {
            return response()->json(['message' => "Firebase token add fail"], 400);
        }
    }


    public function messages()
    {
        $conversion = Conversation::where(['creator_id' => auth()->id(), 'creator_type' => Deliveryboy::class])->first();
         $messages = Message::where(['conversation_id' => $conversion->id])
            ->orderByDesc('id')->get();

        $data = [];
        foreach ($messages as $message) {

            $profile_pic = get_media_file($message->sender, config('const.media.profile_picture'));
            $data[] = ['_id' => $message->id,
                'text' => $message->message,
                'createdAt' => $message->created_at,
                'user' => [
                    '_id' => $message->sender->email,
                    'name' => $message->sender->name,
                    'avatar' => ($profile_pic) ? $profile_pic : setting(config('const.settings.site_logo'), asset('assets/img/logo.png')),
                ]
            ];
        }

        return [
            'current_page' => 1,
            'next_page_url' => 10,
            'data' => $data
        ];
    }

    public function sendDeliveryBoyMessage(Request $request)
    {
        $msg = $request->input('message');
        $conversion = Conversation::where(['creator_id' => auth()->id(), 'creator_type' => Deliveryboy::class])->first();
        $message = $conversion->messages()->create([
            'guid' => uniqid(),
            'sender_id' => auth()->id(),
            'sender_type' => Deliveryboy::class,
            'message' => $msg
        ]);
        $profile_pic = get_media_file($message->sender, config('const.media.profile_picture'));
        event(new MessageEvent($msg,$conversion->channel_id));
        return response()->json(['_id' => $message->id,
            'text' => $message->message,
            'createdAt' => $message->created_at,
            'user' => [
                '_id' => $message->sender->email,
                'name' => $message->sender->name,
                'avatar' => ($profile_pic) ? $profile_pic : setting(config('const.settings.site_logo'), asset('assets/img/logo.png')),
            ]
        ]);
    }
}

