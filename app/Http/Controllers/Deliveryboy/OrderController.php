<?php

namespace App\Http\Controllers\Deliveryboy;

use App\Http\Requests\DeliveryBoy\CustomerUpdateRequest;
use App\Http\Requests\DeliveryBoy\ScheduleProductRequest;
use App\Models\Customer;
use App\Models\Order;
use App\Models\OrderItem;
use App\Notifications\Customer\OrderCompleteNotification;
use App\Notifications\Customer\OrderDeliveryNotification;
use App\Notifications\Customer\OrderPackedNotification;
use App\Notifications\DeliveryBoy\ProductsPackedNotification;
use App\Repositories\DeliveryBoyRepository;
use App\Http\Controllers\Controller;
use App\Repositories\OrderRepository;
use App\Repositories\ProductRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;

class OrderController extends Controller
{
    public function collectProducts(Request $request)
    {
        $order_item_id = intval($request->input('order_item_id'));
        if (is_integer($order_item_id)) {
            DB::beginTransaction();
            try {
                $order_item = OrderItem::with(['order'])->where(["id" => $order_item_id])->first();
                $order_item->update(['item_status' => config('const.order_item_status.collected')]);
                $packedItems = OrderItem::where(["order_id" => $order_item->order->id, 'item_status' => config('const.order_item_status.packed')])->count();
                if ($packedItems == 0) {
                    Notification::send($order_item->order->customer, new OrderPackedNotification($order_item->order));
                }

                DB::commit();
                return ['orders' => $this->getDeliveryBoyOrders(), 'order_items' => ProductRepository::get_order_items($order_item->order), 'pending_items' => ProductRepository::get_total_pending($order_item->order->id)];

            } catch (\Exception $exception) {
                DB::rollBack();
                return response()->json(["message" => "Something went wrong"], $exception->getCode());
            }
        } else {
            return response()->json(["message" => "Something went wrong"], 401);
        }
    }

    //get order list of delivery boy
    public function getDeliveryBoyOrders()
    {
        $delivery_boy = DeliveryBoyRepository::getDeliveryBoyId();
        $orders = Order::with(['customer', 'couponCode', 'items'])->where(['delivery_boy' => $delivery_boy])->orderByDesc('id')->get();
        $orderList = [];
        foreach ($orders as $order) {
            $profile_pic = get_media_file($order->customer, config('const.media.profile_picture'));
            if (!$profile_pic) {
                $profile_pic = setting(config('const.settings.site_logo'), asset('assets/img/logo.png'));
            }
            $orderList[] = [
                'order' => $order,
                'display_status' => config('const.display_status')[$order->order_status],
                'pending_items' => ProductRepository::get_total_pending($order->id),
                'order_items' => ProductRepository::get_order_items($order),
                'profile_img' => $profile_pic,
            ];
        }
        return $orderList;
    }

    public function updateOrderItemStatus(Request $request)
    {
        $order_item = OrderItem::where(['id' => $request->input('order_item_id')])->first();
        $order_item->update(['item_status' => config('const.order_item_status.collected')]);
        $order = Order::with('items')->where(['id' => $order_item->order])->first();
        return ['orders' => $this->getDeliveryBoyOrders(), 'order_items' => ProductRepository::get_order_items($order), 'pending_items' => ProductRepository::get_total_pending($order_item->order->id)];
    }

    public function makeOrderToDelivery(Request $request)
    {
        $order_id = $request->input('order_id');
        $order = Order::with(['customer', 'items', 'couponCode'])->where(['id' => $order_id])->first();
        $order->update(['order_status' => config('const.delivery_boy_order_status.delivering')]);
        $current_order = OrderRepository::get_current_order($order);
        Notification::send($order->customer, new OrderDeliveryNotification($order));
        return ['orders' => $this->getDeliveryBoyOrders(), 'order' => $order, 'current_order' => $current_order];
    }

    public function completeOrder(Request $request)
    {
        $orderData = $request->input('order');
        $delivery_charge = $request->input('delivery_charge');
        $order_id = $orderData['id'];
        $total = $orderData['total']+$delivery_charge;
        $order_total = 0;
        $order_items = $request->input('order_items');
        $order = Order::with(['customer'])->where(['id' => $order_id])->first();
        foreach ($order_items as $order_item) {
            $order_total = $order_total + DeliveryBoyRepository::order_item_qty_price($order_item);
        }
        $discount_amount = ProductRepository::coupon_discount($order_id, $order_total);
        if ($order->delivery_charge > 0) {
            $total_price = ($order_total + $order->delivery_charge) - $discount_amount;
        } else {
            $total_price = $order_total - $discount_amount;
        }
        $total_value = 0;
        if ($total_price == $total) {
            DB::beginTransaction();
            try {
                foreach ($order_items as $order_item) {
                    $total_value = $total_value + DeliveryBoyRepository::order_item_delivery($order_item);
                }

                $order->update([
                    "grand_total" => $order_total,
                    "total" => $total_price,
                    "coupon_discount" => $discount_amount,
                    "order_status" => config('const.order_status.complete'),
                    "delivery_time" => Carbon::now(),
                    "order_value" => $total_value]);
                DB::commit();
                Notification::send($order->customer, new OrderCompleteNotification($order));
                return response()->json(['error' => false, 'message' => "Order Completed"]);
            } catch (\Exception $exception) {
                DB::rollBack();
                return response()->json(['error' => true, 'message' => $exception->getMessage()]);
            }
        } else {
            return response()->json(['error' => true, 'message' => "Order calculation doesn't match"]);
        }
    }

    public function scheduleCustomerProduct(ScheduleProductRequest $request)
    {
        $order_item_id = $request->input('order_item_id');
        $schedule_date = $request->input('date');
        $order_item_data = OrderItem::with(['order'])->where(['id' => $order_item_id])->first();
        $order = $order_item_data->order;
        $order_item_data->update([
            'delivery_date' => $schedule_date,
        ]);
        $order_details = [
            'order' => $order,
            'display_status' => config('const.display_status')[$order->order_status],
            'pending_items' => ProductRepository::get_total_pending($order->id),
            'order_items' => ProductRepository::get_order_items($order),
            'profile_img' => get_profile_pic($order->customer),
        ];
        DB::commit();
        return response()->json(['message' => "Products collection success", "order_details" => $order_details]);
    }

    public function deliveryCustomerProduct(Request $request)
    {
        $order_item_id = $request->input('order_item_id');
        $order_item_data = OrderItem::with(['order'])->where(['id' => $order_item_id])->first();
        $order_item_data->update([
            'delivered_quantity' => $order_item_data->quantity,
            'item_status' => config('const.laundry_item_status.delivered')
        ]);
        $order = $order_item_data->order;
        $order_details = [
            'order' => $order,
            'display_status' => config('const.display_status')[$order->order_status],
            'pending_items' => ProductRepository::get_total_pending($order->id),
            'order_items' => ProductRepository::get_order_items($order),
            'profile_img' => get_profile_pic($order->customer),
        ];
        DB::commit();
        return response()->json(['message' => "Products collection success", "order_details" => $order_details]);
    }

    public function collectCustomerProduct(Request $request)
    {
        $order_item_id = $request->input('order_item_id');
        $target_quantity = $request->input('quantity');
        $total_price = 0;
        $total_value = 0;
        DB::beginTransaction();
        try {
            $order_item_data = OrderItem::with(['order'])->where(['id' => $order_item_id])->first();
            $order_item_data->update(['item_status' => config('const.laundry_item_status.delivery_boy_collect')]);
            $order = $order_item_data->order;
            foreach ($order->items as $order_item) {
                if ($order_item) {
                    $quantity = ($order_item->id == $order_item_id) ? $target_quantity : $order_item->quantity;
                    $value = ($order_item->discount_amount - $order_item->cost) * $quantity;
                    $sub_total = $order_item->discount_amount * $quantity;
                    $total_price = $total_price + $sub_total;
                    $total_value = $total_value + $value;
                    $order_item->sub_total = $sub_total;
                    $order_item->quantity = $quantity;
                    $order_item->item_value = $value;
                    $order_item->save();
                } else {
                    DB::rollBack();
                    break;
                }
            }
            $gTotal = $total_price + $order->delivery_charge;
            if ($order->coupon_discount != null) {
                $coupon_discount = coupon_code_calculate($total_price, $order->couponCode);
                $order->coupon_discount = $coupon_discount;
                $order->total = $gTotal - $coupon_discount;
                $order->order_value = $total_value - $coupon_discount;
            } else {
                $order->total = $gTotal;
                $order->order_value = $total_value;
            }
            $order->grand_total = $gTotal;
            $order->save();
            $order_details = [
                'order' => $order,
                'display_status' => config('const.display_status')[$order->order_status],
                'pending_items' => ProductRepository::get_total_pending($order->id),
                'order_items' => ProductRepository::get_order_items($order),
                'profile_img' => get_profile_pic($order->customer),
            ];
            DB::commit();
            return response()->json(['message' => "Products collection success", "order_details" => $order_details]);
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json(['message' => "Something went wrong"], $exception->getCode());
        }
    }

    public function updateCustomer(CustomerUpdateRequest $request)
    {
        $phone = phone($request->input('phone'), ['BD'], $format = null);
        $customer_id = $request->input('customer_id');
        $order_id = $request->input('order_id');
        $address = $request->input('address');
        DB::beginTransaction();
        try {
            $customer = Customer::where(['id' => $customer_id, 'phone' => $phone])->first();
            $customer->name = $request->input('name');
            $customer->address = $address;
            $customer->save();
            Order::where(['id' => $order_id])->update([
                'delivery_address' => $address
            ]);
            DB::commit();
            return response()->json(['message' => "Customer info update successfully done"]);
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json(['message' => "Something went wrong"], $exception->getCode());
        }

    }

    public function makeOrderToAgent(Request $request)
    {
        $order_id = $request->input('order_id');
        DB::beginTransaction();
        try {
            $order = Order::with(['customer', 'items', 'couponCode'])->where(['id' => $order_id])->first();
            $order->update(['order_status' => config('const.order_status.on_agent')]);
            OrderItem::where(['order_id' => $order_id])->update(['item_status' => config('const.laundry_item_status.agent_collect')]);
            $my_order = Order::with(['items'])->where(['id' => $order_id])->first();
            $order_details = [
                'order' => $order,
                'display_status' => config('const.display_status')[$order->order_status],
                'pending_items' => ProductRepository::get_total_pending($order->id),
                'order_items' => ProductRepository::get_order_items($my_order),
                'profile_img' => get_profile_pic($order->customer),
            ];
            DB::commit();
            return response()->json(['message' => "Agent collect products", "order_details" => $order_details]);
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json(['message' => "Something went wrong"], $exception->getCode());
        }
    }
}
