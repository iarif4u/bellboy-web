<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use App\Models\Conversation;
use App\Models\Customer;
use App\Models\Deliveryboy;
use App\Models\Manager;
use App\Notifications\MessageNotification;
use App\Repositories\ChatRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class ChatController extends Controller
{
    public function getCustomerChatView()
    {
        $customersNoConversation = Customer::with('conversation')->whereDoesntHave('conversation')->get();
        ChatRepository::conversation_fixed($customersNoConversation);
        $customers = Customer::with('conversation')->get();
        return view('manager.chat.customer_chat', ['customers' => $customers]);
    }

    public function getDeliveryBoyChatView()
    {
        $deliveryboysNoConversation = Deliveryboy::with('conversation')->whereDoesntHave('conversation')->get();
        ChatRepository::conversation_fixed($deliveryboysNoConversation);
        $deliveryboys = Deliveryboy::with('conversation')->get();
        return view('manager.chat.deliveryboy_chat', ['deliveryboys' => $deliveryboys]);
    }

    public function sendMessageToCustomer(Request $request)
    {

        $request->validate([
            'channelId' => 'required|exists:conversations,channel_id',
            'message' => 'required'
        ]);
        $channelId = $request->input('channelId');
        $msg = $request->input('message');
        $conversion = Conversation::with(['customers', 'messages'])->where(['channel_id' => $channelId, 'creator_type' => Customer::class])->first();
        $message = $conversion->messages()->create([
            'guid' => uniqid(),
            'sender_id' => auth()->id(),
            'sender_type' => Manager::class,
            'message' => $msg
        ]);
        Notification::send($conversion->customers, new MessageNotification($conversion, $message));

    }
    public function sendMessageToDeliveryBoy(Request $request)
    {

        $request->validate([
            'channelId' => 'required|exists:conversations,channel_id',
            'message' => 'required'
        ]);
        $channelId = $request->input('channelId');
        $msg = $request->input('message');
        $conversion = Conversation::with(['deliveryboys', 'messages'])->where(['channel_id' => $channelId, 'creator_type' => Deliveryboy::class])->first();

        $message = $conversion->messages()->create([
            'guid' => uniqid(),
            'sender_id' => auth()->id(),
            'sender_type' => Manager::class,
            'message' => $msg
        ]);

        Notification::send($conversion->deliveryboys, new MessageNotification($conversion, $message));

    }
}
