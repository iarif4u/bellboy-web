<?php

namespace App\Http\Controllers\Manager;

use App\DataTables\Manager\ApprovedOrdersDataTable;
use App\DataTables\Manager\CanceledOrdersDataTable;
use App\DataTables\Manager\DeliveredOrdersDataTable;
use App\DataTables\Manager\PendingOrdersDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Manager\OrderApproveRequest;
use App\Http\Requests\Manager\OrderInfoRequest;
use App\Http\Requests\Manager\OrderItemAgentAssignRequest;
use App\Models\Agent;
use App\Models\Deliveryboy;
use App\Models\Order;
use App\Models\OrderDeliveryBoy;
use App\Models\OrderItem;
use App\Notifications\Agent\NewOrderNotification;
use App\Notifications\Agent\PriceApproveNotification;
use App\Notifications\Customer\OrderApproveNotification;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;

class OrderController extends Controller
{
    public function getPendingOrders(PendingOrdersDataTable $dataTable)
    {
        return $dataTable->render('manager.order.pending_orders');
    }

    public function getApprovedOrders(ApprovedOrdersDataTable $dataTable)
    {
        return $dataTable->render('manager.order.approve_orders');
    }
    public function getDeliveredOrders(DeliveredOrdersDataTable $dataTable)
    {
        return $dataTable->render('manager.order.delivered_orders');
    }
    public function getCanceledOrders(CanceledOrdersDataTable $dataTable)
    {

        return $dataTable->render('manager.order.delivered_orders');
    }
    public function deliveryboy_search()
    {
        try {
            $string = request()->input('search', '');
            $response = Deliveryboy::where('name', 'like', "%$string%")
                ->orWhere("employee_id", 'like', "%$string%")
                ->orWhere("email", 'like', "%$string%")
                ->orWhere("phone", 'like', "%$string%")
                ->limit(10)->get();
            return response()->json(['total' => $response->count(), 'data' => $response]);
        } catch (\Exception $exception) {
            return response()->json(['message' => $exception->getMessage()], 500);
        }
    }

    public function agent_search()
    {
        try {
            $string = request()->input('search', '');
            $response = Agent::where('name', 'like', "%$string%")
                ->orWhere("email", 'like', "%$string%")
                ->limit(10)->get();
            return response()->json(['total' => $response->count(), 'data' => $response]);
        } catch (\Exception $exception) {
            return response()->json(['message' => $exception->getMessage()], 500);
        }
    }

    public function getOrderInfo(OrderInfoRequest $request)
    {
        $order = Order::with(['items','deliveryBoy', 'customer'])->where(['id' => $request->input('order_id')])->first();
        return $order;
    }


    public function addAgentToOrderItem(OrderItemAgentAssignRequest $request)
    {
        try {
            OrderItem::where(['id' => $request->input('order_item')])->update(['agent_id' => $request->input('agent')]);
            return redirect()->back()->with('success', "Agent assign successfully done");
        } catch (\Exception $exception) {
            return redirect()->back()->withErrors("Internal Server Error")->with("exception", $exception->getMessage());
        }
    }

    public function addDeliveryBoyToOrder(OrderApproveRequest $request)
    {
        $order_id = $request->input('order_id');
        $delivery_boy = $request->input('delivery_boy');
        $itemList = OrderItem::where(['order_id' => $order_id, 'agent_id' => null])->get();
        if ($itemList->count()) {
            return redirect()->back()->withErrors([$itemList->pluck('variation.variant_name')->implode(' , ') . " product doesn't found agent"]);
        } else {
            $order_item_list = OrderItem::where(['order_id' => $order_id])->get();
            DB::beginTransaction();
            try {
                $order = Order::with('customer')->where(['id' => $order_id])->first();
                $order->delivery_boy = $delivery_boy;
                $order->order_status = config('const.order_status.approve');
                $order->order_modified = Carbon::now();
                $order->save();
                OrderDeliveryBoy::create(["order_id" => $order_id, "delivery_boy_id" => $delivery_boy, "status" => config('const.delivery_boy_order_status.assign'), "status_time" => Carbon::now(), "assigner_id" => auth()->guard('manager')->user()->id]);
                DB::commit();
                foreach ($order_item_list as $order_item) {
                    Notification::send($order_item->agent, new NewOrderNotification($order_item));
                }
                Notification::send($order->customer, new OrderApproveNotification($order));
               // return redirect()->back()->with('success', "Order assign to delivery boy success");
            } catch (\Exception $exception) {
                DB::rollBack();
                return redirect()->back()->withErrors("Internal Server Error")->with("exception", $exception->getMessage());
            }

        }
    }
}
