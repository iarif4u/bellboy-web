<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\BaseFormRequest;

class AgentBalanceWithdrawRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'agent_id' => 'required|exists:agents,id',
            'paymentAmount' => 'required',
            'paymentType' => 'required',
            'paymentNote' => 'required',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'agent_id.required' => 'Agent is required',
            'paymentAmount.required' => 'Payment amount is required',
            'paymentType.required' => 'Payment type is required',
            'paymentNote.required' => 'Payment note is required',
            'agent_id.unique' => 'Agent is invalid'
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'agent_id' => 'trim|escape|strip_tags|digit',
            'paymentAmount' => 'trim|escape|strip_tags',
            'paymentType' => 'trim|escape|strip_tags',
            'paymentNote' => 'trim|escape|strip_tags',
        ];
    }
}
