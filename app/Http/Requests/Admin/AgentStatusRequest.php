<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\BaseFormRequest;

class AgentStatusRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'agent_id' => 'required|exists:agents,id',
            'agent_status' => 'required|string'
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'agent_id.required' => 'Agent is required',
            'agent_id.unique' => 'Agent is invalid',
            'agent_status.required' => 'Agent status is required',
            'agent_status.string' => 'Agent status is invalid'
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'agent_id' => 'trim|escape|strip_tags|digit',
            'agent_status' => 'trim|escape|strip_tags|lowercase'
        ];
    }
}
