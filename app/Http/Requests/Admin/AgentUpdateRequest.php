<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\BaseFormRequest;

class AgentUpdateRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'agent_id' => 'required|exists:agents,id',
            'email' => 'required|email|unique:agents,email,' . request()->input('agent_id'),
            'agent_name' => 'required|string|max:50',
            'shop_name' => 'required|string|max:50',
            'password' => 'nullable|min:6',
            'phone' => 'required|phone:AUTO,BD|unique:agents,phone,' . request()->input('agent_id'),
            'agreement_date' => 'required|date|date_format:Y-m-d|before_or_equal:today',
            'trade_license' => 'nullable|file|image',
            'trade_license_second' => 'nullable|file|image',
            'nid_card_front' => 'nullable|file|image',
            'nid_card_back' => 'nullable|file|image',
            'profile_picture' => 'nullable|file|image',
            'shop_address' => 'required',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'shop_address.required' => 'Shop address is required',
            'email.required' => 'Email address is required',
            'email.email' => 'Email address is invalid',
            'email.unique' => 'The email address has already been taken',
            'agent_name.required' => 'Agent name is required',
            'agent_name.string' => 'Agent name is invalid',
            'agent_name.max' => 'Agent name is invalid',
            'shop_name.required' => 'Shop name is required',
            'shop_name.string' => 'Shop name is invalid',
            'shop_name.max' => 'Shop name is invalid',
            'password.required' => 'Password is required',
            'phone.required' => 'Phone number is required',
            'phone.phone' => 'Phone number is invalid',
            'phone.unique' => 'Phone number has already been taken',
            'agreement_date.required' => 'Agreement date is required',
            'agreement_date.date' => 'Agreement date is invalid',
            'agreement_date.date_format' => 'Agreement date has invalid format',
            'agreement_date.before_or_equal' => "Agreement date can't be after today",
            'trade_license.required' => "Trade license file is required",
            'trade_license.file' => "Trade license file is invalid",
            'trade_license.image' => "Trade license file must be an image",
            'nid_card_front.required' => "Nid card front file is required",
            'nid_card_front.file' => "Nid card front file is invalid",
            'nid_card_front.image' => "Nid card front file must be an image",
            'trade_license_second.required' => "Trade license second file is required",
            'trade_license_second.file' => "Trade license file second is invalid",
            'trade_license_second.image' => "Trade license file second must be an image",
            'nid_card_back.required' => "Nid card back file is required",
            'nid_card_back.file' => "Nid card back file is invalid",
            'nid_card_back.image' => "Nid card back file must be an image",
            'profile_picture.required' => "Agent profile picture is required",
            'profile_picture.file' => "Agent profile picture is invalid",
            'profile_picture.image' => "Agent profile picture must be an image",
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'agent_name' => 'trim|escape|strip_tags|capitalize',
            'shop_name' => 'trim|escape|strip_tags|capitalize',
            'password' => 'trim|escape|strip_tags',
            'phone' => 'trim|escape|strip_tags',
            'agreement_date' => 'trim|escape|strip_tags',
            'email' => 'trim|lowercase',
            'shop_address' => 'trim|escape|strip_tags',
        ];
    }
}
