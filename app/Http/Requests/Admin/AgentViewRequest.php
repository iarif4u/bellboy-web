<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\BaseFormRequest;

class AgentViewRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'agent_id' => 'required|exists:agents,id'
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'agent_id.required' => 'Agent is required',
            'agent_id.exists' => 'Agent not found',
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'agent_id' => 'trim|escape|strip_tags',
        ];
    }
}
