<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\BaseFormRequest;

class AreaDeleteRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'area_id' => 'required|exists:areas,id'
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'area_id.required' => 'Area is required',
            'area_id.unique' => 'Area is invalid'
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'agent_id' => 'trim|escape|strip_tags|digit'
        ];
    }
}
