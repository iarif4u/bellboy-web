<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\BaseFormRequest;

class AreaStoreRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'area_name' => 'required|unique:areas,area_name'
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'area_name.required' => 'Area name is required',
            'area_name.unique' => 'Area name has already been taken',
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'area_name' => 'trim|escape|strip_tags',
        ];
    }
}
