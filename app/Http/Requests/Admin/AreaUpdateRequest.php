<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\BaseFormRequest;

class AreaUpdateRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'area_id' => 'required|exists:areas,id',
            'area_name' => 'required|unique:areas,area_name,' . request()->input('area_id'),
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'area_id.required' => 'Area is required',
            'area_id.exists' => 'Area is invalid or not found',
            'area_name.required' => 'Area name is required',
            'area_name.unique' => 'Area name has already been taken',
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'area_id' => 'trim|escape|strip_tags',
            'area_name' => 'trim|escape|strip_tags',
        ];
    }
}
