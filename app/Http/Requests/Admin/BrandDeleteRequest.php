<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\BaseFormRequest;

class BrandDeleteRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'brand_id' => 'required|exists:brands,id'
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'brand_id.required' => 'Brand is required',
            'brand_id.unique' => 'Brand is invalid'
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'brand_id' => 'trim|escape|strip_tags|digit'
        ];
    }
}
