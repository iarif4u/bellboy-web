<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\BaseFormRequest;

class BrandStoreRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'brand_name' => 'required|unique:brands,name',
            'brand_img' => 'required|file|image'
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'brand_name.required' => 'Brand name is required',
            'brand_name.unique' => 'Brand name has already been taken',
            'brand_img.required' => 'Brand image is required',
            'brand_img.file' => 'Brand image is invalid',
            'brand_img.image' => 'Brand image must be a valid image',
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'brand_name' => 'trim|escape|strip_tags',
        ];
    }
}
