<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\BaseFormRequest;

class CategoryDeleteRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id' => 'required|exists:categories,id'
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'category_id.required' => 'Category is required',
            'category_id.unique' => 'Category is invalid'
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'category_id' => 'trim|escape|strip_tags|digit'
        ];
    }
}
