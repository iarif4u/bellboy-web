<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\BaseFormRequest;

class CategoryStoreRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_name' => 'required|unique:categories,name',
            'parent_category' => 'nullable|exists:categories,id',
            'category_image' => 'required|file|image'
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'parent_category.exists' => 'Parent Category is invalid',
            'category_name.required' => 'Category name is required',
            'category_name.unique' => 'Category name has already been taken',
            'category_image.required' => 'Category image is required',
            'category_image.file' => 'Category image is invalid',
            'category_image.image' => 'Category image must be a valid image',
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'category_name' => 'trim|escape|strip_tags',
        ];
    }
}
