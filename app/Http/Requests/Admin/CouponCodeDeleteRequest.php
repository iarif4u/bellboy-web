<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\BaseFormRequest;

class CouponCodeDeleteRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return ['coupon_id' => 'required|exists:coupon_codes,id'];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'coupon_id.required' => 'Coupon id is required',
            'coupon_id.exists' => 'Coupon id is invalid'
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'coupon_id' => 'trim|escape|strip_tags'
        ];
    }
}
