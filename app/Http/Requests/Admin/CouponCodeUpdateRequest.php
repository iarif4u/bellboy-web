<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\BaseFormRequest;

class CouponCodeUpdateRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'coupon_code_id' => 'required|exists:coupon_codes,id',
            'coupon_code' => 'required|unique:coupon_codes,coupon_code,'.request()->input('coupon_code_id'),
            'type' => 'required|string|in:'.implode(",",array_values(config('const.coupon_codes'))),
            'amount' => 'required|numeric',
            'max_amount' => 'required|numeric',
            'qty' => 'required|numeric',
            'expire_date' => 'required|date|date_format:Y-m-d|after_or_equal:today',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'coupon_code_id.required' => 'Coupon Code id is required',
            'coupon_code_id.exists' => 'Coupon Code id is invalid',
            'coupon_code.required' => 'Coupon Code is required',
            'coupon_code.unique' => 'Coupon Code has already been taken',
            'type.required' => 'Coupon Code type is required',
            'type.string' => 'Coupon Code type is invalid',
            'type.in' => 'Coupon Code type is unknown',
            'amount.required' => 'Coupon Code amount is required',
            'amount.numeric' => 'Coupon Code amount is invalid',
            'max_amount.required' => 'Maximum amount is required',
            'max_amount.numeric' => 'Maximum amount is invalid',
            'qty.required' => 'Qty is required',
            'qty.numeric' => 'Qty is invalid',
            'expire_date.required' => 'Expire date is required',
            'expire_date.date' => 'Expire date is invalid',
            'expire_date.date_format' => 'Expire date has invalid format',
            'expire_date.before_or_equal' => "Expire date can't be before today"
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'coupon_code' => 'trim|escape|strip_tags',
            'type' => 'trim|escape|strip_tags',
            'amount' => 'trim|escape|strip_tags',
            'max_amount' => 'trim|escape|strip_tags',
            'qty' => 'trim|escape|strip_tags',
            'expire_date' => 'trim|escape|strip_tags'
        ];
    }
}
