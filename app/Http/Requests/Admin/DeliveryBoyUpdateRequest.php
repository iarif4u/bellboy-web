<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\BaseFormRequest;

class DeliveryBoyUpdateRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'deliveryboy_id' => 'required|exists:deliveryboys,id',
            'employeeID' => 'required|unique:deliveryboys,employee_id,'.request()->input('deliveryboy_id'),
            'email' => 'required|email|unique:deliveryboys,email,'.request()->input('deliveryboy_id'),
            'name' => 'required|string|max:50',
            'password' => 'nullable|min:6',
            'phone' => 'required|phone:AUTO,BD|unique:deliveryboys,phone,'.request()->input('deliveryboy_id'),
            'nid_card' => 'nullable|file|image',
            'profile_picture' => 'nullable|file|image',
            'area' => 'required|exists:areas,id',
            'address' => 'required',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'area.required' => 'Area is required',
            'area.exists' => 'Area is invalid',
            'email.required' => 'Email address is required',
            'email.email' => 'Email address is invalid',
            'employeeID.unique' => 'The employee id has already been taken',
            'employeeID.required' => 'Employee id is required',
            'email.unique' => 'The email address has already been taken',
            'name.required' => 'Delivery boy name is required',
            'name.string' => 'Delivery boy name is invalid',
            'name.max' => 'Delivery boy name is invalid',
            'phone.required' => 'Phone number is required',
            'phone.phone' => 'Phone number is invalid',
            'phone.unique' => 'Phone number has already been taken',
            'nid_card.file' => "Nid card file is invalid",
            'nid_card.image' => "Nid card file must be an image",
            'profile_picture.file' => "Profile picture is invalid",
            'profile_picture.image' => "Profile picture must be an image",
            'address.required' => "Address is required",
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'deliveryboy_id' => 'trim|escape|strip_tags',
            'employeeID' => 'trim|escape|strip_tags',
            'name' => 'trim|escape|strip_tags',
            'area' => 'trim|escape|strip_tags',
            'password' => 'trim|escape|strip_tags',
            'phone' => 'trim|escape|strip_tags',
            'agreement_date' => 'trim|escape|strip_tags',
            'email' => 'trim|lowercase',
            'address' => 'trim|escape|strip_tags',
        ];
    }
}
