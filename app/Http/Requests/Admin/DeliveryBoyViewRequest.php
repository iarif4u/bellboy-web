<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\BaseFormRequest;

class DeliveryBoyViewRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return ['delivery_boy_id' => 'required|exists:deliveryboys,id'];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'delivery_boy_id.required' => 'Delivery boy is required',
            'delivery_boy_id.exists' => 'Delivery boy is invalid'
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'delivery_boy_id' => 'trim|escape|strip_tags|digit'
        ];
    }
}
