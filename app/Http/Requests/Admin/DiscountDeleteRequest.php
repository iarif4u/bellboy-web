<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\BaseFormRequest;

class DiscountDeleteRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return ['discount_id' => 'required|exists:discounts,id'];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'discount_id.required' => 'Discount id is required',
            'discount_id.exists' => 'Discount id is invalid'
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'discount_id' => 'trim|escape|strip_tags'
        ];
    }
}
