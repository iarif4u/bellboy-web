<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\BaseFormRequest;

class DiscountUpdateRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'discount_id' => 'required|exists:discounts,id',
            'discount_name' => 'required|unique:discounts,discount_name,'.request()->input('discount_id'),
            'discount_type' => 'required|string|in:'.implode(",",array_values(config('const.discount'))),
            'discount' => 'required|numeric',
            'valid_till' => 'required|date|date_format:Y-m-d|after_or_equal:today',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'discount_id.required' => 'Discount id is required',
            'discount_id.exists' => 'Discount id is invalid',
            'discount_name.required' => 'Discount name is required',
            'discount_name.unique' => 'Discount name has already been taken',
            'discount_type.required' => 'Discount type is required',
            'discount_type.string' => 'Discount type is invalid',
            'discount_type.in' => 'Discount type is unknown',
            'discount.required' => 'Discount is required',
            'discount.numeric' => 'Discount is invalid',
            'valid_till.required' => 'Discount last date is required',
            'valid_till.date' => 'Discount last date is invalid',
            'valid_till.date_format' => 'Discount last date has invalid format',
            'valid_till.before_or_equal' => "Discount last date can't be before today"
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'discount_name' => 'trim|escape|strip_tags|capitalize',
            'discount_type' => 'trim|escape|strip_tags',
            'discount_id' => 'trim|escape|strip_tags',
            'discount' => 'trim|escape|strip_tags',
            'valid_till' => 'trim|escape|strip_tags'
        ];
    }
}
