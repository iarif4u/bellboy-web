<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\BaseFormRequest;

class ExpenseCategoryUpdateRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id' => 'required|exists:expense_categories,id',
            'category' => 'required|unique:expense_categories,category,'.request()->input('category_id').',id,deleted_at,NULL',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'category.required' => 'Category name is required',
            'category.unique' => 'Category name has already been taken',
            'category_id.required' => 'Category is required',
            'category_id.exists' => 'Category not found'
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'category' => 'trim|escape|strip_tags',
        ];
    }
}
