<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\BaseFormRequest;

class ExpenseInfoRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return ['expense_id' => 'required|exists:account_expenses,id'];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'expense_id.required' => 'Expense is required',
            'expense_id.exists' => 'Expense is invalid',
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'expense_id' => 'trim|escape|strip_tags'
        ];
    }
}
