<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\BaseFormRequest;

class ExpenseStoreRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'expense_name' => 'required|max:191',
            'expense_category' => 'required|exists:expense_categories,id',
            'expense_amount' => 'required|numeric',
            'date' => 'required|date|date_format:Y-m-d|before_or_equal:today',
            'expense_note' => 'nullable',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'expense_category.required' => 'Expense category is required',
            'expense_category.exists' => 'Expense category is invalid',
            'expense_name.required' => 'Expense name is required',
            'expense_name.max' => 'Expense name is invalid',
            'expense_amount.required' => 'Expense amount is required',
            'expense_amount.numeric' => 'Expense amount is invalid',
            'date.required' => 'Date is required',
            'date.date' => 'Date is invalid',
            'date.date_format' => 'Date has invalid format',
            'date.before_or_equal' => "Date can't be after today",
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'expense_category' => 'trim|escape|strip_tags',
            'expense_name' => 'trim|escape|strip_tags',
            'expense_amount' => 'trim|escape|strip_tags',
            'expense_note' => 'trim|escape|strip_tags',
            'date' => 'trim|escape|strip_tags'
        ];
    }
}
