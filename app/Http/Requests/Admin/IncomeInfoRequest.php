<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\BaseFormRequest;

class IncomeInfoRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return ['income_id' => 'required|exists:account_incomes,id'];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'income_id.required' => 'Income name is required',
            'income_id.exists' => 'Income name is invalid',
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'income_id' => 'trim|escape|strip_tags'
        ];
    }
}
