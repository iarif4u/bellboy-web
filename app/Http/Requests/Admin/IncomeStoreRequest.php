<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\BaseFormRequest;

class IncomeStoreRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'income_name' => 'required|max:191',
            'amount' => 'required|numeric',
            'date' => 'required|date|date_format:Y-m-d|before_or_equal:today',
            'note' => 'nullable',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'income_name.required' => 'Income name is required',
            'income_name.max' => 'Income name is invalid',
            'amount.required' => 'Income amount is required',
            'amount.numeric' => 'Income amount is invalid',
            'date.required' => 'Date is required',
            'date.date' => 'Date is invalid',
            'date.date_format' => 'Date has invalid format',
            'date.before_or_equal' => "Date can't be after today",
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'note' => 'trim|escape|strip_tags',
            'date' => 'trim|escape|strip_tags',
            'amount' => 'trim|escape|strip_tags',
            'income_name' => 'trim|escape|strip_tags'
        ];
    }
}
