<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\BaseFormRequest;

class MailSettingsRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mailHost' => 'required|max:191',
            'mailPort' => 'required|numeric',
            'mailAddress' => 'required|email',
            'mailPassword' => 'required',
            'fromName' => 'required|string',
            'encryption' => 'required'
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'mailHost.required' => 'Mail host is required',
            'mailHost.max' => 'Mail host is invalid',
            'mailPort.required' => 'Mail port is required',
            'mailPort.numeric' => 'Mail port is invalid',
            'mailPassword.required' => 'Mail password is required',
            'mailAddress.required' => 'Mail address is required',
            'mailAddress.email' => 'Mail address is invalid',
            'encryption.required' => 'Encryption is required',
            'fromName.required' => 'From name is required',
            'fromName.string' => 'From name is invalid',
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'mailHost' => 'trim|escape|strip_tags',
            'mailPort' => 'trim|escape|strip_tags',
            'mailAddress' => 'trim|escape|strip_tags',
            'mailPassword' => 'trim|escape|strip_tags',
            'fromName' => 'trim|escape|strip_tags',
            'encryption' => 'trim|escape|strip_tags',
        ];
    }
}
