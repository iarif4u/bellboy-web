<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\BaseFormRequest;

class NotificationRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'notification_to' => 'required',
            'title' => 'required',
            'body' => 'required',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'title.required' => 'Notification title is required',
            'body.required' => 'Notification body is required',
            'notification_to.required' => 'Notification to is required',
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'title' => 'trim|escape|strip_tags',
            'notification_to' => 'trim|escape|strip_tags',
            'body' => 'trim|escape|strip_tags',
        ];
    }
}
