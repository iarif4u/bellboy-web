<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\BaseFormRequest;

class ProductAgentAssignRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'variation_id' => 'required|exists:products_variants,id',
            'assign_agent' => 'required|exists:agents,id',
            'stock' => 'required|numeric',
            'cost' => 'required|numeric'
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'cost.required' => 'Product cost is required',
            'cost.numeric' => 'Product cost is invalid',
            'stock.required' => 'Product stock is required',
            'stock.numeric' => 'Product stock is invalid',
            'assign_agent.required' => 'Agent is required',
            'assign_agent.exists' => 'Agent is invalid',
            'variation_id.required' => 'Variation is required',
            'variation_id.exists' => 'Variation is invalid',
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'assign_agent' => 'trim|escape|strip_tags|digit',
            'variation_id' => 'trim|escape|strip_tags|digit',
            'stock' => 'trim|escape|strip_tags|digit',
            'cost' => 'trim|escape|strip_tags|digit'
        ];
    }
}
