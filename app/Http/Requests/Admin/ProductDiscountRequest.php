<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\BaseFormRequest;

class ProductDiscountRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_id' => 'required|exists:products,id',
            'discount' => 'nullable|exists:discounts,id'
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'product_id.required' => 'Product is required',
            'product_id.exists' => 'Product is invalid',
            'discount.required' => 'Discount is required',
            'discount.exists' => 'Discount is invalid'
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'product_id' => 'trim|escape|strip_tags|digit',
            'product_status' => 'trim|escape|strip_tags|lowercase'
        ];
    }
}
