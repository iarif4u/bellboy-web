<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\BaseFormRequest;

class ProductOfferApproveRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'offer_id' => 'required|exists:agent_offers,id',
            'cost' => 'required|numeric',
            'stock' => 'required|numeric',
            'price' => 'required|numeric',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'cost.required' => 'Product cost is required',
            'cost.numeric' => 'Product cost is invalid',
            'price.required' => 'Product price is required',
            'price.numeric' => 'Product price is invalid',
            'offer_id.required' => 'Product offer is required',
            'offer_id.exists' => 'Product offer is invalid',
            'stock.required' => 'Product stock is required',
            'stock.numeric' => 'Product stock is invalid',
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'offer_id' => 'trim|escape|strip_tags|digit',
            'stock' => 'trim|escape|strip_tags|digit',
            'cost' => 'trim|escape|strip_tags|digit'
        ];
    }
}
