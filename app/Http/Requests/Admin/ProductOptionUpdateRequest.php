<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\BaseFormRequest;

class ProductOptionUpdateRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'option_id' => 'required|exists:product_options,id',
            'option_name' => 'required|unique:product_options,name,'.request()->input('option_id'),
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'option_id.required' => 'Option is required',
            'option_id.exists' => 'Option is invalid',
            'option_name.required' => 'Option name is required',
            'option_name.unique' => 'Option name has already been taken'
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'option_id' => 'trim|escape|strip_tags',
            'option_name' => 'trim|escape|strip_tags',
        ];
    }
}
