<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\BaseFormRequest;

class ProductOptionValueUpdateRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_option' => 'required|exists:product_options,id',
            'option_value_id' => 'required|exists:product_option_values,id',
            'option_value' => 'required|unique:product_option_values,value,'.request()->input('option_value_id'),
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'option_value_id.required' => 'Option value is required',
            'option_value_id.exists' => 'Option value is invalid',
            'product_option.required' => 'Option name is required',
            'product_option.exists' => 'Option name is invalid',
            'option_value.required' => 'Option value is required',
            'option_value.unique' => 'Option value has already been taken'
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'product_option' => 'trim|escape|strip_tags',
            'option_value' => 'trim|escape|strip_tags',
            'option_value_id' => 'trim|escape|strip_tags',
        ];
    }
}
