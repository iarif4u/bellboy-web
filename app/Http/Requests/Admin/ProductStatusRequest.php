<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\BaseFormRequest;

class ProductStatusRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_id' => 'required|exists:products,id',
            'product_status' => 'required|string|in:'.implode(',',config('const.product.status'))
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'product_id.required' => 'Product is required',
            'product_id.exists' => 'Product is invalid',
            'product_status.required' => 'Product status is required',
            'product_status.string' => 'Product status is invalid'
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'product_id' => 'trim|escape|strip_tags|digit',
            'product_status' => 'trim|escape|strip_tags|lowercase'
        ];
    }
}
