<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\BaseFormRequest;

class ProductStoreRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_name' => 'required|unique:products,product_name',
            'product_code' => 'required|unique:products,product_code',
            'brand' => 'nullable|exists:brands,id',
            'discount' => 'nullable|exists:discounts,id',
            'product_description' => 'nullable',
            'laundry' => 'required|boolean',
            'categories' => 'required|array',
            'categories.*' => 'required|exists:categories,id',
            'properties_name' => 'nullable|array',
            'properties_name.*' => 'nullable|exists:properties,id|distinct',
            'properties_value' => 'nullable|array',
            'properties_value.*' => 'nullable',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'product_description.required' => 'Product description is required',
            'product_name.required' => 'Product name is required',
            'laundry.required' => 'Product type is required',
            'laundry.boolean' => 'Product type is invalid',
            'product_name.unique' => 'The product name has already been taken',
            'brand.exists' => 'Brand name is invalid',
            'discount.exists' => 'Discount is invalid',
            'categories.required' => 'Categories name is required',
            'categories.*.required' => 'Category name is required',
            'categories.*.exists' => 'Category name not found or invalid',
            'categories.array' => 'Categories name is invalid',
            'properties_name.array' => 'Properties name is invalid',
            'properties_name.*.exists' => 'Properties name is invalid',
            'properties_name.*.distinct' => "Properties name can't be one more times",
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'product_name' => 'trim|escape|strip_tags|capitalize',
            'product_code' => 'trim|escape|strip_tags',
            'brand' => 'trim|escape|strip_tags',
            'discount' => 'trim|escape|strip_tags',
            'product_description' => 'trim|escape|strip_tags',
            'categories' => 'trim|escape',
            'properties_name' => 'trim|escape|strip_tags',
            'properties_value' => 'trim|escape|strip_tags',
        ];
    }
}
