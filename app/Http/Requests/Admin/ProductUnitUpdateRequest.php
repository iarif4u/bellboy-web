<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\BaseFormRequest;

class ProductUnitUpdateRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'unit_id' => 'required|exists:product_units,id',
            'unit_name' => 'required|unique:product_units,unit_name,'.request()->input('unit_id')
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'unit_id.required' => 'Unit id is required',
            'unit_id.exists' => 'Unit id is invalid',
            'unit_name.required' => 'Unit name is required',
            'unit_name.unique' => 'Unit name has already been taken',
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'unit_name' => 'trim|escape|strip_tags',
            'unit_id' => 'trim|escape|strip_tags',
        ];
    }
}
