<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\BaseFormRequest;

class ProductVariationStoreRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_id' => 'required|exists:products,id',
            'variation_unit' => 'required|exists:product_units,id',
            'variation_name' => 'required',
            'variation_unit_value' => 'required',
            'sku' => 'required|unique:products_variants,sku',
            'product_cost' => 'required',
            'product_price' => 'required',
            'option_name' => 'nullable|array',
            'option_name.*' => 'nullable|exists:product_options,id',
            'option_value' => 'nullable|array',
            'option_value.*' => 'nullable|exists:product_option_values,id|distinct',
            'product_image' => 'required|file|image'
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'product_image.required' => 'Product image is required',
            'product_image.file' => 'Product image is invalid',
            'product_image.image' => 'Product image file must be an image',
            'variation_name.required' => 'Product variation name is required',
            'sku.required' => 'SKU is required',
            'sku.unique' => 'The SKU has already been taken',
            'product_id.required' => 'Product is required',
            'product_id.exists' => 'Product is invalid',
            'product_cost.numeric' => 'Product cost is invalid',
            'product_price.numeric' => 'Product price is invalid',
            'option_name.required' => 'Variation option name is required',
            'option_name.array' => 'Variation option name must be an array',
            'option_name.*.required' => 'Variation option name is required',
            'option_name.*.exists' => 'Variation option name not found or invalid',
            'option_value.array' => 'Variation option value is invalid',
            'option_value.required' => 'Variation option value is invalid',
            'option_value.*.exists' => 'Variation option value is invalid',
            'option_value.*.distinct' => "Variation option value can't be one more times",
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'product_id' => 'trim|escape|strip_tags',
            'variation_name' => 'trim|escape|strip_tags',
            'sku' => 'trim|escape|strip_tags',
            'product_cost' => 'trim|escape|strip_tags',
            'product_price' => 'trim|escape',
            'option_name' => 'trim|escape|strip_tags',
            'option_value' => 'trim|escape|strip_tags',
        ];
    }
}
