<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\BaseFormRequest;

class ProductVariationUpdateRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'variant_id' => 'required|exists:products_variants,id',
            'product_id' => 'required|exists:products,id',
            'variation_unit' => 'required|exists:product_units,id',
            'variation_unit_value' => 'required',
            'variation_name' => 'required',
            'sku' => 'required|unique:products_variants,sku,'.request()->input('variant_id'),
            'product_cost' => 'nullable|numeric',
            'product_price' => 'nullable|numeric',
            'product_image' => 'nullable|file|image'
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'variant_id.required' => 'Product variant is required',
            'variant_id.exists' => 'Product variant not found',
            'product_image.required' => 'Product image is required',
            'product_image.file' => 'Product image is invalid',
            'product_image.image' => 'Product image file must be an image',
            'variation_name.required' => 'Product variation name is required',
            'sku.required' => 'SKU is required',
            'sku.unique' => 'The SKU has already been taken',
            'product_id.required' => 'Product is required',
            'product_id.exists' => 'Product is invalid',
            'product_cost.numeric' => 'Product cost is invalid',
            'product_price.numeric' => 'Product price is invalid',
            'option_name.required' => 'Variation option name is required',
            'option_name.array' => 'Variation option name must be an array',
            'option_name.*.required' => 'Variation option name is required',
            'option_name.*.exists' => 'Variation option name not found or invalid',
            'option_value.array' => 'Variation option value is invalid',
            'option_value.required' => 'Variation option value is invalid',
            'option_value.*.exists' => 'Variation option value is invalid',
            'option_value.*.distinct' => "Variation option value can't be one more times",
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'variant_id' => 'trim|escape|strip_tags',
            'product_id' => 'trim|escape|strip_tags',
            'variation_name' => 'trim|escape|strip_tags',
            'sku' => 'trim|escape|strip_tags',
            'product_cost' => 'trim|escape|strip_tags',
            'product_price' => 'trim|escape',
            'option_name' => 'trim|escape|strip_tags',
            'option_value' => 'trim|escape|strip_tags',
        ];
    }
}
