<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\BaseFormRequest;

class PropertyDeleteRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'property_id' => 'required|exists:properties,id'
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'property_id.required' => 'Property is required',
            'property_id.exists' => 'Property is invalid or not found',
            'property_name.required' => 'Property name is required'
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'property_id' => 'trim|escape|strip_tags'
        ];
    }
}
