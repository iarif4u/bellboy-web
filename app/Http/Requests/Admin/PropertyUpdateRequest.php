<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\BaseFormRequest;

class PropertyUpdateRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'property_id' => 'required|exists:properties,id',
            'property_name' => 'required|unique:properties,name,' . request()->input('property_id'),
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'property_id.required' => 'Property is required',
            'property_id.exists' => 'Property is invalid or not found',
            'property_name.required' => 'Property name is required',
            'property_name.unique' => 'Property name has already been taken',
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'property_id' => 'trim|escape|strip_tags',
            'property_name' => 'trim|escape|strip_tags',
        ];
    }
}
