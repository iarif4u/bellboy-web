<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\BaseFormRequest;

class SMSSettingsRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'url' => 'required|max:191',
            'method' => 'required|max:191|in:GET,POST',
            'send_to_param_name' => 'required|max:191',
            'msg_param_name' => 'required|max:191',
            'header_name' => 'nullable|required_with:header_value|max:191|array',
            'header_name.*' => 'nullable|required_with:header_value.*|max:191|distinct',
            'header_value' => 'nullable|required_with:header_name|max:191|array',
            'header_value.*' => 'nullable|required_with:header_name.*|max:191',
            'option_name' => 'nullable|required_with:option_value|max:191|array',
            'option_name.*' => 'nullable|required_with:option_value.*|distinct',
            'option_value' => 'nullable|required_with:option_name|max:191|array',
            'option_value.*' => 'nullable|required_with:option_name.*|max:191',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'option_value.array' => 'Option value must be an array',
            'option_value.required_with' => 'Option value is required when option name given',
            'option_value.max' => 'Option value is invalid',
            'header_value.array' => 'Header value must be an array',
            'header_value.required_with' => 'Header value is required when header name given',
            'header_value.max' => 'Header value is invalid',
            'option_name.array' => 'Option name must be an array',
            'option_name.required_with' => 'Option name is required when option value given',
            'option_name.max' => 'Option name is invalid',
            'option_name.*.distinct' => 'Option name can not be duplicate',
            'header_name.required_with' => 'Header name is required when header value given',
            'header_name.max' => 'Header name is invalid',
            'header_name.*.distinct' => 'Header name can not be duplicate',
            'header_name.array' => 'Header name must be an array',
            'url.required' => 'API URL is required',
            'url.mx' => 'API URL is invalid',
            'method.required' => 'Request method is required',
            'method.max' => 'Request method is invalid',
            'method.in' => 'Request method must be get or post',
            'send_to_param_name.required' => 'Send to param name is required',
            'send_to_param_name.max' => 'Send to param name is invalid',
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'url' => 'trim|escape|strip_tags',
            'send_to_param_name' => 'trim|escape|strip_tags',
            'method' => 'trim|escape|strip_tags',
            'header_name' => 'trim|escape|strip_tags',
            'header_name.*' => 'trim|escape|strip_tags',
            'header_value' => 'trim|escape|strip_tags',
            'header_value.*' => 'trim|escape|strip_tags',
            'option_name' => 'trim|escape|strip_tags',
            'option_name.*' => 'trim|escape|strip_tags',
            'option_value' => 'trim|escape|strip_tags',
            'option_value.*' => 'trim|escape|strip_tags',
        ];
    }
}
