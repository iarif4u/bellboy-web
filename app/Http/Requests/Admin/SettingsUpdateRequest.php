<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\BaseFormRequest;

class SettingsUpdateRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'site_title' => 'required|max:191',
            'copyright' => 'required|max:191',
            'brief_info' => 'required|max:191',
            'delivery_charge' => 'required|max:191',
            'laundry_charge' => 'required|max:191',
            'phone' => 'required|phone:AUTO,BD',
            'timeZone' => 'required|max:191|timezone',
            'site_logo' => 'nullable|file|image',
            'favicon' => 'nullable|file|image'
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'delivery_charge.required' => 'Delivery charge is required',
            'laundry_charge.required' => 'Laundry charge is required',
            'brief_info.required' => 'Brief info is required',
            'brief_info.max' => 'Brief info is invalid',
            'site_title.required' => 'Site title is required',
            'site_title.max' => 'Site title is invalid',
            'copyright.required' => 'Copyright is required',
            'copyright.max' => 'Copyright is invalid',
            'timeZone.required' => 'Timezone is required',
            'timeZone.max' => 'Timezone is invalid',
            'favicon.file' => 'Favicon is invalid',
            'site_logo.file' => 'Site logo is invalid',
            'site_logo.image' => 'Site logo must be a valid image',
            'favicon.image' => 'Favicon must be a valid image',
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'brief_info' => 'trim|escape|strip_tags',
            'delivery_charge' => 'trim|escape|strip_tags',
            'laundry_charge' => 'trim|escape|strip_tags',
            'site_title' => 'trim|escape|strip_tags',
            'copyright' => 'trim|escape|strip_tags',
            'timeZone' => 'trim|escape|strip_tags',
            'phone' => 'trim|escape|strip_tags',
        ];
    }
}
