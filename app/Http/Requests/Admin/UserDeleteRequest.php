<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\BaseFormRequest;
use Illuminate\Support\Arr;

class UserDeleteRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user_type = request()->input('user_type');
        $tables = config('const.tables');
        return [
            'user_id' => 'required|exists:' . $tables[$user_type] . ',id',
            'user_type' => 'required|string|in:' . implode(',', config('const.users')),
        ];

    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'user_id.required' => 'User is required',
            'user_id.exists' => 'User not found',
            'user_type.string' => "User type is invalid",
            'user_type.in' => "User type is unknown",
            'user_type.required' => "User type is required"
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'user_id' => 'trim|escape',
            'user_type' => 'trim|escape|strip_tags',
        ];
    }
}
