<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\BaseFormRequest;

class UserStoreRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|unique:users,email|unique:callcenters,email|unique:managers,email',
            'name' => 'required|string|max:50',
            'password' => 'required|min:6',
            'phone' => 'required|phone:AUTO,BD|unique:users,phone|unique:callcenters,phone|unique:managers,phone',
            'user_type' => 'required|string|in:'.implode(',',config('const.users')),
            'profile_picture' => 'required|file|image'
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'email.required' => 'Email address is required',
            'email.email' => 'Email address is invalid',
            'email.unique' => 'The email address has already been taken',
            'name.required' => 'User name is required',
            'name.string' => 'User name is invalid',
            'name.max' => 'User name is invalid',
            'password.required' => 'Password is required',
            'phone.required' => 'Phone number is required',
            'phone.phone' => 'Phone number is invalid',
            'phone.unique' => 'Phone number has already been taken',
            'user_type.string' => "User type is invalid",
            'user_type.in' => "User type is unknown",
            'user_type.required' => "User type is required",
            'profile_picture.required' => "User profile picture is required",
            'profile_picture.file' => "User profile picture is invalid",
            'profile_picture.image' => "User profile picture must be an image",
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'name' => 'trim|escape|strip_tags|capitalize',
            'password' => 'trim|escape|strip_tags',
            'phone' => 'trim|escape|strip_tags',
            'email' => 'trim|lowercase',
            'user_type' => 'trim|escape|strip_tags',
        ];
    }
}
