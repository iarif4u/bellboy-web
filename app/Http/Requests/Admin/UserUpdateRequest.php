<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\BaseFormRequest;
use Illuminate\Support\Arr;

class UserUpdateRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $type = request()->segment(4);
        $user_id = request()->segment(2);
        $user_type = request()->input('user_type');
        $tables = config('const.tables');

        if ($user_type == $type) {
            return [
                'email' => 'required|email|unique:' . $tables[$type] . ',email,' . $user_id,
                'name' => 'required|string|max:50',
                'password' => 'nullable|min:6',
                'phone' => 'required|phone:AUTO,BD|unique:' . $tables[$type] . ',phone,' . $user_id,
                'user_type' => 'required|string|in:' . implode(',', config('const.users')),
                'profile_picture' => 'nullable|file|image'
            ];
        } else {
            $email_rules = "required|email|unique:" . implode(",email|unique:", Arr::except(config('const.tables'), $type)) . ",email";
            $phone_rules = "required|phone:AUTO,BD|unique:" . implode(",phone|unique:", Arr::except(config('const.tables'), $type)) . ",phone";
            return [
                'email' => $email_rules,
                'name' => 'required|string|max:50',
                'password' => 'nullable|min:6',
                'phone' => $phone_rules,
                'user_type' => 'required|string|in:' . implode(',', config('const.users')),
                'profile_picture' => 'nullable|file|image'
            ];
        }
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'email.required' => 'Email address is required',
            'email.email' => 'Email address is invalid',
            'email.unique' => 'The email address has already been taken',
            'name.required' => 'User name is required',
            'name.string' => 'User name is invalid',
            'name.max' => 'User name is invalid',
            'password.required' => 'Password is required',
            'phone.required' => 'Phone number is required',
            'phone.phone' => 'Phone number is invalid',
            'phone.unique' => 'Phone number has already been taken',
            'user_type.string' => "User type is invalid",
            'user_type.in' => "User type is unknown",
            'user_type.required' => "User type is required",
            'profile_picture.required' => "User profile picture is required",
            'profile_picture.file' => "User profile picture is invalid",
            'profile_picture.image' => "User profile picture must be an image",
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'name' => 'trim|escape|strip_tags|capitalize',
            'password' => 'trim|escape|strip_tags',
            'phone' => 'trim|escape|strip_tags',
            'email' => 'trim|lowercase',
            'user_type' => 'trim|escape|strip_tags',
        ];
    }
}
