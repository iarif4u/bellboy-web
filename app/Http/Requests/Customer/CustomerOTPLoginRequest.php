<?php

namespace App\Http\Requests\Customer;

use App\Http\Requests\BaseFormRequest;

class CustomerOTPLoginRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => 'required|phone:AUTO,BD',
            'otp_code' => 'required|exists:o_t_p_codes,otp_code'
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'otp_code.required' => 'OTP code is required',
            'otp_code.exists' => 'OTP code is invalid',
            'phone.required' => 'Phone number is required',
            'phone.phone' => 'Phone number is invalid',
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'phone' => 'trim|escape|strip_tags',
            'otp_code' => 'trim|escape|strip_tags',
        ];
    }
}
