<?php

namespace App\Http\Requests\Customer;

use App\Http\Requests\BaseFormRequest;

class CustomerOTPRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => 'required|phone:AUTO,BD'
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'phone.required' => 'Phone number is required',
            'phone.phone' => 'Phone number is invalid',
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'phone' => 'trim|escape|strip_tags',
        ];
    }
}
