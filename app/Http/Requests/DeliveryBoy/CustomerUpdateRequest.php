<?php

namespace App\Http\Requests\DeliveryBoy;

use App\Http\Requests\BaseFormRequest;

class CustomerUpdateRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customer_id' => 'required|exists:customers,id',
            'order_id' => 'required|exists:orders,id',
            'name' => 'required',
            'address' => 'required',
        ];
    }
    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'customer_id' => 'trim|escape|strip_tags|digit',
            'order_id' => 'trim|escape|strip_tags|digit',
            'name' => 'trim|escape|strip_tags',
            'address' => 'trim|escape|strip_tags',
            'phone' => 'trim|escape|strip_tags|digit',
        ];
    }
}
