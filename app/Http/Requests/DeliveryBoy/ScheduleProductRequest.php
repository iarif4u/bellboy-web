<?php

namespace App\Http\Requests\DeliveryBoy;

use App\Http\Requests\BaseFormRequest;

class ScheduleProductRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order_item_id' => 'required|exists:orders,id',
            'date' => 'required|date|date_format:Y-m-d|after_or_equal:today'
        ];
    }
    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'order_item_id' => 'trim|escape|strip_tags|digit',
            'date' => 'trim|escape|strip_tags',
        ];
    }
}
