<?php

namespace App\Http\Requests\Manager;

use App\Http\Requests\BaseFormRequest;

class OrderApproveRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order_id' => 'required|exists:orders,id',
            'delivery_boy' => 'required|exists:deliveryboys,id',
            'customer_address' => 'required',
            'customer_name' => 'required',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'customer_name.required' => 'Customer name is required',
            'customer_address.required' => 'Customer address is required',
            'order_id.required' => 'Order is required',
            'order_id.exists' => 'Order is invalid',
            'delivery_boy.required' => 'Delivery boy is required',
            'delivery_boy.exists' => 'Delivery boy is invalid',
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'order_id' => 'trim|escape|strip_tags',
            'delivery_boy' => 'trim|escape|strip_tags',
        ];
    }
}
