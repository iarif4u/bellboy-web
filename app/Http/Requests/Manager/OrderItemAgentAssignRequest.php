<?php

namespace App\Http\Requests\Manager;

use App\Http\Requests\BaseFormRequest;

class OrderItemAgentAssignRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order_item' => 'required|exists:order_items,id',
            'agent' => 'required|exists:agents,id',
            "variation_id" => 'required|exists:products_variants,id',
            "item_cost" => 'required',
            "item_stock" => "required"
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'agent.required' => 'Agent is required',
            'order_item.required' => 'Order item is required',
            'order_item.exists' => 'Order item is invalid',
            'agent.exists' => 'Agent is invalid',
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'agent' => 'trim|escape|strip_tags',
            'order_item' => 'trim|escape|strip_tags',
        ];
    }
}
