<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order_id' => 'required|exists:orders,id',
            'coupon' => 'nullable|exists:coupon_codes,id',
            'customer_name' => 'required',
            'customer_address' => 'required',
            'order_item_id' => 'required|array',
            'order_item_id.*' => 'required|exists:order_items,id',
        ];
    }
    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'customer_name.required' => 'Customer name is required',
            'customer_address.required' => 'Customer address is required',
            'order_id.required' => 'Order is required',
            'order_id.exists' => 'Order is invalid',
            'coupon.exists' => 'Coupon code is invalid',
            'order_item_id.required' => 'Order item is required',
            'order_item_id.*.required' => 'Order item is required',
            'order_item_id.array' => 'Order item is invalid',
            'order_item_id.*.exists' => 'Order item is invalid',
        ];
    }
    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'order_id' => 'trim|escape|strip_tags|digit',
            'order_item_id.*' => 'trim|escape|strip_tags|digit',
            'coupon' => 'trim|escape|strip_tags'
        ];
    }
}
