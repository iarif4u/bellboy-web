<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AccountExpense extends Model
{
    use SoftDeletes;
    protected $fillable = ["expense_name", "user_id", "category_id", "expense_amount", "reference", "date"];

    /**
     * Get the expense note
     */
    public function note()
    {
        return $this->morphOne(Note::class, 'notable');
    }

    public function category(){
        return $this->hasOne(ExpenseCategory::class,'id','category_id');
    }
}
