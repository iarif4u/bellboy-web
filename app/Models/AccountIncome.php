<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AccountIncome extends Model
{
    use SoftDeletes;
    protected $fillable = ["income_name","user_id", "income_amount", "reference", "date"];

    /**
     * Get the income note
     */
    public function note()
    {
        return $this->morphOne(Note::class, 'notable');
    }
}
