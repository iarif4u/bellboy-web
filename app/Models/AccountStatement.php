<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AccountStatement extends Model
{
    use SoftDeletes;
    protected $fillable = ["type", "amount", "calculate_amount", "ref_id"];

    public function expense(){
       return $this->belongsTo(AccountExpense::class,'ref_id','id');
    }

    public function income(){
      return  $this->belongsTo(AccountIncome::class,'ref_id','id');
    }
}
