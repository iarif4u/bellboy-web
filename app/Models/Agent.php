<?php

namespace App\Models;

use App\Notifications\AgentResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notification;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Agent extends Authenticatable implements HasMedia, JWTSubject
{
    use Notifiable, HasMediaTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone', 'status', 'agreement_date',"balance","withdraw_req"
    ];

    public function balanceWithdraw(){
        return $this->hasMany(BalanceWithdraw::class,'agent_id','id');
    }
    public function conversation()
    {
        return $this->morphOne(Conversation::class, 'conversationable','creator_type','creator_id');
    }

    /**
     * Get all of the messages.
     */
    public function messages()
    {
        return $this->morphMany(Message::class, 'messageable','sender_type','sender_id');
    }



    /**
     * Get all of the firebase's token.
     */
    public function firebase_tokens()
    {
        return $this->morphMany(FirebaseToken::class, 'tokenable');
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Send the password reset notification.
     *
     * @param string $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new AgentResetPassword($token));
    }

    /**
     * @inheritDoc
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * @inheritDoc
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * Get all of the agent's profile data
     */
    public function profiles()
    {
        return $this->morphMany(ProfileData::class, 'profileable');
    }

    /**
     * Get all of the agent's profile data
     */
    public function shopName()
    {
        return $this->morphOne(ProfileData::class, 'profileable')->where('data_name', 'shop_name');
    }

    /**
     * Get all of the agent's profile data
     */
    public function shopAddress()
    {
        return $this->morphOne(ProfileData::class, 'profileable')->where('data_name', 'shop_address');
    }

    public function routeNotificationForSmsApi()
    {
        return $this->phone;
    }

    /**
     * Route notifications for the FCM channel.
     *
     * @param Notification $notification
     * @return array
     */
    public function routeNotificationForFcm($notification)
    {
        return array_values((array)$this->firebase_tokens->pluck('token'))[0];
    }

    public function orderItems(){
        return $this->hasMany(OrderItem::class,'agent_id','id');
    }
}
