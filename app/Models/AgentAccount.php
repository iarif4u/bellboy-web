<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgentAccount extends Model
{
    protected $fillable = ["agent_id","amount","order_item_id"];
}
