<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @method static updateOrCreate(array $array)
 */
class AgentOffer extends Model
{
    protected $fillable = ["variation_id", "agent_id", "offer_price","offer_stock", "negotiate_price", "message", "offer_status"];

    public function agent(){
        return $this->hasOne(Agent::class,'id','agent_id')->with(['shopName','shopAddress']);
    }

    public function variation(){
        return $this->hasOne(ProductsVariant::class,'id','variation_id')->with(['options','product','unit']);
    }
}

