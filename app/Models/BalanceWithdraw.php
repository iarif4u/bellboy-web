<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BalanceWithdraw extends Model
{
    protected $fillable =["agent_id","payment_by","amount","payment_method","bank","reference","note"];
}
