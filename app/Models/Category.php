<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Category extends Model implements HasMedia
{
    use HasSlug, HasMediaTrait;
    protected $fillable = ['name', 'slug', 'category_id'];

    public function products()
    {
        return $this->belongsToMany(Product::class, ProductCategory::class);
    }

    public function parent_category()
    {
        return $this->hasOne(Category::class, 'id', 'category_id');
    }

    public function childrenCategories()
    {
        return $this->hasMany(Category::class);
    }

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }

}
