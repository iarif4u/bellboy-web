<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{
    protected $fillable = ['title', 'creator_id', 'creator_type', 'channel_id'];

    public function participants()
    {
        return $this->hasMany(Participant::class, 'conversation_id', 'id');
    }

    public function agents()
    {
        return $this->hasMany(Agent::class, 'id', 'creator_id');
    }

    public function customers()
    {
        return $this->hasMany(Customer::class, 'id', 'creator_id');
    }
    public function deliveryboys()
    {
        return $this->hasMany(Deliveryboy::class, 'id', 'creator_id');
    }

    public function messages()
    {
        return $this->hasMany(Message::class, 'conversation_id', 'id')->orderByDesc('id');
    }

    public function messageList()
    {
        return $this->hasMany(Message::class, 'conversation_id', 'id');
    }

    /**
     * Get the owning imageable model.
     */
    public function conversationable()
    {
        return $this->morphTo();
    }
}
