<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CouponCode extends Model
{
    protected $fillable = ["coupon_code","type","customer_times","expire_date","amount","max_amount","qty"];
}
