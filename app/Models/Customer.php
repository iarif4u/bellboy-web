<?php

namespace App\Models;

use App\Notifications\CustomerResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notification;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Customer extends Authenticatable implements JWTSubject,HasMedia
{
    use Notifiable,HasMediaTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'phone', 'address', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get all of the post's comments.
     */
    public function firebase_tokens()
    {
        return $this->morphMany(FirebaseToken::class, 'tokenable');
    }
    /**
     * Route notifications for the FCM channel.
     *
     * @param Notification $notification
     * @return array
     */
    public function routeNotificationForFcm($notification)
    {
        return array_values((array)$this->firebase_tokens->pluck('token'))[0];
    }

    public function routeNotificationForSmsApi() {
        return trim($this->phone,'+');
    }
    /**
     * Send the password reset notification.
     *
     * @param string $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new CustomerResetPassword($token));
    }

    /**
     * @inheritDoc
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * @inheritDoc
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function conversation()
    {
        return $this->morphOne(Conversation::class, 'conversationable','creator_type','creator_id');
    }

    /**
     * Get all of the post's comments.
     */
    public function messages()
    {
        return $this->morphMany(Message::class, 'messageable','sender_type','sender_id');
    }

    /**
     * Get all of the post's comments.
     */
    public function otpCodes()
    {
        return $this->morphMany(OTPCode::class, 'otpable');
    }

    public function orders(){
        return $this->hasMany(Order::class,'customer_id','id');
    }

}
