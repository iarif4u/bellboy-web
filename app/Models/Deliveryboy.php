<?php

namespace App\Models;

use App\Notifications\DeliveryboyResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notification;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Deliveryboy extends Authenticatable implements JWTSubject,HasMedia
{
    use Notifiable,HasMediaTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'employee_id', 'name', 'email', 'password', 'phone', 'area_id', 'status' ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Send the password reset notification.
     *
     * @param string $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new DeliveryboyResetPassword($token));
    }


    /**
     * Get all of the post's comments.
     */
    public function firebase_tokens()
    {
        return $this->morphMany(FirebaseToken::class, 'tokenable');
    }
    /**
     * Get all of the delivery boy's profile data
     */
    public function profiles()
    {
        return $this->morphMany(ProfileData::class, 'profileable');
    }

    /**
     * Get all of the delivery boy's area
     */
    public function area()
    {
        return $this->hasOne(Area::class, 'id','area_id');
    }

    /**
     * Get the deliveryboy address
     */
    public function address()
    {
        return $this->morphOne(ProfileData::class, 'profileable')->where('data_name','address');
    }

    /**
     * @inheritDoc
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * @inheritDoc
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function conversation()
    {
        return $this->morphOne(Conversation::class, 'conversationable','creator_type','creator_id');
    }

    /**
     * Get all of the messages.
     */
    public function messages()
    {
        return $this->morphMany(Message::class, 'messageable','sender_type','sender_id');
    }
    /**
     * Route notifications for the FCM channel.
     *
     * @param Notification $notification
     * @return array
     */
    public function routeNotificationForFcm($notification)
    {
        return array_values((array)$this->firebase_tokens->pluck('token'))[0];
    }

}
