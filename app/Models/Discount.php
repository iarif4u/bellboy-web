<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    protected $fillable = ["discount_name", "discount_type", "discount", "valid_till", "status", "added_by"];
}
