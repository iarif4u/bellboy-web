<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FirebaseToken extends Model
{
    protected $fillable = ["token", "tokenable_id", "tokenable_type"];

    /**
     * Get all of the owning tokenable models.
     */
    public function tokenable()
    {
        return $this->morphTo();
    }
}
