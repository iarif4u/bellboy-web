<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = ['guid', 'conversation_id', 'sender_id', 'sender_type', 'message_type', 'message'];

    public function conversion()
    {
        return $this->hasOne(Conversation::class, 'id', 'conversation_id');
    }

    /**
     * Get the owning messageable model.
     */
    public function sender()
    {
        return $this->morphTo();
    }



}
