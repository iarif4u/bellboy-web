<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    protected $fillable = ["note", "notable_type", "notable_id"];

    public function notable()
    {
        return $this->morphTo();
    }
}
