<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OTPCode extends Model
{
    protected $fillable = ["otp_code","valid_till","otpable_id","otpable_type"];

    /**
     * Get the owning commentable model.
     */
    public function otpable()
    {
        return $this->morphTo();
    }
}
