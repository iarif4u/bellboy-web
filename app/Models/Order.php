<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ["order_id", "customer_id", "delivery_boy", "coupon_id", "grand_total", "total", "coupon_discount", "order_status", "comment", "order_modified","delivery_time", "order_value", "delivery_address","delivery_charge","laundry"];

    public function items()
    {
        return $this->hasMany(OrderItem::class, 'order_id', 'id')->with(['agent', 'variation','agent_offers']);
    }


    public function customer()
    {
        return $this->hasOne(Customer::class, 'id', 'customer_id');
    }

    public function deliveryBoy()
    {
        return $this->hasOne(Deliveryboy::class, 'id', 'delivery_boy');
    }

    public function couponCode()
    {
        return $this->hasOne(CouponCode::class, 'id', 'coupon_id');
    }
}
