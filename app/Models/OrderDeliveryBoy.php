<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderDeliveryBoy extends Model
{
    protected $fillable = ["order_id", "delivery_boy_id", "status", "status_time", "assigner_id"];
}
