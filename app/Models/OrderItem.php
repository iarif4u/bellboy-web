<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $fillable = ["order_id", "variation_id", "quantity", "delivered_quantity","delivery_date","delivered_time", "cost", "price", "agent_id", "discount_id", "discount_amount", "sub_total", "item_status", "item_value"];

    public function agent(){
        return $this->hasOne(Agent::class, 'id', 'agent_id')->with(['shopAddress', 'shopName']);
    }
    public function agent_offers(){
        return $this->hasMany(AgentOffer::class, 'variation_id', 'variation_id')->with(['agent'])->where(['offer_status' => config('const.agent_status.active')]);
    }
    public function agentAccount(){
        return $this->hasMany(AgentAccount::class, 'agent_id', 'agent_id');
    }
    public function order(){
        return $this->hasOne(Order::class, 'id', 'order_id')->with(['deliveryBoy','customer']);
    }
    public function variation(){
        return $this->hasOne(ProductsVariant::class, 'id', 'variation_id')->with('unit');
    }
}
