<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    protected $fillable = ["product_name", "product_code", "product_brand", "laundry", "description", 'product_discount', "added_by", "in_stock", "status"];

    public function categories()
    {
        return $this->belongsToMany(Category::class, ProductCategory::class)->withTimestamps();;
    }

    public function properties()
    {
        return $this->hasMany(ProductPropertiesValue::class, 'product_id', 'id')->with('property');
    }

    public function brand()
    {
        return $this->hasOne(Brand::class, 'id', 'product_brand');
    }

    public function discount()
    {
        return $this->hasOne(Discount::class, 'id', 'product_discount');
    }

    public function valid_discount()
    {
        return $this->hasOne(Discount::class, 'id', 'product_discount')
            ->where('status', config('const.status.active'))->where('valid_till', ">=", Carbon::today());
    }

    public function variations()
    {
        return $this->hasMany(ProductsVariant::class, 'product_id', 'id')->with('options');
    }
}
