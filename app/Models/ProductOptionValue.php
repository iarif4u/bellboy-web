<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductOptionValue extends Model
{
    protected $fillable = ["option_id", "value"];

    public function option(){
        return $this->hasOne(ProductOption::class,'id','option_id');
    }
    public function variants()
    {
        return $this->belongsToMany(ProductsVariant::class, ProductsVariantsOption::class);
    }
}
