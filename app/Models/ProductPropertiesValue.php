<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductPropertiesValue extends Model
{
    protected $fillable = ["property_id", "product_id", "value"];

    public function property(){
        return $this->hasOne(Property::class,'id','property_id');
    }
}
