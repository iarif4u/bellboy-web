<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class ProductsVariant extends Model implements HasMedia
{
    use HasMediaTrait;
    protected $fillable = ["product_id", "variant_name", "sku", "unit_id", "unit_value", "cost", "price", "on_hand", "available_on_demand", "status"];

    public function options()
    {
        return $this->belongsToMany(ProductOptionValue::class, ProductsVariantsOption::class)->withTimestamps()->with('option');
    }

    public function product()
    {
        return $this->hasOne(Product::class, 'id', 'product_id')->with(['valid_discount', 'brand', 'properties', 'categories']);
    }

    public function unit()
    {
        return $this->hasOne(ProductUnit::class, 'id', "unit_id");
    }
    public function order_items(){
        return $this->hasMany(OrderItem::class, 'variation_id', "id");
    }
    public function agentOffers(){
        return $this->hasMany(AgentOffer::class, 'variation_id', "id");
    }
    public function agentActiveOffers(){
        return $this->hasMany(AgentOffer::class, 'variation_id', "id")->where('offer_status','=',config('const.agent_status.active'));
    }

}
