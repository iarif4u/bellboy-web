<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductsVariantsOption extends Model
{
    protected $fillable = ["products_variant_id", "product_option_value_id"];


}
