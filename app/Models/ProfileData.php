<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProfileData extends Model
{
    protected $fillable = ['data_name', 'data_value', 'profileable_id', 'profileable_type'];

    /**
     * Get all of the owning profileable models.
     */
    public function profileable()
    {
        return $this->morphTo();
    }

}
