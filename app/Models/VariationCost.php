<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VariationCost extends Model
{
    protected $fillable = ["variant_id", "cost", "from", "to", "order_ref_id", "status"];
}
