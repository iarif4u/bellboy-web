<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VariationPrice extends Model
{
    protected $fillable=["variant_id","price","from","to","status"];
}
