<?php

namespace App\Notifications\Agent;

use Benwilkins\FCM\FcmMessage;
use Illuminate\Notifications\Notification;

class BalanceWithdrawNotification extends Notification
{
    private $paymentAmount;
    private $paymentType;

    /**
     * Create a new notification instance.
     * @param $paymentAmount
     * @param $paymentType
     */
    public function __construct($paymentAmount,$paymentType)
    {
        $this->paymentAmount = $paymentAmount;
        $this->paymentType = $paymentType;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['fcm'];
    }


    public function toFcm($notifiable)
    {
        $message = new FcmMessage();
        $message->content([
            'title' => "Balance withdraw",
            'body' => "Dear " . $notifiable->name . ", you withdraw ".$this->paymentAmount." tk. By ".$this->paymentType,
            'sound' => 'notification'
        ])->data([
            'notification_id' => $notifiable->id,
            'title' => "Balance withdraw",
            'body' => "Dear " . $notifiable->name . ", you withdraw ".$this->paymentAmount." tk. By ".$this->paymentType,
            'action_type' => 'notification',
        ])->priority(FcmMessage::PRIORITY_HIGH);
        return $message;
    }
}
