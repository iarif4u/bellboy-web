<?php

namespace App\Notifications\Agent;

use Benwilkins\FCM\FcmMessage;
use Illuminate\Notifications\Notification;

class NewOrderNotification extends Notification
{
    private $orderItem;

    /**
     * Create a new notification instance.
     * @param mixed $offer
     */
    public function __construct($orderItem)
    {
        $this->orderItem = $orderItem;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['fcm'];
    }


    public function toFcm($notifiable)
    {

        $message = new FcmMessage();
        $message->content([
            'title' => "New Order",
            'body' => "Dear " . $notifiable->name . ", your have new order",
            'sound'=>'notification'
        ])->data([
            'title' => "New Order",
            'body' => "Dear " . $notifiable->name . ", your have new order",
            'action_type' => 'notification_pending_order',
            'offer_id' => $this->orderItem->id,
            'order_id'=> $this->orderItem->order_id,
            'variation_id' => $this->orderItem->variation_id,
            'notification_id' => $this->orderItem->order_id,
            'quantity' => $this->orderItem->quantity,
            'data' => [
                "order_item_id" => $this->orderItem->id,
                "variation_id" => $this->orderItem->variation_id,
                "cost" => $this->orderItem->cost,
                "variation_name" =>$this->orderItem->variation->variant_name,
                "quantity" => $this->orderItem->quantity,
                'unit' => $this->orderItem->variation->unit->unit_name,
                'unit_value' => $this->orderItem->variation->unit_value,
                'item_status' => $this->orderItem->item_status,
                'product_image' => get_media_file($this->orderItem->variation, config('const.product.media.product_image')),
            ]
        ])->priority(FcmMessage::PRIORITY_HIGH);
        return $message;
    }
}
