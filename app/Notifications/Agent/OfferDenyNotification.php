<?php

namespace App\Notifications\Agent;

use Benwilkins\FCM\FcmMessage;
use Illuminate\Notifications\Notification;

class OfferDenyNotification extends Notification
{
    private $offer;

    /**
     * Create a new notification instance.
     * @param mixed $offer
     */
    public function __construct($offer)
    {
        $this->offer = $offer;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['fcm'];
    }


    public function toFcm($notifiable)
    {
        $message = new FcmMessage();
        $message->content([
            'title' => "Deny product price",
            'body' => "Dear " . $notifiable->name . ", Admin deny your product price request",
            'sound' => 'notification'
        ])->data([
            'title' => "Deny product price",
            'body' => "Dear " . $notifiable->name . ", Admin deny your product price request",
            'action_type' => 'notification',
            'offer_id' => $this->offer->id,
            'variation_id' => $this->offer->variation_id,
            'agent_id' => $this->offer->agent_id,
            'notification_id' => $this->offer->id,
            'offer_price' => $this->offer->offer_price,
        ])->priority(FcmMessage::PRIORITY_HIGH);
        return $message;
    }
}
