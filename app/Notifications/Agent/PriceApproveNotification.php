<?php

namespace App\Notifications\Agent;

use Benwilkins\FCM\FcmMessage;
use Illuminate\Notifications\Notification;

class PriceApproveNotification extends Notification
{
    private $offer;
    private $product_img;

    /**
     * Create a new notification instance.
     * @param mixed $offer
     * @param $product_img
     */
    public function __construct($offer,$product_img)
    {
        $this->offer = $offer;
        $this->product_img = $product_img;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['fcm'];
    }


    public function toFcm($notifiable)
    {
        $message = new FcmMessage();
        $message->content([
            'title' => "Approve product price",
            'body' => "Dear " . $notifiable->name . ", Admin approve your product price request",
            'sound' => 'notification'
        ])->data([
            'title' => "Approve product price",
            'body' => "Dear " . $notifiable->name . ", Admin approve your product price request",
            'action_type' => 'notification',
            'image' => $this->product_img,
            'offer_id' => $this->offer->id,
            'variation_id' => $this->offer->variation_id,
            'agent_id' => $this->offer->agent_id,
            'notification_id' => $this->offer->id,
            'offer_price' => $this->offer->offer_price,
        ])->priority(FcmMessage::PRIORITY_HIGH);
        return $message;
    }
}
