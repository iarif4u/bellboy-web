<?php

namespace App\Notifications;

use App\Models\AgentOffer;
use Illuminate\Notifications\Notification;
use Benwilkins\FCM\FcmMessage;

class AgentPriceNotification extends Notification
{
    private $agent_offer;

    /**
     * Create a new notification instance.
     *
     * @param AgentOffer $agent_offer
     */
    public function __construct(AgentOffer $agent_offer)
    {
        $this->agent_offer = $agent_offer;

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'fcm'];
    }


    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'offer_id' => $this->agent_offer->id,
            'variation_id' => $this->agent_offer->variation_id,
            'agent_id' => $this->agent_offer->agent_id,
            'offer_price' => $this->agent_offer->offer_price,
        ];
    }

    public function toFcm($notifiable)
    {
        $message = new FcmMessage();
        $message->content([
            'title' => "Agent offer with new price",
            'body' => "Agent request for approve his product",
            'click_action' => route('product.variation.offers', ['variation_id' => $this->agent_offer->variation_id, 'offer_id' => $this->agent_offer->id]) // Optional
        ])->data([
            'offer_id' => $this->agent_offer->id,
            'variation_id' => $this->agent_offer->variation_id,
            'agent_id' => $this->agent_offer->agent_id,
            'offer_price' => $this->agent_offer->offer_price,
        ])->priority(FcmMessage::PRIORITY_HIGH);
        return $message;
    }
}
