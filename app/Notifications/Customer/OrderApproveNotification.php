<?php

namespace App\Notifications\Customer;

use Benwilkins\FCM\FcmMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;

class OrderApproveNotification extends Notification
{
    use Queueable;
    private $order;

    /**
     * Create a new notification instance.
     *
     * @param $order
     */
    public function __construct($order)
    {
        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['fcm'];
    }

    public function toFcm($notifiable)
    {
        $message = new FcmMessage();
        $message->content([
            'title' => "Accept Order",
            'body' => "Dear " . $notifiable->name . ", we accept your order",
            'sound' => 'notification'
        ])->data([
            'title' => "Accept Order",
            'body' => "Dear " . $notifiable->name . ", we accept your order",
            'notification_id' => $this->order->id,
            'action_type' => 'notification_accept_order',
            'route' => 'order',
            'order' => $this->order->id,
            'order_id' => $this->order->order_id,
        ])->priority(FcmMessage::PRIORITY_HIGH);
        return $message;
    }
}
