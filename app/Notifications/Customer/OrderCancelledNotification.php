<?php

namespace App\Notifications\Customer;

use Benwilkins\FCM\FcmMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;

class OrderCancelledNotification extends Notification
{
    use Queueable;
    private $order;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($order)
    {
        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['fcm'];
    }

    public function toFcm($notifiable)
    {
        $message = new FcmMessage();
        $message->content([
            'title' => "Cancel Order",
            'body' => ($notifiable->name)?"Dear " . $notifiable->name . ", we cancelled your order":"We cancelled your order",
            'sound' => 'notification'
        ])->data([
            'title' => "New Order",
            'body' => ($notifiable->name)?"Dear " . $notifiable->name . ", we cancelled your order":"We cancelled your order",
            'notification_id' => $this->order->id,
            'action_type' => 'notification_accept_order',
            'order' => $this->order->id,
            'route' => 'order',
            'order_id' => $this->order->order_id,
        ])->priority(FcmMessage::PRIORITY_HIGH);
        return $message;
    }
}
