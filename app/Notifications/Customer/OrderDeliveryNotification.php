<?php

namespace App\Notifications\Customer;

use Benwilkins\FCM\FcmMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;

class OrderDeliveryNotification extends Notification
{
    private $order;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($order)
    {
        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['fcm'];
    }

    public function toFcm($notifiable)
    {
        $message = new FcmMessage();
        $message->content([
            'title' => "Order Delivering",
            'body' => "Dear " . $notifiable->name . ", Your order ready to delivery",
            'sound' => 'notification'
        ])->data([
            'title' => "Order Delivering",
            'body' => "Dear " . $notifiable->name . ", Your order ready to delivery",
            'notification_id' => $this->order->id,
            'action_type' => 'notification_accept_order',
            'route' => 'order',
            'order' => $this->order->id,
            'order_id' => $this->order->order_id,
        ])->priority(FcmMessage::PRIORITY_HIGH);
        return $message;
    }
}
