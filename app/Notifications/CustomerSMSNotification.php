<?php

namespace App\Notifications;

use Gr8Shivam\SmsApi\Notifications\SmsApiChannel;
use Gr8Shivam\SmsApi\Notifications\SmsApiMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class CustomerSMSNotification extends Notification
{
    use Queueable;
    private $otp_code;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($otp_code)
    {
        $this->otp_code = $otp_code;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [SmsApiChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return SmsApiMessage
     * @throws FileNotFoundException
     */
    public function toSmsApi($notifiable)
    {
        return (new SmsApiMessage)->content("Dear Customer, BC-".$this->otp_code." is your verification code. ")
            ->headers(get_sms_config_header())->params(get_sms_config_all_options());
    }
}
