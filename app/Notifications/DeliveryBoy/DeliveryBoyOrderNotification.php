<?php

namespace App\Notifications\DeliveryBoy;

use Benwilkins\FCM\FcmMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;

class DeliveryBoyOrderNotification extends Notification
{
    use Queueable;
    private $order;

    /**
     * Create a new notification instance.
     *
     * @param $order
     */
    public function __construct($order)
    {
        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['fcm'];
    }

    public function toFcm($notifiable)
    {
        $message = new FcmMessage();
        $message->content([
            'title' => "New Order",
            'body' => "Mr. " . $notifiable->name . ", You have new order",
            'sound' => 'notification'
        ])->data([
            'title' => "New Order",
            'body' => "Mr. " . $notifiable->name . ", You have new order",
            'notification_id' => $this->order->id,
            'action_type' => 'notification',
            'order' => $this->order->id,
            'order_id' => $this->order->order_id,
        ])->priority(FcmMessage::PRIORITY_HIGH);
        return $message;
    }
}
