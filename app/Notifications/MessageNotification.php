<?php

namespace App\Notifications;

use Benwilkins\FCM\FcmMessage;
use Illuminate\Notifications\Notification;

class MessageNotification extends Notification
{
    private $conversion;
    private $message;

    /**
     * Create a new notification instance.
     *
     * @param $conversion
     * @param $message
     */
    public function __construct($conversion, $message)
    {
        $this->conversion = $conversion;
        $this->message = $message;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['fcm'];
    }

    public function toFcm($notifiable)
    {
        $message = new FcmMessage();
        $profile_pic = get_media_file($this->message->sender, config('const.media.profile_picture'));
        $chat = [
            "_id" => $this->message->id,
            "createdAt" => $this->message->created_at,
            "text" => $this->message->message,
            "user" => [
                '_id' => $this->message->sender->id,
                'avatar' => ($profile_pic) ? $profile_pic : setting(config('const.settings.site_logo'), asset('assets/img/logo.png')),
                "name" => $this->message->sender->name]];

        $message->content([
            'title' => "New Message",
            'body' =>"You get new message from bellboy",
            'icon'         => 'ic_start',
            'smallIcon' => 'ic_start',
            'sound' => 'default',
            'color'=>'red'
        ])->data([
            'title' => "New Message",
            'body' => "You get new message from bellboy",
            'action_type' => 'message',
            'route' => 'help',
            'conversion_id' => $this->conversion->id,
            'channel_id' => $this->conversion->channel_id,
            'notification_id' => $this->conversion->id,
            'sound' => 'default',
            'message' => $this->message->message,
            'chat' => $chat
        ])->priority(FcmMessage::PRIORITY_HIGH);
        return $message;
    }

}
