<?php


namespace App\Repositories\Agent;


use App\Models\Agent;
use App\Models\ProfileData;
use Illuminate\Http\Request;

class AgentRepository implements AgentRepositoryInterface
{
    // model property on class instances
    private $agent;
    // Constructor to bind model to repo

    /**
     *
     * @param Agent $agent
     */


    public function __construct(Agent $agent)
    {
        $this->agent = $agent;
    }


    public function all()
    {
        return Agent::all();
    }


    public function create(Request $data)
    {
        $this->agent->name = $data->input('agent_name');
        $this->agent->email = $data->input('email');
        $this->agent->phone = $data->input('phone');
        $this->agent->password = bcrypt($data->input('password'));
        $this->agent->status = config('const.status.active');
        $this->agent->agreement_date = $data->input('agreement_date');
        $this->agent->save();
        return $this->agent;
    }

    public function update(array $data, $id)
    {
        return $this->agent->find($id)->update($data);
    }

    public function delete($id)
    {
        return $this->agent->find($id)->delete();
    }

    public function show($id)
    {
        return $this->agent->find($id);
    }

    public function save_profile_data(array $request)
    {
        $profiles_data = [];
        foreach ($request as $name => $data) {
            $profiles_data[] = new ProfileData(['data_name' => $name, 'data_value' => $data]);
        }
        $this->agent->profiles()->saveMany($profiles_data);
        return $this->agent;
    }

    public function update_profile_data(array $request, $agent_id)
    {
        $this->agent = $this->show($agent_id);
        $this->agent->profiles()->delete();
        $profiles_data = [];
        foreach ($request as $name => $data) {
            $profiles_data[] = new ProfileData(['data_name' => $name, 'data_value' => $data]);
        }
        $this->agent->profiles()->saveMany($profiles_data);
        return $this->agent;
    }

    public function get_agent_details($agent_id)
    {
        $this->agent = $this->show($agent_id);
        $this->agent->shopName;
        $this->agent->shopAddress;
        $this->agent->trade_license = get_media_file($this->agent,config('const.media.trade_license'));
        $this->agent->trade_license_second = get_media_file($this->agent,config('const.media.trade_license_second'));
        $this->agent->nid_card_front = get_media_file($this->agent,config('const.media.nid_card_front'));
        $this->agent->nid_card_back = get_media_file($this->agent,config('const.media.nid_card_back'));
        $this->agent->profile_picture = get_media_file($this->agent,config('const.media.profile_picture'));
        $this->agent->shopName = $this->agent->shopName->data_value;
        $this->agent->shopAddress = $this->agent->shopAddress->data_value;
        return $this->agent;
    }
}
