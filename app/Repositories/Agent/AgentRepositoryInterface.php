<?php


namespace App\Repositories\Agent;


use Illuminate\Http\Request;

interface AgentRepositoryInterface
{
    public function all();

    public function create(Request $data);

    public function update(array $data, $id);

    public function delete($id);

    public function show($id);
}
