<?php


namespace App\Repositories\Area;


use App\Models\Area;
use App\Models\ProfileData;
use Illuminate\Http\Request;

class AreaRepository implements AreaRepositoryInterface
{
    // model property on class instances
    private $area;
    // Constructor to bind model to repo

    /**
     *
     * @param Area $area
     */


    public function __construct(Area $area)
    {
        $this->area = $area;
    }


    public function all()
    {
        return Area::all();
    }


    public function create(Request $data)
    {
        $this->area->area_name = $data->input('area_name');
        $this->area->added_by = auth()->id();
        $this->area->save();
        return $this->area;
    }

    public function update(array $data, $id)
    {
        return $this->area->find($id)->update($data);
    }

    public function delete($id)
    {
        return $this->area->find($id)->delete();
    }

    public function show($id)
    {
        return $this->area->find($id);
    }

    public function save_profile_data(array $request)
    {
        $profiles_data = [];
        foreach ($request as $name => $data) {
            $profiles_data[] = new ProfileData(['data_name' => $name, 'data_value' => $data]);
        }
        $this->area->profiles()->saveMany($profiles_data);
        return $this->area;
    }

    public function update_profile_data(array $request, $area_id)
    {
        $this->area = $this->show($area_id);
        $this->area->profiles()->delete();
        $profiles_data = [];
        foreach ($request as $name => $data) {
            $profiles_data[] = new ProfileData(['data_name' => $name, 'data_value' => $data]);
        }
        $this->area->profiles()->saveMany($profiles_data);
        return $this->area;
    }

    public function get_area_details($area_id)
    {
        $this->area = $this->show($area_id);
        $this->area->shopName;
        $this->area->shopAddress;
        $this->area->trade_license = asset($this->area->getFirstMediaUrl(config('const.media.trade_license')));
        $this->area->nid_card = asset($this->area->getFirstMediaUrl(config('const.media.nid_card')));
        $this->area->profile_picture = asset($this->area->getFirstMediaUrl(config('const.media.profile_picture')));
        $this->area->shopName = $this->area->shopName->data_value;
        $this->area->shopAddress = $this->area->shopAddress->data_value;
        return $this->area;
    }

    public function search($string,$limit=10){
       return Area::where('area_name','like',"%$string%")->limit($limit)->get();
    }

}
