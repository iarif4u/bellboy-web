<?php


namespace App\Repositories\Area;


use Illuminate\Http\Request;

interface AreaRepositoryInterface
{
    public function all();

    public function create(Request $data);

    public function update(array $data, $id);

    public function delete($id);

    public function show($id);
}
