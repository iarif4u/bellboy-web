<?php
namespace App\Repositories;

use App\Models\Customer;

class ChatRepository
{
    public static function conversation_fixed($usersNoConversation)
    {
        if ($usersNoConversation->count()>0){
            foreach ($usersNoConversation as $userNoConversation){
                $chat = ($userNoConversation->name) ?$userNoConversation->name: $userNoConversation->phone;
                $userNoConversation->conversation()->create([
                    'title' => "Chat with " . $chat,
                    'channel_id' => uniqid('chat_')
                ]);
            }
        }
    }


}
