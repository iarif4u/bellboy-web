<?php


namespace App\Repositories;

use App\Models\AgentAccount;
use App\Models\AgentOffer;
use App\Models\OrderItem;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Auth;

class DeliveryBoyRepository
{
    public static function getDeliveryBoy()
    {
        return self::guard()->user();
    }

    public static function getDeliveryBoyId()
    {
        return self::guard()->id();
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return Guard
     */
    public static function guard()
    {
        return Auth::guard('deliveryboy');
    }

    public static function order_item_price($order_item_id)
    {
        $item = OrderItem::where(['id' => $order_item_id])->first();
        return $item->discount_amount;
    }

    public static function order_item_qty_price($order_item)
    {
        $price = self::order_item_price($order_item['order_item_id']);
        return $order_item['productQty'] * $price;
    }

//["cost", "price", "agent_id", "discount_id", "discount_amount", "sub_total", "item_status", "item_value"];
    public static function order_item_delivery($order_item)
    {
        $quantity = $order_item['productQty'];
        if ($quantity > 0) {
            $item_id = $order_item['order_item_id'];
            $item = OrderItem::where(['id' => $item_id])->first();

            $cost = $quantity * $item->cost;
            $price = $quantity * $item->discount_amount;
            $value = $price - $cost;
            $item->agent->increment('balance', $cost);
            $item->variation->decrement('on_hand', $quantity);
            AgentOffer::where([
                "variation_id"=>$item->variation->id,
                "agent_id"=>$item->agent->id,
                "offer_status"=>config('const.agent_status.active')])->decrement('offer_stock', $quantity);
            AgentAccount::create(["agent_id" => $item->agent->id, "amount" => $cost, "order_item_id" => $item_id]);
            $item->update([
                'delivered_quantity' => $quantity,
                'sub_total' => $price,
                'item_status' => config('const.order_item_status.delivered'),
                'item_value' => $value,
            ]);
            return $value;
        } else {
            return 0;
        }
    }
}
