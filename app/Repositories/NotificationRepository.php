<?php


namespace App\Repositories;

use App\Models\Agent;
use App\Models\AgentOffer;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\ProductsVariant;
use App\Models\VariationCost;
use App\Notifications\AgentPriceNotification;


class NotificationRepository
{
    public static function createNotificationListItem($notification)
    {
        if ($notification->type == AgentPriceNotification::class) {
            $agent = Agent::where('id', '=', $notification->data['agent_id'])->first();
            $variation = ProductsVariant::where('id', '=', $notification->data['variation_id'])->first();
            $offer_price = $notification->data['offer_price'];
            $title = $agent->name . " offer " . $offer_price . " tk for " . $variation->variant_name;
            $link = route('product.variation.offers', ['variation_id' => $variation->id, 'offer_id' => $notification->data['offer_id']]);
            if ($notification->unread()) {
                return '<a class="text-danger" href="' . $link . '">' . $title . '</a>';
            }
            return '<a href="' . $link . '">' . $title . '</a>';
        }
    }

}
