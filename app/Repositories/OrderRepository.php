<?php


namespace App\Repositories;

use App\Models\AgentOffer;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\ProductsVariant;
use App\Models\VariationCost;
use Carbon\Carbon;

class OrderRepository
{
    public static function get_current_order($order)
    {

        $profile_pic = get_media_file($order->customer, config('const.media.profile_picture'));
        if (!$profile_pic) {
            $profile_pic = setting(config('const.settings.site_logo'), asset('assets/img/logo.png'));
        }
        return [
            'order' => $order,
            'display_status' => config('const.display_status')[$order->order_status],
            'pending_items' => ProductRepository::get_total_pending($order->id),
            'order_items' => ProductRepository::get_order_items($order),
            'profile_img' => $profile_pic,
        ];
    }

    public static function update_order_items($agents, $order_items, $order)
    {
        ;
        //dd($agents,$order_items);
        $total_price = 0;
        $total_value = 0;
        for ($i = 0; $i < sizeof($order_items); $i++) {
            $order_item_id = $order_items[$i];
            $agent_id = $agents[$i];
            $order_item = OrderItem::with(['agent', 'order', 'variation'])->where(['id' => $order_item_id])->first();
            $variation = $order_item->variation;
            $productQty = $order_item->quantity;
            $agent_offer = AgentOffer::where(['agent_id' => $agent_id, 'variation_id' => $variation->id, 'offer_status' => config('const.agent_status.active')])->first();
            $variation_agent_cost = $agent_offer->negotiate_price;
            $discount_price = $order_item->discount_amount;
            $value = ($discount_price - $variation_agent_cost) * $productQty;
            $sub_total = $discount_price * $productQty;
            $total_price = $total_price + $sub_total;
            $total_value = $total_value + $value;
            $order_item->cost = $variation_agent_cost;
            $order_item->agent_id = $agent_id;
            $order_item->sub_total = $sub_total;
            $order_item->item_value = $value;
            $order_item->save();
        }
        if ($order->coupon_discount != null) {
            $order->total = $total_price - $order->coupon_discount;
            $order->order_value = $total_value - $order->coupon_discount;
        } else {
            $order->total = $total_price;
            $order->order_value = $total_value;
        }
        $order->grand_total = $total_price;
        $order->save();

    }

    public static function order_counter()
    {
        $orders['pending_orders'] = Order::where(['order_status' => config('const.order_status.pending')])->count();
        $orders['approve_orders'] = Order::where(['order_status' => config('const.order_status.approve')])->count();
        $orders['canceled_orders'] = Order::where(['order_status' => config('const.order_status.deny')])->count();
        $orders['processed_orders'] = Order::where(['order_status' => config('const.order_status.on_agent')])->count();
        $orders['packed_orders'] = Order::where(['order_status' => config('const.order_status.packed')])->count();
        $orders['delivering_orders'] = Order::where(['order_status' => config('const.order_status.delivering')])->count();
        $orders['complete_orders'] = Order::where(['order_status' => config('const.order_status.complete')])->count();
        return $orders;
    }
}
