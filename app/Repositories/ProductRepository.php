<?php


namespace App\Repositories;

use App\Models\AgentOffer;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\ProductsVariant;
use App\Models\VariationCost;
use Carbon\Carbon;

class ProductRepository
{
    public static function variation_options($product_variation)
    {
        $options = [];
        $values = [];
        foreach ($product_variation->options as $variation_options) {
            $options[$variation_options->option->name][] = $variation_options->value;
        }
        foreach ($options as $option => $value) {
            $values[] = ['option' => $option, 'value' => $value];
        }
        return $values;
    }

    public static function product_properties($product)
    {
        $properties = [];
        foreach ($product->properties as $property) {
            $properties[] = ['property' => $property->property->name, 'value' => $property->value];
        }
        return $properties;
    }

    public static function product_discount($product)
    {
        if ($product->valid_discount) {
            return (object)[
                'id' => $product->valid_discount->id,
                'name' => $product->valid_discount->discount_name,
                'type' => $product->valid_discount->discount_type,
                'discount' => $product->valid_discount->discount,
                'valid_till' => $product->valid_discount->valid_till,
                'bn_discount' => en2bnNumber($product->valid_discount->discount),
            ];
        } else {
            return false;
        }
    }

    public static function product_price_after_discount($product_variation)
    {
        $price = $product_variation->price;
        $discount = self::product_discount($product_variation->product);
        if ($discount) {
            switch ($discount->type) {
                case config('const.discount.percentage'):
                    $price = $price - ($price * $discount->discount) / 100;
                    break;
                default:
                    $price = $price - $discount->discount;
            }
        }
        return intval($price);
    }

    public static function coupon_discount($order_id, $order_total)
    {
        $order = Order::with(['couponCode'])->where(['id' => $order_id])->first();
        if ($order->coupon_id == null) {
            return 0;
        } else {
            return self::calculate_discount($order->couponCode, $order_total);
        }
    }

    public static function calculate_discount($coupon, $total)
    {

        if ($coupon->type == config('const.coupon_codes.fixed')) {
            $discount = $coupon->amount;
        } else {
            $discount = ($total * $coupon->amount) / 100;
        }
        if ($discount < $coupon->max_amount) {
            return round($discount);
        } else {
            return $coupon->max_amount;
        }
    }

    public static function product_discount_info($product_variation)
    {

        $discount = self::product_discount($product_variation->product);
        if ($discount) {
            return $discount->id;
        }
        return null;
    }

    public static function product_variation_brand($product_variation)
    {
        if ($product_variation->product->brand) {
            return (object)[
                'brand_name' => $product_variation->product->brand->name,
                'brand_img' => get_media_file($product_variation->product->brand, config('const.media.brand_image')),
            ];
        } else {
            return false;
        }
    }

    public static function get_categories_data($categories)
    {
        $category_list = [];
        foreach ($categories as $category) {
            if ($category->childrenCategories->count() > 0) {
                $child = self::get_categories_data($category->childrenCategories);
            } else {
                $child = false;
            }
            $products = [];
            if ($child == false) {
                foreach ($category->products as $product) {
                    foreach ($product->variations as $variation) {
                        $products[] = self::get_product_response($variation);
                    }
                }
            }
            $category_list[] = [
                'id' => $category->id,
                'name' => $category->name,
                'slug' => $category->slug,
                'subcategories' => $child,
                'products' => ($category->products->count() > 0) ? $products : false,
                'img' => get_media_file($category, config('const.media.category_image')),
            ];
        }
        return $category_list;
    }


    public static function get_product_response($product_variation)
    {
        $discount = ProductRepository::product_price_after_discount($product_variation);
        return [
            'product_id' => $product_variation->product->id,
            'laundry' => $product_variation->product->laundry,
            'product_name' => $product_variation->product->product_name,
            'variant_id' => $product_variation->id,
            'variant_name' => $product_variation->variant_name,
            'product_image' => get_media_file($product_variation, config('const.product.media.product_image')),
            'cost' => $product_variation->cost,
            'price' => $product_variation->price,
            'bn_price' => en2bnNumber($product_variation->price),
            'unit' => $product_variation->unit->unit_name,
            'unit_value' => $product_variation->unit_value,
            'description' => $product_variation->product->description,
            'brand' => ProductRepository::product_variation_brand($product_variation),
            'discount_price' => $discount,
            'discount_bn_price' => en2bnNumber($discount),
            'discount' => ProductRepository::product_discount($product_variation->product),
            'properties' => ProductRepository::product_properties($product_variation->product),
            'variations' => ProductRepository::variation_options($product_variation)
        ];
    }

    public static function get_total_pending($order_id)
    {
        return OrderItem::where(['order_id' => $order_id, 'item_status' => config('const.order_item_status.pending')])->count();
    }

    public static function get_order_items($order)
    {
        $orderItems = [];
        foreach ($order->items as $item):
            $discount = ProductRepository::product_discount($item->variation->product);
            $orderItems[] = [
                "order_item_id" => $item->id,
                "bnPrice" => en2bnNumber($item->price),
                'discount' => $discount,
                "discountBnPrice" => en2bnNumber($item->discount_amount),
                "discountPrice" => $item->discount_amount,
                "price" => $item->price,
                "productId" => $item->variation->product->id,
                "productName" => $item->variation->variant_name,
                "productQty" => $item->quantity,
                "deliveryQty" => $item->delivered_quantity,
                'unit' => $item->variation->unit->unit_name,
                'unit_value' => $item->variation->unit_value,
                'item_status' => $item->item_status,
                'product_image' => get_media_file($item->variation, config('const.product.media.product_image')),
                "valid_till" => ($discount) ? $discount->valid_till : null,
                'agent' => self::get_order_item_agent($item),
                'laundry' => $item->variation->product->laundry,
                "delivery_date"=> $item->delivery_date,
                "delivered_time"=> $item->delivered_time,
                'display_status' => get_item_status($item->item_status),
                'is_own_status' => self::check_status_deliveryboy($item->item_status,$item->variation->product->laundry)
            ];
        endforeach;
        return $orderItems;
    }
    public static function check_status_deliveryboy($status,$laundry){
        if($laundry){
            $status_list = [
                'pending' => true,
                'delivery_boy_collect' => true,
                'agent_collect' => false,
                'packed' => true,
                'collected' => false,
                'delivered' => false,
                'deny' => false
            ];
        }else{
            $status_list = [
                'pending' => false,
                'delivery_boy_collect' => false,
                'agent_collect' => true,
                'packed' => true,
                'collected' => false,
                'delivered' => false,
                'deny' => false
            ];
        }
        return $status_list[$status];
    }
    public static function get_order_item_agent($item)
    {
        $agent = $item->agent;
        if ($agent) {
            $agent->profile_img = get_media_file($agent, config('const.media.profile_picture'));
            $agent->shop_img = get_media_file($agent, config('const.media.shop_img'));
            $agent->agent_shop = $agent->shopName->data_value;
            $agent->agent_shop_address = $agent->shopAddress->data_value;
            unset($agent->media);
            return $agent;
        }
        return false;
    }

    public static function product_list_response($productList)
    {
        $products = [];
        foreach ($productList as $product) {
            foreach ($product->variations as $variation) {
                $products[] = ProductRepository::get_product_response($variation);
            }
        }
        return $products;
    }

    public static function products_notIn_response($productList, $ids)
    {
        $products = [];
        foreach ($productList as $product) {
            foreach ($product->variations as $variation) {
                if (!in_array($variation->id, $ids)) {
                    $products[] = ProductRepository::get_product_response($variation);
                }
            }
        }
        return $products;
    }

    public static function order_items_details($items)
    {
        $orderItems = [];
        $total_cost = 0;
        $is_packed = true;
        foreach ($items as $pendingItem) {
            if ($pendingItem->item_status != config('const.order_item_status.packed')) {
                $is_packed = false;
            }
            $product = self::get_product_response($pendingItem->variation);
            $cost = $pendingItem->cost * $pendingItem->quantity;
            $total_cost = $cost + $total_cost;
            $orderItems[] = [
                "order_item_id" => $pendingItem->id,
                'product' => $product,
                "quantity" => $pendingItem->quantity,
                "delivered_quantity" => $pendingItem->delivered_quantity,
                "cost" => $pendingItem->cost,
                "item_status" => $pendingItem->item_status,
                "display_item_status" => get_item_status($pendingItem->item_status),
                "quantity_cost" => $cost
            ];
        }

        return ['items' => $orderItems, 'total_cost' => $total_cost, 'is_packed' => $is_packed];
    }

    public static function create_agent_new_offer($variation_id, $cost, $agent, $stock)
    {
        $offer = AgentOffer::updateOrCreate([
            'variation_id' => $variation_id,
            'negotiate_price' => bn2enNumber($cost),
            'agent_id' => $agent,
            'offer_price' => bn2enNumber($cost),
            'offer_stock' => bn2enNumber($stock),
            'offer_status' => config('const.agent_status.active'),
        ]);

        VariationCost::updateOrCreate(["variant_id" => $variation_id, "order_ref_id" => $offer->id, "cost" => bn2enNumber($cost)]);
        AgentOffer::where('id', '!=', $offer->id)
            ->where(['variation_id' => $variation_id, 'agent_id' => $agent, 'offer_status' => config('const.agent_status.active')])
            ->update(['offer_status' => config('const.agent_status.inactive')]);
        $product_variant = ProductsVariant::where(['id' => $offer->variation_id])->first();
        $product_img = get_media_file($product_variant, config('const.product.media.product_image'));
        $product_variant->update(['cost' => bn2enNumber($cost)]);
        $product_variant->increment('on_hand', bn2enNumber($stock));

        return ['product_img' => $product_img, 'offer' => $offer];
    }


    public static function get_agent_variation_cost($variation_id, $agent_id)
    {
        $offer = AgentOffer::where([
            'variation_id' => $variation_id,
            'agent_id' => $agent_id,
            'offer_status' => config('const.agent_status.active'),
        ])->first();
        return $offer->negotiate_price;
    }

    public static function make_customer_order($cart_items, $coupon, $customer,$delivery_charge,$laundry,$order_id=false)
    {
        if(!$order_id){
            $order_id= "#" . date("ds") . $customer->id . rand(0, 9);
        }
        $items = [];
        $total_value = 0;
        $total_price = $delivery_charge;
        foreach ($cart_items as $cart_item) {
            $productQty = $cart_item["productQty"];
            if ($productQty > 0):
                $variation = ProductsVariant::with('product')->where(["id" => $cart_item["productId"]])->first();
                $agent_offer = AgentOffer::with('agent')
                    ->where(['variation_id' => $variation->id, 'offer_status' => config('const.agent_status.active')])
                    ->where('offer_stock', '>=', $productQty)
                    ->orderBy('negotiate_price')
                    ->first();
                $variation_agent_cost = ($agent_offer) ? $agent_offer->negotiate_price : 0;
                $discount_price = ProductRepository::product_price_after_discount($variation);
                $value = ($discount_price - $variation_agent_cost) * $productQty;
                $sub_total = $discount_price * $productQty;
                $total_price = $total_price + $sub_total;
                $total_value = $total_value + $value;

                $agent_id = null;
                if ($agent_offer) {
                    if ($agent_offer->agent) {
                        $agent_id = $agent_offer->agent->id;
                    }
                }
                $items[] = [
                    "variation_id" => $variation->id,
                    "quantity" => $productQty,
                    "cost" => $variation_agent_cost,
                    "price" => $variation->price,
                    "agent_id" => $agent_id,
                    "delivered_quantity" => 0,
                    "discount_id" => ProductRepository::product_discount_info($variation),
                    "discount_amount" => $discount_price,
                    "sub_total" => $sub_total,
                    "item_status" => config('const.order_item_status.pending'),
                    "item_value" => $value
                ];
            endif;
        }
        if ($coupon) {
            if ($coupon->type === config('const.discount.fixed')) {
                $coupon_discount = $coupon->amount;
            } else {
                $coupon_discount = $total_price * ($coupon->amount / 100);
            }
            $get_coupon_discount = ($coupon_discount < $coupon->max_amount) ? $coupon_discount : $coupon->max_amount;
            $order = Order::create([
                "order_id" => $order_id,
                "customer_id" => $customer->id,
                "coupon_id" => $coupon->id,
                "total" => $total_price - $get_coupon_discount,
                'grand_total' => $total_price,
                "coupon_discount" => $get_coupon_discount,
                "order_status" => config('const.order_status.pending'),
                "order_modified" => Carbon::now(),
                "order_value" => $total_value - $get_coupon_discount,
                "delivery_address" => $customer->address,
                "delivery_charge" => $delivery_charge,
                "laundry"=>$laundry
            ]);
        } else {
            $order = Order::create([
                "order_id" =>$order_id,
                "customer_id" => $customer->id,
                "coupon_id" => null,
                "grand_total" => $total_price,
                "total" => $total_price,
                "coupon_discount" => null,
                "order_status" => config('const.order_status.pending'),
                "order_modified" => Carbon::now(),
                "order_value" => $total_value,
                "delivery_address" => $customer->address,
                "delivery_charge" => $delivery_charge,
                "laundry"=>$laundry
            ]);
        }
        $order->items()->createMany($items);
        return $order;
    }
}
