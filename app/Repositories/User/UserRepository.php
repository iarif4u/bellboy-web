<?php


namespace App\Repositories\User;


use App\Models\Callcenter;
use App\Models\Manager;
use App\User;

class UserRepository
{


    public function all()
    {
        return User::all();
    }

    public static function create($user, $type)
    {
        switch ($type) {
            case config('const.users.admin'):
                $model = User::create($user);
                break;
            case config('const.users.callcenter'):
                $model = Callcenter::create($user);
                break;
            case config('const.users.manager'):
                $model = Manager::create($user);
                break;
            default:
                $model = false;
        }
        return $model;
    }

    public function update(array $data, $id)
    {
        // TODO: Implement update() method.
    }

    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    public static function show($user_id,$user_type)
    {
        switch ($user_type) {
            case config('const.users.admin'):
                $user = User::where(['id' => $user_id])->first();
                break;
            case config('const.users.callcenter'):
                $user = Callcenter::where(['id' => $user_id])->first();
                break;
            case config('const.users.manager'):
                $user = Manager::where(['id' => $user_id])->first();
                break;
            default:
                $user=false;
        }
        return $user;
    }
}
