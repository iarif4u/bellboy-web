<?php

namespace App;

use App\Models\FirebaseToken;
use App\Models\Message;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Notifications\Notification;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * @property mixed firebase_tokens
 */
class User extends Authenticatable implements JWTSubject, HasMedia
{
    use Notifiable, HasMediaTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password', 'phone', 'added_by', 'status','chat'];

    /**
     * Get all of the post's comments.
     */
    public function messages()
    {
        return $this->morphMany(Message::class, 'messageable');
    }
    /**
     * Get all of the post's comments.
     */
    public function firebase_tokens()
    {
        return $this->morphMany(FirebaseToken::class, 'tokenable');
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @inheritDoc
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * @inheritDoc
     */
    public function getJWTCustomClaims()
    {
        return [];
    }


    /**
     * Route notifications for the FCM channel.
     *
     * @param Notification $notification
     * @return array
     */
    public function routeNotificationForFcm($notification)
    {
        return array_first(array_values((array)$this->firebase_tokens->pluck('token')));
    }
}
