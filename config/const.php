<?php
return [
    'status' => [
        'active' => 'active',
        'inactive' => 'inactive'
    ],
    'media' => [
        'nid_card' => 'nid_card',
        'nid_card_front' => 'nid_card',
        'nid_card_back' => 'nid_card_back',
        'shop_img' => 'shop_img',
        'trade_license' => 'trade_license',
        'trade_license_second' => 'trade_license_second',
        'profile_picture' => 'profile_picture',
        'brand_image' => 'brand_image',
        'category_image' => 'category_image',
    ],
    'discount' => [
        'fixed' => "fixed",
        'percentage' => "percentage",
    ],
    'coupon_codes' => [
        'fixed' => "fixed",
        'percentage' => "percentage",
    ],
    'product' => [
        'status' => ['active' => 'active', 'inactive' => 'inactive'],
        'media' => ['product_image' => 'product_image', 'thumbnail' => 'thumbnail'],
    ],
    'settings' => [
        'site_title' => 'site_title',
        'copyright' => 'copyright',
        'phone' => 'phone',
        'timeZone' => 'timeZone',
        'delivery_charge' => 'delivery_charge',
        'brief_info' => 'brief_info',
        'laundry_charge' => 'laundry_charge',
        'site_logo' => 'site_logo',
        'favicon' => 'favicon',
    ],
    'sms_config' => [
        'method',
        'url',
        'params' => [
            'send_to_param_name',
            'msg_param_name',
            'others'
        ],
        'headers',
        'add_code' => true
    ],
    'users' => [
        'admin' => 'admin',
        'manager' => 'manager',
        'callcenter' => 'callcenter',
    ],
    'tables' => [
        'admin' => 'users',
        'manager' => 'managers',
        'callcenter' => 'callcenters',
    ],
    'agent_status' => [
        'active' => 'active',
        'pending' => 'pending',
        'inactive' => 'inactive',
        'deny' => 'deny',
    ],
    'order_status' => [
        'approve' => 'approve',
        'on_agent' => 'on_agent',
        'packed' => 'packed',
        'pending' => 'pending',
        'deny' => 'deny',
        'delivering' => 'delivering',
        'complete' => 'complete',
    ],
    "order_item_status" => [
        'packed' => 'packed',
        'pending' => 'pending',
        'collected' => 'collected',
        'delivered' => 'delivered',
        'deny' => 'deny'
    ],
    "laundry_item_status" => [
        'pending' => 'pending',
        'delivery_boy_collect' => 'delivery_boy_collect',
        'agent_collect' => 'agent_collect',
        'packed' => 'packed',
        'collected' => 'collected',
        'delivered' => 'delivered',
        'deny' => 'deny'
    ],
    "delivery_boy_order_status" => [
        'assign' => "assign",
        'collecting' => "collecting",
        'delivering' => "delivering",
        'delivered' => "delivered",
    ],
    'order_processed_status' => [
        'approve' => 'approve',
        'on_agent' => 'on_agent',
        'packed' => 'packed',
        'delivering' => 'delivering',
        'complete' => 'complete',
    ],
    'display_status'=>[
        'pending' => 'অর্ডারটি প্রক্রিয়াধীন রয়েছে',
        'approve' => 'অর্ডারটি কনফার্ম হয়েছে',
        'on_agent' => 'অর্ডারটি প্রক্রিয়াধীন',
        'packed' => 'পন্যগুলো প্যাকেট হয়েছে ',
        'delivering' => 'ডেলিভারীর জন্যে প্রস্তুত',
        'complete' => 'ডেলিভারী সম্পন্ন হয়েছে',
        'deny' => 'অর্ডারটি বাতিল হয়েছে',
    ]
];
