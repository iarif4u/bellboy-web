<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsVariantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_variants', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('product_id');
            $table->string('variant_name');
            $table->string('sku');
            $table->integer('unit_id');
            $table->integer('unit_value');
            $table->integer('cost')->nullable();
            $table->integer('price')->nullable();
            $table->integer('on_hand')->nullable();
            $table->integer('agent_id')->nullable();
            $table->boolean('available_on_demand')->default(true);
            $table->enum('status',config('const.product.status'))->default(config('const.product.status.active'));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products_variants');
    }
}
