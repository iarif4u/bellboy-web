<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVariationCostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('variation_costs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('variant_id');
            $table->string('cost');
            $table->dateTime('from')->useCurrent();
            $table->dateTime('to')->nullable();
            $table->integer('order_ref_id')->nullable();
            $table->boolean('status')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('variation_costs');
    }
}
