<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgentOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agent_offers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('variation_id');
            $table->integer('agent_id');
            $table->integer('offer_price');
            $table->integer('offer_stock');
            $table->integer('negotiate_price')->nullable();
            $table->string('message')->nullable();
            $table->string('offer_status')->default("pending");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agent_offers');
    }
}
