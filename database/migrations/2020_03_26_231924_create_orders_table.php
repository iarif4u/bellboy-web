<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("order_id");
            $table->integer("customer_id");
            $table->integer("delivery_boy")->nullable();
            $table->integer("coupon_id")->nullable();
            $table->integer("grand_total");
            $table->integer("total");
            $table->integer("coupon_discount")->nullable();
            $table->string("order_status");
            $table->string("comment")->nullable();
            $table->dateTime("order_modified");
            $table->dateTime("delivery_time")->nullable();
            $table->string('order_value')->nullable();
            $table->string('delivery_address')->nullable();
            $table->string('delivery_charge')->nullable();
            $table->boolean('laundry')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
