<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer("order_id");
            $table->integer("variation_id");
            $table->integer("quantity");
            $table->integer("delivered_quantity")->nullable();
            $table->date("delivery_date")->nullable();
            $table->dateTime("delivered_time")->nullable();
            $table->integer("cost");
            $table->integer("price");
            $table->integer("agent_id")->nullable();
            $table->integer("discount_id")->nullable();
            $table->integer("discount_amount")->nullable();
            $table->integer("sub_total")->nullable();
            $table->string("item_status")->nullable();
            $table->integer("item_value")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_items');
    }
}
