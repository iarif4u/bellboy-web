<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderDeliveryBoysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_delivery_boys', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer("order_id");
            $table->integer("delivery_boy_id");
            $table->string("status");
            $table->dateTime("status_time");
            $table->integer("assigner_id");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_delivery_boys');
    }
}
