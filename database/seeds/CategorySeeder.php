<?php

use App\Models\Category;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 15; $i++) {
            $faker = Faker::create();
            $parent = Category::inRandomOrder()->first();
            $parent_id = ($parent) ? $parent->id : null;
            $random_arr = [$parent_id, null];
            $category = Category::create(['name' => $faker->name, 'category_id' => array_rand($random_arr)]);
            $url = "https://picsum.photos/200";
            $category->addMediaFromUrl($url)->toMediaCollection(config('const.media.category_image'));
        }
    }
}
