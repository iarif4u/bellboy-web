importScripts('https://www.gstatic.com/firebasejs/7.8.2/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.8.2/firebase-messaging.js');


// Initialize the Firebase app in the service worker by passing in the
// messagingSenderId.
firebase.initializeApp({
    apiKey: "AIzaSyAlqMTtwjWITzyEooQxHEqhPHTvHdqXEwo",
    authDomain: "bellboy-winnerdevs.firebaseapp.com",
    databaseURL: "https://bellboy-winnerdevs.firebaseio.com",
    projectId: "bellboy-winnerdevs",
    storageBucket: "bellboy-winnerdevs.appspot.com",
    messagingSenderId: "1075212422904",
    appId: "1:1075212422904:web:ee6118cbba5ef71933350d",
    measurementId: "G-NM5C7PVYQK"
});
console.log("Hello, waiting for notification");
// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();
messaging.setBackgroundMessageHandler(function (payload) {
    console.log('[firebase-messaging-sw.js] Received background message ', payload);
    // Customize notification here
    const notificationTitle = 'Background Message Title';
    const notificationOptions = {
        body: 'Background Message body.',
        click_action: 'http://localhost:30321/'
    };

    return self.registration.showNotification(notificationTitle,
        notificationOptions);
});

self.addEventListener('notificationclick', function (event) {
    console.log('On notification click: ', event.notification.tag);
    event.notification.close();

    // This looks to see if the current is already open and
    // focuses if it is
    event.waitUntil(clients.matchAll({
        type: "window"
    }).then(function (clientList) {
        for (var i = 0; i < clientList.length; i++) {
            var client = clientList[i];
            if (client.url === '/' && 'focus' in client)
                return client.focus();
        }
        if (clients.openWindow)
            return clients.openWindow('http://localhost:30321/');
    }));
});
/*
messaging.setBackgroundMessageHandler(function(payload) {
    console.log('[firebase-messaging-sw.js] Received background message ', payload);
    // Customize notification here
    const notificationTitle = 'Background Message Title';
    const notificationOptions = {
        body: 'Background Message body.',
        icon: '/assets/img/jp.png'
    };

    return self.registration.showNotification(notificationTitle,
        notificationOptions);
});
*/
