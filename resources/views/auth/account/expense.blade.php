@extends('auth.layouts.master')
@section('css')

    <link rel="stylesheet" href="{{asset('plugins/datepicker/css/datepicker.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/select2/select2.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('plugins/table/datatable/datatables.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset("plugins/table/datatable/custom_dt_html5.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("plugins/table/datatable/dt-global_style.css")}}">
@endsection
@section('content')
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
        <div class="widget widget-chart-one">
            <div class="widget-heading">
                <h5 class="">Expanse List</h5>
                <ul class="tabs tab-pills">
                    <li>
                        <button type="button" class="btn btn-primary mb-2 mr-2" data-toggle="modal"
                                data-target="#addExpanse">
                            Add Expanse
                        </button>
                    </li>
                </ul>
            </div>

            <div class="widget-content">
                <div class="tabs tab-content">
                    <div id="content_1" class="tabcontent">
                        <div class="table-responsive mb-4 mt-4">
                            {{$dataTable->table(['class'=>"table table-hover non-hover","style"=>"width:100%"])}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{asset('plugins/table/datatable/datatables.js')}}"></script>
    <script src="{{asset('plugins/datepicker/js/bootstrap-datepicker.js')}}"></script>
    <script src="{{asset('plugins/select2/select2.min.js')}}"></script>
    {{$dataTable->scripts()}}
    <script>
        $(".datepicker").datepicker({
            format: 'yyyy-mm-dd'
        });
        $("#expense-category").select2({
            dropdownParent: $("#addExpanse"),
        });
        $("#update-expense-category").select2({
            dropdownParent: $("#updateExpanse"),
        });

        const open_update_modal = (expense_id) => {
            $("#updateExpanse").modal("show");
            $("#update_expense_note").val('');
            axios.post('{{route('account.expense.info')}}', {
                expense_id: expense_id,
                _token: '{{csrf_token()}}',
            })
                .then(function (res) {
                    $(".expense_id").val(expense_id);
                    $("#update-expense-category").val(res.data.category_id).trigger('change');
                    $("#update_expense_name").val(res.data.expense_name);
                    $('#update_date').datepicker('update', res.data.date);
                    $("#update_amount").val(res.data.expense_amount);
                    if (res.data.note) {
                        $("#update_expense_note").val(res.data.note.note);
                    }

                })
                .catch(function (err) {
                    /* ... */
                });
        }

        const delete_data = (expense_id) => {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success btn-rounded',
                    cancelButton: 'btn btn-danger btn-rounded mr-3',
                },
                buttonsStyling: false
            });
            swalWithBootstrapButtons.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    blockUI("#expense-table");
                    axios.delete(`{{route('account.expense.delete')}}?expense_id=${expense_id}`)
                        .then(function (response) {
                            swalWithBootstrapButtons.fire('Deleted!', response.data.message, 'success');
                        })
                        .catch(function (error) {
                            showError(error.response.data.message);
                        })
                        .then(function () {
                            $("#expense-table").DataTable().ajax.reload(null, false);
                            unBlockUI("#expense-table");
                        });
                } else if (
                    /* Read more about handling dismissals below */
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire('Cancelled', 'Expense delete cancelled', 'error');
                }
            });
        }
    </script>
@endsection

@section('modal')
    <div id="addExpanse" class="modal animated zoomInUp custo-zoomInUp" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add New Expanse</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                             viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                             stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                            <line x1="18" y1="6" x2="6" y2="18"></line>
                            <line x1="6" y1="6" x2="18" y2="18"></line>
                        </svg>
                    </button>
                </div>
                <form method="post" class="simple-example" action="{{route('account.expense.add')}}" novalidate="">
                    @csrf
                    <div class="modal-body">
                        <div class="form-row">
                            <div class="col-md-12 mb-12">
                                <div class="form-group">
                                    <label for="expense_name">Expanse Name *</label>
                                    <input id="expense_name" type="text" name="expense_name" placeholder="Expanse Name"
                                           class="form-control" required="required">
                                </div>
                            </div>
                            <div class="col-md-12 mb-12">
                                <div class="form-group">
                                    <label for="expense-category">Expanse Category *</label>
                                    <select name="expense_category" id="expense-category"
                                            class="form-control expense_category">
                                        <option value="">Select Category</option>
                                        @if($categories->count()>0)
                                            @foreach($categories as $category)
                                                <option value="{{$category->id}}">{{$category->category}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12 mb-12">
                                <div class="form-group">
                                    <label for="expense_amount">Amount *</label>
                                    <input id="expense_amount" type="number" class="form-control" name="expense_amount">
                                </div>
                            </div>
                            <div class="col-md-12 mb-12">
                                <div class="form-group">
                                    <label for="date">Date *</label>
                                    <input id="date" value="{{date('Y-m-d')}}" type="text"
                                           class="form-control datepicker" name="date">
                                </div>
                            </div>
                            <div class="col-md-12 mb-12">
                                <div class="form-group">
                                    <label for="expense_note">Note</label>
                                    <textarea name="expense_note" id="expense_note" cols="30" rows="2"
                                              class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="updateExpanse" class="modal animated zoomInUp custo-zoomInUp" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Update Expanse</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                             viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                             stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                            <line x1="18" y1="6" x2="6" y2="18"></line>
                            <line x1="6" y1="6" x2="18" y2="18"></line>
                        </svg>
                    </button>
                </div>
                <form class="simple-example" method="post" action="{{route('account.expense.update')}}" novalidate="">
                    @csrf
                    <input type="hidden" class="expense_id" name="expense_id">
                    <div class="modal-body">
                        <div class="form-row">
                            <div class="col-md-12 mb-12">
                                <div class="form-group">
                                    <label for="update_expense_name">Expanse Name *</label>
                                    <input id="update_expense_name" type="text" name="expense_name"
                                           placeholder="Expanse Name" class="form-control" required="required">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="update-expense-category">Expanse Category *</label>
                                    <select name="expense_category" id="update-expense-category"
                                            class="form-control expense_category">
                                        <option value="">Select Category</option>
                                        @if($categories->count()>0)
                                            @foreach($categories as $category)
                                                <option value="{{$category->id}}">{{$category->category}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12 mb-12">
                                <div class="form-group">
                                    <label for="update_amount">Amount *</label>
                                    <input id="update_amount" type="number" class="form-control" name="expense_amount">
                                </div>
                            </div>
                            <div class="col-md-12 mb-12">
                                <div class="form-group">
                                    <label for="update_date">Date *</label>
                                    <input id="update_date" value="{{date('Y-m-d')}}" type="text"
                                           class="form-control datepicker" name="date">
                                </div>
                            </div>
                            <div class="col-md-12 mb-12">
                                <div class="form-group">
                                    <label for="update_expense_note">Note</label>
                                    <textarea name="expense_note" id="update_expense_note" cols="30" rows="2"
                                              class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
