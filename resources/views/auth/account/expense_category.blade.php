@extends('auth.layouts.master')
@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('plugins/table/datatable/datatables.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset("plugins/table/datatable/custom_dt_html5.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("plugins/table/datatable/dt-global_style.css")}}">
@endsection

@section('content')

    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
        <div class="widget widget-chart-one">
            <div class="widget-heading">
                <h5 class="">Expanse Category List</h5>
                <ul class="tabs tab-pills">
                    <li>
                        <button type="button" class="btn btn-primary mb-2 mr-2" data-toggle="modal"
                                data-target="#addCategory">
                            Add Category
                        </button>
                    </li>
                </ul>
            </div>

            <div class="widget-content">
                <div class="tabs tab-content">
                    <div id="content_1" class="tabcontent">
                        <div class="table-responsive mb-4 mt-4">
                            {{$dataTable->table(['class'=>"table table-hover non-hover","style"=>"width:100%"])}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{asset('plugins/table/datatable/datatables.js')}}"></script>
    {{$dataTable->scripts()}}
    <script>
        const open_update_modal = (category_id) => {
            $("#update_category_id").val(category_id);
            $("#update_category").val($.trim($(`.category-${category_id}`).text()));
            $("#updateCategory").modal("show");
        }
        const delete_data = (category_id) => {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success btn-rounded',
                    cancelButton: 'btn btn-danger btn-rounded mr-3',
                },
                buttonsStyling: false
            });
            swalWithBootstrapButtons.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    blockUI("#expense-categories-table");
                    axios.delete(`{{route('account.expense.delete_category')}}?category_id=${category_id}`)
                        .then(function (response) {
                            swalWithBootstrapButtons.fire('Deleted!', response.data.message, 'success');
                        })
                        .catch(function (error) {
                            showError(error.response.data.message);
                        })
                        .then(function () {
                            $("#expense-categories-table").DataTable().ajax.reload(null, false);
                            unBlockUI("#expense-categories-table");
                        });
                } else if (
                    /* Read more about handling dismissals below */
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire('Cancelled', 'Expense category delete cancelled', 'error');
                }
            });
        }
    </script>
@endsection

@section('modal')
    <div id="addCategory" class="modal animated zoomInUp custo-zoomInUp" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Expanse Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                             viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                             stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                            <line x1="18" y1="6" x2="6" y2="18"></line>
                            <line x1="6" y1="6" x2="18" y2="18"></line>
                        </svg>
                    </button>
                </div>
                <form class="simple-example" action="{{route('account.expense.add_category')}}" method="post"
                      novalidate="">
                    @csrf
                    <div class="modal-body">
                        <div class="form-row">
                            <div class="col-md-12 mb-12">
                                <div class="form-group">
                                    <label for="category">Category Name *</label>
                                    <input id="category" type="text" name="category" placeholder="Category Name"
                                           class="form-control" required="required">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="updateCategory" class="modal animated zoomInUp custo-zoomInUp" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Update Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                             viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                             stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                            <line x1="18" y1="6" x2="6" y2="18"></line>
                            <line x1="6" y1="6" x2="18" y2="18"></line>
                        </svg>
                    </button>
                </div>
                <form class="simple-example" action="{{route('account.expense.update_category')}}" method="post"
                      novalidate="">
                    @csrf
                    <input type="hidden" id="update_category_id" name="category_id">
                    <div class="modal-body">
                        <div class="form-row">
                            <div class="col-md-12 mb-12">
                                <div class="form-group">
                                    <label for="update_category">Category Name *</label>
                                    <input id="update_category" type="text" name="category" value="Category Name"
                                           class="form-control" required="required">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i>Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
