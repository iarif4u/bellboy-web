@extends('auth.layouts.master')
@section('css')
    <link rel="stylesheet" href="{{asset('plugins/datepicker/css/datepicker.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('plugins/table/datatable/datatables.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset("plugins/table/datatable/custom_dt_html5.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("plugins/table/datatable/dt-global_style.css")}}">
@endsection
@section('content')
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
        <div class="widget widget-chart-one">
            <div class="widget-heading">
                <h5 class="">Income List</h5>
                <ul class="tabs tab-pills">
                    <li>
                        <button type="button" class="btn btn-primary mb-2 mr-2" data-toggle="modal"
                                data-target="#addIncome">
                            Add Income
                        </button>
                    </li>
                </ul>
            </div>

            <div class="widget-content">
                <div class="tabs tab-content">
                    <div id="content_1" class="tabcontent">
                        <div class="table-responsive mb-4 mt-4">
                            {{$dataTable->table(['class'=>"table table-hover non-hover","style"=>"width:100%"])}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{asset('plugins/datepicker/js/bootstrap-datepicker.js')}}"></script>
    <script src="{{asset('plugins/table/datatable/datatables.js')}}"></script>
    {{$dataTable->scripts()}}
    <script>
        $(".datepicker").datepicker({
            format: 'yyyy-mm-dd'
        });

        const open_update_modal = (income_id) => {
            $("#updateIncome").modal("show");
            $("#update_note").val("");
            axios.post('{{route('account.income_info')}}', {
                income_id: income_id,
                _token: '{{csrf_token()}}',
            })
                .then(function (res) {
                    $("#update_income_id").val(res.data.id);
                    $("#update_income_name").val(res.data.income_name);
                    $('#update_date').datepicker('update', res.data.date);
                    $("#update_amount").val(res.data.income_amount);
                    if (res.data.note) {
                        $("#update_note").val(res.data.note.note);
                    }

                })
                .catch(function (err) {
                    /* ... */
                });
        };

        const delete_data = (income_id) => {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success btn-rounded',
                    cancelButton: 'btn btn-danger btn-rounded mr-3',
                },
                buttonsStyling: false
            });
            swalWithBootstrapButtons.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    blockUI("#incomes-table");
                    axios.delete(`{{route('account.income_delete')}}?income_id=${income_id}`)
                        .then(function (response) {
                            swalWithBootstrapButtons.fire('Deleted!', response.data.message, 'success');
                        })
                        .catch(function (error) {
                            showError(error.response.data.message);
                        })
                        .then(function () {
                            $("#incomes-table").DataTable().ajax.reload(null, false);
                            unBlockUI("#incomes-table");
                        });
                } else if (
                    /* Read more about handling dismissals below */
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire('Cancelled', 'Income delete cancelled', 'error');
                }
            });
        };
    </script>
@endsection

@section('modal')
    <div id="addIncome" class="modal animated zoomInUp custo-zoomInUp" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add New Income</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                             viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                             stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                            <line x1="18" y1="6" x2="6" y2="18"></line>
                            <line x1="6" y1="6" x2="18" y2="18"></line>
                        </svg>
                    </button>
                </div>
                <form class="simple-example" action="{{route('account.income')}}" method="post" novalidate="">
                    @csrf
                    <div class="modal-body">
                        <div class="form-row">
                            <div class="col-md-12 mb-12">

                                <div class="form-group">

                                    <label for="income_name">Income Name *</label>
                                    <input id="income_name" type="text" name="income_name" placeholder="Income Name"
                                           class="form-control" required="required">
                                </div>

                            </div>
                            <div class="col-md-12 mb-12">
                                <div class="form-group">
                                    <label for="amount">Amount *</label>
                                    <input id="amount" type="number" class="form-control" name="amount">
                                </div>
                            </div>
                            <div class="col-md-12 mb-12">
                                <div class="form-group">
                                    <label for="date">Date *</label>
                                    <input id="date" value="{{date('Y-m-d')}}" type="text"
                                           class="form-control datepicker" name="date">
                                </div>
                            </div>
                            <div class="col-md-12 mb-12">
                                <div class="form-group">
                                    <label for="note">Note</label>
                                    <textarea name="note" id="note" cols="30" rows="2" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i>Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="updateIncome" class="modal animated zoomInUp custo-zoomInUp" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Update Income</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                             viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                             stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                            <line x1="18" y1="6" x2="6" y2="18"></line>
                            <line x1="6" y1="6" x2="18" y2="18"></line>
                        </svg>
                    </button>
                </div>
                <form class="simple-example" action="{{route('account.income_update')}}" method="post" novalidate="">
                    @csrf
                    <input type="hidden" id="update_income_id" name="income_id">
                    <div class="modal-body">
                        <div class="form-row">
                            <div class="col-md-12 mb-12">
                                <div class="form-group">
                                    <label for="update_income_name">Income Name *</label>
                                    <input id="update_income_name" type="text" name="income_name"
                                           placeholder="Income Name" class="form-control" required="required">
                                </div>

                            </div>
                            <div class="col-md-12 mb-12">
                                <div class="form-group">
                                    <label for="update_amount">Amount *</label>
                                    <input id="update_amount" type="number" class="form-control" name="amount">
                                </div>
                            </div>
                            <div class="col-md-12 mb-12">
                                <div class="form-group">
                                    <label for="update_date">Date *</label>
                                    <input id="update_date" value="{{date('Y-m-d')}}" type="text"
                                           class="form-control datepicker" name="date">
                                </div>
                            </div>
                            <div class="col-md-12 mb-12">
                                <div class="form-group">
                                    <label for="update_note">Note</label>
                                    <textarea name="note" id="update_note" cols="30" rows="2"
                                              class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
