@extends('auth.layouts.master')
@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('plugins/table/datatable/datatables.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset("plugins/table/datatable/custom_dt_html5.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("plugins/table/datatable/dt-global_style.css")}}">
@endsection
@section('content')

    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
        <div class="widget widget-chart-one">
            <div class="widget-heading">
                <h5 class="">Price Approve List</h5>
                <ul class="tabs tab-pills">
                    <li>
                        <a href="{{route('product.all')}}" type="button" class="btn btn-primary mb-2 mr-2">
                            Product List
                        </a>
                    </li>
                </ul>
            </div>
            <div class="table-responsive mb-4 mt-4">
                {{$dataTable->table(['class'=>"table table-hover non-hover","style"=>"width:100%"])}}
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{asset('plugins/table/datatable/datatables.js')}}"></script>
    {{$dataTable->scripts()}}
    <script !src="">
        function approve_agent_offer(offer_id,variation_name,agent_name,offer_price,stock,price) {
            $("#approveOfferModal").modal("show");
            $(".variation-name").text(variation_name);
            $(".agent-name").text(agent_name);
            $(".offer-id").val(offer_id);
            $("#variation-cost").val(offer_price);
            $("#variation-stock").val(stock);
            $("#variation-price").val(price);
        }
        function deny_agent_offer(offer_id) {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success btn-rounded',
                    cancelButton: 'btn btn-danger btn-rounded mr-3',
                },
                buttonsStyling: false
            });

            swalWithBootstrapButtons.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, reject it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    blockUI("#offer-table");
                    axios.post('{{route('product.variation.offer_deny')}}', {
                        offer_id: offer_id,
                        _token: '{{csrf_token()}}'
                    })
                        .then(function (response) {
                            swalWithBootstrapButtons.fire('Deny!', 'Agent offer rejected', 'success');
                        })
                        .catch(function (error) {
                            showError(error.response.data.message);
                        })
                        .then(function () {
                            $("#offer-table").DataTable().ajax.reload(null, false);
                            unBlockUI("#offer-table");
                        });
                } else if (
                    /* Read more about handling dismissals below */
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire('Cancelled', 'Agent offer deny cancelled', 'error');
                }
            })
        }
    </script>
@endsection

@section('modal')
    <!-- Modal -->
    <div class="modal fade" id="approveOfferModal" role="dialog" aria-labelledby="exampleModalCenterTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Approve Agent Price</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                            <line x1="18" y1="6" x2="6" y2="18"></line>
                            <line x1="6" y1="6" x2="18" y2="18"></line>
                        </svg>
                    </button>
                </div>
                <form enctype="multipart/form-data" class="simple-example" action="{{route('product.variation.offer_approve')}}" method="post" novalidate="">
                    @csrf
                    <input type="hidden" class="offer-id" name="offer_id">
                    <div class="modal-body">
                        <div class="form-row">
                            <div class="col-md-12 mb-12">
                                <div class="form-group">
                                    <h5>Variation Name: <span class="variation-name"></span></h5>
                                </div>
                            </div>
                            <div class="col-md-12 mb-12">
                                <div class="form-group">
                                    <h5>Agent Name: <span class="agent-name"></span></h5>
                                </div>
                            </div>
                            <div class="col-md-12 mb-12">
                                <div class="form-group">
                                    <label for="variation-cost">Product Cost *</label>
                                    <input id="variation-cost" type="number" name="cost" placeholder="Product Cost" class="form-control" required="required">
                                </div>
                            </div>
                            <div class="col-md-12 mb-12">
                                <div class="form-group">
                                    <label for="variation-stock">Stock *</label>
                                    <input id="variation-stock" type="number" name="stock" placeholder="Product Stock" class="form-control" required="required">
                                </div>
                            </div>
                            <div class="col-md-12 mb-12">
                                <div class="form-group">
                                    <label for="variation-price">Price *</label>
                                    <input id="variation-price" type="number" name="price" placeholder="Product Price" class="form-control" required="required">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>

    </div>
@endsection
