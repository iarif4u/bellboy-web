<button class="btn btn-dark btn-sm dropdown-toggle" type="button"
        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Action
    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
         viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
         stroke-linecap="round" stroke-linejoin="round"
         class="feather feather-chevron-down">
        <polyline points="6 9 12 15 18 9"></polyline>
    </svg>
</button>
<div class="dropdown-menu" aria-labelledby="dropdownMenuReference1">
    <a class="dropdown-item" href="{{route('agent.edit',['agent_id'=>$agent->id])}}">Edit</a>
    <a class="dropdown-item" onclick="delete_agent('{{$agent->id}}')" href="javascript:void(0);">Delete</a>
    <a class="dropdown-item" onclick="view_agent('{{$agent->id}}')" href="javascript:void(0);">View</a>

    <div class="dropdown-divider"></div>
    @foreach(config('const.status') as $status)
        @if ($agent->status != $status)
            <a onclick="change_status('{{$agent->id}}','{{$status}}')" class="dropdown-item message change_status"
               href="javascript:void(0);">{{ucfirst($status)}}</a>
        @endif
    @endforeach
</div>

