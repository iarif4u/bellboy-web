@if($status==config('const.status.active'))
    <div class="d-flex">
        <div class="badge badge-success">{{$status}}</div>
    </div>
@else
    <div class="d-flex">
        <div class="badge badge-info">{{$status}}</div>
    </div>
@endif
