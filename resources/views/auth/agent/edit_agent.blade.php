@extends('auth.layouts.master')
@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('plugins/dropify/dropify.min.css')}}">
    <link href="{{asset('assets/css/users/account-setting.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/css/users/user-profile.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('plugins/file-upload/file-upload-with-preview.min.css')}}" rel="stylesheet" type="text/css"/>
@endsection
@section('content')
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
        <div class="widget widget-chart-one">
            <div class="widget-heading">
                <h5 class="">Update Agent</h5>
            </div>

            <div class="widget-content">
                <div class="tabs tab-content">
                    <div id="content_1" class="tabcontent">
                        <form class="needs-validation" method="post" enctype="multipart/form-data" novalidate
                              action="{{route('agent.edit',['agent_id'=>$agent->id])}}">
                            @csrf
                            <input type="hidden" name="agent_id" value="{{$agent->id}}">
                            <div class="form-row">

                                <div class="col-md-6 mb-6">
                                    <div class="form-group">
                                        <label for="agent_name">Agent Name *</label>
                                        <input id="agent_name" type="text" name="agent_name"
                                               value="{{old('agent_name',$agent->name)}}" class="form-control"
                                               required="">
                                    </div>
                                </div>
                                <div class="col-md-6 mb-6">
                                    <div class="form-group">
                                        <label for="shop_name">Shop Name *</label>
                                        <input id="shop_name" type="text" name="shop_name"
                                               value="{{old('shop_name',$agent->shopName->data_value)}}"
                                               class="form-control" required="">
                                    </div>
                                </div>
                                <div class="col-md-6 mb-6">
                                    <div class="form-group">
                                        <label for="email">E-Mail</label>
                                        <input id="email" type="email" name="email"
                                               value="{{old('email',$agent->email)}}"
                                               class="form-control" required="">
                                    </div>
                                </div>
                                <div class="col-md-6 mb-6">
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input id="password" placeholder="*******" type="password" name="password"
                                               class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6 mb-6">
                                    <div class="form-group">
                                        <label for="phone">Phone No *</label>
                                        <input id="phone" type="tel" name="phone" value="{{old('phone',$agent->phone)}}"
                                               class="form-control" required="">
                                    </div>
                                </div>
                                <div class="col-md-6 mb-6">
                                    <div class="form-group">
                                        <label for="agreement_date">Agreement Date *</label>
                                        <input id="agreement_date" type="date" name="agreement_date"
                                               value="{{old('agreement_date',$agent->agreement_date)}}"
                                               class="form-control" required="">
                                    </div>
                                </div>
                                <div class="col-md-12 mb-12">
                                    <div class="form-group">
                                        <label for="shop_address">Shop Address*</label>

                                        <textarea class="form-control" id="shop_address" name="shop_address"
                                                  rows="1">{{old('shop_address',$agent->shopAddress->data_value)}}</textarea>

                                    </div>
                                </div>
                                <div class="upload col-md-4 mb-12">
                                    <input type="file" id="input-file-profile-picture" name="profile_picture"
                                           class="dropify"
                                           data-default-file="{{asset($agent->getFirstMediaUrl(config('const.media.profile_picture')))}}"
                                           data-max-file-size="2M"/>
                                    <p class="mt-2"><i class="flaticon-cloud-upload mr-1"></i> Profile Picture</p>
                                </div>
                                <div class="upload col-md-4 mb-12">
                                    <input type="file" id="input-file-nid-card" name="nid_card_front" class="dropify"
                                           data-default-file="{{get_media_file($agent,config('const.media.nid_card_front'))}}"
                                           data-max-file-size="2M"/>
                                    <p class="mt-2"><i class="flaticon-cloud-upload mr-1"></i> NID Card (Front)</p>
                                </div>
                                <div class="upload col-md-4 mb-12">
                                    <input type="file" id="input-file-nid-card" name="nid_card_back" class="dropify"
                                           data-default-file="{{get_media_file($agent,config('const.media.nid_card_back'))}}"
                                           data-max-file-size="2M"/>
                                    <p class="mt-2"><i class="flaticon-cloud-upload mr-1"></i> NID Card (Back)</p>
                                </div>
                                <div class="upload col-md-4 mb-12">
                                    <input type="file" id="input-file-trade-license" name="trade_license"
                                           class="dropify"
                                           data-default-file="{{asset($agent->getFirstMediaUrl(config('const.media.trade_license')))}}"
                                           data-max-file-size="2M"/>
                                    <p class="mt-2"> Trade License (1st Part)</p>
                                </div>
                                <div class="upload col-md-4 mb-12">
                                    <input type="file" id="input-file-trade-license" name="trade_license_second" class="dropify" data-default-file="{{asset($agent->getFirstMediaUrl(config('const.media.trade_license_second')))}}" data-max-file-size="2M"/>
                                    <p class="mt-2"> Trade License (2nd Part)</p>
                                </div>

                            </div>
                            <button class="btn btn-primary submit-fn mt-2" type="submit">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{asset('assets/js/users/account-settings.js')}}"></script>
    <script src="{{asset('plugins/file-upload/file-upload-with-preview.min.js')}}"></script>
    <script src="{{asset('plugins/dropify/dropify.min.js')}}"></script>
@endsection
