@extends('auth.layouts.master')
@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('plugins/dropify/dropify.min.css')}}">
    <link href="{{asset('assets/css/users/account-setting.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/css/users/user-profile.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('plugins/file-upload/file-upload-with-preview.min.css')}}" rel="stylesheet" type="text/css"/>
    <style>
        .custom-file-container__image-preview {
            height: 180px;
            border: 1px solid #ddd;
        }
    </style>
@endsection
@section('js')
    <script src="{{asset('assets/js/users/account-settings.js')}}"></script>
    <script src="{{asset('plugins/file-upload/file-upload-with-preview.min.js')}}"></script>
    <script src="{{asset('plugins/dropify/dropify.min.js')}}"></script>
    <script>
        new FileUploadWithPreview('nidCardFront');
        new FileUploadWithPreview('nidCardBack');
        new FileUploadWithPreview('tradeLicense');
        new FileUploadWithPreview('tradeLicenseSecond');
    </script>
@endsection
@section('content')
    <div class="col-xl-12 col-lg-12 col-md-12 layout-spacing">

        <form id="general-info" class="needs-validation section general-info" method="post"
              enctype="multipart/form-data" novalidateaction="{{route('agent.new')}}">
            @csrf
            <div class="info">
                <h6 class="">Agent Information</h6>
                <div class="row">
                    <div class="col-lg-11 mx-auto">
                        <div class="row">
                            <div class="col-xl-2 col-lg-12 col-md-4">
                                <div class="upload mt-4 pr-md-4">
                                    <input type="file" id="input-file-max-fs" name="profile_picture" class="dropify" data-default-file="{{asset("assets/img/200x200.jpg")}}" data-max-file-size="2M"/>
                                    <p class="mt-2"><i class="flaticon-cloud-upload mr-1"></i>Profile Picture</p>
                                </div>
                            </div>
                            <div class="col-xl-10 col-lg-12 col-md-8 mt-md-0 mt-4">
                                <div class="form">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="agent_name">Agent Name *</label>
                                                <input value="{{old('agent_name')}}" id="agent_name" type="text" name="agent_name" placeholder="Agent Nmae" class="form-control" required="">
                                                <div class="invalid-feedback">Please enter the agent name</div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="shop_name">Shop Name *</label>
                                                <input value="{{old('shop_name')}}" id="shop_name" type="text" name="shop_name" placeholder="Shop Name" class="form-control" required="">
                                                <div class="invalid-feedback">Please enter the agent shop name</div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="email">E-Mail</label>
                                                <input value="{{old('email')}}" id="email" type="email" name="email" placeholder="example@domain.com" class="form-control" required="">
                                                <div class="invalid-feedback">Please enter valid email address</div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="password">Password*</label>
                                                <input id="password" type="password" minlength="6" name="password" placeholder="Password" class="form-control" required="">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <label for="phone">Phone No *</label>
                                                    <input id="phone" type="tel" value="{{old('phone')}}" name="phone"
                                                           placeholder="Some Text..." class="form-control" required="">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="agreement_date">Agreement Date *</label>
                                                <input value="{{old('agreement_date')}}" id="agreement_date" type="date" name="agreement_date" class="form-control" required="">
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="shop_address">Shop Address *</label>
                                                <textarea class="form-control" required id="shop_address"
                                                          name="shop_address"
                                                          rows="1">{{old('shop_address')}}</textarea>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="custom-file-container" data-upload-id="nidCardFront">
                                                <label>NID Card (Front Side) <a href="javascript:void(0)" class="custom-file-container__image-clear" title="Clear Image">x</a></label>
                                                <label class="custom-file-container__custom-file">
                                                    <input type="file" name="nid_card_front" class="custom-file-container__custom-file__custom-file-input" accept="image/*">
                                                    <input type="hidden" name="MAX_FILE_SIZE" value="10485760"/>
                                                    <span
                                                        class="custom-file-container__custom-file__custom-file-control"></span>
                                                </label>
                                                <div class="custom-file-container__image-preview"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="custom-file-container" data-upload-id="nidCardBack">
                                                <label>NID Card (Back Side) <a href="javascript:void(0)" class="custom-file-container__image-clear" title="Clear Image">x</a></label>
                                                <label class="custom-file-container__custom-file">
                                                    <input type="file" name="nid_card_back" class="custom-file-container__custom-file__custom-file-input" accept="image/*">
                                                    <input type="hidden" name="MAX_FILE_SIZE" value="10485760"/>
                                                    <span
                                                        class="custom-file-container__custom-file__custom-file-control"></span>
                                                </label>
                                                <div class="custom-file-container__image-preview"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="custom-file-container" data-upload-id="tradeLicense">
                                                <label>Trade License (First File) <a href="javascript:void(0)" class="custom-file-container__image-clear" title="Clear Image">x</a></label>
                                                <label class="custom-file-container__custom-file">
                                                    <input type="file" name="trade_license"
                                                           class="custom-file-container__custom-file__custom-file-input"
                                                           accept="image/*">
                                                    <input type="hidden" name="MAX_FILE_SIZE" value="10485760"/>
                                                    <span
                                                        class="custom-file-container__custom-file__custom-file-control"></span>
                                                </label>
                                                <div class="custom-file-container__image-preview"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="custom-file-container" data-upload-id="tradeLicenseSecond">
                                                <label>Trade License (Second File) <a href="javascript:void(0)" class="custom-file-container__image-clear" title="Clear Image">x</a></label>
                                                <label class="custom-file-container__custom-file">
                                                    <input type="file" name="trade_license_second" class="custom-file-container__custom-file__custom-file-input"
                                                           accept="image/*">
                                                    <input type="hidden" name="MAX_FILE_SIZE" value="10485760"/>
                                                    <span
                                                        class="custom-file-container__custom-file__custom-file-control"></span>
                                                </label>
                                                <div class="custom-file-container__image-preview"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <button class="btn btn-primary submit-fn mt-2" type="submit">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
