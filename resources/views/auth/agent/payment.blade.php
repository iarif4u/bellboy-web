@extends('auth.layouts.master')

@section('css')
    <!-- END PAGE LEVEL PLUGINS/CUSTOM STYLES -->
    <link rel="stylesheet" href="{{asset('plugins/editors/markdown/simplemde.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('plugins/table/datatable/datatables.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset("plugins/table/datatable/custom_dt_html5.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("plugins/table/datatable/dt-global_style.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("assets/css/users/user-profile.css")}}">
@endsection
@section('content')


    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
                <div class="widget-content">

                    <div class="widget-content widget-content-area br-6">
                        <div class="widget-heading">
                            <h5 class="">Agent Balance Sheet</h5>
                        </div>
                        <div class="table-responsive mb-4 mt-4">
                            <table id="html5-extension" class="table table-hover non-hover" style="width:100%">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Shop Name</th>
                                    <th>Phone</th>
                                    <th>Shop Address</th>
                                    <th>Balance</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($agents as $agent)
                                <tr>
                                    <td>{{$agent->name}}</td>
                                    <td>{{$agent->shopName->data_value}}</td>
                                    <td>{{$agent->phone}}</td>
                                    <td>{{$agent->shopAddress->data_value}}</td>
                                    <td>
                                        <div class="badge badge-info">{{$agent->balance}}</div>
                                    </td>

                                    <td>
                                        <a onclick="make_agent_payment('{{$agent->id}}','{{$agent->name}}','{{$agent->phone}}','{{$agent->shopName->data_value}}','{{$agent->shopAddress->data_value}}','{{$agent->balance}}')" class="btn btn-outline-dark mb-2 btn-sm" href="javascript:void(0);">Payment</a>
                                    </td>
                                </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
@endsection
@section('modal')
    <div class="modal fade paymentInvoice" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">

                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="myLargeModalLabel">Agent Payment</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                 viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                 stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                <line x1="6" y1="6" x2="18" y2="18"></line>
                            </svg>
                        </button>
                    </div>
                    <form action="{{route('agent.withdraw')}}" method="post">
                        @csrf
                        <input class="agent_id" type="hidden" name="agent_id">
                    <div class="modal-body">
                    <div class="layout-spacing">
                                <div class="content-section  animated animatedFadeInUp fadeInUp">
                                    <div class="row inv--head-section">
                                        <div class="col-md-6 mb-12">
                                            <h3 class="in-heading">INVOICE</h3>
                                        </div>
                                        <div class="col-md-6 mb-12 align-self-center text-sm-right">
                                            <div class="company-info">
                                                <h5 class="inv-brand-name">BellBoy</h5>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 align-self-center">
                                            <p class="inv-customer-name"></p>
                                            <p class="inv-street-phone"></p>
                                            <p class="inv-street-addr"></p>
                                        </div>
                                        <div class="col-md-6 align-self-center  text-sm-right order-2">
                                            <p class="inv-created-date">
                                                <span class="inv-title">Invoice Date : </span>
                                                <span class="inv-date">{{date('d-m-Y')}}</span>
                                            </p>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row inv--product-table-section">
                                        <div class="col-12">
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <thead class="">
                                                    <tr>
                                                        <th scope="col">Balance</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td class="balance" scope="col"></td>
                                                        <td class="text-right">
                                                            <input id="balance" type="number" name="paymentAmount"
                                                                   class="form-control">
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-6 mb-12">
                                            <div class="form-group">
                                                <label for="paymentType"> Payment Type *</label>
                                                <select id="paymentType" name="paymentType" class="form-control" onchange="showMe(this);">
                                                    <option value="Cash" selected="selected">Cash</option>
                                                    <option value="Bank">Bank</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6 mb-12">
                                            <div class="form-group">
                                                <label for="exampleFormControlInput1"> Payment Note </label>
                                                <textarea name="paymentNote" id="paymentNote"  name="paymentNote" cols="30" rows="1" class="form-control"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="idShowMe" style="display: none">
                                        <div class="form-row">

                                            <div class="col-md-6 mb-12">
                                                <div class="form-group">
                                                    <label for="bankName"> Bank Name *</label>
                                                    <input type="text" id="bankName" name="bankName" class="form-control" placeholder="Bank Name">
                                                </div>
                                            </div>
                                            <div class="col-md-6 mb-12">
                                                <div class="form-group">
                                                    <label for="accNo"> Acc No *</label>
                                                    <input type="text" id="accNo" name="accNo" class="form-control" placeholder="202002232323">
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                    </form>
                </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="{{asset('plugins/table/datatable/datatables.js')}}"></script>
    <script>
        function make_agent_payment(agent_id,agent_name,phone,shop,shop_address,balance){
            $(".inv-customer-name").text(agent_name);
            $(".inv-street-phone").text(phone);
            $(".inv-street-addr").text(shop_address);
            $(".balance").text(balance);
            $(".agent_id").val(agent_id);
            $("#balance").val(balance);
            $(".paymentInvoice").modal("show");
        }
    </script>
    <script>
        $('#html5-extension').DataTable({
            "oLanguage": {
                "oPaginate": {
                    "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                    "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
                },
                "sInfo": "Showing page _PAGE_ of _PAGES_",
                "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                "sSearchPlaceholder": "Search...",
                "sLengthMenu": "Results :  _MENU_",
            },
            "stripeClasses": [],
            "lengthMenu": [7, 10, 20, 50],
            "pageLength": 7
        });

        function showMe(e) {
            var strdisplay = e.options[e.selectedIndex].value;
            var e = document.getElementById("idShowMe");
            if (strdisplay == "Cash") {
                e.style.display = "none";
            } else {
                e.style.display = "block";
            }
        }
    </script>
@endsection
