@extends('auth.layouts.master')
@section('css')

    <link href="{{asset('assets/css/users/account-setting.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/css/users/user-profile.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('plugins/file-upload/file-upload-with-preview.min.css')}}" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="{{asset('plugins/table/datatable/datatables.css')}}">

    <link rel="stylesheet" href="{{asset('plugins/editors/markdown/simplemde.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset("plugins/table/datatable/custom_dt_html5.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("plugins/table/datatable/dt-global_style.css")}}">
@endsection


@section('content')
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
        <div class="widget widget-chart-one">
            <div class="widget-heading">
                <h5 class="">Brand List</h5>
                <ul class="tabs tab-pills">
                    <li>
                        <button type="button" class="btn btn-primary mb-2 mr-2" data-toggle="modal"
                                data-target="#addBrand">
                            Add Brand
                        </button>
                    </li>
                </ul>
            </div>

            <div class="widget-content">
                <div class="tabs tab-content">
                    <div id="content_1" class="tabcontent">
                        <div class="table-responsive mb-4 mt-4">
                            {{$dataTable->table(['class'=>"table table-hover non-hover","style"=>"width:100%"])}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <!-- BEGIN PAGE LEVEL CUSTOM SCRIPTS -->
    <script src="{{asset('plugins/table/datatable/datatables.js')}}"></script>
    <!-- NOTE TO Use Copy CSV Excel PDF Print Options You Must Include These Files  -->
    <script src="{{asset('plugins/table/datatable/button-ext/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('plugins/table/datatable/button-ext/jszip.min.js')}}"></script>
    <script src="{{asset('plugins/table/datatable/button-ext/buttons.html5.min.js')}}"></script>
    <script src="{{asset('plugins/table/datatable/button-ext/buttons.print.min.js')}}"></script>
    {{$dataTable->scripts()}}
    <script>
        function open_update_brand_modal(id, name, logo) {
            $("#updateBrand").modal('show');
            $(".update_brand_id").val($.trim(id));
            $("#update_brand_name").val($.trim(name));
            $(".brand-img").attr("src", logo);
        }

        function delete_brand(brand_id) {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success btn-rounded',
                    cancelButton: 'btn btn-danger btn-rounded mr-3',
                },
                buttonsStyling: false
            });
            swalWithBootstrapButtons.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    blockUI("#brands-table");
                    axios.delete(`{{route('brand.delete')}}?brand_id=${brand_id}`)
                        .then(function (response) {
                            swalWithBootstrapButtons.fire('Deleted!', response.data.message, 'success');
                        })
                        .catch(function (error) {
                            showError(error.response.data.message);
                        })
                        .then(function () {
                            $("#brands-table").DataTable().ajax.reload(null, false);
                            unBlockUI("#brands-table");
                        });
                } else if (
                    /* Read more about handling dismissals below */
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire('Cancelled', 'Brand delete cancelled', 'error');
                }
            });
        }
    </script>
@endsection

@section('modal')
    <!-- Modal -->
    <div id="addBrand" class="modal animated zoomInUp custo-zoomInUp" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"> New Brand</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                             viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                             stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                            <line x1="18" y1="6" x2="6" y2="18"></line>
                            <line x1="6" y1="6" x2="18" y2="18"></line>
                        </svg>
                    </button>
                </div>
                <form class="simple-example" action="{{route('brand.new')}}" novalidate="" method="post"
                      enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="form-row">
                            <div class="col-md-12 mb-12">
                                <div class="form-group">
                                    <label for="brand_name">Brand Name *</label>
                                    <input id="brand_name" type="text" name="brand_name" placeholder="Brand Name"
                                           class="form-control" required="required">
                                </div>
                            </div>
                            <div class="col-md-12 mb-12">
                                <div class="form-group">
                                    <label for="brand_img">Brand Image *</label>
                                    <div class="custom-file">
                                        <input type="file" name="brand_img" class="custom-file-input" id="brand_img">
                                        <label class="custom-file-label" for="brand_img">Choose file</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="updateBrand" class="modal animated zoomInUp custo-zoomInUp" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Update Brand</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                             viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                             stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                            <line x1="18" y1="6" x2="6" y2="18"></line>
                            <line x1="6" y1="6" x2="18" y2="18"></line>
                        </svg>
                    </button>
                </div>
                <form class="simple-example" action="{{route('brand.update')}}" method="post"
                      enctype="multipart/form-data" novalidate="">
                    @csrf
                    <input type="hidden" class="update_brand_id" name="brand_id">
                    <div class="modal-body">
                        <div class="form-row">
                            <div class="col-md-12 mb-12">
                                <div class="form-group">
                                    <label for="update_brand_name">Brand Name *</label>
                                    <input id="update_brand_name" type="text" name="brand_name" class="form-control"
                                           required="required">
                                </div>
                            </div>
                            <div class="col-md-8 mb-12">
                                <div class="form-group">
                                    <label for="brandImage">Brand Image</label>
                                    <div class="custom-file">
                                        <input type="file" name="brand_img" class="custom-file-input" id="brandImage">
                                        <label class="custom-file-label" for="brandImage">Choose file</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 mb-12">
                                <div class="text-center user-info">
                                    <img height="120" width="120" src="{{asset('assets/img/120x120.jpg')}}"
                                         class="brand-img" alt="logo">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
