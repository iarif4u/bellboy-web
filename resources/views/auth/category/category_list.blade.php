@extends('auth.layouts.master')
@section('css')
    <link href="{{asset('assets/css/users/account-setting.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/css/users/user-profile.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('plugins/file-upload/file-upload-with-preview.min.css')}}" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="{{asset('plugins/table/datatable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/editors/markdown/simplemde.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset("plugins/table/datatable/custom_dt_html5.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("plugins/table/datatable/dt-global_style.css")}}">
    <link href="{{asset('plugins/select2/select2.min.css')}}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
        <div class="widget widget-chart-one">
            <div class="widget-heading">
                <h5 class="">Category List</h5>
                <ul class="tabs tab-pills">
                    <li>
                        <button type="button" class="btn btn-primary mb-2 mr-2" data-toggle="modal" data-target="#addSubCategory">
                            Category
                        </button>
                    </li>
                </ul>
            </div>

            <div class="widget-content">
                <div class="tabs tab-content">
                    <div id="content_1" class="tabcontent">
                        <div class="table-responsive mb-4 mt-4">
                            {{$dataTable->table(['class'=>"table table-hover non-hover","style"=>"width:100%"])}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modal')
    <!-- Modal -->
    <div class="modal category-modal fade" id="addSubCategory" role="dialog" aria-labelledby="exampleModalCenterTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add New Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                             viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                             stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                            <line x1="18" y1="6" x2="6" y2="18"></line>
                            <line x1="6" y1="6" x2="18" y2="18"></line>
                        </svg>
                    </button>
                </div>
                <form enctype="multipart/form-data" class="simple-example" action="{{route('category.new')}}"
                      method="post" novalidate="">
                    @csrf
                    <div class="modal-body">
                        <div class="form-row">
                            <div class="col-md-12 mb-12">
                                <div class="form-group">
                                    <label for="category_name">Category Name *</label>
                                    <input id="category_name" type="text" name="category_name"
                                           placeholder="Category Name" class="form-control" required="required">
                                </div>
                            </div>
                            <div class="col-md-12 mb-12">
                                <div class="form-group">
                                    <label for="parent_category">Parent Category</label>
                                    <select class="form-control" name="parent_category" id="parent_category">
                                        <option selected=""></option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12 mb-12">
                                <div class="form-group">
                                    <label for="category_image">Category Image *</label>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" name="category_image" id="category_image">
                                        <label class="custom-file-label" for="categoryImage">Choose file</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>

    </div>
    <div id="updateSubCategory" class="modal category-modal animated zoomInUp custo-zoomInUp" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Update Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                             viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                             stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                            <line x1="18" y1="6" x2="6" y2="18"></line>
                            <line x1="6" y1="6" x2="18" y2="18"></line>
                        </svg>
                    </button>
                </div>
                <form class="simple-example" action="{{route('category.update')}}" novalidate="" method="post"
                      enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" id="update_category_id" name="category_id">
                    <div class="modal-body">
                        <div class="form-row">
                            <div class="col-md-12 mb-12">
                                <div class="form-group">
                                    <label for="update_category_name">Category Name *</label>
                                    <input id="update_category_name" type="text" name="category_name" class="form-control" required="required">
                                </div>
                            </div>
                            <div class="col-md-12 mb-12">
                                <div class="form-group">
                                    <label for="update_parent_category">Parent Category</label>
                                    <select class="form-control" name="parent_category" id="update_parent_category"></select>
                                </div>
                            </div>
                            <div class="col-md-8 mb-12">
                                <div class="form-group">
                                    <label for="update_category_image">Category Image</label>
                                    <div class="custom-file">
                                        <input name="category_image" type="file" class="custom-file-input" id="update_category_image">
                                        <label class="custom-file-label" for="update_category_image">Choose file</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 mb-12">
                                <div class="text-center user-info">
                                    <img class="category-img-file" height="120" width="120" alt="avatar" src="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <!-- BEGIN PAGE LEVEL CUSTOM SCRIPTS -->
    <script src="{{asset('plugins/table/datatable/datatables.js')}}"></script>
    <!-- NOTE TO Use Copy CSV Excel PDF Print Options You Must Include These Files  -->
    <script src="{{asset('plugins/table/datatable/button-ext/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('plugins/table/datatable/button-ext/jszip.min.js')}}"></script>
    <script src="{{asset('plugins/table/datatable/button-ext/buttons.html5.min.js')}}"></script>
    <script src="{{asset('plugins/table/datatable/button-ext/buttons.print.min.js')}}"></script>
    <script src="{{asset('plugins/select2/select2.min.js')}}"></script>
    {{$dataTable->scripts()}}
    <script>
        function delete_category(category_id) {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success btn-rounded',
                    cancelButton: 'btn btn-danger btn-rounded mr-3',
                },
                buttonsStyling: false
            });
            swalWithBootstrapButtons.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    blockUI("#categories-table");
                    axios.delete(`{{route('category.delete')}}?category_id=${category_id}`)
                        .then(function (response) {
                            swalWithBootstrapButtons.fire('Deleted!', response.data.message, 'success');
                        })
                        .catch(function (error) {
                            showError(error.response.data.message);
                        })
                        .then(function () {
                            $("#categories-table").DataTable().ajax.reload(null, false);
                            unBlockUI("#categories-table");
                        });
                } else if (
                    /* Read more about handling dismissals below */
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire('Cancelled', 'Category delete cancelled', 'error');
                }
            });
        }
        function open_update_category_modal(category_id, parent_id, parent_name, category_name, category_img) {
            $("#updateSubCategory").modal("show");
            let $newOption;
            if (parent_id) {
                $newOption = $("<option selected='selected'></option>").val(parent_id).text(parent_name);
            } else {
                $newOption = $("<option selected='selected'></option>").val(null).text("Select One");
            }
            $("#update_parent_category").append($newOption).trigger('change');
            $(".category-img-file").attr("src", category_img);
            $("#update_category_name").val(category_name);
            $("#update_category_id").val(category_id);
        }
    </script>
    <script>
        $("#parent_category").select2({
            dropdownParent: $("#addSubCategory"),
            placeholder: "Select Category",
            ajax: {
                url: '{{route('category.search')}}',
                dataType: 'json',
                data: function (params) {
                    return {
                        search: params.term,
                        type: "public"
                    };
                },
                processResults: function (response) {
                    return {
                        results: $.map(response.data, function (txt, val) {
                            return {id: txt.id, text: txt.name}
                        })
                    };
                }
            }
        });

        $("#update_parent_category").select2({
            dropdownParent: $("#updateSubCategory"),
            allowClear: true,
            placeholder: "Select Category",
            ajax: {
                url: '{{route('category.search')}}',
                dataType: 'json',
                data: function (params) {
                    // Query parameters will be ?search=[term]&type=public
                    return {
                        search: params.term,
                        type: "public"
                    };
                },
                processResults: function (response) {
                    return {
                        results: $.map(response.data, function (txt, val) {
                            return {id: txt.id, text: txt.name}
                        })
                    };
                }
                // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
            }
        });
    </script>
@endsection

