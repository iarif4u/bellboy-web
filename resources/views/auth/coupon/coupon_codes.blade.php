@extends('auth.layouts.master')

@section('content')
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
        <div class="widget widget-chart-one">
            <div class="widget-heading">
                <h5 class="">Coupon Code</h5>
                <ul class="tabs tab-pills">
                    <li>
                        <button type="button" class="btn btn-primary mb-2 mr-2" data-toggle="modal"
                                data-target="#addCouponCode">
                            Add Coupon Code
                        </button>
                    </li>
                </ul>
            </div>

            <div class="widget-content">
                <div class="tabs tab-content">
                    <div id="content_1" class="tabcontent">
                        <div class="table-responsive mb-4 mt-4">
                            {{$dataTable->table(['class'=>"table table-hover non-hover","style"=>"width:100%"])}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('modal')
    <!-- Modal -->
    <div class="modal fade" id="addCouponCode" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add New Coupon</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                             viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                             stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                            <line x1="18" y1="6" x2="6" y2="18"></line>
                            <line x1="6" y1="6" x2="18" y2="18"></line>
                        </svg>
                    </button>
                </div>
                <form class="simple-example" method="post" action="{{route("coupon.new_code")}}" novalidate="">
                    @csrf
                    <div class="modal-body">
                        <div class="form-row">
                            <div class="col-md-6 mb-12">

                                <div class="form-group">
                                    <label for="coupon_code">Coupon Code *</label>
                                    <div class="input-group">
                                        <input id="coupon_code" type="text" name="coupon_code" class="form-control" placeholder="Coupon Code">
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6 mb-12">
                                <div class="form-group">
                                    <label for="type"> Type *</label>
                                    <select id="type" name="type" class="form-control new-coupon-codes">
                                        <option value="">Select Type</option>
                                        <option value="{{config('const.coupon_codes.fixed')}}">Fixed (৳)</option>
                                        <option value="{{config('const.coupon_codes.percentage')}}">Percentage (%)</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12 mb-12">
                                <div class="form-group">
                                    <label for="expire_date">Expired Date</label>
                                    <input id="expire_date" type="text" name="expire_date" class="form-control date-picker" required="">
                                </div>
                            </div>
                            <div class="col-md-6 mb-12">
                                <div class="form-group">
                                    <label for="amount">Amount *</label>
                                    <input type="number" id="amount" name="amount" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6 mb-12">
                                <div class="form-group">
                                    <label for="max_amount">Maximum Amount</label>
                                    <input type="number" id="max_amount" name="max_amount" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12 mb-12">
                                <div class="form-group">
                                    <label for="qty">Qty *</label>
                                    <input type="number" id="qty" name="qty" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>

    </div>
    <div id="updateCoupon" class="modal animated zoomInUp custo-zoomInUp" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Update Coupon</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                             viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                             stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                            <line x1="18" y1="6" x2="6" y2="18"></line>
                            <line x1="6" y1="6" x2="18" y2="18"></line>
                        </svg>
                    </button>
                </div>
                <form method="post" class="simple-example" action="{{route('coupon.update_code')}}" novalidate="">
                    @csrf
                    <input type="hidden" id="coupon_code_id" name="coupon_code_id">
                    <div class="modal-body">
                        <div class="form-row">
                            <div class="col-md-6 mb-12">
                                <div class="form-group">
                                    <label for="update_coupon_code">Coupon Code *</label>
                                    <div class="input-group">
                                        <input id="update_coupon_code" type="text" name="coupon_code" class="form-control" placeholder="Coupon Code">
                                        <div class="input-group-append">
                                            <button class="btn btn-dark dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor"
                                                     stroke-width="2" fill="none" stroke-linecap="round"
                                                     stroke-linejoin="round" class="css-i6dzq1">
                                                    <polyline points="23 4 23 10 17 10"></polyline>
                                                    <polyline points="1 20 1 14 7 14"></polyline>
                                                    <path d="M3.51 9a9 9 0 0 1 14.85-3.36L23 10M1 14l4.64 4.36A9 9 0 0 0 20.49 15"></path>
                                                </svg>
                                            </button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6 mb-12">
                                <div class="form-group">
                                    <label for="update_type"> Type *</label>
                                    <select id="update_type" name="type" class="form-control coupon-codes-update">
                                        <option value="">Select Type</option>
                                        <option value="{{config('const.coupon_codes.fixed')}}">Fixed (৳)</option>
                                        <option value="{{config('const.coupon_codes.percentage')}}">Percentage (%)</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12 mb-12">
                                <div class="form-group">
                                    <label for="update_expire_date">Expired Date</label>
                                    <input id="update_expire_date" type="text" name="expire_date" class="form-control date-picker" required="">
                                </div>
                            </div>
                            <div class="col-md-6 mb-12">
                                <div class="form-group">
                                    <label for="update_amount">Amount *</label>
                                    <input type="number" id="update_amount" name="amount" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6 mb-12">
                                <div class="form-group">
                                    <label for="update_max_amount">Maximum Amount</label>
                                    <input type="number" id="update_max_amount" name="max_amount" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12 mb-12">
                                <div class="form-group">
                                    <label for="update_qty">Qty *</label>
                                    <input type="number" id="update_qty" name="qty" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('css')
    <link rel="stylesheet" href="{{asset('plugins/select2/select2.min.css')}}">
    <link href="{{asset('plugins/datepicker/css/datepicker.css')}}" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="{{asset('plugins/table/datatable/datatables.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset("plugins/table/datatable/custom_dt_html5.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("plugins/table/datatable/dt-global_style.css")}}">
@endsection
@section('js')
    <script src="{{asset('plugins/select2/select2.min.js')}}"></script>
    <script src="{{asset('plugins/datepicker/js/bootstrap-datepicker.js')}}"></script>
    <script src="{{asset('plugins/table/datatable/datatables.js')}}"></script>
    <script src="{{asset('plugins/select2/select2.min.js')}}"></script>
    <script src="{{asset('plugins/datepicker/js/bootstrap-datepicker.js')}}"></script>
    {{$dataTable->scripts()}}
    <script>
        $(".new-coupon-codes").select2({
            dropdownParent: $("#addCouponCode")
        });
        $("#update_type").select2({
            dropdownParent: $("#updateCoupon")
        });
        $('.date-picker').datepicker({
            format: 'yyyy-mm-dd',
        });
    </script>
    <script !src="">
        function open_update_modal(coupon_id,coupon_code,type,expire_date,amount,max_amount,qty) {
            $("#coupon_code_id").val(coupon_id);
            $("#update_qty").val(qty);
            $("#update_max_amount").val(max_amount);
            $("#update_amount").val(amount);
            $('#update_expire_date').datepicker('update',expire_date);
            $("#update_type").val(type).trigger('change');
            $("#update_coupon_code").val(coupon_code);
            $("#updateCoupon").modal("show");
        }
        function delete_coupon(coupon_id) {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success btn-rounded',
                    cancelButton: 'btn btn-danger btn-rounded mr-3',
                },
                buttonsStyling: false
            });
            swalWithBootstrapButtons.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    blockUI("#discounts-table");
                    axios.delete(`{{route('coupon.delete_code')}}?coupon_id=${coupon_id}`)
                        .then(function (response) {
                            swalWithBootstrapButtons.fire('Deleted!', response.data.message, 'success');
                        })
                        .catch(function (error) {
                            showError(error.response.data.message);
                        })
                        .then(function () {
                            $("#discounts-table").DataTable().ajax.reload(null, false);
                            unBlockUI("#discounts-table");
                        });
                } else if (
                    /* Read more about handling dismissals below */
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire('Cancelled', 'Coupon code delete cancelled', 'error');
                }
            });
        }
    </script>
@endsection
