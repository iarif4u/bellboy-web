@extends('auth.layouts.master')

@section('css')
    <!-- END PAGE LEVEL PLUGINS/CUSTOM STYLES -->
    <link rel="stylesheet" href="{{asset('plugins/editors/markdown/simplemde.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('plugins/table/datatable/datatables.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset("plugins/table/datatable/custom_dt_html5.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("plugins/table/datatable/dt-global_style.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("assets/css/users/user-profile.css")}}">
@endsection
@section('content')

    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
        <div class="widget-content">

            <div class="widget-content widget-content-area br-6">
                <div class="widget-heading">
                    <h5 class="">Customer List</h5>
                </div>
                <div class="table-responsive mb-4 mt-4">
                    {{$dataTable->table(['class'=>"table table-hover non-hover","style"=>"width:100%"])}}
                </div>
            </div>
        </div>

    </div>

@endsection

@section('modal')
    <div class="modal fade agentDetails" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="myExtraLargeModalLabel">Agent Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                             viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                             stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                            <line x1="18" y1="6" x2="6" y2="18"></line>
                            <line x1="6" y1="6" x2="18" y2="18"></line>
                        </svg>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <!-- Content -->
                        <div class="col-xl-4 col-lg-6 col-md-5 col-sm-12 layout-top-spacing">
                            <div class="user-profile">
                                <div class="widget-content widget-content-area">
                                    <div class="d-flex justify-content-between">
                                        <h3 class="">Profile</h3>
                                        <a href="javascript:void (0);" class="mt-2 edit-profile"> </a>
                                    </div>
                                    <div class="text-center user-info">
                                        <img class="agent-img" src="{{asset("assets/img/600x300.jpg")}}" height="100"
                                             width="100" alt="avatar">
                                        <p class="agent-name">Agent Name</p>
                                    </div>
                                    <div class="user-info-list">

                                        <div class="">
                                            <ul class="contacts-block list-unstyled">
                                                <li class="contacts-block__item">
                                                    <a class="agent-email-to" href="mailto:example@mail.com">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                             viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                             stroke-width="2" stroke-linecap="round"
                                                             stroke-linejoin="round" class="feather feather-mail">
                                                            <path
                                                                d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path>
                                                            <polyline points="22,6 12,13 2,6"></polyline>
                                                        </svg>
                                                        <span class="agent-email">example@mail.com</span></a>
                                                </li>
                                                <li class="contacts-block__item">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                         viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                         stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                         class="feather feather-phone">
                                                        <path
                                                            d="M22 16.92v3a2 2 0 0 1-2.18 2 19.79 19.79 0 0 1-8.63-3.07 19.5 19.5 0 0 1-6-6 19.79 19.79 0 0 1-3.07-8.67A2 2 0 0 1 4.11 2h3a2 2 0 0 1 2 1.72 12.84 12.84 0 0 0 .7 2.81 2 2 0 0 1-.45 2.11L8.09 9.91a16 16 0 0 0 6 6l1.27-1.27a2 2 0 0 1 2.11-.45 12.84 12.84 0 0 0 2.81.7A2 2 0 0 1 22 16.92z"></path>
                                                    </svg>
                                                    <span class="agent-phone">8801921000000</span>

                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-8 col-lg-6 col-md-7 col-sm-12 layout-top-spacing">

                            <div class="bio layout-spacing ">
                                <div class="widget-content widget-content-area">
                                    <h3 class="">Bio</h3>
                                    <p>Shop Name : <span class="shop-name">WinnerDevs</span></p>
                                    <p>Agreement Date : <span class="agreement-date">20/02/20202</span></p>
                                    <p>Shop Address : <span
                                            class="shop-address">House 52, Road 28 , Agrabad Chittagong</span></p>

                                    <div class="bio-skill-box">

                                        <div class="row">

                                            <div class="col-12 col-xl-6 col-lg-12 mb-xl-0 mb-5 ">

                                                <div class="d-flex">
                                                    <div>
                                                    </div>
                                                    <div class="">
                                                        <img class="nid-card" src="" height="200" width="200"
                                                             alt="Nid Card">
                                                        <p class="mt-2"> NID Card </p>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="col-12 col-xl-6 col-lg-12 mb-xl-0 mb-0 ">

                                                <div class="d-flex">
                                                    <div>
                                                    </div>
                                                    <div class="">
                                                        <img src="" class="trade-license" height="200" width="200"
                                                             alt="Trade License">
                                                        <p class="mt-2"> Trade License</p>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                </div>
                            </div>

                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <!-- BEGIN PAGE LEVEL CUSTOM SCRIPTS -->
    <script src="{{asset('plugins/table/datatable/datatables.js')}}"></script>
    <!-- NOTE TO Use Copy CSV Excel PDF Print Options You Must Include These Files  -->
    <script src="{{asset('plugins/table/datatable/button-ext/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('plugins/table/datatable/button-ext/jszip.min.js')}}"></script>
    <script src="{{asset('plugins/table/datatable/button-ext/buttons.html5.min.js')}}"></script>
    <script src="{{asset('plugins/table/datatable/button-ext/buttons.print.min.js')}}"></script>
    {{$dataTable->scripts()}}
    <script>

        function view_agent(agent_id) {
            blockUI("#agents-table");
            axios.post(`{{route('agent.view')}}`, {
                agent_id: agent_id,
                _token: '{{csrf_token()}}'
            })
                .then(function (response) {
                    const profile = response.data;
                    $(".agent-img").attr("src", profile.profile_picture);
                    $(".trade-license").attr("src", profile.trade_license);
                    $(".nid-card").attr("src", profile.nid_card);
                    $(".agent-email-to").attr("href", `mailto:${profile.email}`);
                    $(".shop-name").html(profile.shopName);
                    $(".agreement-date").html(profile.agreement_date);
                    $(".shop-address").html(profile.shopAddress);
                    $(".agent-name").text(profile.name);
                    $(".agent-email").text(profile.email);
                    $(".agent-phone").text(profile.phone);
                })
                .catch(function (error) {
                    console.log(error);
                })
                .then(function () {
                    $(".agentDetails").modal("show");
                    unBlockUI("#agents-table");
                });

        }

        function change_status(agent_id, agent_status) {
            blockUI("#agents-table");
            axios.post('{{route('agent.status')}}', {
                agent_id: agent_id,
                agent_status: agent_status,
                _token: '{{csrf_token()}}'
            })
                .then(function (response) {
                    showSuccess(response.data.message)
                })
                .catch(function (error) {
                    console.log(error);
                })
                .then(function () {
                    $("#agents-table").DataTable().ajax.reload(null, false);
                    unBlockUI("#agents-table");
                });

        }

        function delete_agent(agent_id) {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success btn-rounded',
                    cancelButton: 'btn btn-danger btn-rounded mr-3',
                },
                buttonsStyling: false
            });

            swalWithBootstrapButtons.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    blockUI("#agents-table");
                    axios.delete(`{{route('agent.delete')}}?agent_id=${agent_id}`)
                        .then(function (response) {
                            swalWithBootstrapButtons.fire('Deleted!', response.data.message, 'success');
                        })
                        .catch(function (error) {
                            showError(error.response.data.message);
                        })
                        .then(function () {
                            $("#agents-table").DataTable().ajax.reload(null, false);
                            unBlockUI("#agents-table");
                        });
                } else if (
                    /* Read more about handling dismissals below */
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire('Cancelled', 'Agent delete cancelled', 'error');
                }
            })
        }
    </script>
@endsection
