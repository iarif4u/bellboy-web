@extends('auth.layouts.master')

@section('content')
    {{--Start Row--}}
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
        <div class="widget">
            <div class="widget-heading">
                <h5 class="">Order's Overall View</h5>
            </div>
            <div class="widget-content">
                <button type="button"  style="background-color: #1abc9c; color:#fff;"  class="btn position-relative btn-lg mt-2 mb-2">
                    <span>Pending</span>
                    <span class="badge badge-danger counter">{{$orders_counter['pending_orders']}}</span>
                </button>
                <button type="button"  style="background-color: #3498db; color:#fff;"  class="btn position-relative btn-lg mt-2 mb-2">
                    <span>Approve</span>
                    <span class="badge badge-danger counter">{{$orders_counter['approve_orders']}}</span>
                </button>
                <button type="button" style="background-color: #34495e; color:#fff;" class="btn position-relative btn-lg mt-2 mb-2">
                    <span>Picked up</span>
                    <span class="badge badge-danger counter">{{$orders_counter['pending_orders']}}</span>
                </button>
                <button type="button" style="background-color: #f1c40f; color:#fff;" class="btn position-relative btn-lg mt-2 mb-2">
                    <span>In progress</span>
                    <span class="badge badge-danger counter">{{$orders_counter['processed_orders']}}</span>
                </button>
                <button type="button" style="background-color: #e67e22; color:#fff;" class="btn position-relative btn-lg mt-2 mb-2">
                    <span>Packed</span>
                    <span class="badge badge-danger counter">{{$orders_counter['packed_orders']}}</span>
                </button>
                <button type="button" style="background-color: #c0392b; color:#fff;" class="btn position-relative btn-lg mt-2 mb-2">
                    <span>Delivering</span>
                    <span class="badge badge-danger counter">{{$orders_counter['delivering_orders']}}</span>
                </button>
                <button type="button" style="background-color: #27ae60; color:#fff;"  class="btn position-relative btn-lg mt-2 mb-2">
                    <span>Successful</span>
                    <span class="badge badge-danger counter">{{$orders_counter['complete_orders']}}</span>
                </button>
                <button type="button" style="background-color: #546de5; color:#fff;" class="btn position-relative btn-lg mt-2 mb-2">
                    <span>Canceled</span>
                    <span class="badge badge-danger counter">{{$orders_counter['canceled_orders']}}</span>
                </button>

            </div>
        </div>
    </div>
    {{--End Row--}}

    <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
        <div class="widget widget-table-two">

            <div class="widget-heading">
                <h5 class="">Recent Orders</h5>
            </div>

            <div class="widget-content">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>
                                <div class="th-content">Customer</div>
                            </th>
                            <th>
                                <div class="th-content">Product</div>
                            </th>
                            <th>
                                <div class="th-content">Invoice</div>
                            </th>
                            <th>
                                <div class="th-content th-heading">Price</div>
                            </th>
                            <th>
                                <div class="th-content">Status</div>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($orders->count()>0)
                            @foreach($orders as $order)
                            <tr>
                                <td>
                                    @if($order->customer->name)
                                        <div class="td-content customer-name">{{$order->customer->name}}</div>
                                    @else
                                        <div class="td-content customer-name">{{$order->customer->phone}}</div>
                                    @endif
                                </td>
                                <td>
                                    <div class="td-content product-brand text-center">{{$order->items->count()}}</div>
                                </td>
                                <td>
                                    <div class="td-content">{{$order->order_id}}</div>
                                </td>
                                <td>
                                    <div class="td-content pricing"><span class="">{{order_total($order)}}</span></div>
                                </td>
                                <td>
                                    @if($order->order_status == config('const.order_status.complete'))
                                        <div class="td-content"><span class="badge outline-badge-success">{{ucfirst($order->order_status)}}</span></div>
                                    @elseif($order->order_status == config('const.order_status.deny'))
                                        <div class="td-content"><span class="badge outline-badge-danger">{{ucfirst($order->order_status)}}</span></div>
                                    @else
                                        <div class="td-content"><span class="badge outline-badge-primary">{{ucfirst($order->order_status)}}</span></div>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <br>
        <div class="widget widget-table-one">
            <div class="widget-heading">
                <h5 class="">Transactions</h5>
            </div>

            <div class="widget-content">
                @if($statements->count()>0)
                    @foreach($statements as $statement)
                        @if($statement->type == \App\Models\AccountExpense::class)
                            <div class="transactions-list">
                                <div class="t-item">
                                    <div class="t-company-name">
                                        <div class="t-name">
                                            <h4>{{$statement->expense->expense_name}}</h4>
                                            <p class="meta-date">{{\Carbon\Carbon::parse($statement->created_at)->format('d M h:i A')}}</p>
                                        </div>

                                    </div>
                                    <div class="t-rate rate-dec">
                                        <p><span>-{{$statement->amount}}</span>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                                 fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                                 stroke-linejoin="round" class="feather feather-arrow-down">
                                                <line x1="12" y1="5" x2="12" y2="19"></line>
                                                <polyline points="19 12 12 19 5 12"></polyline>
                                            </svg>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="transactions-list">
                                <div class="t-item">
                                    <div class="t-company-name">
                                        <div class="t-name">
                                            <h4>{{$statement->income->income_name}}</h4>
                                            <p class="meta-date">{{\Carbon\Carbon::parse($statement->created_at)->format('d M h:i A')}}</p>
                                        </div>
                                    </div>
                                    <div class="t-rate rate-inc">
                                        <p><span>+{{$statement->amount}}</span>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                                 fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                                 stroke-linejoin="round" class="feather feather-arrow-up">
                                                <line x1="12" y1="19" x2="12" y2="5"></line>
                                                <polyline points="5 12 12 5 19 12"></polyline>
                                            </svg>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                @endif
            </div>
        </div>
    </div>

    <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
        <div class="widget widget-table-three">

            <div class="widget-heading">
                <h5 class="">Top Selling Product</h5>
            </div>

            <div class="widget-content">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>
                                <div class="th-content">Product</div>
                            </th>
                            <th>
                                <div class="th-content th-heading">Price</div>
                            </th>
                            <th>
                                <div class="th-content">Unit</div>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($popular_variations->count()>0)
                            @foreach($popular_variations as $variation)
                        <tr>
                            <td>
                                <div class="td-content product-name">
                                    <img src="{{get_media_file($variation,config('const.product.media.product_image'))}}" alt="product">
                                    {{$variation->variant_name}}
                                </div>
                            </td>
                            <td>
                                <div class="td-content"><span class="pricing">{{$variation->price}}</span></div>
                            </td>
                            <td>
                                <div class="td-content">{{$variation->unit_value}} {{$variation->unit->unit_name}}</div>
                            </td>
                        </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


@endsection
@section('js')
    <!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS -->
    <script src="{{asset("plugins/apex/apexcharts.min.js")}}"></script>
    <script src="{{asset("assets/js/dashboard/dash_1.js")}}"></script>
    <!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS -->
@endsection
