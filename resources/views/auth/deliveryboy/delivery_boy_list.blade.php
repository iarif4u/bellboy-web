@extends('auth.layouts.master')
@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('plugins/dropify/dropify.min.css')}}">
    <link href="{{asset('assets/css/users/user-profile.css')}}" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="{{asset('plugins/table/datatable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/editors/markdown/simplemde.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset("plugins/table/datatable/custom_dt_html5.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("plugins/table/datatable/dt-global_style.css")}}">
@endsection

@section('content')
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
        <div class="widget widget-chart-one">
            <div class="widget-heading"><h5 class="">Delivery Boy List</h5></div>
            <div class="widget-content">
                <div class="tabs tab-content">
                    <div id="content_1" class="tabcontent">
                        <div class="table-responsive mb-4 mt-4">
                            {{$dataTable->table(['class'=>"table table-hover non-hover","style"=>"width:100%"])}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modal')
    <div id="deliveryBoyDetails" class="modal animated zoomInUp custo-zoomInUp" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Delivery Boy Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                             viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                             stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                            <line x1="18" y1="6" x2="6" y2="18"></line>
                            <line x1="6" y1="6" x2="18" y2="18"></line>
                        </svg>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="col-md-6 mb-6">
                            <div class="form-group">
                                <label for="vName">Name</label>
                                <input id="vName" type="text" name="name" class="form-control" readonly="readonly">
                            </div>
                        </div>
                        <div class="col-md-6 mb-6">
                            <div class="form-group">
                                <label for="deliveryboy-phone">Phone No</label>
                                <input id="deliveryboy-phone" type="tel" name="" class="form-control" readonly="readonly">
                            </div>
                        </div>
                        <div class="col-md-6 mb-6">
                            <div class="form-group">
                                <label for="deliveryboy-area">Area</label>
                                <input id="deliveryboy-area" type="text" class="form-control" readonly="readonly">
                            </div>
                        </div>
                        <div class="col-md-6 mb-6">
                            <div class="form-group">
                                <label for="deliveryboy-address">Address</label>
                                <textarea class="form-control" id="deliveryboy-address" rows="1" readonly="readonly">This is address</textarea>
                            </div>
                        </div>
                        <div class="col-md-6 mb-6">
                            <div class="form-group">
                                <label for="exampleFormControlInput1">Profile Picture</label>
                                <div class="text-center user-info">
                                    <img height="200" width="200" src="{{asset('assets/img/120x120.jpg')}}" class="profile-picture" alt="avatar">

                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mb-6">
                            <div class="form-group">
                                <label for="exampleFormControlInput1">NID Card </label>
                                <div class="text-center user-info">
                                    <img height="200" width="200" class="nid-card" src="{{asset('assets/img/120x120.jpg')}}" alt="Nid Card">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer md-button">
                    <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <!-- BEGIN PAGE LEVEL CUSTOM SCRIPTS -->
    <script src="{{asset('plugins/table/datatable/datatables.js')}}"></script>
    <!-- NOTE TO Use Copy CSV Excel PDF Print Options You Must Include These Files  -->
    <script src="{{asset('plugins/table/datatable/button-ext/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('plugins/table/datatable/button-ext/jszip.min.js')}}"></script>
    <script src="{{asset('plugins/table/datatable/button-ext/buttons.html5.min.js')}}"></script>
    <script src="{{asset('plugins/table/datatable/button-ext/buttons.print.min.js')}}"></script>
    {{$dataTable->scripts()}}
    <script>
        function getDeliveryBoyView(delivery_boy_id) {
            $('#deliveryBoyDetails').modal("show");
            blockUI("#delivery-boys-table");
            axios.post('{{route('deliveryboy.view')}}', {
                delivery_boy_id: delivery_boy_id,
                _token: '{{csrf_token()}}'
            })
                .then(function (response) {
                    $("#vName").val(response.data.name);
                    $("#deliveryboy-address").val(response.data.address.data_value);
                    $("#deliveryboy-area").val(response.data.area.area_name);
                    $("#deliveryboy-phone").val(response.data.phone);
                    $(".profile-picture").attr('src',response.data.profile);
                    $(".nid-card").attr('src',response.data.nid_card);
                })
                .catch(function (error) {
                    console.log(error);
                })
                .then(function () {
                    unBlockUI("#delivery-boys-table");
                });
        }

        function deleteDeliveryBoy(delivery_boy_id) {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success btn-rounded',
                    cancelButton: 'btn btn-danger btn-rounded mr-3',
                },
                buttonsStyling: false
            });

            swalWithBootstrapButtons.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    blockUI("#delivery-boys-table");
                    axios.delete(`{{route('deliveryboy.delete')}}?delivery_boy_id=${delivery_boy_id}`)
                        .then(function (response) {
                            swalWithBootstrapButtons.fire('Deleted!', response.data.message, 'success');
                        })
                        .catch(function (error) {
                            showError(error.response.data.message);
                        })
                        .then(function () {
                            $("#delivery-boys-table").DataTable().ajax.reload(null, false);
                            unBlockUI("#delivery-boys-table");
                        });
                } else if (
                    /* Read more about handling dismissals below */
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire('Cancelled', 'Delivery boy delete cancelled', 'error');
                }
            });

        }
    </script>
@endsection
