@extends('auth.layouts.master')
@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('plugins/dropify/dropify.min.css')}}">
    <link href="{{asset('assets/css/users/account-setting.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/css/users/user-profile.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('plugins/file-upload/file-upload-with-preview.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('plugins/select2/select2.min.css')}}" rel="stylesheet" type="text/css"/>
@endsection
@section('js')
    <script src="{{asset('assets/js/users/account-settings.js')}}"></script>
    <script src="{{asset('plugins/file-upload/file-upload-with-preview.min.js')}}"></script>
    <script src="{{asset('plugins/dropify/dropify.min.js')}}"></script>
    <script src="{{asset('plugins/select2/select2.min.js')}}"></script>
    <script>
        $(".area").select2({
            ajax: {
                url: '{{route('deliveryboy.area.search')}}',
                dataType: 'json',
                data: function (params) {
                    // Query parameters will be ?search=[term]&type=public
                    return {
                        search: params.term,
                        type: "public"
                    };
                },
                processResults: function (response) {
                    return {
                        results: $.map(response.data, function (txt, val) {
                            return {id: txt.id, text: txt.area_name}
                        })
                    };
                }
                // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
            }
        });
    </script>
@endsection
@section('content')
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
        <div class="widget widget-chart-one">
            <div class="widget-heading">
                <h5 class=""> Update Delivery Boy Details</h5>
                <!--<ul class="tabs tab-pills">
                    <li><a href="javascript:void(0);" id="tb_1" class="tabmenu">Monthly</a></li>
                </ul>-->
            </div>

            <div class="widget-content">
                <div class="tabs tab-content">
                    <div id="content_1" class="tabcontent">
                        <form method="post" class="simple-example" action="{{route('deliveryboy.edit',['deliveryboy_id'=>$deliveryboy->id])}}" novalidate="" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="deliveryboy_id" value="{{$deliveryboy->id}}">
                            <div class="form-row">
                                <div class="col-md-6 mb-6">
                                    <div class="form-group">
                                        <label for="employeeID">Employee ID *</label>
                                        <input id="employeeID" type="text" name="employeeID" value="{{$deliveryboy->employee_id}}" class="form-control" required="">
                                    </div>
                                </div>
                                <div class="col-md-6 mb-6">
                                    <div class="form-group">
                                        <label for="name">Name *</label>
                                        <input id="name" type="text" name="name" value="{{$deliveryboy->name}}" class="form-control" required="">
                                    </div>
                                </div>


                                <div class="col-md-6 mb-6">
                                    <div class="form-group">
                                        <label for="phone">Phone No *</label>
                                        <input id="phone" type="tel" name="phone" value="{{$deliveryboy->phone}}" class="form-control" required="">
                                    </div>
                                </div>
                                <div class="col-md-6 mb-6">
                                    <div class="form-group">
                                        <label for="email">E-Mail *</label>
                                        <input id="email" type="email" name="email" value="{{$deliveryboy->email}}" class="form-control" required="">
                                    </div>
                                </div>
                                <div class="col-md-6 mb-6">
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input id="password" type="password" name="password" placeholder="******" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6 mb-6">
                                    <div class="form-group">
                                        <label for="area">Area *</label>
                                        <select class="form-control area" id="area" name="area">
                                            @if($deliveryboy->area)
                                            <option selected value="{{$deliveryboy->area->id}}">{{$deliveryboy->area->area_name}}</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12 mb-12">
                                    <div class="form-group">
                                        <label for="exampleFormControlInput1"> Address *</label>
                                        <textarea class="form-control" id="address" name="address" rows="1">{{$deliveryboy->address->data_value}}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-6">
                                    <div class="form-group">
                                        <label for="profile_picture">Profile Picture</label>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="profile_picture" name="profile_picture">
                                            <label class="custom-file-label" for="tradeLicense">Choose file</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 mb-6 layout-spacing">
                                    <div class="form-group">
                                        <label for="nid_card">NID Card *</label>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="nid_card" name="nid_card">
                                            <label class="custom-file-label" for="nidCard">Choose file</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-6">
                                    <div class="text-center user-info">
                                        <img width="200" height="200" src="{{profile_picture($deliveryboy)}}" alt="avatar">
                                    </div>
                                </div>
                                <div class="col-md-6 mb-6 layout-spacing">
                                    <div class="text-center user-info">
                                        <img src="{{nid_card($deliveryboy)}}" width="200" height="200" alt="nid card">
                                    </div>
                                </div>


                            </div>

                            <button class="btn btn-primary submit-fn mt-2" type="submit">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
