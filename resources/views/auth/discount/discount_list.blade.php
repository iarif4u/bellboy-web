@extends('auth.layouts.master')
@section('css')
    <link href="{{asset('assets/css/users/account-setting.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/css/users/user-profile.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('plugins/file-upload/file-upload-with-preview.min.css')}}" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="{{asset('plugins/editors/markdown/simplemde.min.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('plugins/table/datatable/datatables.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset("plugins/table/datatable/custom_dt_html5.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("plugins/table/datatable/dt-global_style.css")}}">
    <link href="{{asset('plugins/select2/select2.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('plugins/datepicker/css/datepicker.css')}}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')

    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
        <div class="widget widget-chart-one">
            <div class="widget-heading">
                <h5 class="">Discount List</h5>
                <ul class="tabs tab-pills">
                    <li>
                        <button type="button" class="btn btn-primary mb-2 mr-2" data-toggle="modal" data-target="#addDiscount">
                            Add Discount
                        </button>
                    </li>
                </ul>
            </div>

            <div class="widget-content">
                <div class="tabs tab-content">
                    <div id="content_1" class="tabcontent">
                        <div class="table-responsive mb-4 mt-4">
                            {{$dataTable->table(['class'=>"table table-hover non-hover","style"=>"width:100%"])}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modal')
    <!-- Modal -->
    <div class="modal fade" id="addDiscount" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add Discount</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                             viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                             stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                            <line x1="18" y1="6" x2="6" y2="18"></line>
                            <line x1="6" y1="6" x2="18" y2="18"></line>
                        </svg>
                    </button>
                </div>
                <form class="simple-example" action="{{route('product.discount.new')}}" method="post" novalidate="">
                    @csrf
                    <div class="modal-body">
                        <div class="form-row">
                            <div class="col-md-12 mb-12">
                                <div class="form-group">
                                    <label for="discount_name"> Name *</label>
                                    <input id="discount_name" type="text" name="discount_name"
                                           placeholder="Discount Name" class="form-control" required="required">
                                </div>
                            </div>
                            <div class="col-md-6 mb-6">
                                <div class="form-group">
                                    <label for="discount_type">Discount Type</label>
                                    <select name="discount_type" class="form-control date-picker" id="discount_type">
                                        <option value="{{config('const.discount.fixed')}}">Fixed (৳)</option>
                                        <option value="{{config('const.discount.percentage')}}">Percentage (%)</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 mb-6">
                                <div class="form-group">
                                    <label for="discount">Discount</label>
                                    <input id="discount" type="text" name="discount" placeholder="10"
                                           class="form-control" required="required">
                                </div>
                            </div>
                            <div class="col-md-12 mb-12">
                                <div class="form-group">
                                    <label for="valid_till">Discount Last Date</label>
                                    <input id="valid_till" type="text" name="valid_till"
                                           class="form-control date-picker" required="required">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i>Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>

    </div>
    <div id="updateDiscount" class="modal animated zoomInUp custo-zoomInUp" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Update Discount</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                             viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                             stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                            <line x1="18" y1="6" x2="6" y2="18"></line>
                            <line x1="6" y1="6" x2="18" y2="18"></line>
                        </svg>
                    </button>
                </div>
                <form class="simple-example" action="{{route('product.discount.update')}}" method="post" novalidate="">
                    @csrf
                    <input type="hidden" id="update_discount_id" name="discount_id">
                    <div class="modal-body">
                        <div class="form-row">
                            <div class="col-md-12 mb-12">
                                <div class="form-group">
                                    <label for="update_discount_name">Name *</label>
                                    <input id="update_discount_name" type="text" name="discount_name" placeholder="Discount Name" class="form-control" required="required">
                                </div>
                            </div>
                            <div class="col-md-6 mb-6">
                                <div class="form-group">
                                    <label for="update_discount_type">Discount Type</label>
                                    <select name="discount_type" class="form-control date-picker" id="update_discount_type">
                                        <option value="{{config('const.discount.fixed')}}">Fixed (৳)</option>
                                        <option value="{{config('const.discount.percentage')}}">Percentage (%)</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 mb-6">
                                <div class="form-group">
                                    <label for="update_discount">Discount</label>
                                    <input id="update_discount" type="text" name="discount" placeholder="10" class="form-control" required="required">
                                </div>
                            </div>
                            <div class="col-md-12 mb-12">
                                <div class="form-group">
                                    <label for="update_valid_till">Discount Last Date</label>
                                    <input id="update_valid_till" type="text" name="valid_till" class="form-control date-picker" required="required">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <!-- BEGIN PAGE LEVEL CUSTOM SCRIPTS -->
    <script src="{{asset('plugins/table/datatable/datatables.js')}}"></script>
    <script src="{{asset('plugins/select2/select2.min.js')}}"></script>
    <script src="{{asset('plugins/datepicker/js/bootstrap-datepicker.js')}}"></script>
    {{$dataTable->scripts()}}
    <script>
        $("#discount_type").select2({
            dropdownParent: $("#addDiscount")
        });
        $("#update_discount_type").select2({
            dropdownParent: $("#updateDiscount")
        });
        $('.date-picker').datepicker({
            format: 'yyyy-mm-dd',
            orientation: 'top auto',
        });

        function delete_discount(discount_id) {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success btn-rounded',
                    cancelButton: 'btn btn-danger btn-rounded mr-3',
                },
                buttonsStyling: false
            });
            swalWithBootstrapButtons.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    blockUI("#discounts-table");
                    axios.delete(`{{route('product.discount.delete')}}?discount_id=${discount_id}`)
                        .then(function (response) {
                            swalWithBootstrapButtons.fire('Deleted!', response.data.message, 'success');
                        })
                        .catch(function (error) {
                            showError(error.response.data.message);
                        })
                        .then(function () {
                            $("#discounts-table").DataTable().ajax.reload(null, false);
                            unBlockUI("#discounts-table");
                        });
                } else if (
                    /* Read more about handling dismissals below */
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire('Cancelled', 'Discount delete cancelled', 'error');
                }
            });
        }

        function open_update_discount_modal(discount_id, discount_name, discount_type, discount, valid_till, status) {
            $("#updateDiscount").modal("show");
            $('#update_valid_till').datepicker('update',valid_till);
            $("#update_discount").val(discount);
            $("#update_discount_type").val(discount_type).trigger('change');
            $("#update_discount_name").val(discount_name);
            $("#update_discount_id").val(discount_id);
        }
    </script>
@endsection

