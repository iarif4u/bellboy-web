<div class="sidebar-wrapper sidebar-theme">

    <nav id="sidebar">
        <div class="shadow-bottom"></div>
        <ul class="list-unstyled menu-categories" id="accordionExample">
            <li class="menu">
                <a href="{{route('home')}}" data-active="{{activeMenu('home','true','false')}}" class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                             fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                             stroke-linejoin="round" class="feather feather-home">
                            <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                            <polyline points="9 22 9 12 15 12 15 22"></polyline>
                        </svg>
                        <span>Dashboard</span>
                    </div>
                </a>
            </li>

            <li class="menu">
                <a href="#agent" data-toggle="collapse" data-active="{{areActiveRoutes(['agent.withdraw','agent.new','agent.all'],'true','false')}}" aria-expanded="false" class="dropdown-toggle collapsed">
                    <div class="">
                        <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2"
                             fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1">
                            <path d="M16 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path>
                            <circle cx="8.5" cy="7" r="4"></circle>
                            <line x1="20" y1="8" x2="20" y2="14"></line>
                            <line x1="23" y1="11" x2="17" y2="11"></line>
                        </svg>
                        <span>Agent</span>
                    </div>
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                             class="feather feather-chevron-right">
                            <polyline points="9 18 15 12 9 6"></polyline>
                        </svg>
                    </div>
                </a>
                <ul class="collapse submenu list-unstyled {{areActiveRoutes(['agent.withdraw','agent.new','agent.all','agent.edit'],'show')}}" id="agent" data-parent="#accordionExample">
                    <li class="{{activeMenu('agent.new')}}">
                        <a href="{{route('agent.new')}}"> Add Agent </a>
                    </li>
                    <li class="{{areActiveRoutes(['agent.all','agent.edit'])}}">
                        <a href="{{route('agent.all')}}"> All Agent </a>
                    </li>
                    <li class="{{areActiveRoutes(['agent.withdraw'])}}">
                        <a href="{{route('agent.withdraw')}}"> Balance Withdraw </a>
                    </li>
                </ul>
            </li>

            <li class="menu">
                <a href="#deliveryBoy" data-toggle="collapse" data-active="{{areActiveRoutes(['deliveryboy.edit','deliveryboy.deliveryBoys','deliveryboy.area.areas','deliveryboy.new'],'true','false')}}" aria-expanded="false" class="dropdown-toggle collapsed">
                    <div class="">
                        <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><path d="M16 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="8.5" cy="7" r="4"></circle><line x1="20" y1="8" x2="20" y2="14"></line><line x1="23" y1="11" x2="17" y2="11"></line></svg>
                        <span>Delivery Boy</span>
                    </div>
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>
                    </div>
                </a>
                <ul class="collapse submenu list-unstyled {{areActiveRoutes(['deliveryboy.deliveryBoys','deliveryboy.edit','deliveryboy.area.areas','deliveryboy.new'],'show')}}" id="deliveryBoy" data-parent="#accordionExample">
                    <li class="{{areActiveRoutes(['deliveryboy.new'])}}">
                        <a href="{{route('deliveryboy.new')}}"> Add Delivery Boy </a>
                    </li>
                    <li class="{{areActiveRoutes(['deliveryboy.deliveryBoys','deliveryboy.edit'])}}">
                        <a href="{{route('deliveryboy.deliveryBoys')}}"> All Delivery Boy  </a>
                    </li>
                    <li class="{{areActiveRoutes(['deliveryboy.area.areas'])}}">
                        <a href="{{route('deliveryboy.area.areas')}}"> Area List</a>
                    </li>
                </ul>
            </li>

            <li class="menu">
                <a href="#customer" data-toggle="collapse" data-active="{{areActiveRoutes(['customers.all'],'true','false')}}" aria-expanded="false" class="dropdown-toggle collapsed">
                    <div class="">
                        <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><path d="M16 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="8.5" cy="7" r="4"></circle><line x1="20" y1="8" x2="20" y2="14"></line><line x1="23" y1="11" x2="17" y2="11"></line></svg>
                        <span>Customer</span>
                    </div>
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>
                    </div>
                </a>
                <ul class="collapse submenu list-unstyled {{areActiveRoutes(['customers.all'],'show')}}" id="customer" data-parent="#accordionExample">
                    <li class="{{areActiveRoutes(['customers.all'])}}">
                        <a href="{{route('customers.all')}}">Customer List</a>
                    </li>
                </ul>
            </li>

            <li class="menu">
                <a href="#product" data-toggle="collapse" data-active="{{areActiveRoutes(['product.variation.variations','product.deleted','agent.approve','agent.offer','coupon.codes','product.unit','product.variation.all','product.all','product.variation.edit','product.variation.add','product.new','product.discount.all','product.property.all','product.option.all','category.all','brand.all','product.variation.offers'],'true','false')}}" aria-expanded="false" class="dropdown-toggle collapsed">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-zap"><polygon points="13 2 3 14 12 14 11 22 21 10 12 10 13 2"></polygon></svg>
                        <span>Product</span>
                        @if(agent_offers()>0)
                            <span class="badge badge-primary text-white">{{agent_offers()}}</span>
                        @endif
                    </div>
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>
                    </div>
                </a>
                <ul class="collapse submenu list-unstyled {{areActiveRoutes(['product.variation.variations','product.deleted','agent.approve','agent.offer','coupon.codes','product.unit','product.variation.edit','product.all','product.variation.all','product.variation.add','product.update','product.all','product.discount.all','product.new','product.property.all','product.option.all','brand.all','category.all','product.variation.offers'],'show')}}" id="product" data-parent="#accordionExample">
                    <li class="{{areActiveRoutes(['product.new'])}}">
                        <a href="{{route('product.new')}}">Add Product</a>
                    </li>
                    <li class="{{areActiveRoutes(['product.variation.edit','product.variation.offers','product.all','product.update','product.variation.add','product.variation.all'])}}">
                        <a href="{{route('product.all')}}">All Product</a>
                    </li>
                    <li class="{{areActiveRoutes(['product.variation.variations'])}}">
                        <a href="{{route('product.variation.variations')}}">Variations</a>
                    </li>
                    <li class="{{areActiveRoutes(['product.deleted'])}}">
                        <a href="{{route('product.deleted')}}">Deleted Product</a>
                    </li>
                    <li class="{{areActiveRoutes(['category.all'])}}">
                        <a href="{{route('category.all')}}">Category</a>
                    </li>
                    <li class="{{areActiveRoutes(['agent.offer'])}}">
                        <a href="{{route('agent.offer')}}">
                            Agent Offers
                            <span class="badge badge-primary">{{agent_offers()}}</span>
                        </a>
                    </li>
                    <li class="{{areActiveRoutes(['agent.approve'])}}">
                        <a href="{{route('agent.approve')}}">Price Approve List</a>
                    </li>
                    <li class="{{areActiveRoutes(['product.unit'])}}">
                        <a href="{{route('product.unit')}}">Unit</a>
                    </li>
                    <li class="{{areActiveRoutes(['brand.all'])}}">
                        <a href="{{route('brand.all')}}">Brand</a>
                    </li>
                    {{--<li class="{{areActiveRoutes(['product.option.all'])}}"><a href="{{route('product.option.all')}}">Product Options</a></li>--}}
                    <li class="{{areActiveRoutes(['product.property.all'])}}"><a href="{{route('product.property.all')}}">Product Properties</a></li>
                    <li class="{{areActiveRoutes(['product.discount.all'])}}"><a href="{{route('product.discount.all')}}">Discount</a></li>
                    <li class="{{areActiveRoutes(['coupon.codes'])}}"><a href="{{route('coupon.codes')}}">Coupon Code</a></li>
                </ul>
            </li>

            <li class="menu">
                <a href="#chat" data-toggle="collapse" data-active="{{areActiveRoutes(['chat.agent','chat.customer','chat.deliveryboy'],'true','false')}}" aria-expanded="false" class="dropdown-toggle collapsed">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-message-square"><path d="M21 15a2 2 0 0 1-2 2H7l-4 4V5a2 2 0 0 1 2-2h14a2 2 0 0 1 2 2z"></path></svg>
                        <span>Chat</span>
                    </div>
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>
                    </div>
                </a>
                <ul class="collapse submenu list-unstyled {{areActiveRoutes(['chat.agent','chat.customer','chat.deliveryboy'],'show')}}" id="chat" data-parent="#accordionExample">
                    <li class="{{areActiveRoutes(['chat.agent'])}}">
                        <a href="{{route('chat.agent')}}"> Agent </a>
                    </li>
                    <li class="{{areActiveRoutes(['chat.deliveryboy'])}}">
                        <a href="{{route('chat.deliveryboy')}}"> Delivery Boy </a>
                    </li>
                    <li class="{{areActiveRoutes(['chat.customer'])}}">
                        <a href="{{route('chat.customer')}}"> Customer </a>
                    </li>
                </ul>
            </li>
            <li class="menu">
                <a href="#order" data-toggle="collapse"
                   data-active="{{areActiveRoutes(['order.delivered','order.canceled','order.approved','order.pending'],'true','false')}}" aria-expanded="false"
                   class="dropdown-toggle collapsed">
                    <div class="">
                        <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2"
                             fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1">
                            <circle cx="9" cy="21" r="1"></circle>
                            <circle cx="20" cy="21" r="1"></circle>
                            <path d="M1 1h4l2.68 13.39a2 2 0 0 0 2 1.61h9.72a2 2 0 0 0 2-1.61L23 6H6"></path>
                        </svg>
                        <span>Order</span>
                        <span class="badge badge-primary">{{pending_orders()}}</span>
                    </div>
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                             class="feather feather-chevron-right">
                            <polyline points="9 18 15 12 9 6"></polyline>
                        </svg>
                    </div>
                </a>
                <ul class="collapse submenu list-unstyled {{areActiveRoutes(['order.canceled','order.delivered','order.approved','order.pending'],'show')}}"
                    id="order" data-parent="#accordionExample">
                    <li class="{{activeMenu('order.pending')}}">
                        <a href="{{route('order.pending')}}">
                            Order List
                            <span class="badge badge-primary">{{pending_orders()}}</span>
                        </a>

                    </li>
                    <li class="{{activeMenu('order.approved')}}">
                        <a href="{{route('order.approved')}}"> Assign Order </a>
                    </li>
                    <li class="{{activeMenu('order.delivered')}}">
                        <a href="{{route('order.delivered')}}"> Complete Order </a>
                    </li>
                    <li class="{{activeMenu('order.canceled')}}">
                        <a href="{{route('order.canceled')}}">Cancel Order </a>
                    </li>
                </ul>
            </li>

            <li class="menu">
                <a href="#reports" data-toggle="collapse" data-active="{{areActiveRoutes(['report.profit','report.stock','report.top_selling','report.monthly','report.daily'],'true','false')}}" aria-expanded="false" class="dropdown-toggle collapsed">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-bar-chart"><line x1="12" y1="20" x2="12" y2="10"></line><line x1="18" y1="20" x2="18" y2="4"></line><line x1="6" y1="20" x2="6" y2="16"></line></svg>
                        <span>Report</span>
                    </div>
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>
                    </div>
                </a>
                <ul class="collapse submenu list-unstyled {{areActiveRoutes(['report.profit','report.stock','report.top_selling','report.monthly','report.daily'],'show')}}" id="reports" data-parent="#accordionExample">
                    <li class="{{activeMenu('report.daily')}}">
                        <a href="{{route('report.daily')}}"> Daily Sale </a>
                    </li>
                    <li class="{{activeMenu('report.monthly')}}">
                        <a href="{{route('report.monthly')}}"> Monthly Sale </a>
                    </li>
                    <li class="{{activeMenu('report.top_selling')}}">
                        <a href="{{route('report.top_selling')}}"> Top Selling Product </a>
                    </li>
                    <li class="{{activeMenu('report.stock')}}">
                        <a href="{{route('report.stock')}}"> Stock Report </a>
                    </li>
                    <li class="{{activeMenu('report.profit')}}">
                        <a href="{{route('report.profit')}}"> Profit/Loss</a>
                    </li>
                </ul>
            </li>


            <li class="menu">
                <a href="#users"  data-toggle="collapse" data-active="{{areActiveRoutes(['users.add','users.all','users.edit'],'true','false')}}" aria-expanded="false" class="dropdown-toggle collapsed">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-users"><path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="9" cy="7" r="4"></circle><path d="M23 21v-2a4 4 0 0 0-3-3.87"></path><path d="M16 3.13a4 4 0 0 1 0 7.75"></path></svg>
                        <span>Users</span>
                    </div>
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>
                    </div>
                </a>
                <ul class="collapse submenu list-unstyled {{areActiveRoutes(['users.add','users.all','users.edit'],'show')}}" id="users" data-parent="#accordionExample">
                    <li class="{{areActiveRoutes(['users.add'])}}">
                        <a href="{{route('users.add')}}"> Add User </a>
                    </li>
                    <li class="{{areActiveRoutes(['users.all','users.edit'])}}">
                        <a href="{{route('users.all')}}"> All User </a>
                    </li>
                </ul>
            </li>
            <li class="menu">
                <a href="#notification"  data-toggle="collapse" data-active="{{areActiveRoutes(['notification.send'],'true','false')}}" aria-expanded="false" class="dropdown-toggle collapsed">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-bell"><path d="M18 8A6 6 0 0 0 6 8c0 7-3 9-3 9h18s-3-2-3-9"></path><path d="M13.73 21a2 2 0 0 1-3.46 0"></path></svg>
                        <span>Notification</span>
                    </div>
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>
                    </div>
                </a>
                <ul class="collapse submenu list-unstyled {{areActiveRoutes(['notification.send'],'show')}}" id="notification" data-parent="#accordionExample">
                    <li class="{{areActiveRoutes(['notification.send'])}}">
                        <a href="{{route('notification.send')}}">Send</a>
                    </li>
                </ul>
            </li>
            <li class="menu">
                <a href="#account" data-toggle="collapse" data-active="{{areActiveRoutes(['account.statement','account.expense.categories','account.expense.all','account.income'],'true','false')}}" aria-expanded="false" class="dropdown-toggle collapsed">
                    <div class="">
                        <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><rect x="1" y="4" width="22" height="16" rx="2" ry="2"></rect><line x1="1" y1="10" x2="23" y2="10"></line></svg>
                        <span>Account</span>
                    </div>
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>
                    </div>
                </a>
                <ul class="collapse submenu list-unstyled {{areActiveRoutes(['account.statement','account.expense.categories','account.expense.all','account.income'],'show')}}" id="account" data-parent="#accordionExample">
                    <li class="{{areActiveRoutes(['account.income'])}}">
                        <a href="{{route('account.income')}}">Income</a>
                    </li>
                    <li class="{{areActiveRoutes(['account.expense.all'])}}">
                        <a href="{{route('account.expense.all')}}">Expense</a>
                    </li>
                    <li class="{{areActiveRoutes(['account.expense.categories'])}}">
                        <a href="{{route('account.expense.categories')}}">Expense Category</a>
                    </li>
                    <li class="{{areActiveRoutes(['account.statement'])}}">
                        <a href="{{route('account.statement')}}">Account Statement </a>
                    </li>
                </ul>
            </li>


            <li class="menu">
                <a href="#settings" data-toggle="collapse" data-active="{{areActiveRoutes(['settings.sms','settings.mail','settings.general'],'true','false')}}" aria-expanded="false" class="dropdown-toggle collapsed">
                    <div class="">
                        <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><circle cx="12" cy="12" r="3"></circle><path d="M19.4 15a1.65 1.65 0 0 0 .33 1.82l.06.06a2 2 0 0 1 0 2.83 2 2 0 0 1-2.83 0l-.06-.06a1.65 1.65 0 0 0-1.82-.33 1.65 1.65 0 0 0-1 1.51V21a2 2 0 0 1-2 2 2 2 0 0 1-2-2v-.09A1.65 1.65 0 0 0 9 19.4a1.65 1.65 0 0 0-1.82.33l-.06.06a2 2 0 0 1-2.83 0 2 2 0 0 1 0-2.83l.06-.06a1.65 1.65 0 0 0 .33-1.82 1.65 1.65 0 0 0-1.51-1H3a2 2 0 0 1-2-2 2 2 0 0 1 2-2h.09A1.65 1.65 0 0 0 4.6 9a1.65 1.65 0 0 0-.33-1.82l-.06-.06a2 2 0 0 1 0-2.83 2 2 0 0 1 2.83 0l.06.06a1.65 1.65 0 0 0 1.82.33H9a1.65 1.65 0 0 0 1-1.51V3a2 2 0 0 1 2-2 2 2 0 0 1 2 2v.09a1.65 1.65 0 0 0 1 1.51 1.65 1.65 0 0 0 1.82-.33l.06-.06a2 2 0 0 1 2.83 0 2 2 0 0 1 0 2.83l-.06.06a1.65 1.65 0 0 0-.33 1.82V9a1.65 1.65 0 0 0 1.51 1H21a2 2 0 0 1 2 2 2 2 0 0 1-2 2h-.09a1.65 1.65 0 0 0-1.51 1z"></path></svg>
                        <span>Settings</span>
                    </div>
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>
                    </div>
                </a>
                <ul class="collapse submenu list-unstyled {{areActiveRoutes(['settings.sms','settings.mail','settings.general'],'show')}}" id="settings" data-parent="#accordionExample">
                    <li class="{{areActiveRoutes(['settings.general'])}}">
                        <a href="{{route('settings.general')}}"> General Setting </a>
                    </li>
                    <li class="{{areActiveRoutes(['settings.mail'])}}">
                        <a href="{{route('settings.mail')}}"> Mail Setting </a>
                    </li>
                    <li class="{{areActiveRoutes(['settings.sms'])}}">
                        <a href="{{route('settings.sms')}}"> SMS Setting </a>
                    </li>

                </ul>
            </li>

        </ul>
        <!-- <div class="shadow-bottom"></div> -->

    </nav>

</div>
