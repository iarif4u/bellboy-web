@extends('auth.layouts.master')
@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset("plugins/select2/select2.min.css")}}">
@endsection

@section('content')
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
        <div class="widget widget-chart-one">
            <div class="widget-heading">
                <h5 class="">Send Notification</h5>
            </div>

            <div class="widget-content">
                <div class="tabs tab-content">
                    <div class="form">
                        <form method="post" action="{{route('notification.send')}}">
                            @csrf
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="notification_to">Notification To</label>
                                    <select required name="notification_to" id="notification_to">
                                        <option value="">Select One</option>
                                        <option value="all_customer">All Customer</option>
                                        <option value="registered_customer">Registered Customer</option>
                                        <option value="agent">Agent</option>
                                        <option value="delivery_boy">Delivery Boy</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="title">Title *</label>
                                    <input value="{{old('title')}}" id="title" type="text" name="title" placeholder="Notification Title" class="form-control" required="">
                                    <div class="invalid-feedback">Please enter the notification title</div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="body">Notification Body *</label>
                                    <textarea class="form-control" required id="body" name="body" rows="1">{{old('body')}}</textarea>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <button class="btn btn-primary submit-fn mt-2" type="submit">Submit</button>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="{{asset('plugins/select2/select2.min.js')}}"></script>
    <script>
        $("#notification_to").select2();
    </script>
@endsection
