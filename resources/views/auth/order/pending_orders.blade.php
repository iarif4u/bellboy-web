@extends('auth.layouts.master')
@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('plugins/table/datatable/datatables.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset("plugins/table/datatable/custom_dt_html5.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("plugins/table/datatable/dt-global_style.css")}}">
    <link href="{{asset('plugins/select2/select2.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('plugins/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <style>
        #variation-products {
            background-color: rgba(0, 0, 0, 0.5);
        }
        .modal { overflow-y: auto }
        .has-error {
            border: 1px solid red !important;
        }
    </style>
@endsection
@section('content')
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
        <div class="widget widget-chart-one">
            <div class="widget-heading">
                <h5 class="">Order List</h5>
                <ul class="tabs tab-pills">
                    <li>
                        <a href="{{route('order.approved')}}" class="btn btn-primary mb-2 mr-2">
                            Assign Order List
                        </a>
                    </li>
                    <li>
                        <a href="{{route('order.delivered')}}" class="btn btn-primary mb-2 mr-2">
                            Complete Order List
                        </a>
                    </li>
                    <li>
                        <a href="{{route('order.canceled')}}" class="btn btn-primary mb-2 mr-2">
                            Cancel List
                        </a>
                    </li>

                </ul>
            </div>

            <div class="widget-content">
                <div class="tabs tab-content">
                    <div id="content_1" class="tabcontent">
                        <div class="table-responsive mb-4 mt-4">
                            {{$dataTable->table(['class'=>"table table-hover non-hover"])}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <!-- BEGIN PAGE LEVEL CUSTOM SCRIPTS -->
    <script src="{{asset('plugins/table/datatable/datatables.js')}}"></script>
    <script src="{{asset('plugins/table/datatable/button-ext/buttons.html5.min.js')}}"></script>
    <script src="{{asset('plugins/table/datatable/button-ext/buttons.print.min.js')}}"></script>
    {{$dataTable->scripts()}}
    <script src="{{asset('plugins/select2/select2.min.js')}}"></script>
    <script src="{{asset('plugins/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js')}}"></script>

    <script>
        let search = true;

        $("#delivery_boy").select2({
            dropdownParent: $("#assignOrder"),
            placeholder: "Select Delivery Boy",
            ajax: {
                url: '{{route('order.deliveryboy_search')}}',
                dataType: 'json',
                data: function (params) {
                    return {
                        search: params.term,
                        type: "public"
                    };
                },
                processResults: function (response) {
                    return {
                        results: $.map(response.data, function (txt, val) {
                            return {id: txt.id, text: txt.name}
                        })
                    };
                }
            }
        });

        $(".agent-list").select2({
            dropdownParent: $("#assignAgent"),
            placeholder: "Select Agent",
            ajax: {
                url: '{{route('order.agent_search')}}',
                dataType: 'json',
                data: function (params) {
                    return {
                        search: params.term,
                        type: "public"
                    };
                },
                processResults: function (response) {
                    return {
                        results: $.map(response.data, function (txt, val) {
                            return {id: txt.id, text: txt.name}
                        })
                    };
                }
            }
        });



        function cancel_order(order_id) {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success btn-rounded',
                    cancelButton: 'btn btn-danger btn-rounded mr-3',
                },
                buttonsStyling: false
            });
            swalWithBootstrapButtons.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, Cancel it!',
                cancelButtonText: 'No!',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    Swal.fire({
                        title: 'Enter the reason of cancel order',
                        input: 'textarea',
                        inputAttributes: {
                            autocapitalize: 'off'
                        },
                        showCancelButton: true,
                        confirmButtonText: 'Submit',
                        showLoaderOnConfirm: true,
                        preConfirm: (comment) => {
                            axios.post(`{{route('order.cancel')}}`, {
                                order_id: order_id,
                                comment: comment,
                                _token: '{{csrf_token()}}'
                            })
                                .then(function (response) {
                                    return response;
                                })
                                .catch(function (error) {
                                    Swal.showValidationMessage(
                                        `Request failed: ${error}`
                                    )
                                })
                        },
                        allowOutsideClick: () => false
                    }).then((result) => {
                        if (result.value) {
                            swalWithBootstrapButtons.fire('Success', 'Order has been cancelled', 'success');
                        } else {
                            swalWithBootstrapButtons.fire('Fail!!', 'Something went wrong', 'error');
                        }
                        $("#orders-table").DataTable().ajax.reload(null, false);
                        unBlockUI("#orders-table");
                    })
                } else if (
                    /* Read more about handling dismissals below */
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire('Fail', 'Order cancel fail', 'error');
                }
            });
        }

        function assign_order(order_item_id) {
            axios.post('{{route('order.order_info')}}', {
                order_id: order_item_id,
                _token: '{{csrf_token()}}', assign_order
            })
                .then(function (res) {
                    $("#order_id").val(res.data.id);
                    update_order_dom(res.data);
                })
                .catch(function (err) {
                    console.log(err);
                }).then(() => {
                $(".selected-agents").select2({
                    dropdownParent: $("#assignOrder"),
                });
                $(".assignOrder").modal("show");

            });
        }

        function update_order_dom(order) {
            let customer = order.customer;
            let items = order.items;
            const items_dom = items.map(item => {
                if (item.agent_id == null) {
                    return `<tr><td>${item.variation.variant_name}</td><td>${item.variation.unit_value} ${item.variation.unit.unit_name}</td><td>${item.quantity}x${item.discount_amount}</td><td>${item.quantity * item.discount_amount}</td><td class="text-center"><a onClick="assign_agent('${item.id}','${item.cost}','${item.quantity}','${item.variation.id}')" href="javascript:void(0);" class="btn btn-dark mb-3 rounded bs-tooltip" title="Assign Agent">Assign Agent</a></td></tr>`;
                }
                let agents = `<select class="form-control selected-agents" name="agent_id[]">`
                agents += item.agent_offers.map(agentOffer => {
                    if (item.agent_id === agentOffer.agent.id) {
                        return `<option selected value="${agentOffer.agent.id}">${agentOffer.agent.name}</option>`;
                    }
                    return `<option value="${agentOffer.agent.id}">${agentOffer.agent.name}</option>`;

                })
                agents += `</select>`;
                return `<tr><td>${item.variation.variant_name} ${makeHiddenField('order_item_id[]', item.id)}</td><td>${item.variation.unit_value} ${item.variation.unit.unit_name}</td><td>${item.quantity}x${item.discount_amount}</td><td>${item.quantity * item.discount_amount}</td><td class="text-center">${agents}</td></tr>`;
            });
            $(".order-items").html(items_dom);
            $(".customer-name").val(customer.name);
            $(".customer-phone").text(customer.phone);
            $(".customer-address").val(customer.address);
            $(".customer-charge").text(order.delivery_charge);
            $(".customer-discount-price").text(order.coupon_discount);
            $(".customer-total-price").text(parseInt(order.grand_total)-parseInt(order.delivery_charge));
            $(".customer-total-payable").text(order.total);
            $(".customer-coupon-discount").text(order.coupon_discount);
        }

        function assign_agent(order_item_id, item_cost, item_quantity, variation_id) {
            $("#order-item-id").val(order_item_id);
            $("#variation-id").val(variation_id);
            $("#item_cost").val(item_cost);
            $("#item_stock").val(item_quantity);
            $("#assignAgent").modal("show");
        }

        const makeHiddenField = (name, value) => {
            return `<input name="${name}" value="${value}" type="hidden">`;
        }

    </script>
    <script>
        //  $("input[name='productQuantity']").on('touchspin.on.startspin', function () {alert("HI");});
        function update_quantity(quantity, item_id, price) {
            if (quantity > 0) {
                $(`.product-${item_id}`).removeClass('is-invalid');
            } else if (quantity < 0) {
                $(`.product-${item_id}`).val(0);
            } else {
                $(`.product-${item_id}`).addClass('is-invalid');
            }
            $(`.qty-${item_id}`).text(quantity);
            $(`.sub-total-${item_id}`).text(quantity * price);
            update_sum();
        }

        function update_sum() {
            var order_charge = parseInt($(".order-charge").text().trim());
            var sum = order_charge;
            let coupon_id = $(".coupon-id").val();
            $('.sub-total').each(function () {
                sum += parseFloat($(this).text());
            });
            $(".order-total-price").text(sum-order_charge);
            if (coupon_id > 0) {
                axios.post('{{route('coupon.discount')}}', {
                    total: sum,
                    coupon_id: coupon_id,
                    _token: '{{csrf_token()}}',
                }).then(function (res) {
                    $(".order-discount").text(res.data);
                    $('.order-total-payable').text(sum - res.data);
                }).catch(function (err) {
                    console.log(err);
                })
            } else {
                $('.order-total-payable').text(sum);
            }

        }

        function update_order(order_id) {
            axios.post('{{route('order.order_info')}}', {
                order_id: order_id,
                _token: '{{csrf_token()}}',
            }).then(function (res) {
                design_table_update_dom(res.data)
            }).catch(function (err) {
                console.log(err);
            }).then(() => {
                // Change button class
                $(".productQuantity").TouchSpin({
                    buttondown_class: "btn-danger btn",
                    buttonup_class: "btn-success btn"
                });
                $("#updateOrder").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            });
        }

        function design_table_update_dom(order) {
            const customer = order.customer;
            $(".order-total-price").text(order.grand_total-order.delivery_charge);
            $(".coupon-id").val(order.coupon_id);
            $(".order-id").val(order.id);
            $(".order-discount").text(order.coupon_discount);
            $(".order-charge").text(order.delivery_charge);
            $(".order-total-payable").text(order.total);
            $(".order-coupon-discount").text(order.coupon_discount);
            $(".order-customer-name").val(customer.name);
            $(".order-customer-phone").text(customer.phone);
            $(".order-customer-address").val(customer.address);
            const rows = order.items.map(item => {
                return `<tr><td>${item.variation.variant_name} ${makeHiddenField('order_item_id[]', item.id)} ${makeHiddenField('product_variation_id[]', item.variation.id)}</td>
<td>${item.variation.unit_value} ${item.variation.unit.unit_name}</td>
<td><input onkeyup="update_quantity(this.value,${item.variation.id},${item.discount_amount})" onchange="update_quantity(this.value,${item.variation.id},${item.discount_amount})" class="productQuantity product-${item.variation.id}" type="text" value="${item.quantity}" name="productQuantity[]">
                                </td>
                            <td>
                                <span class="qty-${item.variation.id}">${item.quantity}</span>x${item.discount_amount}
                            </td>
                            <td>
                               <span class="sub-total sub-total-${item.variation.id}">${item.sub_total}</span>
                            </td>
                        </tr>`;
            });
            $("#update_table_order").html(rows);
        }
    </script>
    <script>
        const variationModal = $('#variation-products');
        $(".add-variation").click(() => {
            var variations = $("input[name='product_variation_id[]']")
                .map(function(){return $(this).val();}).get();
            $(".variation-list").select2({
                dropdownParent: $("#variation-products"),
                placeholder: "Select Product",
                ajax: {
                    url: '{{route('product.variation.search')}}',
                    dataType: 'json',
                    type:'post',
                    data: function (params) {
                        return {
                            search: params.term,
                            variations:variations,
                            _token: '{{csrf_token()}}'
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: $.map(response.data, function (txt, val) {
                                return {
                                    id: txt.variant_id,
                                    text: txt.variant_name + " (" + txt.unit_value + txt.unit + ") ৳ " + txt.discount_bn_price
                                }
                            })
                        };
                    }
                }
            });
            variationModal.modal({
                backdrop: 'static',
                keyboard: false
            });
        });
        $(".add-order-item").click(() => {
            //product_variation_id
            var error = false;
            let variation = $("#variation").val();
            let quantity = $("#quantity").val();
            if (variation > 0) {
                $('#variation').next().find('.select2-selection').removeClass('has-error');
            } else {
                error = true;
                $('#variation').next().find('.select2-selection').addClass('has-error');
            }
            if (quantity > 0) {
                $("#quantity").removeClass('is-invalid');
            } else {
                error = true;
                $("#quantity").addClass('is-invalid');
            }
            if (error === false) {
                axios.post(`{{route('product.variation.info')}}`, {
                    variation_id: variation,
                    _token: '{{csrf_token()}}'
                })
                    .then(function (response) {
                        let item = response.data;
                        const row = `<tr><td>${item.variant_name} ${makeHiddenField('new_item_id[]', variation)} ${makeHiddenField('product_variation_id[]', variation)}</td>
<td>${item.unit_value} ${item.unit}</td>
<td><input onkeyup="update_quantity(this.value,${variation},${item.discount_price})" onchange="update_quantity(this.value,${variation},${item.discount_price})" class="productQuantity product-${variation}" type="text" value="${quantity}" name="productNewQuantity[]">
                                </td>
                            <td>
                                <span class="qty-${variation}">${quantity}</span>x${item.discount_price}
                            </td>
                            <td>
                               <span class="sub-total sub-total-${variation}">${item.discount_price*quantity}</span>
                            </td>
                        </tr>`
                        $("#update_table_order").append(row);

                    })
                    .catch(function (error) {
                        Swal.showValidationMessage(
                            `Request failed: ${error}`
                        )
                    }).then(() => {
                    update_sum();
                    // Change button class
                    $(".productQuantity").TouchSpin({
                        buttondown_class: "btn-danger btn",
                        buttonup_class: "btn-success btn"
                    });
                    $("#variation-products").modal('hide');
                    $("#variation").val('').trigger('change');
                    $("#quantity").val("");
                });

            }
        });

        $("#quantity").keyup(() => {
            if ($("#quantity").val() > 0) {
                $("#quantity").removeClass('is-invalid');
            } else {
                $("#quantity").addClass('is-invalid');
            }
        })

        $(document.body).on("change", "#variation", function () {
            if ($('#variation').val() > 0) {
                $('#variation').next().find('.select2-selection').removeClass('has-error');
            } else {
                $('#variation').next().find('.select2-selection').addClass('has-error');
            }
        });
    </script>
@endsection

@section('modal')
    <div id="assignOrder" class="modal fade assignOrder" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="myLargeModalLabel">Assign Order</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                            <line x1="18" y1="6" x2="6" y2="18"></line>
                            <line x1="6" y1="6" x2="18" y2="18"></line>
                        </svg>
                    </button>
                </div>
                <form action="{{route('order.delivery_boy_assign')}}" method="post">
                    @csrf
                    <input type="hidden" id="order_id" name="order_id">
                    <div class="modal-body">
                        <div id="tableHover" class="col-lg-12 col-12 layout-spacing">
                            <div class="statbox">
                                <div>
                                    <div class="form-group">
                                        <label for="delivery_boy">Select Delivery Boy *</label>
                                        <select name="delivery_boy" id="delivery_boy" class="form-control" data-live-search="true"></select>
                                    </div>
                                </div>
                                <div class="widget-header layout-spacing">
                                    <div class="row">
                                        <div class="col-md-6 mb-12">
                                            <label class="label-primary" for="customer-name">Customer Name :
                                                <input id="customer-name" type="text" name="customer_name" class="customer-name form-control">
                                            </label>
                                        </div>
                                        <div class="col-md-6 mb-12">
                                            <label for="customer-address">Address :
                                                <textarea class="customer-address form-control" name="customer_address" id="customer-address" cols="30" rows="1"></textarea>
                                            </label>
                                        </div>
                                        <div class="col-md-6 mb-12">
                                            <p>Phone No : <span class="customer-phone"></span></p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 mb-12">
                                            <p>Total : <span class="customer-total-price"></span></p>
                                        </div>

                                        <div class="col-md-6 mb-12">
                                            <p>Delivery Charge : <span class="customer-charge"></span></p>
                                        </div>

                                        <div class="col-md-6 mb-12">
                                            <p>Payable Total : <span class="customer-total-payable"></span></p>
                                        </div>
                                        <div class="col-md-6 mb-12">
                                            <p>Discount : <span class="customer-discount-price"></span></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="widget-content">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover mb-4">
                                            <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Unit</th>
                                                <th>Quantity</th>
                                                <th>Price</th>
                                                <th class="text-center">Agent</th>
                                            </tr>
                                            </thead>
                                            <tbody class="order-items">
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Close</button>
                        <button type="submit" class="btn btn-info">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="assignAgent" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="myLargeModalLabel">Assign Agent</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                             viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                             stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                            <line x1="18" y1="6" x2="6" y2="18"></line>
                            <line x1="6" y1="6" x2="18" y2="18"></line>
                        </svg>
                    </button>
                </div>
                <form action="{{route('order.agent_assign')}}" method="post">
                    @csrf
                    <input type="hidden" id="order-item-id" name="order_item">
                    <input type="hidden" id="variation-id" name="variation_id">
                    <div class="modal-body">
                        <div id="tableHover" class="col-lg-12 col-12 layout-spacing">
                            <div class="statbox">
                                <div class="form-group">
                                    <label for="agent">Select Agent</label>
                                    <select name="agent" id="agent" class="form-control agent-list"></select>
                                </div>
                                <div class="form-group">
                                    <label for="item_cost">Product Cost *</label>
                                    <input id="item_cost" type="number" name="item_cost" placeholder="Product Cost" class="form-control" required="required">
                                </div>
                                <div class="form-group">
                                    <label for="item_stock">Stock</label>
                                    <input type="text" id="item_stock" name="item_stock" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Close</button>
                        <button type="submit" class="btn btn-info">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="updateOrder" class="modal fade">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="myLargeModalLabel">Update Order</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                            <line x1="18" y1="6" x2="6" y2="18"></line>
                            <line x1="6" y1="6" x2="18" y2="18"></line>
                        </svg>
                    </button>
                </div>
                <form method="post" action="{{route('order.update')}}">
                    @csrf
                    <input name="coupon" class="coupon-id" type="hidden">
                    <input name="order_id" class="order-id" type="hidden">
                    <div class="modal-body">
                        <div id="tableHover" class="col-lg-12 col-12 layout-spacing">
                            <div class="statbox">
                                <div class="widget-header layout-spacing">
                                    <div class="row">
                                        <div class="col-md-6 mb-12">
                                            <label for="order-customer-name">
                                                Customer Name
                                                <input type="text" name="customer_name" id="order-customer-name"
                                                       class="order-customer-name form-control">
                                            </label>
                                            <p>Phone No : <span class="order-customer-phone"></span></p>
                                        </div>
                                        <div class="col-md-6 mb-12">
                                            <label for="order-customer-address">
                                                Customer Address
                                                <textarea name="customer_address" id="order-customer-address"
                                                          class="order-customer-address form-control"></textarea>
                                            </label>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-6 mb-12">
                                            <p>Total : <span class="order-total-price"></span></p>
                                        </div>

                                        <div class="col-md-6 mb-12">
                                            <p>Delivery Charge : <span class="order-charge"></span></p>
                                        </div>

                                        <div class="col-md-6 mb-12">
                                            <p>Payable Total : <span class="order-total-payable"></span></p>
                                        </div>
                                        <div class="col-md-6 mb-12">
                                            <p>Discount : <span class="order-discount"></span></p>
                                        </div>

                                    </div>
                                </div>
                                <div class="widget-content">
                                    <!-- Trigger the modal with a button -->
                                    <button type="button" class="btn add-variation btn-info">Add New</button>
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover mb-4">
                                            <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Unit</th>
                                                <th>Quantity</th>
                                                <th>Amount</th>
                                                <th>Price</th>
                                            </tr>
                                            </thead>
                                            <tbody id="update_table_order">
                                            </tbody>
                                        </table>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Close</button>
                        <button type="submit" class="btn btn-info">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="variation-products" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="myLargeModalLabel">Add Product</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                             viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                             stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                            <line x1="18" y1="6" x2="6" y2="18"></line>
                            <line x1="6" y1="6" x2="18" y2="18"></line>
                        </svg>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="col-md-12 mb-12">
                            <div class="form-group">
                                <label for="variation">Product</label>
                                <select name="variation" class="variation-list" id="variation"></select>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-12 mb-12">
                            <div class="form-group">
                                <label for="quantity">Quantity</label>
                                <input type="number" name="quantity" id="quantity" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Discard</button>
                    <button type="button" class="btn add-order-item btn-primary">Add to order</button>
                </div>
            </div>
        </div>
    </div>
@endsection
