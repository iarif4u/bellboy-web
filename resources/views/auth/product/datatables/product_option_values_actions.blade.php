<div class="dropdown custom-dropdown text-center">
    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink12"
       data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
             viewBox="0 0 24 24" fill="none" stroke="currentColor"
             stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
             class="feather feather-more-horizontal">
            <circle cx="12" cy="12" r="1"></circle>
            <circle cx="19" cy="12" r="1"></circle>
            <circle cx="5" cy="12" r="1"></circle>
        </svg>
    </a>
    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink12">
        <a class="dropdown-item" onclick="open_update_option_value_modal('{{$option_value->id}}','{{$option_value->value}}','{{$option_value->option->name}}','{{$option_value->option->id}}')" href="javascript:void(0);">Edit</a>
        <a class="dropdown-item" onclick="delete_product_option_value('{{$option_value->id}}')" href="javascript:void(0);">Delete</a>
    </div>
</div>
