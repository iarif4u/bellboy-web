<label class="switch s-icons s-outline  s-outline-success  mb-4 mr-2">
    @if($product->status==config('const.product.status.active'))
        <input onchange="change_product_status('{{$product->id}}','{{config('const.product.status.inactive')}}')" type="checkbox" checked data-toggle="toggle">
    @else
        <input onchange="change_product_status('{{$product->id}}','{{config('const.product.status.active')}}')" type="checkbox" data-toggle="toggle">
    @endif
    <span class="slider round"></span>
</label>
