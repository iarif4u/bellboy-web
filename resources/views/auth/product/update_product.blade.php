@extends('auth.layouts.master')
@section('css')
    <link rel="stylesheet" href="{{asset('plugins/select2/select2.min.css')}}">
    <style>
        .select2-selection {
            background: #fff !important;
            padding: 4px !important;
        }

        .invalid-tooltip {
            margin-top: -40px;
            margin-left: 3px;
        }
    </style>
@endsection
@section('content')
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
        <div class="widget widget-chart-one">
            <div class="widget-heading">
                <h5 class="">Update Product</h5>
            </div>
            <div class="widget-content">
                <div class="tabs tab-content">
                    <div id="content_1" class="tabcontent">
                        <form class="needs-validation" action="{{route('product.update',['product_id'=>$product->id])}}"
                              method="post" novalidate="">
                            @csrf
                            <input type="hidden" value="{{$product->id}}" name="product_id">
                            <div class="form-row">
                                <div class="col-md-6 mb-12">
                                    <div class="form-group">
                                        <label for="product_name">Product Name *</label>
                                        <input value="{{$product->product_name}}" id="product_name" type="text"
                                               name="product_name" placeholder="Product Name" class="form-control"
                                               required="">
                                    </div>
                                </div>
                                <div class="col-md-6 mb-12">
                                    <div class="form-group">
                                        <label for="categories">Category Name *</label>
                                        <select multiple="multiple" id="categories" name="categories[]"
                                                class="form-control">
                                            @foreach($product->categories as $category)
                                                <option selected value="{{$category->id}}">{{$category->name}}</option>
                                            @endforeach
                                        </select>
                                        <div class="invalid-tooltip">
                                            Please Select Product Category
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 mb-12">
                                    <div class="form-group">
                                        <label for="product_code">Product Code *</label>
                                        <div class="input-group">
                                            <input value="{{$product->product_code}}" id="product_code" type="text"
                                                   name="product_code" class="form-control" placeholder="Product Code">
                                            <div class="input-group-append">
                                                <button class="btn btn-dark product-code" type="button"
                                                        aria-haspopup="true" aria-expanded="false">
                                                    <svg viewBox="0 0 24 24" width="24" height="24"
                                                         stroke="currentColor" stroke-width="2" fill="none"
                                                         stroke-linecap="round" stroke-linejoin="round"
                                                         class="css-i6dzq1">
                                                        <polyline points="23 4 23 10 17 10"></polyline>
                                                        <polyline points="1 20 1 14 7 14"></polyline>
                                                        <path d="M3.51 9a9 9 0 0 1 14.85-3.36L23 10M1 14l4.64 4.36A9 9 0 0 0 20.49 15"></path>
                                                    </svg>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 mb-12">
                                    <div class="form-group">
                                        <label for="brand">Brand </label>
                                        <select id="brand" name="brand" class="form-control">
                                            @if($product->product_brand)
                                                <option selected
                                                        value="{{$product->brand->id}}">{{$product->brand->name}}</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4 mb-12">
                                    <div class="form-group">
                                        <label for="discount">Discount</label>
                                        <select id="discount" name="discount" class="form-control">
                                            @if($product->product_discount)
                                                <option selected value="{{$product->discount->id}}">{{$product->discount->discount_name}}</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-6">
                                    <div class="form-group">
                                        <label for="laundry">Product type (Is Laundry) *</label>
                                        <select required="" id="laundry" name="laundry" class="form-control">
                                            <option @if(!$product->laundry) selected @endif value="0">No</option>
                                            <option @if($product->laundry) selected @endif value="1">Yes</option>
                                        </select>
                                        <div class="invalid-tooltip">
                                            Please Select Product Type
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-6">
                                    <div class="form-group">
                                        <label for="product_description">Product Description *</label>
                                        <textarea id="product_description" class="form-control"
                                                  name="product_description"
                                                  rows="1">{{$product->description}}</textarea>
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-primary submit-fn mt-2" type="submit">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{asset('plugins/select2/select2.min.js')}}"></script>
    <script>
        window.addEventListener('load', function () {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            document.getElementsByTagName('form')
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function (form) {
                form.addEventListener('submit', function (event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    </script>
    <script>
        $("#laundry").select2();
        $(".properties_value").select2({
            placeholder: "Select Properties",
            ajax: {
                url: '{{route('product.property.search')}}',
                dataType: 'json',
                data: function (params) {
                    return {
                        search: params.term,
                        type: "public"
                    };
                },
                processResults: function (response) {
                    return {
                        results: $.map(response.data, function (txt, val) {
                            return {id: txt.id, text: txt.name}
                        })
                    };
                }
            }
        });
        $("#categories").select2({
            multiple: true,
            placeholder: "Select Category",
            ajax: {
                url: '{{route('category.search')}}',
                dataType: 'json',
                data: function (params) {
                    return {
                        search: params.term,
                        type: "public"
                    };
                },
                processResults: function (response) {
                    return {
                        results: $.map(response.data, function (txt, val) {
                            return {id: txt.id, text: txt.name}
                        })
                    };
                }
            }
        });

        $("#discount").select2({
            allowClear: true,
            placeholder: "Select Discount",
            ajax: {
                url: '{{route('product.discount.search')}}',
                dataType: 'json',
                data: function (params) {
                    return {
                        search: params.term,
                        type: "public"
                    };
                },
                processResults: function (response) {
                    return {
                        results: $.map(response.data, function (txt, val) {
                            return {id: txt.id, text: `${txt.discount_name}`}
                        })
                    };
                }
            }
        });
        $("#brand").select2({
            allowClear: true,
            placeholder: "Select Brand",
            ajax: {
                url: '{{route('brand.search')}}',
                dataType: 'json',
                data: function (params) {
                    return {
                        search: params.term,
                        type: "public"
                    };
                },
                processResults: function (response) {
                    return {
                        results: $.map(response.data, function (txt, val) {
                            return {id: txt.id, text: txt.name}
                        })
                    };
                }
            }
        });
        $(".product-code").click(function () {
            $("#product_code").val('{{env('PRODUCT_PREFIX')}}' + Date.now());
        });

    </script>
    <script>
        $("#append_properties").click(function () {
            const property_row = '<tr class="property-row">' +
                '<td><select name="properties_name[]" class="form-control properties_value"></select></td>\n' +
                '<td><div class="form-group"><input type="text" name="properties_value[]" class="form-control" required=""></div></td>\n' +
                '<td><div class="form-group"><a href="javascript:void(0);" class="btn btn-danger property-row-rm  btn-sm">\n' +
                '<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1">\n' +
                '<line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></a></div></td></tr>';
            $("#property-table").append(property_row);

            $(".properties_value:last").select2({
                placeholder: "Select Properties",
                ajax: {
                    url: '{{route('product.property.search')}}',
                    dataType: 'json',
                    data: function (params) {
                        return {
                            search: params.term,
                            type: "public"
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: $.map(response.data, function (txt, val) {
                                return {id: txt.id, text: txt.name}
                            })
                        };
                    }
                }
            }).focus();
            $('.properties_value').removeClass('properties_value');
        });
        /*$(".property-row-rm").click(function () {

        });*/
        $("#property-table").on("click", ".property-row-rm", function () {
            $(this).closest("tr").remove();
        });

    </script>
@endsection
