<button class="btn btn-dark btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Action
    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down">
        <polyline points="6 9 12 15 18 9"></polyline>
    </svg>
</button>
<div class="dropdown-menu" aria-labelledby="dropdownMenuLink12">
    <a class="dropdown-item" href="{{route('product.variation.offers',['variation_id'=>$product_variant->id])}}">Agent Offer</a>
    <a class="dropdown-item" onclick="add_agent_to_variation('{{$product_variant->id}}','{{$product_variant->variant_name}}')" href="javascript:void(0);">Assign Agent</a>
    <a class="dropdown-item" onclick="open_variation_options_modal('{{$product_variant->id}}')" href="javascript:void(0);">View Options</a>
    <div class="dropdown-divider"></div>
    <a class="dropdown-item" href="{{route('product.variation.edit',['variant_id'=>$product_variant->id])}}">Edit</a>
    <a class="dropdown-item" onclick="delete_product_variation('{{$product_variant->id}}')" href="javascript:void(0);">Delete</a>
</div>
