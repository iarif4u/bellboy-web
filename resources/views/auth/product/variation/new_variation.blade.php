@extends('auth.layouts.master')
@section('css')
    <link rel="stylesheet" href="{{asset('plugins/select2/select2.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('plugins/dropify/dropify.min.css')}}">
    <link href="{{asset('assets/css/users/account-setting.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/css/users/user-profile.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('plugins/file-upload/file-upload-with-preview.min.css')}}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
        <div class="widget widget-chart-one">
            <div class="widget-heading">
                <h5 class="">New Variation</h5>

            </div>

            <div class="widget-content">
                <div class="tabs tab-content">
                    <div class="row">
                        <div class="col-md-2">
                            <h6>Product Name:</h6>
                        </div>
                        <div class="col-md-2">
                            <h6>{{$product->product_name}}</h6>
                        </div>
                        <div class="col-md-2">
                            <h6>Product Code:</h6>
                        </div>
                        <div class="col-md-2">
                            <h6>{{$product->product_code}}</h6>
                        </div>
                        <div class="col-md-2">
                            <h6>Product Brand:</h6>
                        </div>
                        <div class="col-md-2">
                            @if($product->brand)
                                <h6>{{$product->brand->name}}</h6>
                            @else
                                <h6>Not Define</h6>
                            @endif
                        </div>
                    </div>
                    <div class="dropdown-divider"></div>
                    <div class="row">
                        <div class="col-md-2">
                            <h6>Product Category:</h6>
                        </div>
                        <div class="col-md-10">
                            <h6>{{ $product->categories->pluck('name')->implode(' | ') }}</h6>
                        </div>
                    </div>
                    <div id="content_1" class="tabcontent">
                        <form action="{{route('product.variation.add',['product_id'=>$product->id])}}"
                              enctype="multipart/form-data" method="post" novalidate="">
                            @csrf
                            <input type="hidden" value="{{$product->id}}" name="product_id">
                            <br>
                            <div class="form-row">
                                <div class="col-md-6 mb-12">
                                    <div class="form-group">
                                        <label for="variation_name">Variation Name *</label>
                                        <input id="variation_name" type="text" value="{{old('variation_name')}}" name="variation_name" placeholder="Variation Name" class="form-control" required="">
                                    </div>
                                </div>
                                <div class="col-md-6 mb-12">
                                    <div class="form-group">
                                        <label for="sku">SKU *</label>
                                        <input value="{{old('sku')}}" id="sku" type="text" name="sku" placeholder="SKU" class="form-control" required="">
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-6 mb-12">
                                    <div class="form-group">
                                        <label for="variation_unit">Variation Unit *</label>
                                        <select name="variation_unit" class="product-variation" id="variation_unit">
                                            @if($product_units->count()>0)
                                                @foreach($product_units as $product_unit)
                                                    @if(old('variation_unit')==$product_unit->id)
                                                        <option selected value="{{$product_unit->id}}">{{$product_unit->unit_name}}</option>
                                                    @else
                                                        <option value="{{$product_unit->id}}">{{$product_unit->unit_name}}</option>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-12">
                                    <div class="form-group">
                                        <label for="variation_unit_value">Variation Unit Value *</label>
                                        <input id="variation_unit_value" type="text" name="variation_unit_value" value="{{old('variation_unit_value')}}" placeholder="Unit Value" class="form-control" required="">
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-6 mb-12">
                                    <div class="form-group">
                                        <label for="product_cost">Product Cost *</label>
                                        <input id="product_cost" type="text" name="product_cost" value="{{old('product_cost')}}" placeholder="Product Cost" class="form-control" required="">
                                    </div>
                                </div>
                                <div class="col-md-6 mb-12">
                                    <div class="form-group">
                                        <label for="product_price">Product Price *</label>
                                        <input id="product_price" type="text" name="product_price" value="{{old('product_price')}}" placeholder="Product Price" class="form-control" required="">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="custom-file-container" data-upload-id="product_image">
                                        <label>Product image <a href="javascript:void(0)" class="custom-file-container__image-clear" title="Clear Image">x</a></label>
                                        <label class="custom-file-container__custom-file">
                                            <input type="file" name="product_image"
                                                   class="custom-file-container__custom-file__custom-file-input"
                                                   accept="image/*">
                                            <input type="hidden" name="MAX_FILE_SIZE" value="10485760"/>
                                            <span class="custom-file-container__custom-file__custom-file-control"></span>
                                        </label>
                                        <div class="custom-file-container__image-preview"></div>
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-primary submit-fn mt-2" type="submit">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script src="{{asset('plugins/select2/select2.min.js')}}"></script>
    <script src="{{asset('assets/js/users/account-settings.js')}}"></script>
    <script src="{{asset('plugins/file-upload/file-upload-with-preview.min.js')}}"></script>
    <script src="{{asset('plugins/dropify/dropify.min.js')}}"></script>
    <script>
        new FileUploadWithPreview('product_image');
    </script>
    <script>
        $(".product-variation").select2();
        $(".option_name").select2({
            placeholder: "Select Option",
            ajax: {
                url: '{{route('product.option.search')}}',
                dataType: 'json',
                data: function (params) {
                    return {
                        search: params.term,
                        type: "public"
                    };
                },
                processResults: function (response) {
                    return {
                        results: $.map(response.data, function (txt, val) {
                            return {id: txt.id, text: txt.name}
                        })
                    };
                }
            }
        });
        $(".option_value").select2({placeholder: "Select Value"});
        $(".property-row").on("change", ".option_name", function () {
            $(this).parent().parent().find('.option_value').empty().trigger("change").select2({
                placeholder: 'Select Value',
                ajax: {
                    url: `{{route('product.option.value_search')}}?option_id=${$(this).val()}`,
                    dataType: 'json',
                    data: function (params) {
                        return {
                            search: params.term,
                            type: "public"
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: $.map(response.data, function (txt, val) {
                                return {id: txt.id, text: txt.value}
                            })
                        };
                    },
                    cache: true
                }
            });
        });

    </script>
    <script>
        $("#add_variation_option").click(function () {
            let new_class_name = makeRandomStr();
            const option_row = `<tr class="property-row"><td><select onchange="product_option_changed(this, '${new_class_name}')" name="option_name[]" class="form-control option_name product-option"></select></td>\n` +
                `<td><div class="form-group"><select name="option_value[]" class="form-control ${new_class_name} option_value"></select></div></td>\n` +
                '<td><div class="form-group"><a href="javascript:void(0);" class="btn btn-danger property-row-rm  btn-sm"><svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></a></div></td></tr>';
            $("#variation-option-table").append(option_row);
            $(".option_value").select2({placeholder: "Select Value"});
            $('.option_value').removeClass('option_value');
            $(".option_name:last").select2({
                placeholder: "Select Option",
                ajax: {
                    url: '{{route('product.option.search')}}',
                    dataType: 'json',
                    data: function (params) {
                        return {
                            search: params.term,
                            type: "public"
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: $.map(response.data, function (txt, val) {
                                return {id: txt.id, text: txt.name}
                            })
                        };
                    }
                }
            }).focus();

        });

        function product_option_changed(context, class_name) {
            $(`.${class_name}`).empty().trigger("change").select2({
                placeholder: 'Select Value',
                ajax: {
                    url: `{{route('product.option.value_search')}}?option_id=${context.value}`,
                    dataType: 'json',
                    data: function (params) {
                        return {
                            search: params.term,
                            type: "public"
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: $.map(response.data, function (txt, val) {
                                return {id: txt.id, text: txt.value}
                            })
                        };
                    },
                    cache: true
                }
            });
        }

        $("#variation-option-table").on("click", ".property-row-rm", function () {
            $(this).closest("tr").remove();
        });
    </script>

@endsection
