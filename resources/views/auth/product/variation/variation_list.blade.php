@extends('auth.layouts.master')
@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('plugins/table/datatable/datatables.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset("plugins/table/datatable/custom_dt_html5.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("plugins/table/datatable/dt-global_style.css")}}">
    <link href="{{asset('plugins/select2/select2.min.css')}}" rel="stylesheet" type="text/css"/>
@endsection
@section('content')
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
        <div class="widget widget-chart-one">
            <div class="widget-heading">
                <h5 class="">Product Variations</h5>
            </div>
            <div class="widget-content">
                <div class="tabs tab-content">
                    <div class="row">
                        <div class="col-md-2">
                            <h6>Product Name:</h6>
                        </div>
                        <div class="col-md-2">
                            <h6>{{$product->product_name}}</h6>
                        </div>
                        <div class="col-md-2">
                            <h6>Product Code:</h6>
                        </div>
                        <div class="col-md-2">
                            <h6>{{$product->product_code}}</h6>
                        </div>
                        <div class="col-md-2">
                            <h6>Product Brand:</h6>
                        </div>
                        <div class="col-md-2">
                            @if($product->brand)
                                <h6>{{$product->brand->name}}</h6>
                            @else
                                <h6>Not Define</h6>
                            @endif
                        </div>
                    </div>
                    <div class="dropdown-divider"></div>
                    <div class="row">
                        <div class="col-md-2">
                            <h6>Product Category:</h6>
                        </div>
                        <div class="col-md-10">
                            <h6>{{ $product->categories->pluck('name')->implode(' | ') }}</h6>
                        </div>
                    </div>
                </div>
            </div>
            <div class="table-responsive mb-4 mt-4">
                {{$dataTable->table(['class'=>"table table-hover non-hover","style"=>"width:100%"])}}
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script src="{{asset('plugins/table/datatable/datatables.js')}}"></script>
    {{$dataTable->scripts()}}
    <script src="{{asset('plugins/select2/select2.min.js')}}"></script>
    <script>
        $("#assign_agent").select2({
            dropdownParent: $("#agentAssign"),
            placeholder: "Select Agent",
            ajax: {
                url: '{{route('agent.search')}}',
                dataType: 'json',
                data: function (params) {
                    return {
                        search: params.term,
                        type: "public"
                    };
                },
                processResults: function (response) {
                    return {
                        results: $.map(response.data, function (txt, val) {
                            return {id: txt.id, text: txt.name}
                        })
                    };
                }
            }
        });
        function open_variation_options_modal(variation_id) {
            axios.post(`{{route('product.variation.option_n_value')}}`, {
                _token: '{{csrf_token()}}',
                variation_id: variation_id
            }).then(function (response) {
                let row = '';
                response.data.map(function (option_value, key) {
                    row += `<tr><td>${option_value.option}</td> <td>${option_value.value.join(" , ")}</td></tr>`;
                });
                $(".variations-values").html(row);
            }).catch(function (error) {
                console.error(error);
            }).then(function () {
                $("#variationOptions").modal("show");
            });
        }

        function add_agent_to_variation(variation_id,variation_nae) {
            $("#agentAssign").modal("show");
            $(".variation-id").val(variation_id);
            $(".variation-name").text(variation_nae);
        }

        function delete_product_variation(variation_id) {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success btn-rounded',
                    cancelButton: 'btn btn-danger btn-rounded mr-3',
                },
                buttonsStyling: false
            });
            swalWithBootstrapButtons.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    blockUI("#product-variations-table");
                    axios.delete(`{{route('product.variation.delete')}}?variation_id=${variation_id}`)
                        .then(function (response) {
                            swalWithBootstrapButtons.fire('Deleted!', response.data.message, 'success');
                        })
                        .catch(function (error) {
                            showError(error.response.data.message);
                        })
                        .then(function () {
                            $("#product-variations-table").DataTable().ajax.reload(null, false);
                            unBlockUI("#product-variations-table");
                        });
                } else if (
                    /* Read more about handling dismissals below */
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire('Cancelled', 'Brand delete cancelled', 'error');
                }
            });
        }
    </script>
@endsection

@section('modal')
    <div class="modal fade" id="agentAssign" role="dialog" aria-labelledby="exampleModalCenterTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Assign Agent</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                            <line x1="18" y1="6" x2="6" y2="18"></line>
                            <line x1="6" y1="6" x2="18" y2="18"></line>
                        </svg>
                    </button>
                </div>
                <form enctype="multipart/form-data" class="simple-example" action="{{route('product.variation.assign_agent')}}" method="post" novalidate="">
                    @csrf
                    <input type="hidden" class="variation-id" name="variation_id">
                    <div class="modal-body">
                        <div class="form-row">
                            <div class="col-md-12 mb-12">
                                <div class="form-group">
                                    <h5>Variation Name: <span class="variation-name"></span></h5>
                                </div>
                            </div>
                            <div class="col-md-12 mb-12">
                                <div class="form-group">
                                    <div class="form-group">
                                        <label for="assign_agent">Agent Name</label>
                                        <select class="form-control" name="assign_agent" id="assign_agent">
                                            <option selected=""></option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-12">
                                <div class="form-group">
                                    <label for="variation-cost">Product Cost *</label>
                                    <input id="variation-cost" type="number" name="cost" placeholder="Product Cost" class="form-control" required="required">
                                </div>
                            </div>
                            <div class="col-md-12 mb-12">
                                <div class="form-group">
                                    <label for="variation-stock">Stock *</label>
                                    <input id="variation-stock" type="number" name="stock" placeholder="Product Stock" class="form-control" required="required">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>

    </div>
    <!-- Modal -->
    <div id="variationOptions" class="modal animated zoomInUp custo-zoomInUp" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"> {{$product->product_name}} Varation Options</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                             viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                             stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                            <line x1="18" y1="6" x2="6" y2="18"></line>
                            <line x1="6" y1="6" x2="18" y2="18"></line>
                        </svg>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="table-responsive mb-4 mt-4">
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Variation Option</th>
                                <th>Option Value</th>
                            </tr>
                            </thead>
                            <tbody class="variations-values">
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i>Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection
