@extends('auth.layouts.master')
@section('css')
    <link href="{{asset('assets/css/users/account-setting.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/css/users/user-profile.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('plugins/file-upload/file-upload-with-preview.min.css')}}" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="{{asset('plugins/table/datatable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/editors/markdown/simplemde.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset("plugins/table/datatable/custom_dt_html5.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("plugins/table/datatable/dt-global_style.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("assets/css/forms/switches.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("plugins/select2/select2.min.css")}}">
@endsection


@section('content')
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
        <div class="widget-chart-one">
            <div class="widget-content widget-content-area br-6">
                <div class="widget-heading">
                    <h5 class="">Product List</h5>
                </div>

                <div class="table-responsive mb-4 mt-4">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <a href="{{route('product.variation.download')}}" class="btn btn-success">Download</a>
                            </div>
                        </div>
                    </div>
                    {{$dataTable->table(['class'=>"table table-hover non-hover","style"=>"width:100%"])}}
                </div>
            </div>
        </div>

    </div>
@endsection

@section('js')
    <!-- BEGIN PAGE LEVEL CUSTOM SCRIPTS -->
    <script src="{{asset('plugins/table/datatable/datatables.js')}}"></script>
    <!-- NOTE TO Use Copy CSV Excel PDF Print Options You Must Include These Files  -->
    <script src="{{asset('plugins/table/datatable/button-ext/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('plugins/table/datatable/button-ext/jszip.min.js')}}"></script>
    <script src="{{asset('plugins/table/datatable/button-ext/buttons.html5.min.js')}}"></script>
    <script src="{{asset('plugins/table/datatable/button-ext/buttons.print.min.js')}}"></script>
    <script src="{{asset('plugins/select2/select2.min.js')}}"></script>
    {{$dataTable->scripts()}}
    <script>
        $.fn.dataTable.ext.errMode = 'none';
        $("#category").select2({
            placeholder: "Select Category",
            ajax: {
                url: '{{route('category.search')}}',
                dataType: 'json',
                data: function (params) {
                    return {
                        search: params.term,
                        type: "public"
                    };
                },
                processResults: function (response) {
                    return {
                        results: $.map(response.data, function (txt, val) {
                            return {id: txt.id, text: txt.name}
                        })
                    };
                }
            }
        });
        $(document.body).on("change","#category",function(){
            dt_products.draw();
        });

        function change_product_status(product_id, status) {
            blockUI("#products-table");
            axios.post(`{{route('product.status')}}`, {
                _token: '{{csrf_token()}}',
                product_status: status,
                product_id: product_id,
            })
                .catch(function (error) {
                    showError(error.response.data.message);
                    $("#products-table").DataTable().ajax.reload(null, false);
                })
                .then(function () {
                    unBlockUI("#products-table");
                });
        }

        function update_product_discount(product_id, product_name, product_code, option_id = false, option_name = false) {
            if (option_id !== false) {
                let $newOption = $("<option selected='selected'></option>").val(option_id).text(option_name);
                $("#discount").append($newOption).trigger('change');
            }else{
                $('#discount').empty().trigger("change");
            }
            $("#productDiscount").modal("show");
            $(".discount-product-code").html(product_code);
            $(".discount-product-name").html(product_name);
            $(".discount-product-id").val(product_id);
        }

        function delete_product(product_id) {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success btn-rounded',
                    cancelButton: 'btn btn-danger btn-rounded mr-3',
                },
                buttonsStyling: false
            });
            swalWithBootstrapButtons.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    blockUI("#products-table");
                    axios.delete(`{{route('product.delete')}}?product_id=${product_id}`)
                        .then(function (response) {
                            swalWithBootstrapButtons.fire('Deleted!', response.data.message, 'success');
                        })
                        .catch(function (error) {
                            showError(error.response.data.message);
                        })
                        .then(function () {
                            $("#products-table").DataTable().ajax.reload(null, false);
                            unBlockUI("#products-table");
                        });
                } else if (
                    /* Read more about handling dismissals below */
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire('Cancelled', 'Product delete cancelled', 'error');
                }
            });
        }

        $("#discount").select2({
            allowClear:true,
            dropdownParent: $("#productDiscount"),
            placeholder: "Select Discount",
            ajax: {
                url: '{{route('product.discount.search')}}',
                dataType: 'json',
                data: function (params) {
                    return {
                        search: params.term,
                        type: "public"
                    };
                },
                processResults: function (response) {
                    return {
                        results: $.map(response.data, function (txt, val) {
                            return {id: txt.id, text: `${txt.discount_name}`}
                        })
                    };
                }
            }
        });
    </script>
@endsection

@section('modal')
    <!-- Modal -->
    <div id="productDiscount" class="modal animated zoomInUp custo-zoomInUp" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Product Discount</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                             viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                             stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                            <line x1="18" y1="6" x2="6" y2="18"></line>
                            <line x1="6" y1="6" x2="18" y2="18"></line>
                        </svg>
                    </button>
                </div>
                <form class="simple-example" action="{{route('product.product_discount')}}" method="post" novalidate="">
                    @csrf
                    <input type="hidden" class="discount-product-id" name="product_id">
                    <div class="modal-body">
                        <div class="form-row">
                            <div class="col-md-6 mb-6">
                                <h6 class="">Product Name</h6>
                            </div>
                            <div class="col-md-6 mb-6">
                                <h6 class="discount-product-name">Product Name</h6>
                            </div>
                            <div class="col-md-6 mb-6">
                                <h6 class="">Product Code</h6>
                            </div>
                            <div class="col-md-6 mb-6">
                                <h6 class="discount-product-code">Product Code</h6>
                            </div>
                            <div class="col-md-12 mb-12">
                                <div class="form-group">
                                    <label for="discount">Discount Label*</label>
                                    <select name="discount" id="discount" class="form-control"></select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
