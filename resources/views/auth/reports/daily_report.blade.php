@extends('auth.layouts.master')
@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('plugins/table/datatable/datatables.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset("plugins/table/datatable/custom_dt_html5.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("plugins/table/datatable/dt-global_style.css")}}">
@endsection
@section('content')
<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
    <div class="widget-content">

        <div class="widget-content widget-content-area br-6">
            <div class="widget-heading">
                <h5 class="">Daily Sale </h5>
            </div>
            <div class="table-responsive mb-4 mt-4">
                <table id="html5-extension" class="table table-hover non-hover" style="width:100%">
                    <thead>
                    <tr>
                        <th>SL</th>
                        <th>Order Id</th>
                        <th>Customer Name</th>
                        <th>Total</th>
                        <th>Time</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($orders->count())
                        @php
                            $counter=1;
                        @endphp
                        @foreach($orders as $order)
                            <tr>
                                <td>{{$counter++}}</td>
                                <td>{{$order->order_id}}</td>
                                <td>{{$order->customer->name}}</td>
                                <td>{{$order->total}}</td>
                                <td>{{date('h:i A',strtotime($order->created_at))}}</td>
                                <td>
                                    <a onclick="get_order_details('{{$order->id}}','{{date('d-m-Y',strtotime($order->created_at))}}')" class="btn btn-outline-dark mb-2 btn-sm" href="javascript:void(0);">View</a>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                    <tfoot>
                    <th class="text-center" colspan="3">Total</th>
                    <th>{{$orders->sum('total')}}</th>
                    <th></th>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

</div>
@endsection
@section('modal')
<div class="modal fade" id="paymentInvoice">
        <div class="modal-dialog modal-lg" role="document">
            <form action="">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="myLargeModalLabel">Customer Invoice</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                 viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                 stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                <line x1="6" y1="6" x2="18" y2="18"></line>
                            </svg>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div id="" class="">
                            <div class="layout-spacing">
                                <div class="content-section  animated animatedFadeInUp fadeInUp">
                                    <div class="row inv--head-section">
                                        <div class="col-md-6 mb-12">
                                            <h3 class="in-heading">INVOICE</h3>
                                        </div>
                                        <div class="col-md-6 mb-12 align-self-center text-sm-right">
                                            <div class="company-info">
                                                <h5 class="inv-brand-name">BellBoy</h5>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 align-self-center">
                                            <p class="order-customer-name"></p>
                                            <p class="order-customer-phone"></p>
                                            <p class="order-customer-address"></p>
                                        </div>
                                        <div class="col-md-6 align-self-center  text-sm-right order-2">
                                            <p class="inv-list-number">
                                                <span class="inv-title">Order Id : </span>
                                                <span class="order-id"></span>
                                            </p>
                                            <p class="inv-created-date">
                                                <span class="inv-title">Order Date : </span>
                                                <span class="order-date"></span>
                                            </p>

                                        </div>
                                    </div>
                                    <br>
                                    <div class="row inv--product-table-section">
                                        <div class="col-12">
                                            <div class="table-responsive">
                                                <table id="html5-extension" class="table table-hover non-hover" style="width:100%">
                                                    <thead>
                                                    <tr>
                                                        <th>SL</th>
                                                        <th>Name</th>
                                                        <th>Unit</th>
                                                        <th>Order Qty</th>
                                                        <th>Delivery Qty</th>
                                                        <th>Unit Price</th>
                                                        <th>Total</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody class="products-rows">

                                                    </tbody>
                                                    <tfoot>
                                                    <tr>
                                                        <th colspan="6" class="text-right">Total</th>
                                                        <th class="order-total text-right"></th>
                                                        <th></th>
                                                    </tr>
                                                    <tr>
                                                        <th colspan="6" class="text-right">Discount</th>
                                                        <th class="order-discount text-right"></th>
                                                        <th></th>
                                                    </tr>
                                                    <tr>
                                                        <th colspan="6" class="text-right">Total Payable</th>
                                                        <th class="order-payable text-right"></th>
                                                        <th></th>
                                                    </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Close</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('js')
    <script src="{{asset('plugins/table/datatable/datatables.js')}}"></script>
    <script>
        $('#html5-extension').DataTable({
            "oLanguage": {
                "oPaginate": {
                    "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                    "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
                },
                "sInfo": "Showing page _PAGE_ of _PAGES_",
                "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                "sSearchPlaceholder": "Search...",
                "sLengthMenu": "Results :  _MENU_",
            },
            "stripeClasses": [],
            "lengthMenu": [7, 10, 20, 50],
            "pageLength": 10
        });

    </script>
    <script>
        function get_order_details(order_id,order_date) {
            axios.post('{{route('order.order_info')}}', {
                order_id: order_id,
                _token: '{{csrf_token()}}',
            }).then(function (res) {
                design_table_dom(res.data,order_date);
            }).catch(function (err) {
                console.log(err);
            }).then(() => {
                $("#paymentInvoice").modal("show");
            });
        }
        function design_table_dom(order,order_date) {
            const customer = order.customer;
            $(".order-total-price").text(order.grand_total);
            $(".coupon-id").val(order.coupon_id);
            $(".order-id").text(order.order_id);
            $(".order-date").text(order_date);
            $(".order-discout-price").text(order.coupon_discount);
            $(".order-total-payable").text(order.total);
            $(".order-coupon-discount").text(order.coupon_discount);
            $(".order-customer-name").text(customer.name);
            $(".order-customer-phone").text(customer.phone);
            $(".order-customer-address").text(order.delivery_address);
            $(".order-total").text(order.grand_total);
            $(".order-discount").text(order.coupon_discount);
            $(".order-payable").text(order.total);
            generate_product_tbody(order);
        }

        function generate_product_tbody(order) {
            const rows = order.items.map((item,index) => {
                return `<tr>
                            <td>${++index}</td>
                            <td>${item.variation.variant_name}</td>
                            <td>${item.variation.unit_value} ${item.variation.unit.unit_name}</td>
                            <td>${item.quantity}</td>
                            <td>${item.delivered_quantity}</td>
                            <td>${item.price}</td>
                            <td class="text-right">${item.sub_total}</td>
                        </tr>`;
            });
            $(".products-rows").html(rows);
        }
    </script>
@endsection
