@extends('auth.layouts.master')
@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('plugins/table/datatable/datatables.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset("plugins/table/datatable/custom_dt_html5.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("plugins/table/datatable/dt-global_style.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("plugins/flatpickr/flatpickr.css")}}">
@endsection
@section('content')
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
        <div class="widget-content">

            <div class="widget-content widget-content-area br-6">
                <div class="widget-heading">
                    <h5 class="">Monthly Sale </h5>
                    <div class="row">
                        <div class="justify-content-lg-center offset-4">
                            <form class="form-inline date-range-form" action="{{route('report.monthly')}}"
                                  method="post">
                                @csrf
                                <label for="monthly-date" class="mr-sm-2">Select Date</label>
                                <input type="text" class="form-control mb-2 mr-sm-2" placeholder="Date"
                                       id="monthly-date">
                                <button type="submit" class="btn btn-primary mb-2">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="table-responsive mb-4 mt-4">
                    {{$dataTable->table(['class'=>"table table-hover non-hover","style"=>"width:100%"])}}
                </div>
            </div>
        </div>

    </div>
@endsection
@section('modal')
    <div class="modal fade" id="paymentInvoice">
        <div class="modal-dialog modal-lg" role="document">
            <form action="">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="myLargeModalLabel">Customer Invoice</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                 viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                 stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                <line x1="6" y1="6" x2="18" y2="18"></line>
                            </svg>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div id="" class="">
                            <div class="layout-spacing">
                                <div class="content-section  animated animatedFadeInUp fadeInUp">
                                    <div class="row inv--head-section">
                                        <div class="col-md-6 mb-12">
                                            <h3 class="in-heading">INVOICE</h3>
                                        </div>
                                        <div class="col-md-6 mb-12 align-self-center text-sm-right">
                                            <div class="company-info">
                                                <h5 class="inv-brand-name">BellBoy</h5>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 align-self-center">
                                            <p class="order-customer-name"></p>
                                            <p class="order-customer-phone"></p>
                                            <p class="order-customer-address"></p>
                                        </div>
                                        <div class="col-md-6 align-self-center  text-sm-right order-2">
                                            <p class="inv-list-number">
                                                <span class="inv-title">Order Id : </span>
                                                <span class="order-id"></span>
                                            </p>
                                            <p class="inv-created-date">
                                                <span class="inv-title">Order Date : </span>
                                                <span class="order-date"></span>
                                            </p>

                                        </div>
                                    </div>
                                    <br>
                                    <div class="row inv--product-table-section">
                                        <div class="col-12">
                                            <div class="table-responsive">
                                                <table id="html5-extension" class="table table-hover non-hover"
                                                       style="width:100%">
                                                    <thead>
                                                    <tr>
                                                        <th>SL</th>
                                                        <th>Name</th>
                                                        <th>Unit</th>
                                                        <th>Order Qty</th>
                                                        <th>Delivery Qty</th>
                                                        <th>Unit Price</th>
                                                        <th>Total</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody class="products-rows">

                                                    </tbody>
                                                    <tfoot>
                                                    <tr>
                                                        <th colspan="6" class="text-right">Total</th>
                                                        <th class="order-total text-right"></th>
                                                        <th></th>
                                                    </tr>
                                                    <tr>
                                                        <th colspan="6" class="text-right">Discount</th>
                                                        <th class="order-discount text-right"></th>
                                                        <th></th>
                                                    </tr>
                                                    <tr>
                                                        <th colspan="6" class="text-right">Total Payable</th>
                                                        <th class="order-payable text-right"></th>
                                                        <th></th>
                                                    </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Close</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('js')
    <script src="{{asset('plugins/table/datatable/datatables.js')}}"></script>
    <script src="{{asset('plugins/flatpickr/flatpickr.js')}}"></script>
    <script>
        var dt_reports = $("#orders-table").DataTable({
            "serverSide": true,
            "processing": false,
            "ajax": {
                "url": "{{route('report.monthly')}}",
                "type": "GET",
                "data": function (data) {
                    if ($('#monthly-date').val() != null) {
                        data.monthlyDate = $('#monthly-date').val();
                    }
                    for (var i = 0, len = data.columns.length; i < len; i++) {
                        if (!data.columns[i].search.value) delete data.columns[i].search;
                        if (data.columns[i].searchable === true) delete data.columns[i].searchable;
                        if (data.columns[i].orderable === true) delete data.columns[i].orderable;
                        if (data.columns[i].data === data.columns[i].name) delete data.columns[i].name;
                    }
                    delete data.search.regex;
                }
            },
            "columns": [
                {"data": "order_id", "name": "order_id", "title": "Order Id", "orderable": true, "searchable": true},
                {
                    "data": "customer.name",
                    "name": "customer.name",
                    "title": "Name",
                    "orderable": true,
                    "searchable": true
                },
                {"data": "total", "name": "total", "title": "Total", "orderable": true, "searchable": true},
                {"data": "created_at", "name": "created_at", "title": "time", "orderable": true, "searchable": true},
                {"data": "action", "name": "action", "title": "Action", "orderable": false, "searchable": false}
            ],
            "language": {
                "paginate": {
                    "previous": "<svg xmlns=\"http:\/\/www.w3.org\/2000\/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-arrow-left\"><line x1=\"19\" y1=\"12\" x2=\"5\" y2=\"12\"><\/line><polyline points=\"12 19 5 12 12 5\"><\/polyline><\/svg>",
                    "next": "<svg xmlns=\"http:\/\/www.w3.org\/2000\/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-arrow-right\"><line x1=\"5\" y1=\"12\" x2=\"19\" y2=\"12\"><\/line><polyline points=\"12 5 19 12 12 19\"><\/polyline><\/svg>"
                },
                "info": "Showing page _PAGE_ of _PAGES_",
                "search": "<svg xmlns=\"http:\/\/www.w3.org\/2000\/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-search\"><circle cx=\"11\" cy=\"11\" r=\"8\"><\/circle><line x1=\"21\" y1=\"21\" x2=\"16.65\" y2=\"16.65\"><\/line><\/svg>",
                "searchPlaceholder": "Search...",
                "lengthMenu": "Results :  _MENU_"
            }, "lengthMenu": [7, 10, 20, 50], "pageLength": 50, "order": [[0, "asc"]]
        });

        $(".date-range-form").submit(function (e) {
            e.preventDefault();
            dt_reports.draw();
        })
    </script>
    <script>
        function get_order_details(order_id, order_date) {
            axios.post('{{route('order.order_info')}}', {
                order_id: order_id,
                _token: '{{csrf_token()}}',
            }).then(function (res) {
                design_table_dom(res.data, order_date);
            }).catch(function (err) {
                console.log(err);
            }).then(() => {
                $("#paymentInvoice").modal("show");
            });
        }

        function design_table_dom(order, order_date) {
            const customer = order.customer;
            $(".order-total-price").text(order.grand_total);
            $(".coupon-id").val(order.coupon_id);
            $(".order-id").text(order.order_id);
            $(".order-date").text(order_date);
            $(".order-discout-price").text(order.coupon_discount);
            $(".order-total-payable").text(order.total);
            $(".order-coupon-discount").text(order.coupon_discount);
            $(".order-customer-name").text(customer.name);
            $(".order-customer-phone").text(customer.phone);
            $(".order-customer-address").text(order.delivery_address);
            $(".order-total").text(order.grand_total);
            $(".order-discount").text(order.coupon_discount);
            $(".order-payable").text(order.total);
            generate_product_tbody(order);
        }

        function generate_product_tbody(order) {
            const rows = order.items.map((item, index) => {
                return `<tr>
                            <td>${++index}</td>
                            <td>${item.variation.variant_name}</td>
                            <td>${item.variation.unit_value} ${item.variation.unit.unit_name}</td>
                            <td>${item.quantity}</td>
                            <td>${item.delivered_quantity}</td>
                            <td>${item.price}</td>
                            <td class="text-right">${item.sub_total}</td>
                        </tr>`;
            });
            $(".products-rows").html(rows);
        }

        var f3 = flatpickr(document.getElementById('monthly-date'), {
            mode: "range"
        });
    </script>
@endsection
