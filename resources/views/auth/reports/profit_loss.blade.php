@extends('auth.layouts.master')
@section('css')

    <link rel="stylesheet" type="text/css" href="{{asset('plugins/table/datatable/datatables.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset("plugins/table/datatable/custom_dt_html5.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("plugins/table/datatable/dt-global_style.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("plugins/flatpickr/flatpickr.css")}}">
@endsection
@section('content')
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
    <div class="widget widget-chart-one">
        <div class="widget-heading">
            <h5 class="">Profit/Loss Report </h5>
            <ul class="tabs tab-pills dateRange">
                <li>
                    <div class="form-group mb-0 form-width">
                        <form class="form-inline date-range-form" action="{{route('report.profit')}}" method="post">
                            @csrf
                            <label class="mr-2" for="dateRange">Select Date
                                <input value="{{$dates}}" id="dateRange" name="dates" class="form-control ml-5 mb-2 mr-sm-2 flatpickr flatpickr-input active" type="text" placeholder="Select Date..">
                            </label>
                            <button type="submit" class="btn btn-primary mb-2">Submit</button>
                        </form>
                    </div>
                </li>
            </ul>
        </div>
        <div class="table-responsive mb-4 mt-4">

            <table class="table table-bordered">
                @if(isset($orderItems))
                <tbody>
                <tr>
                    <td class="text-center">Total Sells</td>
                    <td class="text-center">
                        BDT {{$orderItems->sum('sub_price')}}
                    </td>
                </tr>

                <tr>
                    <td class="text-center">Cost Of Sales</td>
                    <td class="text-center">
                        BDT {{$orderItems->sum('sub_costs')}}
                    </td>
                </tr>


                <tr>
                    <td class="text-center">Gross Profit/Loss</td>
                    <td class="text-center">
                        BDT {{$orderItems->sum('item_value')}}
                    </td>
                </tr>

                <tr>
                    <td class="text-center">Total Expense</td>
                    <td class="text-center">
                        (-) BDT {{$expense->sum('expense_amount')}}
                    </td>
                </tr>


                <tr style="background-color: #f7f7f7;">
                    <td class="text-center"><b>Net Profit/Loss</b></td>
                    <td class="text-center">
                        <b>
                            BDT {{$orderItems->sum('item_value')-$expense->sum('expense_amount')}}
                        </b>
                    </td>
                </tr>
                </tbody>
                @endif
            </table>

        </div>
    </div>
</div>
@endsection
@section('js')
    <script src="{{asset('plugins/table/datatable/datatables.js')}}"></script>
    <script src="{{asset('plugins/flatpickr/flatpickr.js')}}"></script>
    <script>
        $('#html5-extension').DataTable({
            dom: '<"row"<"col-md-12"<"row"<"col-md-6"B><"col-md-6"f> > ><"col-md-12"rt> <"col-md-12"<"row"<"col-md-5"i><"col-md-7"p>>> >',
            buttons: {
                buttons: [
                    {extend: 'copy', className: 'btn'},
                    {extend: 'csv', className: 'btn'},
                    {extend: 'excel', className: 'btn'},
                    {extend: 'print', className: 'btn'}
                ]
            },
            "oLanguage": {
                "oPaginate": {
                    "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                    "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
                },
                "sInfo": "Showing page _PAGE_ of _PAGES_",
                "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                "sSearchPlaceholder": "Search...",
                "sLengthMenu": "Results :  _MENU_",
            },
            "stripeClasses": [],
            "lengthMenu": [7, 10, 20, 50],
            "pageLength": 7
        });

        var f3 = flatpickr(document.getElementById('dateRange'), {
            mode: "range"
        });
    </script>
@endsection
