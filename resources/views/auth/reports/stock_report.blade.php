@extends('auth.layouts.master')
@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('plugins/table/datatable/datatables.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset("plugins/table/datatable/custom_dt_html5.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("plugins/table/datatable/dt-global_style.css")}}">
@endsection
@section('content')
<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
    <div class="widget-content">

        <div class="widget-content widget-content-area br-6">
            <div class="widget-heading">
                <h5 class="">Stock Report </h5>
            </div>
            <div class="table-responsive mb-4 mt-4">
                {{$dataTable->table(['class'=>"table table-hover non-hover","style"=>"width:100%"])}}
            </div>
        </div>
    </div>

</div>
@endsection

@section('modal')
    <!-- Modal -->
    <div class="modal fade" id="addStock">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Approve Agent Price</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                            <line x1="18" y1="6" x2="6" y2="18"></line>
                            <line x1="6" y1="6" x2="18" y2="18"></line>
                        </svg>
                    </button>
                </div>
                <form enctype="multipart/form-data" class="simple-example" action="{{route('report.add_stock')}}" method="post" novalidate="">
                    @csrf
                    <input type="hidden" class="offer-id" name="offer_id">
                    <div class="modal-body">
                        <div class="form-row">
                            <div class="col-md-12 mb-12">
                                <div class="form-group">
                                    <h5>Variation Name: <span class="variation-name"></span></h5>
                                </div>
                            </div>
                            <div class="col-md-12 mb-12">
                                <div class="form-group">
                                    <h5>Agent Name: <span class="agent-name"></span></h5>
                                </div>
                            </div>
                            <div class="col-md-12 mb-12">
                                <div class="form-group">
                                    <label for="variation-cost">Product Cost *</label>
                                    <input id="variation-cost" type="number" name="cost" placeholder="Product Cost" class="form-control" required="required">
                                </div>
                            </div>
                            <div class="col-md-12 mb-12">
                                <div class="form-group">
                                    <label for="variation-stock">Stock *</label>
                                    <input id="variation-stock" type="number" name="stock" placeholder="Product Stock" class="form-control" required="required">
                                </div>
                            </div>
                            <div class="col-md-12 mb-12">
                                <div class="form-group">
                                    <label for="variation-price">Price *</label>
                                    <input id="variation-price" type="number" name="price" placeholder="Product Price" class="form-control" required="required">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>

    </div>

    <div class="modal fade" id="adjustStock">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Approve Agent Price</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                            <line x1="18" y1="6" x2="6" y2="18"></line>
                            <line x1="6" y1="6" x2="18" y2="18"></line>
                        </svg>
                    </button>
                </div>
                <form enctype="multipart/form-data" action="{{route('report.adjust_stock')}}" method="post" novalidate="">
                    @csrf
                    <input type="hidden" class="offer-id" name="offer_id">
                    <div class="modal-body">
                        <div class="form-row">
                            <div class="col-md-12 mb-12">
                                <div class="form-group">
                                    <h5>Variation Name: <span class="variation-name"></span></h5>
                                </div>
                            </div>
                            <div class="col-md-12 mb-12">
                                <div class="form-group">
                                    <h5>Agent Name: <span class="agent-name"></span></h5>
                                </div>
                            </div>
                            <div class="col-md-12 mb-12">
                                <div class="form-group">
                                    <label for="ad-variation-cost">Product Cost *</label>
                                    <input id="ad-variation-cost" type="number" name="cost" placeholder="Product Cost" class="form-control" required="required">
                                </div>
                            </div>
                            <div class="col-md-12 mb-12">
                                <div class="form-group">
                                    <label for="add-variation-stock">Stock *</label>
                                    <input id="ad-variation-stock" type="number" name="stock" placeholder="Product Stock" class="form-control" required="required">
                                </div>
                            </div>
                            <div class="col-md-12 mb-12">
                                <div class="form-group">
                                    <label for="add-variation-price">Price *</label>
                                    <input id="ad-variation-price" type="number" name="price" placeholder="Product Price" class="form-control" required="required">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>

    </div>
@endsection

@section('js')
    <script src="{{asset('plugins/table/datatable/datatables.js')}}"></script>
    {{$dataTable->scripts()}}
    <script>
        function add_stock(offer_id,cost,price,variation_name,agent_name) {
            $(".variation-name").text(variation_name);
            $(".agent-name").text(agent_name);
            $(".offer-id").val(offer_id);
            $("#variation-cost").val(cost);
            $("#variation-price").val(price);
            $("#addStock").modal("show");
        }

        function adjust_stock(offer_id,cost,price,stock,variation_name,agent_name) {
            $(".variation-name").text(variation_name);
            $(".agent-name").text(agent_name);
            $(".offer-id").val(offer_id);
            $("#ad-variation-cost").val(cost);
            $("#ad-variation-price").val(price);
            $("#ad-variation-stock").val(stock);
            $("#adjustStock").modal("show");
        }
    </script>
@endsection
