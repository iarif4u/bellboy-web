@extends('auth.layouts.master')
@section('css')
    <link rel="stylesheet" href="{{asset("plugins/flatpickr/flatpickr.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset('plugins/table/datatable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/editors/markdown/simplemde.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset("plugins/table/datatable/custom_dt_html5.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("plugins/table/datatable/dt-global_style.css")}}">
@endsection
@section('content')
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
        <div class="widget widget-chart-one">
            <div class="widget-heading">
                <h5 class="">Top</h5>
            </div>

            <div class="widget-content">
                <div class="tabs tab-content">
                    <div id="content_1" class="tabcontent">
                        {{--<div class="row"><div class="col-md-6"><input id="rangeCalendarFlatpickr" class="form-control flatpickr flatpickr-input active" type="text" placeholder="Select Date.."> </div><div class="col-md-4"> <button type="button" name="filter" id="filter" class="btn btn-primary">Filter</button> <button type="button" name="refresh" id="refresh" class="btn btn-default">Refresh</button> </div></div></div>--}}
                        <div class="table-responsive mb-4 mt-4">
                            {{$dataTable->table(['class'=>"table table-hover non-hover","style"=>"width:100%"])}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{asset("plugins/flatpickr/flatpickr.js")}}"></script>
    <script src="{{asset('plugins/table/datatable/datatables.js')}}"></script>
    {{$dataTable->scripts()}}
    <script>
        /* var f3 = flatpickr(document.getElementById('rangeCalendarFlatpickr'), {
             mode: "range"
         });*/
    </script>
@endsection

