@extends('auth.layouts.master')
@section('css')
    <link rel="stylesheet" href="{{asset('plugins/select2/select2.min.css')}}">
    <style>
        .select2-container {
            z-index: 1059;
        }
    </style>
@endsection
@section('content')
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
        <div class="widget widget-chart-one">
            <div class="widget-heading"><h5 class="">General Settings</h5></div>
            <div class="widget-content">
                <div class="tabs tab-content">
                    <div id="content_1" class="tabcontent">
                        <form method="post" class="" enctype="multipart/form-data" action="{{route('settings.general')}}" novalidate="">
                            @csrf
                            <div class="form-row">
                                <div class="col-md-6 mb-6">
                                    <div class="form-group">
                                        <label for="siteTitle">Site Title *</label>
                                        <input id="siteTitle" type="text" value="{{setting('site_title')}}" name="site_title" placeholder="Site Title" class="form-control" required="">
                                    </div>
                                </div>
                                <div class="col-md-6 mb-6">
                                    <div class="form-group">
                                        <label for="copyright">CopyRight</label>
                                        <input type="text" id="copyright" value="{{setting('copyright')}}" name="copyright" class="form-control" placeholder="WinnerDevs">
                                    </div>
                                </div>
                                <div class="col-md-6 mb-6">
                                    <div class="form-group">
                                        <label for="phone">Phone *</label>
                                        <input id="phone" type="text" value="{{setting('phone')}}" name="phone" placeholder="+88015********" class="form-control" required="">
                                    </div>
                                    <div class="form-group">
                                        <label for="brief_info">Brief Info *
                                        <textarea required="" class="form-control" name="brief_info" id="brief_info" cols="45" rows="1">{{setting('brief_info')}}</textarea>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-6">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="delivery_charge">Delivery Charge *</label>
                                                <input id="delivery_charge" type="text" value="{{setting('delivery_charge')}}" name="delivery_charge" placeholder="20" class="form-control" required="">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="laundry_charge">Laundry Charge *</label>
                                                <input id="laundry_charge" type="text" value="{{setting('laundry_charge')}}" name="laundry_charge" placeholder="20" class="form-control" required="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="timeZone">Time Zone * </label>
                                        <select name="timeZone" id="timeZone" class="form-control">
                                            <option value="">Select Timezone</option>
                                            @foreach(timezone_list() as $timezone)
                                                @if(setting('timeZone')==$timezone['zone'])
                                                    <option selected value="{{$timezone['zone']}}">{{$timezone['diff_from_GMT']}}</option>
                                                @else
                                                    <option value="{{$timezone['zone']}}">{{$timezone['diff_from_GMT']}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-6">
                                    <div class="form-group">
                                        <label for="siteLogo">Site Logo</label>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input form-control" id="siteLogo" name="site_logo">
                                            <label class="custom-file-label" for="siteLogo">Choose file</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-6">
                                    <div class="form-group">
                                        <label for="favicon">Favicon</label>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input form-control" id="favicon" name="favicon">
                                            <label class="custom-file-label" for="favicon">Choose file</label>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <button class="btn btn-primary submit-fn mt-2" type="submit">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="{{asset('plugins/select2/select2.min.js')}}"></script>
    <script>
        window.addEventListener('load', function () {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms =  document.getElementsByTagName('form')
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function (form) {
                form.addEventListener('submit', function (event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    </script>
    <script>
        $("#timeZone").select2({placeholder:"Select Timezone"});
    </script>
@endsection
