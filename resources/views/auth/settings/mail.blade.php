@extends('auth.layouts.master')
@section('css')
    <link rel="stylesheet" href="{{asset('plugins/select2/select2.min.css')}}">
    <style>
        .select2-container {
            z-index: 1059;
        }
    </style>
@endsection

@section('content')

    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
        <div class="widget widget-chart-one">
            <div class="widget-heading">
                <h5 class="">Mail Settings</h5>
            </div>

            <div class="widget-content">
                <div class="tabs tab-content">
                    <div id="content_1" class="tabcontent">
                        <form class="simple-example" action="{{route('settings.mail')}}" method="post" novalidate="">
                            @csrf
                            <div class="form-row">
                                <div class="col-md-6 mb-6">
                                    <div class="form-group">

                                        <label for="mailHost">Mail Host * *</label>
                                        <input value="{{env('MAIL_HOST')}}" id="mailHost" type="text" name="mailHost" placeholder="smtp.gmail.com" class="form-control" required="">
                                    </div>
                                </div>
                                <div class="col-md-6 mb-6">
                                    <div class="form-group">
                                        <label for="mailPort">Mail Port *</label>
                                        <input value="{{env('MAIL_PORT')}}" type="text" id="mailPort" name="mailPort" class="form-control" placeholder="465">
                                    </div>
                                </div>
                                <div class="col-md-6 mb-6">
                                    <div class="form-group">
                                        <label for="mailAddress">Mail Address *</label>
                                        <input value="{{env('MAIL_FROM_ADDRESS')}}" type="email" id="mailAddress" name="mailAddress" class="form-control" placeholder="yourname@gmail.com">
                                    </div>
                                </div>
                                <div class="col-md-6 mb-6">
                                    <div class="form-group">
                                        <label for="mailPassword">Mail Password *</label>
                                        <input value="{{env('MAIL_PASSWORD')}}" type="password" id="mailPassword" name="mailPassword" class="form-control" placeholder="********">
                                    </div>
                                </div>
                                <div class="col-md-6 mb-6">
                                    <div class="form-group">
                                        <label for="fromName">Mail From Name *</label>
                                        <input value="{{env('MAIL_FROM_NAME')}}" type="text" id="fromName" name="fromName" class="form-control" placeholder="BellBoy">
                                    </div>
                                </div>
                                <div class="col-md-6 mb-6">
                                    <div class="form-group">
                                        <label for="encryption">Encryption *</label>
                                        <input value="{{env('MAIL_ENCRYPTION')}}" type="text" id="encryption" name="encryption" class="form-control" placeholder="SSL">
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-primary submit-fn mt-2" type="submit">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
