@extends('auth.layouts.master')
@section('css')
    <link rel="stylesheet" href="{{asset("plugins/select2/select2.min.css")}}">
@endsection
@section('content')
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
        <div class="widget widget-chart-one">
            <div class="widget-heading">
                <h5 class="">SMS Settings</h5>
            </div>
            <div class="widget-content">
                <div class="tabs tab-content">
                    <div id="content_1" class="tabcontent">
                        <form method="post" class="simple-example" action="{{route('settings.sms')}}" novalidate="">
                            @csrf
                            <div class="form-row">
                                <div class="col-md-6 mb-6">
                                    <div class="form-group">
                                        <label for="url">HTTP API URL *</label>
                                        <input value="{{$sms_config->url}}" id="url" type="url" name="url" placeholder="http://domain.com/message/api/sms" class="form-control" required="">
                                    </div>
                                </div>

                                <div class="col-md-6 mb-6">
                                    <div class="form-group">
                                        <label for="method">HTTP API Method * </label>
                                        <select name="method" id="method" class="form-control">
                                            <option value="">Select Method</option>
                                            <option @if($sms_config->method=="GET") selected @endif value="GET">GET</option>
                                            <option @if($sms_config->method=="POST") selected @endif value="POST">POST</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-6">
                                    <div class="form-group">
                                        <label for="send_to_param_name">Send To Param Name</label>
                                        <input value="{{$sms_config->params->send_to_param_name}}" type="text" id="send_to_param_name" name="send_to_param_name" class="form-control" placeholder="to">
                                    </div>
                                </div>

                                <div class="col-md-6 mb-6">
                                    <div class="form-group">
                                        <label for="msg_param_name">Message Param Name</label>
                                        <input value="{{$sms_config->params->msg_param_name}}" type="text" id="msg_param_name" name="msg_param_name" class="form-control" placeholder="msg">
                                    </div>
                                </div>
                                <div class="dropdown-divider"></div>

                                <div class="col-md-6">
                                    <h6>Headers</h6>
                                </div>
                                <div class="col-md-6 text-right">
                                    <a href="javascript:void(0);" class="btn btn-success add-header">Add</a>
                                </div>
                            </div>
                            <div class="header-content">
                                @if(isset($sms_config->headers)&&is_array($sms_config->headers)&&sizeof($sms_config->headers)>0)
                                    @foreach($sms_config->headers as $header_value)
                                        <div class="row">
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label for="header-name">Header Name</label>
                                                    <input type="text" value="{{array_key_first((array)$header_value)}}" id="header-name" name="header_name[]" class="form-control" placeholder="header">
                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label for="header-value">Header value</label>

                                                    <input value="{{implode("",array_values((array)$header_value))}}" type="text" id="header-value" name="header_value[]" class="form-control" placeholder="header value">
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for="header-name" style="visibility: hidden;">Remove</label>
                                                    <a href="javascript:void(0);" class="btn btn-danger remove-header">Remove</a>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                           <div class="row mt-5">
                               <div class="col-md-6">
                                   <h6>Other Option</h6>
                               </div>
                               <div class="col-md-6 text-right">
                                   <a href="javascript:void(0);" class="btn btn-success add-option">Add</a>
                               </div>
                           </div>
                            <div class="option-content">
                                @if(isset($sms_config->params->others)&&is_array($sms_config->params->others)&&sizeof($sms_config->params->others)>0)
                                    @foreach($sms_config->params->others as $option)
                                        <div class="row">
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label for="option-name">Option Name</label>
                                            <input value="{{array_key_first((array)$option)}}" type="text" id="option-name" name="option_name[]" class="form-control" placeholder="option">
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label for="option-value">Option value</label>
                                            <input value="{{implode("",array_values((array)$option))}}" type="text" id="option-value" name="option_value[]" class="form-control" placeholder="option value">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="option-name" style="visibility: hidden;">Remove</label>
                                            <a href="javascript:void(0);" class="btn btn-danger remove-option">Remove</a>
                                        </div>
                                    </div>
                                </div>
                                    @endforeach
                                @endif
                            </div>
                            <button class="btn btn-primary submit-fn mt-2" type="submit">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="{{asset('plugins/select2/select2.min.js')}}"></script>
    <script>
        $("#method").select2();
        $(".add-header").click(function(){
            const header_row =`<div class="row"> <div class="col-md-5"> <div class="form-group"> <label for="header-name">Header Name</label> <input type="text" id="header-name" name="header_name[]" class="form-control" placeholder="header"> </div> </div> <div class="col-md-5"> <div class="form-group"> <label for="header-value">Header value</label> <input type="text" id="header-value" name="header_value[]" class="form-control" placeholder="header value"> </div> </div> <div class="col-md-2"> <div class="form-group"> <label for="header-name" style="visibility: hidden;">Remove</label> <a href="javascript:void(0);" class="btn btn-danger remove-header">Remove</a> </div> </div> </div>`;
            $(".header-content").append(header_row);
        });
        $(".header-content").on("click",".remove-header", function(){
            $(this).parent().parent().parent().remove();
        });
        $(".option-content").on("click",".remove-option", function(){
            $(this).parent().parent().parent().remove();
        });
        $(".add-option").click(function(){
            const option_row = `<div class="row"> <div class="col-md-5"> <div class="form-group"> <label for="option-name">Option Name</label> <input type="text" id="option-name" name="option_name[]" class="form-control" placeholder="option"> </div> </div> <div class="col-md-5"> <div class="form-group"> <label for="option-value">Option value</label> <input type="text" id="option-value" name="option_value[]" class="form-control" placeholder="option value"> </div> </div> <div class="col-md-2"> <div class="form-group"> <label for="option-name" style="visibility: hidden;">Remove</label> <a href="javascript:void(0);" class="btn btn-danger remove-option">Remove</a> </div> </div> </div>`;
            $(".option-content").append(option_row);
        });
    </script>
@endsection
