@extends('auth.layouts.master')
@section('css')
    <link rel="stylesheet" href="{{asset('plugins/select2/select2.min.css')}}">
@endsection
@section('js')
    <script src="{{asset('plugins/select2/select2.min.js')}}"></script>
    <script !src="">
        $("#user_type").select2();

    </script>
@endsection
@section('content')
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
        <div class="widget widget-chart-one">
            <div class="widget-heading">
                <h5 class="">User Details</h5>
            </div>
            <div class="widget-content">
                <div class="tabs tab-content">
                    <div id="content_1" class="tabcontent">
                        <form class="simple-example" action="{{route('users.edit',['user_id'=>$user->id,'user_type'=>$user_type])}}" method="post" enctype="multipart/form-data" novalidate="">
                            @csrf
                            <div class="form-row">
                                <div class="col-md-6 mb-6">
                                    <div class="form-group">
                                        <label for="name">Name *</label>
                                        <input value="{{$user->name}}" id="name" type="text" name="name" placeholder="Name" class="form-control" required="">
                                    </div>
                                </div>

                                <div class="col-md-6 mb-6">
                                    <div class="form-group">
                                        <label for="email">E-Mail *</label>
                                        <input value="{{$user->email}}" id="email" type="email" name="email" placeholder="name@domain.com" class="form-control" required="">
                                    </div>
                                </div>
                                <div class="col-md-6 mb-6">
                                    <div class="form-group">
                                        <label for="password">Password *</label>
                                        <input id="password" type="password" name="password" placeholder="******" class="form-control" required="">
                                    </div>
                                </div>
                                <div class="col-md-6 mb-6">
                                    <div class="form-group">
                                        <label for="phone">Phone No </label>
                                        <input value="{{$user->phone}}" id="phone" type="text" name="phone" placeholder="880182121212" class="form-control" required="">
                                    </div>
                                </div>
                                <div class="col-md-6 mb-6">
                                    <div class="form-group">
                                        <label for="user_type">User Type * </label>
                                        <select id="user_type" name="user_type" class="form-control">
                                            <option @if($user_type==config('const.users.admin')) selected @endif value="{{config('const.users.admin')}}">Admin</option>
                                            <option @if($user_type==config('const.users.manager')) selected @endif value="{{config('const.users.manager')}}">Manager</option>
                                            <option @if($user_type==config('const.users.callcenter')) selected @endif value="{{config('const.users.callcenter')}}">Call Center</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6 mb-6">
                                    <div class="form-group">
                                        <label for="profile_picture">Profile Picture</label>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input form-control" id="profile_picture" name="profile_picture">
                                            <label class="custom-file-label" for="profile_picture">Choose file</label>
                                        </div>
                                    </div>
                                    <img height="120" src="{{get_media_file($user,config('const.media.profile_picture'))}}" alt="Avatar">
                                </div>
                            </div>
                            <button class="btn btn-primary submit-fn mt-2" type="submit">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
