@extends('auth.layouts.master')

@section('content')
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
        <div class="widget-content">

            <div class="widget-content widget-content-area br-6">
                <div class="widget-heading">
                    <h5 class="">User List</h5>
                </div>
                <div class="table-responsive mb-4 mt-4">
                    <table id="html5-extension" class="table table-hover non-hover" style="width:100%">
                        <thead>
                        <tr>
                            <th>Image</th>
                            <th>Name</th>
                            <th>E-Mail</th>
                            <th>Phone</th>
                            <th>User Type</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $type=>$user_model)
                            @foreach($user_model as $model)
                                <tr class="{{$type}}-{{$model->id}}">
                                    <td>
                                        <div class="d-flex">
                                            <div class="usr-img-frame mr-2 rounded-circle">
                                                <img alt="avatar" class="img-fluid rounded-circle"
                                                     src="{{get_media_file($model,config('const.media.profile_picture'))}}">
                                            </div>
                                        </div>
                                    </td>
                                    <td>{{$model->name}}</td>
                                    <td>{{$model->email}}</td>
                                    <td>{{$model->phone}}</td>
                                    <td>{{ucfirst($type)}}</td>
                                    <td>
                                        <label class="switch s-icons s-outline  s-outline-success  mb-4 mr-2">
                                            @if($model->status==config('const.status.active'))
                                                <input type="checkbox" checked="">
                                            @else
                                                <input type="checkbox">
                                            @endif
                                            <span class="slider"></span>
                                        </label>
                                    </td>
                                    <td>

                                        <button class="btn btn-dark btn-sm dropdown-toggle" type="button"
                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Action
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                 viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                                 stroke-linecap="round" stroke-linejoin="round"
                                                 class="feather feather-chevron-down">
                                                <polyline points="6 9 12 15 18 9"></polyline>
                                            </svg>
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuReference1">
                                            <a class="dropdown-item"
                                               href="{{route('users.edit',['user_id'=>$model->id,'user_type'=>$type])}}">Edit</a>
                                            <a class="dropdown-item warning confirm"
                                               onclick="delete_user('{{$model->id}}','{{$type}}')"
                                               href="javascript:void(0);">Delete</a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('css')
    <link rel="stylesheet" href="{{asset('plugins/editors/markdown/simplemde.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('plugins/table/datatable/datatables.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset("plugins/table/datatable/custom_dt_html5.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("plugins/table/datatable/dt-global_style.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("assets/css/forms/switches.css")}}">
@endsection
@section('js')
    <script src="{{asset('plugins/table/datatable/datatables.js')}}"></script>
    <script>
        $('#html5-extension').DataTable({
            "columns": [
                {"orderable": false},
                null,
                null,
                null,
                null,
                null,
                {"orderable": false}
            ],
            "order": [[1, "asc"]],
            "oLanguage": {
                "oPaginate": {
                    "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                    "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
                },
                "sInfo": "Showing page _PAGE_ of _PAGES_",
                "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                "sSearchPlaceholder": "Search...",
                "sLengthMenu": "Results :  _MENU_",
            },
            "stripeClasses": [],
            "lengthMenu": [7, 10, 20, 50],
            "pageLength": 7
        });

        function delete_user(user_id, user_type) {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success btn-rounded',
                    cancelButton: 'btn btn-danger btn-rounded mr-3',
                },
                buttonsStyling: false
            });

            swalWithBootstrapButtons.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    blockUI("#html5-extension");
                    axios.delete(`{{route('users.delete')}}?user_id=${user_id}&user_type=${user_type}`)
                        .then(function (response) {
                            $(`.${user_type}-${user_id}`).fadeIn(3000).delay(1000).fadeOut("slow",function(){
                                unBlockUI("#html5-extension");
                                $("#html5-extension").DataTable().reload();
                            });

                        })
                        .catch(function (error) {
                            showError(error.response.data.message);
                            unBlockUI("#html5-extension");
                        });
                } else if (
                    /* Read more about handling dismissals below */
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire('Cancelled', 'User delete cancelled', 'error');
                }
            })
        }
    </script>
@endsection
