<table>
    <thead>
    <tr>
        <th>Code</th>
        <th>SKU</th>
        <th>Name</th>
        <th>Brand</th>
        <th>Categories</th>
        <th>Unit</th>
        <th>Stock</th>
        <th>Cost</th>
        <th>Price</th>
    </tr>
    </thead>
    <tbody>
    @foreach($product_variants as $variation)
        @php
        $negotiate_price = (array)$variation->agentActiveOffers->pluck('negotiate_price');
        $cost = array_values($negotiate_price)[0];
        $cost[] = $variation->cost;
        $product_costs  =implode(", ", array_unique($cost));
        @endphp
        <tr>
            <td>{{ $variation->product->product_code }}</td>
            <td>{{ $variation->sku }}</td>
            <td>{{ $variation->variant_name }}</td>
            @if($variation->product->brand)
                <td>{{ $variation->product->brand->name }}</td>
            @else
                <td></td>
            @endif
            <td>{{$variation->product->categories->pluck('name')->implode(', ')}}</td>
            <td>{{$variation->unit_value." ".$variation->unit->unit_name }}</td>
            <td>{{ $variation->on_hand }}</td>
            <td>{{ $product_costs }}</td>
            <td>{{ $variation->price }}</td>
        </tr>
    @endforeach
    </tbody>
</table>

