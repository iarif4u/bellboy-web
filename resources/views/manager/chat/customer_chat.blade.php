@extends('manager.layout.master')
@section('css')
    <link rel="stylesheet" href="{{asset('plugins/perfect-scrollbar/perfect-scrollbar.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/apps/mailing-chat.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/apps/mailbox.css')}}">
@endsection
@section('js')
    <script src="{{asset('plugins/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
    <script src="{{asset('assets/js/apps/mailbox-chat.js')}}"></script>
@endsection
@section('content')
    <div class="col-xl-12 col-lg-12 col-md-12">

        <div class="chat-system">
            <div class="hamburger">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                     stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                     class="feather feather-menu mail-menu d-lg-none">
                    <line x1="3" y1="12" x2="21" y2="12"></line>
                    <line x1="3" y1="6" x2="21" y2="6"></line>
                    <line x1="3" y1="18" x2="21" y2="18"></line>
                </svg>
            </div>
            <div class="user-list-box">
                <div class="search">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                         stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                         class="feather feather-search">
                        <circle cx="11" cy="11" r="8"></circle>
                        <line x1="21" y1="21" x2="16.65" y2="16.65"></line>
                    </svg>
                    <input type="text" class="form-control" placeholder="Search"/>
                </div>
                <div class="people">
                    @if($customers->count()>0)
                        @foreach($customers as $customer)
                            <div class="person" data-chat="{{$customer->conversation->channel_id}}">
                                <div class="user-info">
                                    <div class="f-head">
                                        <img src="{{asset('assets/img/logo.png')}}" alt="avatar">
                                    </div>
                                    <div class="f-body">
                                        <div class="meta-info">
                                            @if($customer->name)
                                            <span class="user-name" data-name="{{$customer->phone}}">{{$customer->name}}</span>
                                            @else
                                                <span class="user-name" data-name="{{$customer->phone}}">{{$customer->phone}}</span>
                                            @endif
                                        </div>
                                        <span class="preview">{{$customer->phone}}</span>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
            <div class="chat-box">

                <div class="chat-not-selected">
                    <p>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                             class="feather feather-message-square">
                            <path d="M21 15a2 2 0 0 1-2 2H7l-4 4V5a2 2 0 0 1 2-2h14a2 2 0 0 1 2 2z"></path>
                        </svg>
                        Click User To Chat
                    </p>
                </div>
                <div class="chat-box-inner">
                    <div class="chat-meta-user">
                        <div class="current-chat-user-name">
                            <span>
                                <img src="assets/img/90x90.jpg" alt="dynamic-image">
                                <span class="name"></span>
                            </span>
                        </div>
                    </div>
                    <div class="chat-conversation-box">
                        <div id="chat-conversation-box-scroll" class="chat-conversation-box-scroll">
                            @foreach($customers as $customer)
                                <div id="mychat" class="chat channel-{{$customer->conversation->channel_id}}" data-chat="{{$customer->conversation->channel_id}}">
                                    <div class="conversation-start">
                                        <span>{{humanDateTime($customer->conversation->created_at)}}</span>>
                                    </div>
                                    @foreach($customer->conversation->messageList as $message)
                                        @if($message->sender_type==\App\Models\Customer::class)
                                            <div class="bubble you {{$message->id}}">
                                                {{$message->message}}
                                            </div>
                                        @else
                                            <div class="bubble me {{$message->id}}">
                                                {{$message->message}}
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="chat-footer">
                        <div class="chat-input">
                            <form class="chat-form" action="javascript:void(0);">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-message-square">
                                    <path d="M21 15a2 2 0 0 1-2 2H7l-4 4V5a2 2 0 0 1 2-2h14a2 2 0 0 1 2 2z"></path>
                                </svg>
                                <input type="text" class="mail-write-box form-control" placeholder="Message"/>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
