<div class="sidebar-wrapper sidebar-theme">

    <nav id="sidebar">
        <div class="shadow-bottom"></div>
        <ul class="list-unstyled menu-categories" id="accordionExample">
            <li class="menu">
                <a href="{{route('home')}}" data-active="{{activeMenu('home','true','false')}}" class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                             fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                             stroke-linejoin="round" class="feather feather-home">
                            <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                            <polyline points="9 22 9 12 15 12 15 22"></polyline>
                        </svg>
                        <span>Dashboard</span>
                    </div>
                </a>
            </li>
            <li class="menu">
                <a href="#chat" data-toggle="collapse"
                   data-active="{{areActiveRoutes(['manager.chat.deliveryboy','manager.chat.customer'],'true','false')}}" aria-expanded="false"
                   class="dropdown-toggle collapsed">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                             class="feather feather-message-square">
                            <path d="M21 15a2 2 0 0 1-2 2H7l-4 4V5a2 2 0 0 1 2-2h14a2 2 0 0 1 2 2z"></path>
                        </svg>
                        <span>Chat</span>
                    </div>
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                             class="feather feather-chevron-right">
                            <polyline points="9 18 15 12 9 6"></polyline>
                        </svg>
                    </div>
                </a>
                <ul class="collapse submenu list-unstyled {{areActiveRoutes(['manager.chat.customer','manager.chat.deliveryboy',],'show')}}"
                    id="chat" data-parent="#accordionExample">
                    <li class="{{activeMenu('manager.chat.customer')}}">
                        <a href="{{route('manager.chat.customer')}}">Customer</a>
                    </li>
                    <li class="{{activeMenu('manager.chat.deliveryboy')}}">
                        <a href="{{route('manager.chat.deliveryboy')}}"> Delivery Boy </a>
                    </li>

                </ul>
            </li>

            <li class="menu">
                <a href="#order" data-toggle="collapse"
                   data-active="{{areActiveRoutes(['manager.order.delivered','manager.order.canceled','manager.order.approved','manager.order.pending'],'true','false')}}" aria-expanded="false"
                   class="dropdown-toggle collapsed">
                    <div class="">
                        <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2"
                             fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1">
                            <circle cx="9" cy="21" r="1"></circle>
                            <circle cx="20" cy="21" r="1"></circle>
                            <path d="M1 1h4l2.68 13.39a2 2 0 0 0 2 1.61h9.72a2 2 0 0 0 2-1.61L23 6H6"></path>
                        </svg>
                        <span>Order</span>
                    </div>
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                             class="feather feather-chevron-right">
                            <polyline points="9 18 15 12 9 6"></polyline>
                        </svg>
                    </div>
                </a>
                <ul class="collapse submenu list-unstyled {{areActiveRoutes(['manager.order.canceled','manager.order.delivered','manager.order.approved','manager.order.pending'],'show')}}"
                    id="order" data-parent="#accordionExample">
                    <li class="{{activeMenu('manager.order.pending')}}">

                        <a href="{{route('manager.order.pending')}}">Order List</a>
                    </li>
                    <li class="{{activeMenu('manager.order.approved')}}">
                        <a href="{{route('manager.order.approved')}}"> Assign Order </a>
                    </li>
                    <li class="{{activeMenu('manager.order.delivered')}}">
                        <a href="{{route('manager.order.delivered')}}"> Delivered Order </a>
                    </li>
                    <li class="{{activeMenu('manager.order.canceled')}}">
                        <a href="{{route('manager.order.canceled')}}">Cancel Order </a>
                    </li>
                </ul>
            </li>
        </ul>
        <!-- <div class="shadow-bottom"></div> -->

    </nav>

</div>
