<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <title>@yield('title',setting(config('const.settings.site_title'),'Bellboy'))</title>
    <link rel="icon" type="image/x-icon" href="{{setting(config('const.settings.favicon'),asset('assets/img/logo.png'))}}"/>
    <link href="{{asset("assets/css/loader.css")}}" rel="stylesheet" type="text/css"/>
    <script src="{{asset("assets/js/loader.js")}}"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    {{--<link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700" rel="stylesheet">--}}
    <link href="{{asset("bootstrap/css/bootstrap.min.css")}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset("assets/css/plugins.css")}}" rel="stylesheet" type="text/css"/>
    <!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM STYLES -->
    <link href="{{asset("plugins/apex/apexcharts.css")}}" rel="stylesheet" type="text/css">
    <link href="{{asset("assets/css/dashboard/dash_1.css")}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset("assets/css/main.css")}}" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="{{ config('sweetalert.animatecss') }}">
    <!-- END PAGE LEVEL PLUGINS/CUSTOM STYLES -->
    @yield('css')
</head>
<body>
<!-- BEGIN LOADER -->
@include('manager.layout.includes.pre_loader')
<!--  END LOADER -->

<!--  BEGIN NAVBAR  -->
@include('manager.layout.includes.navbar')
<!--  END NAVBAR  -->

<!--  BEGIN MAIN CONTAINER  -->
<div class="main-container" id="container">

    <div class="overlay"></div>
    <div class="search-overlay"></div>

    <!--  BEGIN SIDEBAR  -->
@include('manager.layout.includes.sidebar')
<!--  END SIDEBAR  -->

    <!--  BEGIN CONTENT AREA  -->
    <div id="content" class="main-content">
        <div class="layout-px-spacing">
            <div class="row layout-top-spacing">
                @yield('content')
            </div>
        </div>
        <div class="footer-wrapper">
            <div class="footer-section f-section-1">
                <p class="">Copyright © 2020 {{setting(config('const.settings.copyright'))}}, All rights reserved.</p>
            </div>
            <div class="footer-section f-section-2">
                <p class="">Coded with
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                         stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                         class="feather feather-heart">
                        <path
                            d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z"></path>
                    </svg>
                </p>
            </div>
        </div>
    </div>
    <!--  END CONTENT AREA  -->

</div>
<div style="display: none" id="token"></div>
<div style="display: none" id="messages"></div>
<!-- END MAIN CONTAINER -->
<!--  BEGIN MODAL CONTAINER  -->
@yield('modal')
<!--  END MODAL CONTAINER  -->
<!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
<script src="{{asset("assets/js/libs/jquery-3.1.1.min.js")}}"></script>
<script src="{{asset("bootstrap/js/popper.min.js")}}"></script>
<script src="{{asset("bootstrap/js/bootstrap.min.js")}}"></script>
<script src="{{asset("plugins/perfect-scrollbar/perfect-scrollbar.min.js")}}"></script>
<script src="{{asset("assets/js/app.js")}}"></script>
<script src="{{asset("plugins/blockui/jquery.blockUI.min.js")}}"></script>
<script src="{{asset("assets/js/scrollspyNav.js")}}"></script>
<!-- SWEETALERT SCRIPTS -->
<script src="{{ asset('vendor/sweetalert/sweetalert.all.js')  }}"></script>
@include('sweetalert::alert')
<!-- AXIOS SCRIPTS -->
<script src="{{asset("plugins/axios/dist/axios.js")}}"></script>
<script>
    window.addEventListener('load', function () {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function (form) {
            form.addEventListener('submit', function (event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);
</script>
{{--Add Pusher Script CDN--}}
<script src="https://js.pusher.com/5.1/pusher.min.js"></script>
<script>
    $(document).ready(function () {
        App.init();

        // Enable pusher logging - don't include this in production
        Pusher.logToConsole = false;

        var pusher = new Pusher('{{env('PUSHER_APP_KEY')}}', {
            cluster: 'ap1',
            authEndpoint: '{{url('/broadcast')}}',
            forceTLS: true,
            auth: {
                headers: {
                    'X-CSRF-Token': "{{csrf_token()}}"
                }
            }
        });

        var channel = pusher.subscribe('private-message');
        channel.bind('App\\Events\\MessageEvent', function (data) {
            let message_row = `<div class="bubble you">${data.message}</div>`
            $(`.channel-${data.conversion}`).append(message_row);
            const getScrollContainer = document.querySelector('.chat-conversation-box');
            getScrollContainer.scrollTop = getScrollContainer.scrollHeight;
        });
    });
</script>
<script src="{{asset("assets/js/custom.js")}}"></script>
<!-- END GLOBAL MANDATORY SCRIPTS -->


<!-- The core Firebase JS SDK is always required and must be listed first -->
<script src="https://www.gstatic.com/firebasejs/7.8.2/firebase-app.js"></script>

<!-- TODO: Add SDKs for Firebase products that you want to use
     https://firebase.google.com/docs/web/setup#available-libraries -->
<script src="https://www.gstatic.com/firebasejs/7.8.2/firebase-analytics.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.8.2/firebase-messaging.js"></script>

<script>
    // Your web app's Firebase configuration
    var firebaseConfig = {
        apiKey: "AIzaSyAlqMTtwjWITzyEooQxHEqhPHTvHdqXEwo",
        authDomain: "bellboy-winnerdevs.firebaseapp.com",
        databaseURL: "https://bellboy-winnerdevs.firebaseio.com",
        projectId: "bellboy-winnerdevs",
        storageBucket: "bellboy-winnerdevs.appspot.com",
        messagingSenderId: "1075212422904",
        appId: "1:1075212422904:web:ee6118cbba5ef71933350d",
        measurementId: "G-NM5C7PVYQK"
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
    firebase.analytics();
    const messaging = firebase.messaging();


    // [END get_messaging_object]
    // [START set_public_vapid_key]
    // Add the public key generated from the console here.
    messaging.usePublicVapidKey("BKeH5UfIZBJB5jPEwgfL0SUq0n_C6HQlj3VnoRUSyXcC6O_qefaPAvFNlzFxRJ13r9Lr-uhwoLGZkFRs04yNYo0");
    // [START refresh_token]
    // Callback fired if Instance ID token is updated.
    // [START refresh_token]
    // Callback fired if Instance ID token is updated.
    navigator.serviceWorker.register('{{url('/service-worker.js')}}')
        .then((registration) => {
            messaging.useServiceWorker(registration);
        });
    messaging.getToken().then((refreshedToken) => {
        axios.post(`{{route('users.firebase')}}`, {
            firebase_token: refreshedToken,
            _token: '{{csrf_token()}}'
        });
        console.log('Token refreshed.', refreshedToken);
        // Indicate that the new Instance ID token has not yet been sent to the
        // app server.

        setTokenSentToServer(false);
        // Send Instance ID token to app server.
        sendTokenToServer(refreshedToken);
        // [START_EXCLUDE]
        // Display new Instance ID token and clear UI of all previous messages.
        resetUI();
        // [END_EXCLUDE]
    }).catch((err) => {
        console.log('Unable to retrieve refreshed token ', err);
        showToken('Unable to retrieve refreshed token ', err);
    });

    // [END refresh_token]

    // [START receive_message]
    // Handle incoming messages. Called when:
    // - a message is received while the app has focus
    // - the user clicks on an app notification created by a service worker
    //   `messaging.setBackgroundMessageHandler` handler.
    messaging.onMessage((payload) => {
        console.log('Message received. ', payload);
        // [START_EXCLUDE]
        // Update the UI to include the received message.
        appendMessage(payload);
        // [END_EXCLUDE]
    });

    // [END receive_message]

    function resetUI() {
        clearMessages();
        showToken('loading...');
        // [START get_token]
        // Get Instance ID token. Initially this makes a network call, once retrieved
        // subsequent calls to getToken will return from cache.
        messaging.getToken().then((currentToken) => {
            if (currentToken) {
                sendTokenToServer(currentToken);
                updateUIForPushEnabled(currentToken);
            } else {
                // Show permission request.
                console.log('No Instance ID token available. Request permission to generate one.');
                // Show permission UI.
                updateUIForPushPermissionRequired();
                setTokenSentToServer(false);
            }
        }).catch((err) => {

            showToken('Error retrieving Instance ID token. ', err);
            setTokenSentToServer(false);
        });
        // [END get_token]
    }


    function showToken(currentToken) {
        // Show token in console and UI.
        const tokenElement = document.querySelector('#token');
        tokenElement.textContent = currentToken;
    }

    // Send the Instance ID token your application server, so that it can:
    // - send messages back to this app
    // - subscribe/unsubscribe the token from topics
    function sendTokenToServer(currentToken) {
        if (!isTokenSentToServer()) {
            // TODO(developer): Send the current token to your server.
            setTokenSentToServer(true);
        }

    }

    function isTokenSentToServer() {
        return window.localStorage.getItem('sentToServer') === '1';
    }

    function setTokenSentToServer(sent) {
        window.localStorage.setItem('sentToServer', sent ? '1' : '0');
    }

    function showHideDiv(divId, show) {
        const div = document.querySelector('#' + divId);
        if (show) {
            div.style = 'display: visible';
        } else {
            div.style = 'display: none';
        }
    }

    function requestPermission() {
        // [START request_permission]
        Notification.requestPermission().then((permission) => {
            if (permission === 'granted') {
                // TODO(developer): Retrieve an Instance ID token for use with FCM.
                // [START_EXCLUDE]
                // In many cases once an app has been granted notification permission,
                // it should update its UI reflecting this.
                resetUI();
                // [END_EXCLUDE]
            }
        });
        // [END request_permission]
    }

    function deleteToken() {
        // Delete Instance ID token.
        // [START delete_token]
        messaging.getToken().then((currentToken) => {
            messaging.deleteToken(currentToken).then(() => {
                console.log('Token deleted.');
                setTokenSentToServer(false);
                // [START_EXCLUDE]
                // Once token is deleted update UI.
                resetUI();
                // [END_EXCLUDE]
            }).catch((err) => {
                console.log('Unable to delete token. ', err);
            });
            // [END delete_token]
        }).catch((err) => {
            console.log('Error retrieving Instance ID token. ', err);
            showToken('Error retrieving Instance ID token. ', err);
        });

    }

    // Add a message to the messages element.
    function appendMessage(payload) {
        const messagesElement = document.querySelector('#messages');
        const dataHeaderELement = document.createElement('h5');
        const dataElement = document.createElement('pre');
        dataElement.style = 'overflow-x:hidden;';
        dataHeaderELement.textContent = 'Received message:';
        dataElement.textContent = JSON.stringify(payload, null, 2);
        messagesElement.appendChild(dataHeaderELement);
        messagesElement.appendChild(dataElement);
    }

    // Clear the messages element of all children.
    function clearMessages() {
        const messagesElement = document.querySelector('#messages');
        while (messagesElement.hasChildNodes()) {
            messagesElement.removeChild(messagesElement.lastChild);
        }
    }

    function updateUIForPushEnabled(currentToken) {
        showHideDiv(tokenDivId, true);
        showHideDiv(permissionDivId, false);
        showToken(currentToken);
    }

    function updateUIForPushPermissionRequired() {
        showHideDiv(tokenDivId, false);
        showHideDiv(permissionDivId, true);
    }

    resetUI();
    // [END refresh_token]
</script>
<script src="{{asset('assets/js/script.js')}}"></script>
@yield('js')
</body>
</html>
