<button class="btn btn-dark btn-sm dropdown-toggle" type="button"
        data-toggle="dropdown" aria-haspopup="true"
        aria-expanded="false">
    Action
    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
         viewBox="0 0 24 24" fill="none" stroke="currentColor"
         stroke-width="2"
         stroke-linecap="round" stroke-linejoin="round"
         class="feather feather-chevron-down">
        <polyline points="6 9 12 15 18 9"></polyline>
    </svg>
</button>
<div class="dropdown-menu" aria-labelledby="dropdownMenuReference1">
    <a class="dropdown-item" href="javascript:void(0);" onclick="assign_order('{{$order->id}}')" >Assign</a>
    <a class="dropdown-item" href="javascript:void(0);"
       data-toggle="modal" data-target=".updateOrder">Update</a>
    <a class="dropdown-item warning cancel" href="javascript:void(0);">Cancel</a>

</div>
