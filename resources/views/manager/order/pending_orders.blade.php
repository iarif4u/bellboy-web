@extends('manager.layout.master')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('plugins/table/datatable/datatables.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset("plugins/table/datatable/custom_dt_html5.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("plugins/table/datatable/dt-global_style.css")}}">
    <link href="{{asset('plugins/select2/select2.min.css')}}" rel="stylesheet" type="text/css"/>
@endsection
@section('content')

    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
        <div class="widget widget-chart-one">
            <div class="widget-heading">
                <h5 class="">Order List</h5>
                <ul class="tabs tab-pills">
                    <li>
                        <a href="AssignOrderList.php" class="btn btn-primary mb-2 mr-2">
                            Assign Order List
                        </a>
                    </li>
                    <li>
                        <a href="DeliveredList.php" class="btn btn-primary mb-2 mr-2">
                            Delivered List
                        </a>
                    </li>
                    <li>
                        <a href="CancleOrderList.php" class="btn btn-primary mb-2 mr-2">
                            Cancel List
                        </a>
                    </li>

                </ul>
            </div>

            <div class="widget-content">
                <div class="tabs tab-content">
                    <div id="content_1" class="tabcontent">
                        <div class="table-responsive mb-4 mt-4">
                            {{$dataTable->table(['class'=>"table table-hover non-hover","style"=>"width:100%"])}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <!-- BEGIN PAGE LEVEL CUSTOM SCRIPTS -->
    <script src="{{asset('plugins/table/datatable/datatables.js')}}"></script>
    <script src="{{asset('plugins/table/datatable/button-ext/buttons.html5.min.js')}}"></script>
    <script src="{{asset('plugins/table/datatable/button-ext/buttons.print.min.js')}}"></script>
    {{$dataTable->scripts()}}
    <script src="{{asset('plugins/select2/select2.min.js')}}"></script>

    <script>
        let search = true;

        $("#delivery_boy").select2({
            dropdownParent: $("#assignOrder"),
            placeholder: "Select Delivery Boy",
            ajax: {
                url: '{{route('manager.order.deliveryboy_search')}}',
                dataType: 'json',
                data: function (params) {
                    return {
                        search: params.term,
                        type: "public"
                    };
                },
                processResults: function (response) {
                    return {
                        results: $.map(response.data, function (txt, val) {
                            return {id: txt.id, text: txt.name}
                        })
                    };
                }
            }
        });

        $(".agent-list").select2({
            dropdownParent: $("#assignAgent"),
            placeholder: "Select Agent",
            ajax: {
                url: '{{route('manager.order.agent_search')}}',
                dataType: 'json',
                data: function (params) {
                    return {
                        search: params.term,
                        type: "public"
                    };
                },
                processResults: function (response) {
                    return {
                        results: $.map(response.data, function (txt, val) {
                            return {id: txt.id, text: txt.name}
                        })
                    };
                }
            }
        });


        function assign_order(order_id) {
            axios.post('{{route('manager.order.order_info')}}', {
                order_id: order_id,
                _token: '{{csrf_token()}}',
            })
                .then(function (res) {
                    $("#order_id").val(order_id);
                    update_order_dom(res.data);
                })
                .catch(function (err) {
                    console.log(err);
                }).then(() => {
                $(".assignOrder").modal("show");
            });
        }

        function update_order_dom(order) {
            let customer = order.customer;
            let items = order.items;
            const items_dom = items.map(item => {
                if (item.agent_id == null) {
                    return `<tr><td>${item.variation.variant_name}</td><td>${item.quantity}</td><td>${item.price}</td><td class="text-center"><a onClick="assign_agent('${item.id}')" href="javascript:void(0);" class="btn btn-dark mb-3 rounded bs-tooltip" title="Assign Agent">Assign Agent</a></td></tr>`;
                }
                return `<tr><td>${item.variation.variant_name}</td><td>${item.quantity}</td><td>${item.price}</td><td class="text-center"><a href="javascript:void(0);" class="btn btn-dark mb-3 rounded bs-tooltip" title="Address : ${item.agent.shop_name.data_value} | ${item.agent.shop_address.data_value} | Phone : ${item.agent.phone}">${item.agent.name}</a></td></tr>`;
            });
            $(".order-items").html(items_dom);
            $(".customer-name").text(customer.name);
            $(".customer-phone").text(customer.phone);
            $(".customer-address").text(customer.address);

        }

        function assign_agent(order_item_id) {
            $("#order-item-id").val(order_item_id);
            $("#assignAgent").modal("show");
        }
    </script>
@endsection
@section('modal')
    <div id="assignOrder" class="modal fade assignOrder" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="myLargeModalLabel">Assign Order</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                             viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                             stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                            <line x1="18" y1="6" x2="6" y2="18"></line>
                            <line x1="6" y1="6" x2="18" y2="18"></line>
                        </svg>
                    </button>
                </div>
                <form action="{{route('manager.order.delivery_boy_assign')}}" method="post">
                    @csrf
                    <input type="hidden" id="order_id" name="order_id">
                    <div class="modal-body">
                        <div id="tableHover" class="col-lg-12 col-12 layout-spacing">
                            <div class="statbox">
                                <div>
                                    <div class="form-group">
                                        <label for="delivery_boy">Select Delivery Boy *</label>
                                        <select name="delivery_boy" id="delivery_boy" class="form-control"
                                                data-live-search="true"></select>
                                    </div>
                                </div>
                                <div class="widget-header layout-spacing">
                                    <div class="row">
                                        <div class="col-md-12 mb-12 text-center layout-spacing">
                                            <h4>Order Details </h4>
                                        </div>
                                        <div class="col-md-6 mb-12">
                                            <p>Customer Name : <span class="customer-name"></span></p>
                                        </div>
                                        <div class="col-md-6 mb-12">
                                            <p>Address : <span class="customer-address"></span></p>
                                        </div>
                                        <div class="col-md-6 mb-12">
                                            <p>Phone No : <span class="customer-phone"></span></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="widget-content">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover mb-4">
                                            <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Amount</th>
                                                <th>Price</th>
                                                <th class="text-center">Agent</th>
                                            </tr>
                                            </thead>
                                            <tbody class="order-items">
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Close</button>
                        <button type="submit" class="btn btn-info">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="assignAgent" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="myLargeModalLabel">Assign Agent</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                             viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                             stroke-linecap="round" stroke-linejoin="round" class="feather feather-x">
                            <line x1="18" y1="6" x2="6" y2="18"></line>
                            <line x1="6" y1="6" x2="18" y2="18"></line>
                        </svg>
                    </button>
                </div>
                <form action="{{route('manager.order.agent_assign')}}" method="post">
                    @csrf
                    <input type="hidden" id="order-item-id" name="order_item">
                    <div class="modal-body">
                        <div id="tableHover" class="col-lg-12 col-12 layout-spacing">
                            <div class="statbox">
                                <div class="form-group">
                                    <label for="agent">Select Agent</label>
                                    <select name="agent" id="agent" class="form-control agent-list"></select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Close</button>
                        <button type="submit" class="btn btn-info">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
