@if (Session::has('alert.config'))
    <script>
        Swal.fire({!! Session::pull('alert.config') !!});
    </script>
@endif

@if ($errors->any())
    <script>
        // title: errorThrown,
        /*padding: '2em',
            type: 'error',
            animation: true,
            icon: "error"*/
        Swal.fire({
            "title": "Something went wrong",
            "type": "error",
            "icon": "error",
            animation: true,
            "html": "<p class='text-danger'>{!! implode('<br>',$errors->all())!!}</p>"
        });
    </script>
@endif
