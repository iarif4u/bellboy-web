<?php

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['api']], function () {
    /*Categories Manipulation*/
    Route::group(['prefix' => 'customer'], function () {
        Route::post('/brief', function () {
            return setting(config('const.settings.brief_info'));
        });
        Route::post('/settings', function () {
            return [
               'brief_info'=> setting(config('const.settings.brief_info')),
               'delivery_charge'=> setting(config('const.settings.delivery_charge')),
               'laundry_charge'=> setting(config('const.settings.laundry_charge')),
            ];
        });
        Route::post('/token', 'Api\CustomerController@saveAppsToken');
        Route::post('/otp/request', 'Api\CustomerController@requestForOtp');
        Route::post('/otp/login', 'Api\CustomerController@requestForOtpLogin');
        Route::post('/promocode', 'Api\CustomerController@promocodeResponse');
        Route::post('/variation/{variation_id}', 'Api\ProductController@getProductVariationDetails');
        Route::group(['middleware' => 'customer'], function () {
            Route::post('messages', 'Api\CustomerController@getCustomerMessages');
            Route::post('message/send', 'Api\CustomerController@sendCustomerMessage');
            Route::post('/order', 'Api\OrderController@makeCustomerOrder');
            Route::post('/orders', 'Api\OrderController@getCustomerOrders');
        });
    });
    /*Categories Manipulation*/
    Route::group(['prefix' => 'category'], function () {
        Route::post('/categories', 'Api\CategoryController@getParentCategories');
        Route::post('/products', 'Api\CategoryController@getPopularProducts');
        Route::post('/products/search', 'Api\CategoryController@getSearchProducts');
        Route::post('/discount/products', 'Api\CategoryController@getDiscountProducts');
        Route::post('/{category_id}', 'Api\CategoryController@getCategory');
        Route::post('/product/details/{category_id}', 'Api\ProductController@getCategoryProductsDetails');
        Route::post('/product/{category_id}', 'Api\ProductController@getCategoryProducts');

    });
    Route::group(['prefix' => 'product', 'middleware' => ['auth:agent', 'api']], function () {
        Route::post('/variation/{variation_id}', 'Api\ProductController@getProductVariationDetails');
        Route::post('/offer/', 'Api\ProductController@getProductOffer');
        Route::post('/request', 'Api\ProductController@getAgentRequestProducts');
        Route::post('/pending', 'Api\ProductController@getAgentPendingProducts');
        Route::post('/complete', 'Api\ProductController@getAgentCompleteProducts');
        Route::post('/pending/{item_order_id}', 'Api\ProductController@getAgentPendingProductDetails');
        Route::post('/order/pending', 'Api\ProductController@getAgentPendingOrders');
        Route::post('/order/pack', 'Api\ProductController@packAgentProduct');
    });
    Route::post('/firebase', 'Api\ApiController@setUpFirebaseToken');
});


Route::group(['middleware' => 'api', 'prefix' => 'agent'], function () {
    Route::post('login', 'Agent\Auth\AuthController@login');
    Route::post('logout', 'Agent\Auth\AuthController@logout');
    Route::post('refresh', 'Agent\Auth\AuthController@refresh');
    Route::post('me', 'Agent\Auth\AuthController@me');
    Route::post('me/balance/withdraw', 'Agent\Auth\AuthController@balanceWithdrawRequest');
    Route::post('messages', 'Agent\Auth\AuthController@messages');
    Route::post('message/send', 'Agent\Auth\AuthController@sendAgentMessage');
    Route::post('shop/image', 'Agent\Auth\AuthController@uploadShopImage');

});

Route::group(['middleware' => 'api', 'prefix' => 'deliveryboy'], function () {
    Route::post('login', 'Deliveryboy\Auth\AuthController@login');
    Route::post('logout', 'Deliveryboy\Auth\AuthController@logout');
    Route::post('refresh', 'Deliveryboy\Auth\AuthController@refresh');
    Route::post('me', 'Deliveryboy\Auth\AuthController@me');
    Route::post('/firebase', 'Deliveryboy\Auth\AuthController@setUpFirebaseToken');
    Route::post('messages', 'Deliveryboy\Auth\AuthController@messages');
    Route::post('messages/send', 'Deliveryboy\Auth\AuthController@sendDeliveryBoyMessage');


    Route::group(['prefix' => 'orders', 'middleware' => ['auth:deliveryboy']], function () {
        Route::post('customer', 'Deliveryboy\OrderController@updateCustomer');
        Route::post('customer/collect', 'Deliveryboy\OrderController@collectCustomerProduct');
        Route::post('customer/schedule', 'Deliveryboy\OrderController@scheduleCustomerProduct');
        Route::post('customer/delivery', 'Deliveryboy\OrderController@deliveryCustomerProduct');
        Route::post('/', 'Deliveryboy\OrderController@getDeliveryBoyOrders');
        Route::post('/product/collect', 'Deliveryboy\OrderController@collectProducts');
        Route::post('/complete', 'Deliveryboy\OrderController@completeOrder');
        Route::post('/delivery', 'Deliveryboy\OrderController@makeOrderToDelivery');
        Route::post('/agent', 'Deliveryboy\OrderController@makeOrderToAgent');
        Route::post('/item/status', 'Deliveryboy\OrderController@updateOrderItemStatus');
    });
});
