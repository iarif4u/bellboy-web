<?php

Route::get('/home', function () {
    $users[] = Auth::user();
    $users[] = Auth::guard()->user();
    $users[] = Auth::guard('callcenter')->user();

    //dd($users);

    return view('callcenter.home');
})->name('home');

