<?php

Route::get('/home', function () {
    $users[] = Auth::user();
    $users[] = Auth::guard()->user();
    $users[] = Auth::guard('deliveryboy')->user();

    //dd($users);

    return view('deliveryboy.home');
})->name('home');

