<?php

Route::get('/home', function () {
    $users[] = Auth::user();
    $users[] = Auth::guard()->user();
    $users[] = Auth::guard('manager')->user();

    //dd($users);

    return view('manager.home');
})->name('home');
//routes for users chat
Route::group(['prefix' => 'chat', 'as' => 'chat.'], function () {
    Route::get('/customer', 'Manager\ChatController@getCustomerChatView')->name('customer');
    Route::post('/customer', 'Manager\ChatController@sendMessageToCustomer')->name('customer');
    Route::get('/deliveryboy', 'Manager\ChatController@getDeliveryBoyChatView')->name('deliveryboy');
    Route::post('/deliveryboy', 'Manager\ChatController@sendMessageToDeliveryBoy')->name('deliveryboy');
});
Route::group(['prefix' => 'order', 'as' => 'order.'], function () {
    Route::get('/approved', 'Manager\OrderController@getApprovedOrders')->name('approved');
    Route::get('/delivered', 'Manager\OrderController@getDeliveredOrders')->name('delivered');
    Route::get('/canceled', 'Manager\OrderController@getCanceledOrders')->name('canceled');
    Route::get('/', 'Manager\OrderController@getPendingOrders')->name('pending');
    Route::post('/', 'Manager\OrderController@getOrderInfo')->name('order_info');
    Route::post('/assign/', 'Manager\OrderController@addDeliveryBoyToOrder')->name('delivery_boy_assign');
    Route::post('/assign/agent', 'Manager\OrderController@addAgentToOrderItem')->name('agent_assign');
    Route::get('/search/delivery/boy', 'Manager\OrderController@deliveryboy_search')->name('deliveryboy_search');
    Route::get('/search/agent', 'Manager\OrderController@agent_search')->name('agent_search');
});
