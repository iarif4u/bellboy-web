<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return redirect()->route('home');
});
Route::get('/privacy', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    Route::post('/unit/update','Admin\PropertyController@updateProductUnit')->name('update_unit');
    Route::delete('/unit/delete','Admin\PropertyController@deleteProductUnit')->name('delete_unit');
    Route::get('/home', 'Admin\HomeController@index')->name('home');

    //routes for users chat
    Route::group(['prefix' => 'chat', 'as' => 'chat.'], function () {
        Route::get('/agent', 'Admin\ChatController@getAgentChatView')->name('agent');
        Route::post('/agent', 'Admin\ChatController@sendMessageToAgent')->name('agent');
        Route::get('/customer', 'Admin\ChatController@getCustomerChatView')->name('customer');
        Route::post('/customer', 'Admin\ChatController@sendMessageToCustomer')->name('customer');
        Route::get('/deliveryboy', 'Admin\ChatController@getDeliveryBoyChatView')->name('deliveryboy');
        Route::post('/deliveryboy', 'Admin\ChatController@sendMessageToDeliveryBoy')->name('deliveryboy');

    });
    //routes for users manipulation
    Route::group(['prefix' => 'users', 'as' => 'users.'], function () {
        Route::post('/firebase', 'Admin\HomeController@setUpFirebaseToken')->name('firebase');
        Route::get('/add', 'Admin\HomeController@getAddUserView')->name('add');
        Route::post('/add', 'Admin\HomeController@addNewUser')->name('add');
        Route::get('/all', 'Admin\HomeController@getUserList')->name('all');
        Route::get('/{user_id}/edit/{user_type}', 'Admin\HomeController@getUserEditView')->name('edit');
        Route::post('/{user_id}/edit/{user_type}', 'Admin\HomeController@updateUserInfo')->name('edit');
        Route::delete('/user/delete', 'Admin\HomeController@deleteUser')->name('delete');
    });
    //routes for notification manipulation
    Route::group(['prefix' => 'notification', 'as' => 'notification.'], function () {
        Route::get('/send', 'Admin\NotificationController@getNotificationView')->name('send');
        Route::post('/send', 'Admin\NotificationController@sendNotification')->name('send');
    });

    //routes for settings manipulation
    Route::group(['prefix' => 'account', 'as' => 'account.'], function () {
        Route::get('/statement', 'Admin\AccountStatementController@statement')->name('statement');
        Route::get('/income', 'Admin\AccountStatementController@index')->name('income');
        Route::post('/income/info', 'Admin\AccountStatementController@getIncomeInfo')->name('income_info');
        Route::post('/income/update', 'Admin\AccountStatementController@updateIncomeInfo')->name('income_update');
        Route::delete('/income/delete', 'Admin\AccountStatementController@deleteIncomeInfo')->name('income_delete');
        Route::post('/income', 'Admin\AccountStatementController@makeNewIncome')->name('income');
        Route::group(['prefix' => 'expense', 'as' => 'expense.'], function () {
            Route::get('/', 'Admin\AccountStatementController@getExpenseListView')->name('all');
            Route::post('/add', 'Admin\AccountStatementController@addNewExpense')->name('add');
            Route::post('/info', 'Admin\AccountStatementController@getExpenseInfo')->name('info');
            Route::post('/update', 'Admin\AccountStatementController@updateExpenseInfo')->name('update');
            Route::delete('/delete', 'Admin\AccountStatementController@deleteExpenseInfo')->name('delete');
            Route::get('/categories', 'Admin\AccountStatementController@getExpenseCategoriesView')->name('categories');
            Route::post('/categories/add', 'Admin\AccountStatementController@addExpenseCategory')->name('add_category');
            Route::post('/categories/update', 'Admin\AccountStatementController@updateExpenseCategory')->name('update_category');
            Route::delete('/categories/delete', 'Admin\AccountStatementController@deleteExpenseCategory')->name('delete_category');
        });
    });

    //routes for settings manipulation
    Route::group(['prefix' => 'settings', 'as' => 'settings.'], function () {
        Route::get('/general', 'Admin\SettingsController@getGeneralSettingsView')->name('general');
        Route::get('/mail', 'Admin\SettingsController@getMailSettingsView')->name('mail');
        Route::post('/mail', 'Admin\SettingsController@updateMailSettings')->name('mail');
        Route::post('/general', 'Admin\SettingsController@updateGeneralSettingsView')->name('general');
        Route::get('/sms', 'Admin\SettingsController@getSMSSettingsView')->name('sms');
        Route::post('/sms', 'Admin\SettingsController@updateSMSSettings')->name('sms');
    });
    //routes for product manipulation
    Route::group(['prefix' => 'coupon', 'as' => 'coupon.'], function () {
        Route::get('/', 'Admin\CouponCodeController@couponCodes')->name('codes');
        Route::post('/', 'Admin\CouponCodeController@addNewCouponCode')->name('new_code');
        Route::post('/update', 'Admin\CouponCodeController@updateCouponCode')->name('update_code');
        Route::delete('/delete', 'Admin\CouponCodeController@deleteCouponCode')->name('delete_code');
        Route::post('/discount', 'Admin\CouponCodeController@getCouponDiscount')->name('discount');
    });
    //routes for product manipulation
    Route::group(['prefix' => 'product', 'as' => 'product.'], function () {
        Route::group(['prefix' => 'discount', 'as' => 'discount.'], function () {
            Route::get('/', 'Admin\DiscountController@showDiscountsView')->name('all');
            Route::get('/search', 'Admin\DiscountController@search')->name('search');
            Route::post('/new', 'Admin\DiscountController@makeNewDiscount')->name('new');
            Route::post('/update', 'Admin\DiscountController@updateDiscount')->name('update');
            Route::delete('/delete', 'Admin\DiscountController@deleteDiscount')->name('delete');
        });
        Route::get('/', 'Admin\ProductController@index')->name('all');
        Route::get('/deleted', 'Admin\ProductController@deleted')->name('deleted');
        Route::post('/restore', 'Admin\ProductController@restore')->name('restore');
        Route::get('/new', 'Admin\ProductController@create')->name('new');
        Route::post('/new', 'Admin\ProductController@store')->name('new');
        Route::post('/status', 'Admin\ProductController@updateProductStatus')->name('status');
        Route::post('/discount', 'Admin\ProductController@updateProductDiscount')->name('product_discount');
        Route::get('/{product_id}/update', 'Admin\ProductController@updateProductView')->name('update');
        Route::post('/{product_id}/update', 'Admin\ProductController@updateProduct')->name('update');
        Route::delete('/delete', 'Admin\ProductController@deleteProduct')->name('delete');
        Route::group(['prefix' => 'variation', 'as' => 'variation.'], function () {
            Route::get('/{product_id}/variations', 'Admin\ProductController@showProductVariation')->name('all');
            Route::get('/{product_id}/add', 'Admin\ProductController@showNewProductVariationView')->name('add');
            Route::get('/{variant_id}/edit', 'Admin\ProductController@showEditProductVariationView')->name('edit');
            Route::post('/{variant_id}/edit', 'Admin\ProductController@updateProductVariation')->name('edit');
            Route::delete('/delete', 'Admin\ProductController@deleteProductVariation')->name('delete');
            Route::post('/{product_id}/add', 'Admin\ProductController@addProductVariation')->name('add');
            Route::post('/option/value', 'Admin\ProductController@getVariationValues')->name('option_n_value');
            Route::get('/{variation_id}/agent/offer/{offer_id?}', 'Admin\ProductController@getVariationAgentOffer')->name('offers');
            Route::post('/agent/offer/approve', 'Admin\ProductController@approve_agent_offer')->name('offer_approve');
            Route::post('/agent/offer/deny', 'Admin\ProductController@deny_agent_offer')->name('offer_deny');
            Route::post('/agent/assign', 'Admin\ProductController@assignProductAgent')->name('assign_agent');
            Route::get('/', 'Admin\ProductController@getProductVariations')->name('variations');
            Route::post('/search', 'Admin\ProductController@search')->name('search');
            Route::post('/variation/info', 'Admin\ProductController@variation_info')->name('info');
            Route::get('/download', 'Admin\ProductController@downloadVariations')->name('download');
        });
        Route::get('/unit', 'Admin\PropertyController@showProductUnitsView')->name('unit');
        Route::post('/unit/add', 'Admin\PropertyController@addProductUnit')->name('add_unit');

        Route::group(['prefix' => 'option', 'as' => 'option.'], function () {
            Route::get('/', 'Admin\ProductOptionController@showProductOptionsView')->name('all');
            Route::get('/search', 'Admin\ProductOptionController@searchProductOption')->name('search');
            Route::get('/value/search', 'Admin\ProductOptionController@searchProductOptionValue')->name('value_search');
            Route::post('/new', 'Admin\ProductOptionController@makeNewProductOption')->name('new');
            Route::post('/update', 'Admin\ProductOptionController@updateProductOption')->name('update');
            Route::delete('/delete', 'Admin\ProductOptionController@deleteProductOption')->name('delete');
            Route::get('/options/datatable', 'Admin\ProductOptionController@getProductOptionsDataTable')->name('datatable_option');
            Route::post('/value/new', 'Admin\ProductOptionController@makeNewProductOptionValue')->name('new_value');
            Route::post('/value/update', 'Admin\ProductOptionController@updateProductOptionValue')->name('update_value');
            Route::delete('/value/delete', 'Admin\ProductOptionController@deleteProductOptionValue')->name('delete_value');
        });
        Route::group(['prefix' => 'property', 'as' => 'property.'], function () {
            Route::get('/', 'Admin\PropertyController@showProductPropertiesView')->name('all');
            Route::get('/search', 'Admin\PropertyController@search')->name('search');
            Route::post('/new', 'Admin\PropertyController@makeNewProperty')->name('new');
            Route::post('/update', 'Admin\PropertyController@updateProperty')->name('update');
            Route::delete('/delete', 'Admin\PropertyController@deleteProperty')->name('delete');
        });
    });
    //routes for brand manipulation
    Route::group(['prefix' => 'category', 'as' => 'category.'], function () {
        Route::get('/', 'Admin\CategoryController@showCategorytList')->name('all');
        Route::post('/new', 'Admin\CategoryController@makeNewCategory')->name('new');
        Route::post('/update', 'Admin\CategoryController@updateCategory')->name('update');
        Route::delete('/delete', 'Admin\CategoryController@deleteCategory')->name('delete');
        Route::get('/search', 'Admin\CategoryController@search')->name('search');
    });


    //routes for brand manipulation
    Route::group(['prefix' => 'brand', 'as' => 'brand.'], function () {
        Route::get('/', 'Admin\BrandController@showBrandtList')->name('all');
        Route::post('/new', 'Admin\BrandController@makeNewBrand')->name('new');
        Route::post('/update', 'Admin\BrandController@updateBrand')->name('update');
        Route::delete('/delete', 'Admin\BrandController@deleteBrand')->name('delete');
        Route::get('/search', 'Admin\BrandController@search')->name('search');
    });

    //routes for report manipulation
    Route::group(['prefix' => 'report', 'as' => 'report.'], function () {
        Route::get('/daily', 'Admin\ReportController@getDailyReport')->name('daily');
        Route::get('/monthly', 'Admin\ReportController@getMonthlyReport')->name('monthly');
        Route::get('/top/selling', 'Admin\ReportController@getTopSellingReport')->name('top_selling');
        Route::get('/stock', 'Admin\ReportController@getStockReport')->name('stock');
        Route::post('/stock/add', 'Admin\ReportController@addStock')->name('add_stock');
        Route::post('/stock/adjust', 'Admin\ReportController@adjustStock')->name('adjust_stock');
        Route::get('/profit', 'Admin\ReportController@getProfitLossReport')->name('profit');
        Route::post('/profit', 'Admin\ReportController@calculateProfitLossReport')->name('profit');
    });

    //routes for agent manipulation
    Route::group(['prefix' => 'agent', 'as' => 'agent.'], function () {
        Route::get('/new', 'Admin\AgentController@showNewAgentForm')->name('new');
        Route::post('/new', 'Admin\AgentController@makeNewAgent');
        Route::get('/', 'Admin\AgentController@showAgentList')->name('all');
        Route::post('/details', 'Admin\AgentController@getAgentDetails')->name('view');
        Route::post('/status', 'Admin\AgentController@updateAgentStatus')->name('status');
        Route::delete('/delete', 'Admin\AgentController@deleteAgentData')->name('delete');
        Route::get('/{agent_id}/update', 'Admin\AgentController@showAgentEditView')->name('edit');
        Route::post('/{agent_id}/update', 'Admin\AgentController@updateAgentInfo');
        Route::get('/search', 'Admin\AgentController@search')->name('search');
        Route::get('/offer', 'Admin\AgentController@getAgentOffers')->name('offer');
        Route::get('/approve', 'Admin\AgentController@getAgentApproveOffers')->name('approve');
        Route::get('/withdraw', 'Admin\AgentController@getAgentBalanceWithdrawView')->name('withdraw');
        Route::post('/withdraw', 'Admin\AgentController@paymentAgentBalance')->name('withdraw');
    });

    //routes for agent manipulation
    Route::group(['prefix' => 'customers', 'as' => 'customers.'], function () {
        Route::get('/', 'Admin\CustomerController@getCustomers')->name('all');
    });
    //order manipulation for admin
    Route::group(['prefix' => 'order', 'as' => 'order.'], function () {
        Route::get('/approved', 'Admin\OrderController@getApprovedOrders')->name('approved');
        Route::get('/delivered', 'Admin\OrderController@getDeliveredOrders')->name('delivered');
        Route::get('/canceled', 'Admin\OrderController@getCanceledOrders')->name('canceled');
        Route::post('/cancel', 'Admin\OrderController@cancelTheOrder')->name('cancel');
        Route::post('/update', 'Admin\OrderController@updateOrder')->name('update');
        Route::get('/', 'Admin\OrderController@getPendingOrders')->name('pending');
        Route::post('/', 'Admin\OrderController@getOrderInfo')->name('order_info');
        Route::post('/assign/', 'Admin\OrderController@addDeliveryBoyToOrder')->name('delivery_boy_assign');
        Route::post('/assign/agent', 'Admin\OrderController@addAgentToOrderItem')->name('agent_assign');
        Route::get('/search/delivery/boy', 'Admin\OrderController@deliveryboy_search')->name('deliveryboy_search');
        Route::get('/search/agent', 'Admin\OrderController@agent_search')->name('agent_search');
    });

    //routes for delivery boy manipulation
    Route::group(['prefix' => 'deliveryboy', 'as' => 'deliveryboy.'], function () {
        Route::get('/new', 'Admin\Deliveryboy\DeliveryBoyController@showNewDeliveryBoyForm')->name('new');
        Route::post('/new', 'Admin\Deliveryboy\DeliveryBoyController@makeNewDeliveryBoy');
        Route::get('/', 'Admin\Deliveryboy\DeliveryBoyController@getDeliveryBoyList')->name('deliveryBoys');
        Route::get('/{deliveryboy_id}/edit', 'Admin\Deliveryboy\DeliveryBoyController@showEditDeliveryBoyForm')->name('edit');
        Route::post('/{deliveryboy_id}/edit', 'Admin\Deliveryboy\DeliveryBoyController@updateDeliveryBoy')->name('edit');
        Route::post('/view', 'Admin\Deliveryboy\DeliveryBoyController@getDeliveryBoyInfo')->name('view');
        Route::delete('/delete', 'Admin\Deliveryboy\DeliveryBoyController@deleteDeliveryBoy')->name('delete');
        Route::get('/search', 'Admin\Deliveryboy\DeliveryBoyController@search')->name('search');
        Route::group(['prefix' => 'area', 'as' => 'area.'], function () {
            Route::post('/new', 'Admin\Deliveryboy\AreaController@makeNewArea')->name('new');
            Route::post('/update', 'Admin\Deliveryboy\AreaController@updateArea')->name('update');
            Route::get('/', 'Admin\Deliveryboy\AreaController@getAreaList')->name('areas');
            Route::get('/search', 'Admin\Deliveryboy\AreaController@searchAreaList')->name('search');
            Route::delete('/delete', 'Admin\Deliveryboy\AreaController@deleteArea')->name('delete');
        });
    });
});

//customer auth routes start
Route::group(['prefix' => 'customer', 'as' => 'customer.'], function () {
    Route::get('/login', 'Customer\Auth\LoginController@showLoginForm')->name('login');
    Route::post('/login', 'Customer\Auth\LoginController@login');
    Route::post('/logout', 'Customer\Auth\LoginController@logout')->name('logout');

    Route::get('/register', 'Customer\Auth\RegisterController@showRegistrationForm')->name('register');
    Route::post('/register', 'Customer\Auth\RegisterController@register');

    Route::post('/password/email', 'Customer\Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
    Route::post('/password/reset', 'Customer\Auth\ResetPasswordController@reset')->name('password.email');
    Route::get('/password/reset', 'Customer\Auth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
    Route::get('/password/reset/{token}', 'Customer\Auth\ResetPasswordController@showResetForm');
});
//customer auth routes end

//route for pusher subscription verification
Route::post('/broadcast', function (Request $request) {
    $pusher = new Pusher\Pusher(env('PUSHER_APP_KEY'), env('PUSHER_APP_SECRET'), env('PUSHER_APP_ID'));
    return $pusher->socket_auth($request->request->get('channel_name'), $request->request->get('socket_id'));
});

Route::group(['prefix' => 'agent', 'as' => 'agent.'], function () {
    Route::get('/login', 'Agent\Auth\LoginController@showLoginForm')->name('login');
    Route::post('/login', 'Agent\Auth\LoginController@login');
    Route::post('/logout', 'Agent\Auth\LoginController@logout')->name('logout');

    Route::get('/register', 'Agent\Auth\RegisterController@showRegistrationForm')->name('register');
    Route::post('/register', 'Agent\Auth\RegisterController@register');

    Route::post('/password/email', 'Agent\Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
    Route::post('/password/reset', 'Agent\Auth\ResetPasswordController@reset')->name('password.email');
    Route::get('/password/reset', 'Agent\Auth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
    Route::get('/password/reset/{token}', 'Agent\Auth\ResetPasswordController@showResetForm');
});

Route::group(['prefix' => 'deliveryboy', 'as' => 'deliveryboy'], function () {
    Route::get('/login', 'Deliveryboy\Auth\LoginController@showLoginForm')->name('login');
    Route::post('/login', 'Deliveryboy\Auth\LoginController@login');
    Route::post('/logout', 'Deliveryboy\Auth\LoginController@logout')->name('logout');

    Route::get('/register', 'Deliveryboy\Auth\RegisterController@showRegistrationForm')->name('register');
    Route::post('/register', 'Deliveryboy\Auth\RegisterController@register');

    Route::post('/password/email', 'Deliveryboy\Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
    Route::post('/password/reset', 'Deliveryboy\Auth\ResetPasswordController@reset')->name('password.email');
    Route::get('/password/reset', 'Deliveryboy\Auth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
    Route::get('/password/reset/{token}', 'Deliveryboy\Auth\ResetPasswordController@showResetForm');
});

Route::group(['prefix' => 'manager', 'as' => "manager."], function () {
    Route::get('/login', 'Manager\Auth\LoginController@showLoginForm')->name('login');
    Route::post('/login', 'Manager\Auth\LoginController@login');
    Route::post('/logout', 'Manager\Auth\LoginController@logout')->name('logout');

    Route::get('/register', 'Manager\Auth\RegisterController@showRegistrationForm')->name('register');
    Route::post('/register', 'Manager\Auth\RegisterController@register');

    Route::post('/password/email', 'Manager\Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
    Route::post('/password/reset', 'Manager\Auth\ResetPasswordController@reset')->name('password.email');
    Route::get('/password/reset', 'Manager\Auth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
    Route::get('/password/reset/{token}', 'Manager\Auth\ResetPasswordController@showResetForm');
});

Route::group(['prefix' => 'callcenter', 'as' => "callcenter"], function () {
    Route::get('/login', 'Callcenter\Auth\LoginController@showLoginForm')->name('login');
    Route::post('/login', 'Callcenter\Auth\LoginController@login');
    Route::post('/logout', 'Callcenter\Auth\LoginController@logout')->name('logout');

    Route::get('/register', 'Callcenter\Auth\RegisterController@showRegistrationForm')->name('register');
    Route::post('/register', 'Callcenter\Auth\RegisterController@register');

    Route::post('/password/email', 'Callcenter\Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
    Route::post('/password/reset', 'Callcenter\Auth\ResetPasswordController@reset')->name('password.email');
    Route::get('/password/reset', 'Callcenter\Auth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
    Route::get('/password/reset/{token}', 'Callcenter\Auth\ResetPasswordController@showResetForm');
});
